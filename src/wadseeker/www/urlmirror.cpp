//------------------------------------------------------------------------------
// urlmirror.cpp
//------------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301  USA
//
//------------------------------------------------------------------------------
// Copyright (C) 2024 "Zalewa" <zalewapl@gmail.com>
//------------------------------------------------------------------------------
#include "urlmirror.h"

#include "www/urlparser.h"

/*
  UrlMirror
*/

bool UrlMirror::hasUrl(const QUrl &url) const
{
	for (const QUrl &urlOnList : urls)
	{
		if (UrlParser::urlEqualsCaseInsensitive(urlOnList, url))
		{
			return true;
		}
	}
	return false;
}

/*
  UrlMirrorList
*/

void UrlMirrorList::addMirrorUrls(const QList<QUrl> &urls, const QString &resource)
{
	UrlMirror mirror;
	mirror.resource = resource;
	mirror.urls = urls;
	*this << mirror;
}

void UrlMirrorList::addUrl(const QUrl &url, const QString &resource)
{
	QList<QUrl> urls;
	urls << url;
	addMirrorUrls(urls, resource);
}

QList<UrlMirror> UrlMirrorList::mirrorsWithUrl(const QUrl &url) const
{
	QList<UrlMirror> mirrorsWithUrls;
	for (const UrlMirror &mirror : *this)
	{
		if (mirror.hasUrl(url))
		{
			mirrorsWithUrls << mirror;
		}
	}
	return mirrorsWithUrls;
}
