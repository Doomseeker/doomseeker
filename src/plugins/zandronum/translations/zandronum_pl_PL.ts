<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>EngineZandronumConfigBox</name>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="45"/>
        <source>Testing releases</source>
        <translation>Wydania testowe</translation>
    </message>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="48"/>
        <source>Directory for testing releases:</source>
        <translation>Katalog dla wydań testowych:</translation>
    </message>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="55"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="63"/>
        <source>Segmented server query (requires Zandronum 3.2-alpha)</source>
        <translation>Segmentowana odpowiedź serwera (wymaga Zandronum conajmniej 3.2-alpha)</translation>
    </message>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="64"/>
        <source>&lt;p&gt;When enabled, the servers will be asked to provide their info in segments. Enable this if you experience problems when refreshing servers that host a lot of WADs or players. These servers send a lot of data, and enabling this asks them to send this data split between several packets.&lt;/p&gt;&lt;p&gt;This feature requires the server to host at least a Zandronum 3.2-alpha version.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Po włączeniu, serwery gier będą proszone o podawanie informacji o sobie w segmentach. Włącz to jeśli występują problemy z odświeżaniem serwerów, na których jest wielu graczy lub wiele WADów. Takie serwery wysyłają dużo danych, a ta opcja prosi je o podzielenie tych danych na mniejsze pakiety.&lt;/p&gt;&lt;p&gt;Opcja ta wymaga, aby serwer korzystał z Zandronum w wersji conajmniej 3.2-alpha.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../enginezandronumconfigbox.cpp" line="77"/>
        <source>Doomseeker - choose Zandronum testing directory</source>
        <translation>Doomseeker - wybierz katalog testowy dla Zandronum</translation>
    </message>
</context>
<context>
    <name>FlagsPage</name>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="78"/>
        <source>Zandronum</source>
        <translation>Zandronum</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="107"/>
        <source>Zandronum 2 (old)</source>
        <translation>Zandronum 2 (stary)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="106"/>
        <source>Zandronum 3</source>
        <translation>Zandronum 3</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="109"/>
        <source>None</source>
        <translation>Brak</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="110"/>
        <source>Old (ZDoom)</source>
        <translation>Stary (ZDoom)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="111"/>
        <source>Hexen</source>
        <translation>Hexen</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="112"/>
        <source>Strife</source>
        <translation>Strife</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="122"/>
        <source>Allowed</source>
        <translation>Dozwolony</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="123"/>
        <source>Only team</source>
        <translation>Tylko do drużyny</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="124"/>
        <source>Forbidden</source>
        <translation>Zabroniony</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="126"/>
        <source>Automatic</source>
        <translation>Automatyczny</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="127"/>
        <source>Unknown</source>
        <translation>Nieznany</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="214"/>
        <source>Default</source>
        <translation>Domyślnie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="215"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="216"/>
        <source>Yes</source>
        <translation>Tak</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="359"/>
        <source>Unknown Zandronum version in the config. Reverting to default.</source>
        <translation>Nieznana wersja Zandronum w pliku konfiguracyjnym. Używam wartości domyślnej.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="386"/>
        <source>Tried to set unknown Zandronum version. Reverting to default.</source>
        <translation>Próbowano ustawić nieznaną wersję Zandronum. Powracam do wartości domyślnej.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="45"/>
        <source>Game version:</source>
        <translation>Wersja gry:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="68"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;If this is checked then Zandronum may override some of the settings selected here.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(+sv_defaultdmflags)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Gdy to jest zaznaczone, to Zandronum może nadpisać niektóre z ustawień, które są tu wybrane.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(+sv_defaultdmflags)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="76"/>
        <source>Default DMFlags</source>
        <translation>Domyślne DMFlagi</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="90"/>
        <source>Environment</source>
        <translation>Środowisko</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="101"/>
        <source>Falling damage:</source>
        <translation>Ból od upadku:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="132"/>
        <source>No monsters</source>
        <translation>Brak potworów</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="155"/>
        <source>Items respawn</source>
        <translation>Odradzanie przedmiotów</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="162"/>
        <source>Barrels respawn</source>
        <translation>Odradzanie beczek</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="169"/>
        <source>Respawn invulnerability and invisibility spheres.</source>
        <translation>Odradzanie kul nieśmiertelności i niewidzialności.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="172"/>
        <source>Mega powerups respawn</source>
        <translation>Odradzanie mega-powerupów</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="257"/>
        <source>Teams</source>
        <translation>Drużyny</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="263"/>
        <source>Server picks teams</source>
        <translation>Serwer wybiera drużyny</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="270"/>
        <source>Players can&apos;t switch teams</source>
        <translation>Gracze nie mogą zmieniać drużyn</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="277"/>
        <source>Keep teams after a map change</source>
        <translation>Zachowuj drużyny po zmianie mapy</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="300"/>
        <source>Hide allies on the automap</source>
        <translation>Ukrywaj sojuszników na automapie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="307"/>
        <source>Don&apos;t let players spy on allies</source>
        <translation>Nie pozwalaj graczom podglądać sojuszników</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="330"/>
        <source>Instant flag/skull return</source>
        <translation>Natychmiastowy zwrot flagi/czaszki</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="190"/>
        <source>Don&apos;t use ping-based backwards reconciliation for player-fired hitscans and rails.</source>
        <translation>Nie używaj opartej na pingu wstecznej rekoncyljacji dla wystrzelonych przez graczy hitscanów i railguna.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="193"/>
        <source>No unlagged</source>
        <translation>Wyłącz unlagged</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1404"/>
        <source>Always apply LMS spectator settings</source>
        <translation>Zawsze stostuj ustawienia widzów LMS</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="344"/>
        <source>Enforces clients not to show medals, i.e. behave as if cl_medals == 0.</source>
        <translation>Wymusza na klientach nie pokazywanie medali, tak jakby cl_medals == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="347"/>
        <source>No medals</source>
        <translation>Brak medali</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="24"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="378"/>
        <source>Disallow</source>
        <translation>Nie pozwalaj</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="386"/>
        <source>Suicide</source>
        <translation>Samobójstwo</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="393"/>
        <source>Respawn</source>
        <translation>Respawn</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="400"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;P_RadiusAttack doesn&apos;t give players any z-momentum if the attack was made by a player. This essentially disables rocket jumping.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;P_RadiusAttack nie nadaje graczom z-pędu jeżeli źródłem ataku jest sam gracz. To wyłącza skakanie na rakietach.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="403"/>
        <source>Rocket jump</source>
        <translation>Skakanie na rakietach</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="410"/>
        <source>Taunt</source>
        <translation>Okrzyki</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="417"/>
        <source>Item drop</source>
        <translation>Upuszczanie przedmiotów</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="444"/>
        <source>Use automap</source>
        <translation>Używanie automapy</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="451"/>
        <source>Turn off translucency</source>
        <translation>Wyłączanie przezroczystości</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="458"/>
        <source>Use crosshairs</source>
        <translation>Używanie celownika</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="472"/>
        <source>Enforces clients not to identify players, i.e. behave as if cl_identifytarget == 0.</source>
        <translation>Wymusza na klientach brak identyfikacji graczy, tak jakby cl_identifytarget == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="475"/>
        <source>Target identify</source>
        <translation>Identyfikacja celów</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="482"/>
        <source>Enforces clients not to draw coop info, i.e. behave as if cl_drawcoopinfo == 0.</source>
        <translation>Wymusza na klientach brak rysowania informacji w coopie, tak jakby cl_drawcoopinfo == 0.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="485"/>
        <source>Display coop info</source>
        <translation>Wyświetlanie informacji coop</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="510"/>
        <source>Use autoaim</source>
        <translation>Używanie autocelowania</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="517"/>
        <source>Only let the arbitrator set FOV (for all players)</source>
        <translation>Tylko serwer może ustalić FOV (wszystkim graczom)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="520"/>
        <source>Use FOV</source>
        <translation>Używanie FOV</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="527"/>
        <source>Use freelook</source>
        <translation>Patrzenie góra-dół</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="534"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Players are not allowed to use the land CCMD. Because of Zandronum&apos;s default amount of air control, flying players can get a huge speed boast with the land CCMD. Disallowing players to land, allows to keep the default air control most people are used to while not giving flying players too much of an advantage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gracze nie mogą używać komendy konsolowej land. Ze względu na domyślną swobodę kontroli w powietrzu, latający gracze mogą ogromnie przyspieszać za pomocą komendy land. Zabranianie graczom lądować pozwala zachować domyślną kontrolę w powietrzu, do której gracze są przyzwyczajeni, nie dając im jednocześnie znacznej przewagi.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="537"/>
        <source>Use &apos;land&apos; console command</source>
        <translation>Używanie komendy &apos;land&apos;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="544"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Don&apos;t allow players to change how strongly will their screen flash when they get hit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zabroń graczom zmieniać jak mocno zabłyśnie ich ekran, gdy otrzymają obrażenia.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="547"/>
        <source>Change bloodied screen brightness</source>
        <translation>Zmienianie jasności zakrwawionego ekranu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="569"/>
        <source>Abilities</source>
        <translation>Zdolności</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="580"/>
        <source>Jumping:</source>
        <translation>Skakanie:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="590"/>
        <source>Crouching:</source>
        <translation>Kucanie:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="622"/>
        <source>Infinite inventory</source>
        <translation>Nieskończony ekwipunek</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="629"/>
        <source>Infinite ammo</source>
        <translation>Nieskończona amunicja</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="636"/>
        <source>Like Quake 3</source>
        <translation>Jak w Quake 3</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="639"/>
        <source>Slowly lose health when over 100%</source>
        <translation>Powoli trać zdrowie powyżej 100%</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="646"/>
        <source>Can use chasecam</source>
        <translation>Można używać kamery z 3 osoby</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="653"/>
        <source>Allow BFG freeaiming</source>
        <translation>Pozwól na celowanie góra-dół z BFG</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="119"/>
        <source>Players can walk through each other</source>
        <translation>Gracze mogą przenikać przez siebie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="120"/>
        <source>Allies can walk through each other</source>
        <translation>Sojusznicy mogą przez siebie przechodzić</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.cpp" line="118"/>
        <source>Players block each other normally</source>
        <translation>Gracze blokują się normalnie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="660"/>
        <source>Don&apos;t check ammo when switching weapons</source>
        <translation>Nie sprawdzaj amunicji podczas zmiany broni</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="702"/>
        <source>Force inactive players to spectate after:</source>
        <translation>Wymuś tryb widza dla nieaktywnych graczy po:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="725"/>
        <source>min.</source>
        <translation>min.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="370"/>
        <source>Players</source>
        <translation>Gracze</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="929"/>
        <source>score damage, not kills</source>
        <translation>punktują za obrażenia, nie zabójstwa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="791"/>
        <source>Monsters...</source>
        <translation>Potwory...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="797"/>
        <source>are fast (like Nightmare)</source>
        <translation>są szybkie (jak na Nightmare)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="804"/>
        <source>respawn (like Nightmare)</source>
        <translation>odradzają się (jak na Nightmare)</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="811"/>
        <source>must be killed to enable exit</source>
        <translation>muszą zostać zabite aby można było wyjść</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="820"/>
        <source>Kill percentage:</source>
        <translation>Procent zabójstw:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="840"/>
        <source>Multiplier of damage dealt by monsters.</source>
        <translation>Mnożnik obrażeń zadawanych przez potwory.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="843"/>
        <source>Damage factor:</source>
        <translation>Mnożnik obrażeń:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="850"/>
        <source>&lt;html&gt;&lt;body&gt;&lt;p&gt;Multiplier of damage dealt by monsters.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body&gt;&lt;p&gt;Mnożnik obrażeń zadawanych przez potwory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="871"/>
        <source>Kill all monsters spawned by a boss cube when the boss dies</source>
        <translation>Zabij wszystkie potwory spawnięte przez kostkę bossa gdy boss zginie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="946"/>
        <source>On player death...</source>
        <translation>Gdy gracz zginie...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="952"/>
        <source>respawn where died</source>
        <translation>odradza się tam, gdzie zginął</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="959"/>
        <source>lose all inventory</source>
        <translation>traci cały ekwipunek</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="966"/>
        <source>lose armor</source>
        <translation>traci zbroję</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="973"/>
        <source>lose keys</source>
        <translation>traci klucze</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="980"/>
        <source>lose powerups</source>
        <translation>traci powerupy</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="987"/>
        <source>lose weapons</source>
        <translation>traci bronie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="994"/>
        <source>lose all ammo</source>
        <translation>traci całą amunicję</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1001"/>
        <source>lose half ammo</source>
        <translation>traci połowę amunicji</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="909"/>
        <source>Players who lose all lives can keep inventory</source>
        <translation>Gracze którzy tracą wszystkie życia mogą zachować ekwipunek</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="894"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Players will be respawned with full lives but the map will not be reset. Inventory will be preserved in accordance to &amp;quot;Lose inventory&amp;quot; flags. Players will be able to continue from the point where they died.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gracze zostaną odrodzeni z pełną ilością żyć, ale mapa nie zostanie zresetowana. Ekwipunek zostanie zachowany zgodnie z flagami &amp;quot;Tracenia ekwipunku&amp;quot;. Gracze będą mogli kontynuować grę od tego punktu, w którym zginęli.&lt;/p&gt;&lt;/body&gt;&lt;/html</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="752"/>
        <source>Cooperative</source>
        <translation>Kooperacja</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1035"/>
        <source>When players die, they...</source>
        <translation>Gdy gracze giną, to...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1055"/>
        <source>respawn automatically</source>
        <translation>od razu się odradzają</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1048"/>
        <source>drop their weapon</source>
        <translation>upuszczają swoją broń</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1069"/>
        <source>respawn farthest away from others</source>
        <translation>odradzają się najdalej od innych</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1076"/>
        <source>lose a frag</source>
        <translation>tracą fraga</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1041"/>
        <source>respawn with a shotgun</source>
        <translation>odradzają się z shotgunem</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1062"/>
        <source>don&apos;t get respawn protection</source>
        <translation>nie mają ochrony po respawnie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1154"/>
        <source>When someone exits the level...</source>
        <translation>Gdy ktoś opuści poziom...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1160"/>
        <source>continue to the next map</source>
        <translation>kontynuuj do następnej mapy</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1170"/>
        <source>restart the current level</source>
        <translation>restartuj obecny poziom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1177"/>
        <source>kill the player</source>
        <translation>zabij tego gracza</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1200"/>
        <source>Keep frags after map change</source>
        <translation>Zachowuj fragi po zmianie mapy</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1212"/>
        <source>Weapons &amp;&amp; ammo</source>
        <translation>Bronie i amunicja</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1218"/>
        <source>Weapons stay after pickup</source>
        <translation>Bronie nie znikają po podniesieniu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1225"/>
        <source>Double ammo</source>
        <translation>Podwójna amunicja</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="765"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1235"/>
        <source>Don&apos;t spawn...</source>
        <translation>Nie spawnuj...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1241"/>
        <source>health</source>
        <translation>zdrowia</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1248"/>
        <source>armor</source>
        <translation>zbroi</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1255"/>
        <source>runes</source>
        <translation>run</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1029"/>
        <source>Deathmatch</source>
        <translation>Deathmatch</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1291"/>
        <source>Weapons</source>
        <translation>Bronie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1297"/>
        <source>Chainsaw</source>
        <translation>Piła</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1304"/>
        <source>Pistol</source>
        <translation>Pistolet</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1311"/>
        <source>Shotgun</source>
        <translation>Shotgun</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1318"/>
        <source>Super shotgun</source>
        <translation>Dwururka</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1325"/>
        <source>Chaingun</source>
        <translation>Chaingun</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1332"/>
        <source>Minigun</source>
        <translation>Minigun</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1339"/>
        <source>Rocket launcher</source>
        <translation>Rakietnica</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1346"/>
        <source>Grenade launcher</source>
        <translation>Granatnik</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1353"/>
        <source>Plasma rifle</source>
        <translation>Plazmówka</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1360"/>
        <source>Railgun</source>
        <translation>Railgun</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1458"/>
        <source>lmsallowedweapons:</source>
        <translation>lsmallowedweapons:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1425"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="1448"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2092"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2102"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2112"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2143"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2160"/>
        <location filename="../createserverdialogpages/flagspage.ui" line="2170"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1435"/>
        <source>lmsspectatorsettings:</source>
        <translation>lmsspectatorsettings:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1372"/>
        <source>Spectators can...</source>
        <translation>Widzowie mogą...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1378"/>
        <source>talk to active players</source>
        <translation>rozmawiać z żyjącymi graczami</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1385"/>
        <source>view the game</source>
        <translation>oglądać grę</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1283"/>
        <source>LMS</source>
        <translation>LMS</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="182"/>
        <source>Server</source>
        <translation>Serwer</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="200"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Don&apos;t use backwards reconcilation for the hitscan tracers fired from the BFG9000.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nie używaj wstecznej rekoncyljacji dla tracerów hitscan&apos;ów wystrzeliwanych z BFG9000.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="203"/>
        <source>No unlagged on BFG tracers</source>
        <translation>Wyłącz unlagged dla tracerów BFG</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="227"/>
        <source>Server country:</source>
        <translation>Kraj serwera:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="234"/>
        <source>&lt;p&gt;Sets the country of the server, which will be presented to server browsers.&lt;/p&gt; &lt;p&gt;The value of this variable can be one of:&lt;ul&gt;&lt;li&gt;an ISO 3166-1 alpha-3 country code, for example GBR for the United Kingdom, USA for the United States&lt;/li&gt;&lt;li&gt;&amp;quot;automatic&amp;quot;, to tell the launcher to use IP geolocation (default)&lt;/li&gt;&lt;li&gt;or &amp;quot;unknown&amp;quot;.&lt;/li&gt;&lt;/p&gt;

&lt;p&gt;This was introduced to allow hosts to combat the inaccuracies of the IP geolocation that launchers rely on. Not all servers browsers may support this feature and will use the IP geolocation instead.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Kraj serwera prezentowany przeglądarkom.&lt;/p&gt; &lt;p&gt;Przyjmuje:&lt;ul&gt;&lt;li&gt;kod kraju ISO 3166-1 alpha-3, na przykład POL to Polska, USA to Stany Zjednoczone&lt;/li&gt;&lt;li&gt;&amp;quot;automatyczny (automatic)&amp;quot;, nakazuje przeglądarce rozpoznawać kraj po adresie IP serwera (domyślne)&lt;/li&gt;&lt;li&gt;albo &amp;quot;nieznany (unknown)&amp;quot;.&lt;/li&gt;&lt;/p&gt;

&lt;p&gt;Ustawienie tutaj kraju pozwala uniknąć błędnego rozpoznania kraju po adresie IP przez przeglądarkę, ale nie wszystkie przeglądarki serwerów mogą akceptować taki kraj od serwera.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="465"/>
        <source>Use custom renderer settings</source>
        <translation>Zmienianie ustawień rendera</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="554"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If set, stops doors from being manually closed. This prevents players intentionally (or unintentionally) griefing by closing doors that other players (or the same player) have opened.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zabrania zamykać drzwi. Zapobiega sytuacjom, w którym gracze celowo (albo niechcąco) griefują zamykając drzwi, które inni gracze (lub ten sam gracz) otworzyli.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="557"/>
        <source>Closing doors</source>
        <translation>Zamykanie drzwi</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="603"/>
        <source>Blocking:</source>
        <translation>Blokowanie:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="610"/>
        <source>Private chat:</source>
        <translation>Czat prywany:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="667"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Forces the player&apos;s pitch to be limited to what&apos;s allowed in the software renderer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ogranicza zasięg patrzenia góra-dół wszystkim graczom do takiego samego limitu jak w renderze software&apos;owym.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="670"/>
        <source>Limit vertical look range to the limit of the software renderer</source>
        <translation>Limituj patrzenie góra-dół do rendera software&apos;owego</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="677"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Allows players to fire hitscans and projectiles through teammates.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gracze mogą strzelać broniami hitscanowymi oraz pociskowymi przez kolegów z drużyny.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="680"/>
        <source>Players can shoot through allies</source>
        <translation>Strzały przechodzą przez kolegów z drużyny</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="687"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Players aren&apos;t pushed by attacks caused by their teammates (e.g. BFG tracers).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gracze nie są popychani przez ataki swoich kolegów z drużyny (także przez tracery BFG).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="690"/>
        <source>Players don&apos;t push allies when shooting them</source>
        <translation>Strzały nie popychają kolegów z drużyny</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="771"/>
        <source>Deathmatch weapons</source>
        <translation>broni z deathmatcha</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="778"/>
        <source>&lt;p&gt;Spawn map items, monsters and other objects in coop as if the game was single player.&lt;/p&gt;</source>
        <translation>&lt;p&gt;W trybie kooperacji spawnuj przedmioty, potwory i inne obiekty na mapie tak, jakby była to gra dla pojedynczego gracza.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="781"/>
        <source>any multiplayer actor in coop</source>
        <translation>żadnego aktora wieloosobowego w coopie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="878"/>
        <source>Don&apos;t count monsters in the &quot;end level&quot; sector towards kills</source>
        <translation>Pomiń potwory w sektorach &apos;zakończ etap&apos; przy podliczaniu zabójstw</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="888"/>
        <source>Survival</source>
        <translation>Surwiwal</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="897"/>
        <source>No map reset when all players die</source>
        <translation>Nie resetuj mapy gdy wszyscy gracze zginą</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="904"/>
        <source>&lt;p&gt;If set, players who lose all lives and become dead spectators will still keep inventory in accordance to the &amp;quot;Lose inventory&amp;quot; flags. When they will finally be able to play again, all their items will be returned to them.&lt;/p&gt;

&lt;p&gt;If unset, players who lose all lives will always lose their entire inventory, regardless of the &amp;quot;Lose inventory&amp;quot; flags.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Po włączeniu tej opcji, gracze którzy tracą wszystkie życia i stają się &quot;martwymi widzami&quot; wciąż zachowują swój ekwipunek zgodnie z flagami &quot;Tracenia ekwipunku&quot;. Gdy znów będą mogli dołączyć do gry, otrzymają wszystkie swoje przedmioty spowrotem.&lt;/p&gt;

&lt;p&gt;Jeżeli ta opcja pozostanie wyłączona, gracze po utracie wszystkich żyć zawsze będą tracili cały swój ekwipunek niezależnie od flag &quot;Tracenia ekwipunku&quot;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="923"/>
        <source>Players...</source>
        <translation>Gracze...</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="936"/>
        <source>share keys</source>
        <translation>współdzielą klucze</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1098"/>
        <source>sec.</source>
        <translation>sek.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1114"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;How long a player must wait after dying before they can respawn. This doesn&apos;t apply to players who are spawn telefragged.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ile czasu gracz musi czekać po zgonie zanim będzie mógł się zrespawnować. Nie dotyczy graczy stelefragowanych na spawnie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1139"/>
        <source>can respawn only after:</source>
        <translation>mogą się zrespawnować dopiero po:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1395"/>
        <source>LMS flags</source>
        <translation>Flagi LMS</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1401"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Apply lmsspectatorsettings in all game modes, not only in LMS.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stosuj lmsspectatorsettings we wszystkich trybach gry, a nie tylko w LMS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1511"/>
        <source>Enable buggier wall clipping so players can wallrun.</source>
        <translation>Włącz zbugowaną kolizję ze ścianami, aby gracze mogli robić wallrun.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1514"/>
        <source>Enable wall running</source>
        <translation>Włącz wall running</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1521"/>
        <source>Pickups are only heard locally.</source>
        <translation>Podniesione przedmioty słychać tylko lokalnie.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1524"/>
        <source>Don&apos;t let others hear pickups</source>
        <translation>Inni nie słyszą podniesień</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1531"/>
        <source>Allow instant respawn</source>
        <translation>Zezwól na natychmiastowy respawn</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1538"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Disable stealth monsters, since doom2.exe didn&apos;t have them.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: this handles ZDoom&apos;s invisible monsters.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;THIS DOESN&apos;T AFFECT THE PARTIAL INVISIBILITY SPHERE IN ANY WAY. See &amp;quot;Monsters see semi-invisible players&amp;quot; for that.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyłącz potwory &quot;stealth&quot;, ponieważ doom2.exe ich nie miało.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Uwaga: to odnosi się do niewidzialnych potworów z ZDooma.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;TO NIE WPŁYWA NA KULĘ POŁOWICZNEJ NIEWIDZIALNOŚCI W ŻADEN SPOSÓB. Użyj do tego ustawienia &amp;quot;Potwory widzą na wpół-niewidzialnych graczy&amp;quot;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1541"/>
        <source>Disable stealth monsters</source>
        <translation>Wyłącz potwory &quot;stealth&quot;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1548"/>
        <source>Limit actors to one sound at a time.</source>
        <translation>Ogranicz aktorów do jednego dźwięku na raz.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1551"/>
        <source>Allow silent BFG trick</source>
        <translation>Pozwól na trick z cichym BFG</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1558"/>
        <source>When a weapon is picked up, the weapon switch behaves like in vanilla Doom.</source>
        <translation>Zmiana broni po podniesieniu działa jak w vanilla Doom.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1561"/>
        <source>Original weapon switch</source>
        <translation>Oryginalna zmiana broni</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1568"/>
        <source>Limited movement in the air</source>
        <translation>Ograniczone poruszanie w powietrzu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1575"/>
        <source>This affects the partial invisibility sphere.</source>
        <translation>To wpływa na kulę połowicznej niewidzialności.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1578"/>
        <source>Monsters see semi-invisible players</source>
        <translation>Potwory widzą na wpół-niewidzialnych graczy</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1585"/>
        <source>Allow the map01 &quot;plasma bump&quot; bug.</source>
        <translation>Zezwól na błąd &quot;plasma bump&quot; z map01.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1588"/>
        <source>Plasma bump bug</source>
        <translation>Błąd plasma bump</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1595"/>
        <source>Any boss death activates map specials</source>
        <translation>Śmierć dowolnego bossa aktywuje speciale na mapie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1602"/>
        <source>Friction/pushers/pullers affect Monsters</source>
        <translation>Tarcie/popychacze i ciągniki wpływają na potwory</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1609"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Crushed monsters are turned into gibs, rather than replaced by gibs.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zgniecione potwory zamieniają się we flaki, a nie są przez nie zastępowane.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1612"/>
        <source>Crusher gibs by morphing, not replacement</source>
        <translation>Zgniatarka zmienia we flaki, zamiast podmieniać</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1619"/>
        <source>Block monster lines ignore friendly monsters</source>
        <translation>Linie blokujące potwory ignorują przyjazne potwory</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1626"/>
        <source>Find neighboring light level like Doom</source>
        <translation>Szukaj sąsiadującego poziomu światła jak w Doomie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1637"/>
        <source>Use doom2.exe&apos;s original intermission screens/music.</source>
        <translation>Używaj muzyki i ekranów z doom2.exe podczas intermisji.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1640"/>
        <source>Use old intermission screens/music</source>
        <translation>Używaj starych intermisji</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1647"/>
        <source>Scrolling sectors are additive like in Boom.</source>
        <translation>Przesuwające się sektory dodają się jak w Boomie.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1650"/>
        <source>Scrolling sectors are additive</source>
        <translation>Przesuwające sektory dodają się</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1657"/>
        <source>Sector sounds use original method for sound origin.</source>
        <translation>Dźwięki sektorów używają oryginalnej metody pozycjonowania dźwięku.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1660"/>
        <source>Sector sounds use original method</source>
        <translation>Dźwięki sektorów używają oryginalnej metody</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1667"/>
        <source>Monsters cannot move when hanging over a dropoff.</source>
        <translation>Potwory utykają nad krawędziami.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1670"/>
        <source>No monsters dropoff move</source>
        <translation>Potwory utykają nad krawędziami</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1684"/>
        <source>Instantly moving floors are not silent.</source>
        <translation>Natychmiastowo poruszane podłogi nie są ciche.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1687"/>
        <source>Instantly moving floors aren&apos;t silent</source>
        <translation>Natychmiastowo poruszane podłogi nie są ciche</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1694"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clients send ucmd.buttons as &amp;quot;long&amp;quot; instead of as &amp;quot;byte&amp;quot; in CLIENTCOMMANDS_ClientMove. So far this is only necessary if the ACS function GetPlayerInput is used in a server side script to check for buttons bigger than BT_ZOOM. Otherwise this information is completely useless for the server and the additional net traffic to send it should be avoided.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Klienci wysyłają ucmd.buttons jako &amp;quot;long&amp;quot; zamiast jako &amp;quot;byte&amp;quot; w CLIENTCOMMANDS_ClientMove. Jak do tej pory, było to jedynie potrzebne w funkcji ACS GetPlayerInput, gdy jest ona użyta po stronie serwera do sprawdzenia przycisku większego niż BT_ZOOM. W każdym innym przypadku ta informacja jest całkowicie bezużyteczna dla serwera i powinno się unikać dodatkowego zużycia łącza.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1697"/>
        <source>Clients send full button info</source>
        <translation>Klienci wysyłają pełne info o przyciskach</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1704"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use Doom&apos;s random table instead of ZDoom&apos;s random number generator. Affects weapon damage among other things.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Używaj losowej tablicy z Dooma zamiast generatora liczb losowych z ZDooma. Wpływa to, między innymi, na obrażenia od broni.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1707"/>
        <source>Old random number generator</source>
        <translation>Stary generator liczb losowych</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1677"/>
        <source>Monsters can&apos;t be pushed off cliffs</source>
        <translation>Potwory nie mogą być wypchnięte przez przepaść</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1717"/>
        <source>Old damage radius (infinite height)</source>
        <translation>Obrażenia obszarowe mają nieskończoną wysokość</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1724"/>
        <source>Minotaur&apos;s floor flame explodes immediately when feet are clipped</source>
        <translation>Ogień podłogowy Minotaura eksploduje natychmiast gdy stopy są przycięte</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1731"/>
        <source>Original velocity calc. for A_Mushroom in Dehacked</source>
        <translation>Oryginalne obliczanie prędkości dla A_Mushroom w Dehacked</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1738"/>
        <source>Sprite sort order inverted for sprites of equal distance</source>
        <translation>Odwrócone sortowanie sprite&apos;ów gdy są w tej samej odległości</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1745"/>
        <source>Hitscans use original blockmap and hit check code</source>
        <translation>Hitscany używają oryginalnego kodu blockmap i detekcji trafień</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1752"/>
        <source>Draw polyobjects the old fashioned way</source>
        <translation>Rysuj polyobjekty w stary sposób</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1501"/>
        <source>Compatibility</source>
        <translation>Kompatybilność</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1714"/>
        <source>&lt;p&gt;Shooting a rocket against a wall will damage the actors near this wall even if they are far below or above the explosion.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Rakieta rozbita o ścianę zada obrażenia istotom w pobliżu tej ściany nawet jeżeli będą one daleko pod lub ponad samą eksplozją.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1787"/>
        <source>&lt;p&gt;This allows for decorations to be pass-through for projectiles as they were originally in Doom.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Pozwala pociskom przelatywać przez obiekty dekoracyjne tak jak to było w oryginalnym Doomie.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1790"/>
        <source>Use original missile clipping height</source>
        <translation>Używaj oryginalnej wysokości kolizji pocisków</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1797"/>
        <source>Use sector based sound target code</source>
        <translation>Używaj kodu celowania dźwiękiem opartego na sektorach</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1804"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Doom&apos;s hitscan tracing code ignores all lines with both sides in the same sector. ZDoom&apos;s does not. This option reverts to the original but less precise behavior. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kod w Doomie odpowiedzialny za śledzenie hitscanów ignoruje wszystkie linie, które mają ten sam sektor po obu stronach. ZDoom tego nie robi. Ta opcja przywraca oryginalne, lecz mniej dokładne zachowanie. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1807"/>
        <source>Trace ignore lines w/ same sector on both sides</source>
        <translation>Trasowanie ignoruje linie o tym samym sektorze po obu stronach</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1814"/>
        <source>Limit deh.MaxHealth to health bonus</source>
        <translation>Ogranicz deh.MaxHealth do bonusu energii</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1821"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The scrolling floor specials in Heretic and Hexen move the player much faster than the actual texture scrolling speed. Enable this option to restore this effect. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Akcje przesuwające podłogę w Hereticu i Hexenie przesuwają graczy znacznie szybciej, niż teksturę. Włączenie tej opcji przywraca ten efekt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1824"/>
        <source>Raven&apos;s scrollers use original speed</source>
        <translation>Przesuwniki Ravena używają oryginalnych prędkości</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1831"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add NOGRAVITY to actors named InvulnerabilitySphere, Soulsphere, Megasphere and BlurSphere when spawned by the map.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj NOGRAVITY aktorom o nazwach InvulnerabilitySphere, Soulsphere, Megasphere i BlurSphere gdy są spawnowane przez mapę.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1834"/>
        <source>Add NOGRAVITY flag to spheres</source>
        <translation>Dodaj flagę NOGRAVITY kulom</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1841"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When a player leaves the game, don&apos;t stop any scripts of that player that are still running.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gdy gracz opuści grę, nie zatrzymuj skryptów, które są dla niego uruchomione.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1844"/>
        <source>Don&apos;t stop player scripts on disconnect</source>
        <translation>Nie zatrzymuj skryptów gracza po rozłączeniu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1851"/>
        <source>&lt;p&gt;If this is enabled, explosions cause a strong horizontal thrust like in old ZDoom versions.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Gdy jest to włączone, eksplozje silnie popychają w poziomie jak w starych wersjach ZDooma.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1854"/>
        <source>Old ZDoom horizontal thrust</source>
        <translation>Popychanie poziome ze starego ZDooma</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1861"/>
        <source>&lt;p&gt;If this is enabled, non-SOLID things like flags fall through bridges (as they used to do in old ZDoom versions).&lt;/p&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gdy to jest włączone, nie-SOLIDne obiekty, takie jak flagi wpadają pod mosty (tak jak to było w starych wersjach ZDooma).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1864"/>
        <source>Old ZDoom bridge drops</source>
        <translation>Upuszczanie pod most ze starego ZDooma</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1871"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uses old ZDoom jump physics, it&apos;s a minor bug in the gravity code that causes gravity application in the wrong place.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Używaj fizyki skoku ze starego ZDooma. To mały błąd w kodzie, który aplikuje grawitację nie w tym miejscu, co trzeba.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1891"/>
        <source>Ignore compositing when drawing masked midtextures</source>
        <translation>Ignoruj kompozycję podczas rysowania maskowanych midtekstur</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1898"/>
        <source>It is impossible to face directly NSEW</source>
        <translation>Nie można obrócić się bezpośrednio w kierunku kardynalnym</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1905"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;World and global-scope ACS variables/arrays will be reset upon resetting the map like in survival.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resetuj zmienne ACS o zakresie world oraz global gdy mapa jest resetowana w sposób surwiwalowy.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1908"/>
        <source>Reset global/world ACS vars on map reset</source>
        <translation>Resetuj zmienne ACS global/world wraz z mapą</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1953"/>
        <source>&lt;p&gt;Like in vanilla Doom.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Jak w vanilla Doom.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2008"/>
        <source>Use the same floor motion behavior as Doom</source>
        <translation>Użyj takiego zachowania ruchu podłóg, jak w Doomie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2015"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use the jumping behavior known from Skulltag.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Skakanie zachowuje się jak w Skulltagu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2018"/>
        <source>Skulltag jumping</source>
        <translation>Skacz jak w Skulltagu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2025"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Prevent obituary messages from being printed to the console when a player dies.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Teksty pośmiertne dla graczy nie będą się pojawiać w konsoli gry.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2028"/>
        <source>Don&apos;t display player obituaries</source>
        <translation>Nie wyświetlaj tekstów pośmiertnych</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2035"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Disable the window check in CheckForPushSpecial(). For maps that expect non-blocking push lines to activate when you are standing on them and run into a completely different line. Some maps may need this to enable climbing up ladders.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyłącz &quot;sprawdzanie okna&quot; w funkcji CheckForPushSpecial(). Opcja przeznaczona dla map, które spodziewają się, że linie aktywowane poprzez &quot;popchnięcie&quot; będą się aktywować, gdy gracz na nich stanie albo popchnie jakąś zupełnie inna linię. Niektóre mapy mogą wymagać tej opcji, aby można było wspinać się po drabinach.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2038"/>
        <source>Non-blocking lines can be pushed</source>
        <translation>Można popychać nieblokujące linie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2129"/>
        <source>zandronum compatflags</source>
        <translation>zandronum compatflags</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1874"/>
        <source>ZDoom 1.23B33 jump physics</source>
        <translation>Fizyka skoku z ZDooma 1.23B33</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1881"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;p&gt;Zandronum uses more tracers to fill in the gaps, this reverts it to vanilla&apos;s 3 tracer behavior&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;p&gt;Zandronum używa więcej promieni do wypełnienia dziur. To przywraca używanie tylko 3 promieni z vanilli.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1884"/>
        <source>Use vanilla autoaim tracer behavior</source>
        <translation>Trasowanie autocelowania takie jak w vanilli</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1919"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use Doom&apos;s shortest texture find behavior. This is requied by some WADs in MAP07.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Używaj najkrótszego wyszukiwania tekstury z Dooma. Jest to wymagane przez niektóre WADy w MAP07.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1922"/>
        <source>Find shortest textures like Doom</source>
        <translation>Wyszukuj najkrótszej tekstury jak w Doomie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1929"/>
        <source>Limit pain elementals to 20 lost souls</source>
        <translation>Ogranicz pain elemental do 20 lost souls</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1936"/>
        <source>Spawned item drops on the floor</source>
        <translation>Spawnięte przedmioty spadają na podłogę</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1943"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Treat ACS scripts with the SCRIPTF_Net flag to be client side, i.e. executed on the clients, but not on the server.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Traktuj skrypty ACS z flagą SCRIPTF_Net jako skrypty po stronie klienta, tzn. wykonuj je u klientów, a nie na serwerze.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1946"/>
        <source>NET scripts are clientside</source>
        <translation>Skrypty NET są po stronie klienta</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1956"/>
        <source>Actors are infinitely tall</source>
        <translation>Nieskończenie wysocy aktorzy</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1963"/>
        <source>Don&apos;t fix loop index for stair building.</source>
        <translation>Nie naprawiaj indeksu pętli do budowania schodów.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1966"/>
        <source>Use buggier stair building</source>
        <translation>Używaj zbugowanego budowania schodów</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1973"/>
        <source>Disable Boom door light effect</source>
        <translation>Wyłącz Boomowy efekt świateł z drzwi</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1980"/>
        <source>All special lines can drop use lines</source>
        <translation>Wszystkie specjalne linie mogą anulować używanie linii</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1987"/>
        <source>Original sound curve</source>
        <translation>Oryginalna krzywa dźwięku</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1994"/>
        <source>Disallow weapon change until fully drawn or hidden</source>
        <translation>Nie można zmieniać broni w trakcie wyciągania/chowania</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2001"/>
        <source>West spawns are silent</source>
        <translation>Zachodnie spawny są ciche</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="1777"/>
        <source>Compatibility 2</source>
        <translation>Kompatybilność 2</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2063"/>
        <source>Voting</source>
        <translation>Głosowanie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2085"/>
        <source>zandronum dmflags</source>
        <translation>zandronum dmflags</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2078"/>
        <source>dmflags</source>
        <translation>dmflags</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2136"/>
        <source>dmflags2</source>
        <translation>dmflags2</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2122"/>
        <source>compatflags</source>
        <translation>compatflags</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/flagspage.ui" line="2153"/>
        <source>compatflags2</source>
        <translation>compatflags2</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../zandronumserver.h" line="76"/>
        <source>&lt;&lt; Unknown &gt;&gt;</source>
        <translation>&lt;&lt; Nieznana &gt;&gt;</translation>
    </message>
</context>
<context>
    <name>TestingProgressDialog</name>
    <message>
        <location filename="../zandronumbinaries.cpp" line="457"/>
        <source>Downloading testing binaries...</source>
        <translation>Pobieram binarki testowe ...</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="457"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="467"/>
        <source>Doomseeker</source>
        <translation>Doomseeker</translation>
    </message>
</context>
<context>
    <name>VotingSetupWidget</name>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="32"/>
        <source>Use this page</source>
        <translation>Używaj tej strony</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="54"/>
        <source>Who can vote</source>
        <translation>Kto może głosować</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="60"/>
        <source>All can vote</source>
        <translation>Wszyscy mogą głosować</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="70"/>
        <source>No one can vote</source>
        <translation>Nikt nie może głosować</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="77"/>
        <source>Spectators can&apos;t vote</source>
        <translation>Widzowie nie mogą głosować</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="102"/>
        <source>Minimum number of players required to call a vote:</source>
        <translation>Minimalna ilość graczy wymagana do głosu:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="225"/>
        <source>Allow specific votes</source>
        <translation>Zezwól na głosowanie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="241"/>
        <source>pointlimit</source>
        <translation>limit punktów</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="231"/>
        <source>duellimit</source>
        <translation>limit pojedynków</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="134"/>
        <source>Vote cooldown:</source>
        <translation>Czas karencji po głosowaniu:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="155"/>
        <source>&lt;p&gt;Cooldown for the same type of vote is the double of this value. Setting this to 0 disables it.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Czas karencji dla tego samego typu głosowania jest dwukrotnie dłuższy od tej wartości. Ustaw czas na 0 aby całkowicie go wyłączyć.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="171"/>
        <source>min.</source>
        <translation>min.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="180"/>
        <source>Time before client can vote after connecting:</source>
        <translation>Czas karencji dla nowego klienta po podłączeniu:</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="214"/>
        <source>sec.</source>
        <translation>sek.</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="251"/>
        <source>timelimit</source>
        <translation>limit czasu</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="261"/>
        <source>winlimit</source>
        <translation>limit zwycięstw</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="271"/>
        <source>changemap</source>
        <translation>changemap</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="281"/>
        <source>kick</source>
        <translation>kopnięcie</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="291"/>
        <source>map</source>
        <translation>map</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="301"/>
        <source>fraglimit</source>
        <translation>limit fragów</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="311"/>
        <source>force players to spectate</source>
        <translation>wymuszanie oglądania na graczach</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="321"/>
        <source>flags</source>
        <translation>flagi</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="328"/>
        <source>next map</source>
        <translation>następna mapa</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="338"/>
        <source>next secret</source>
        <translation>następna sekretna</translation>
    </message>
    <message>
        <location filename="../createserverdialogpages/votingsetupwidget.ui" line="351"/>
        <source>Vote flooding protection enabled</source>
        <translation>Chroń przed floodowaniem głosowaniami</translation>
    </message>
</context>
<context>
    <name>Zandronum2::Dmflags</name>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="40"/>
        <source>Use Doom&apos;s shortest texture behavior</source>
        <translation>Użyj Doomowego zachowania najkrótszej tekstury</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="42"/>
        <source>Don&apos;t fix loop index for stair building</source>
        <translation>Nie naprawiaj indeksu pętli do budowania schodów</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="44"/>
        <source>Pain elemental is limited to 20 lost souls</source>
        <translation>Pain elemental jest ograniczony do 20 lost souls</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="46"/>
        <source>Pickups are only heard locally</source>
        <translation>Podniesienia przedmiotów słychać tylko lokalnie</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="48"/>
        <source>Infinitely tall actors</source>
        <translation>Nieskończenie wysocy aktorzy</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="50"/>
        <source>Limit actors to only one sound</source>
        <translation>Ogranicz aktorów do tylko jednego dźwięku</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="52"/>
        <source>Enable wallrunning</source>
        <translation>Włącz wallrunning</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="54"/>
        <source>Dropped items spawn on floor</source>
        <translation>Upuszczane przedmioty tworzone są na podłodze</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="56"/>
        <source>Special lines block use line</source>
        <translation>Linie specjalne blokują użycie linii</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="58"/>
        <source>Disable BOOM local door light effect</source>
        <translation>Wyłącz efekt lokalnego światła drzwi z BOOMa</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="60"/>
        <source>Raven&apos;s scrollers use their original speed</source>
        <translation>Przewijacze Ravena używają oryginalnych prędkości</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="62"/>
        <source>Use sector based sound target code</source>
        <translation>Używaj kodu celowania dźwięków opartego na sektorach</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="64"/>
        <source>Limit dehacked MaxHealth to health bonus</source>
        <translation>Ogranicz maks. zdrowie z dehacked do bonusowego zdrowia</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="66"/>
        <source>Trace ignores lines with the same sector on both sides</source>
        <translation>Śledzenie ignoruje linie z tym samym sektorem po obu stronach</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="68"/>
        <source>Monsters can not move when hanging over a drop off</source>
        <translation>Potwory utykają gdy wiszą nad przepaścią</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="70"/>
        <source>Scrolling sectors are additive like Boom</source>
        <translation>Przesuwające sektory dodają się jak w Boomie</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="72"/>
        <source>Monsters can see semi-invisible players</source>
        <translation>Potwory mogą widzieć na-wpół-niewidzialnych graczy</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="74"/>
        <source>Instantly moving floors are not silent</source>
        <translation>Natychmiastowo ruszające się podłogi nie są ciche</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="76"/>
        <source>Sector sounds use original method for sound origin</source>
        <translation>Dźwięki sektorów używają oryginalnej metody pozycjonowania dźwięku</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="78"/>
        <source>Use original Doom heights for clipping against projectiles</source>
        <translation>Używaj oryginalnych wysokości z Dooma podczas kolizji z pociskami</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="80"/>
        <source>Monsters can&apos;t be pushed over dropoffs</source>
        <translation>Potwory nie mogą być wypchnięte przez przepaść</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="82"/>
        <source>Any monster which calls BOSSDEATH counts for level specials</source>
        <translation>Każdy potwór, który wywołuje BOSSDEATH liczy się dla specjali na poziomie</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="84"/>
        <source>Minotaur&apos;s floor flame is exploded immediately when feet are clipped</source>
        <translation>Ogień podłogowy Minotaura eksploduje natychmiast gdy stopy są przycięte</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="86"/>
        <source>Force original velocity calculations for A_Mushroom in Dehacked mods</source>
        <translation>Oryginalne obliczanie prędkości dla A_Mushroom w Dehacked</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="88"/>
        <source>Monsters are affected by friction and pushers/pullers</source>
        <translation>Tarcie oraz popychacze i ciągniki wpływają na potwory</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="90"/>
        <source>Crushed monsters are turned into gibs, rather than replaced by gibs</source>
        <translation>Zgniecione potwory zamieniają się we flaki, a nie są przez nie zastępowane</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="92"/>
        <source>Friendly monsters aren&apos;t blocked by monster-blocking lines</source>
        <translation>Linie blokujące potwory ignorują przyjazne potwory</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="94"/>
        <source>Invert sprite sorting order for sprites of equal distance</source>
        <translation>Odwrócone sortowanie sprite&apos;ów gdy są w tej samej odległości</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="96"/>
        <source>Hitscans use original blockmap and hit check code</source>
        <translation>Hitscany używają oryginalnego kodu blockmap i detekcji trafień</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="98"/>
        <source>Find neighboring light level like like Doom</source>
        <translation>Szukaj sąsiadującego poziomu światła jak w Doomie</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="100"/>
        <source>Draw polyobjects the old fashioned way</source>
        <translation>Rysuj polyobjecty w stary sposób</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="110"/>
        <source>Net scripts are client side</source>
        <translation>Skrypty NET są po stronie klienta</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="112"/>
        <source>Clients send full button info</source>
        <translation>Klienci wysyłają pełne info o przyciskach</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="114"/>
        <source>Players can&apos;t use &apos;land&apos; CCMD</source>
        <translation>Gracze nie mogą używać komendy &apos;land&apos;</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="117"/>
        <source>Use Doom&apos;s original random number generator</source>
        <translation>Używaj oryginalnego generatora liczb losowych z Dooma</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="119"/>
        <source>Spheres have NOGRAVITY flag</source>
        <translation>Kule mają flagę NOGRAVITY</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="122"/>
        <source>Don&apos;t stop player scripts on disconnect</source>
        <translation>Nie zatrzymuj skryptów gracza po rozłączeniu</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="125"/>
        <source>Use horizontal explosion thrust of old ZDoom versions</source>
        <translation>Eksplozje popychają w poziomie jak w starym ZDoomie</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="128"/>
        <source>Non-SOLID things fall through invisible bridges</source>
        <translation>Nie-SOLIDne obiekty przenikają przez aktorów mostu</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="130"/>
        <source>Use old ZDoom jump physics</source>
        <translation>Fizyka skoku ze starego ZDooma</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="133"/>
        <source>Disallow weapon change when in mid raise/lower</source>
        <translation>Nie można zmieniać broni w trakcie wyciągania/chowania</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="135"/>
        <source>Use vanilla&apos;s autoaim tracer behavior</source>
        <translation>Trasowanie autocelowania takie jak w vanilli</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="137"/>
        <source>West spawns are silent</source>
        <translation>Zachodnie spawny są ciche</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="139"/>
        <source>Limited movement in the air</source>
        <translation>Ograniczone poruszanie w powietrzu</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="141"/>
        <source>Allow map01 &quot;plasma bump&quot; bug</source>
        <translation>Pozwól na błąd &quot;plasma bump&quot; z map01</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="143"/>
        <source>Allow instant respawn after death</source>
        <translation>Pozwól na natychmiastowy respawn po śmierci</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="145"/>
        <source>Disable taunting</source>
        <translation>Wyłącz okrzyki</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="147"/>
        <source>Use doom2.exe&apos;s original sound curve</source>
        <translation>Używaj krzywej dźwięku z doom2.exe</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="149"/>
        <source>Use original doom2 intermission music</source>
        <translation>Używaj oryginalnej (doom2) muzyki podczas intermisji</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="151"/>
        <source>Disable stealth monsters</source>
        <translation>Wyłącz potwory &quot;stealth&quot;</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="153"/>
        <source>Radius damage has infinite height</source>
        <translation>Obrażenia obszarowe mają nieskończoną wysokość</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="155"/>
        <source>Disable crosshair</source>
        <translation>Wyłącz celownik</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="157"/>
        <source>Force weapon switch</source>
        <translation>Wymuś zmianę broni</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="165"/>
        <source>Do not spawn health items (DM)</source>
        <translation>Nie spawnuj przedmiotów ze zdrowiem (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="167"/>
        <source>Do not spawn powerups (DM)</source>
        <translation>Nie spawnuj powerupów (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="169"/>
        <source>Weapons remain after pickup (DM)</source>
        <translation>Bronie zostają po podniesieniu (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="171"/>
        <source>Falling damage (old ZDoom)</source>
        <translation>Upadek boli (stary ZDoom)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="173"/>
        <source>Falling damage (Hexen)</source>
        <translation>Upadek boli (Hexen)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="175"/>
        <source>Falling damage (Strife)</source>
        <translation>Upadek boli (Strife)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="177"/>
        <source>Stay on same map when someone exits (DM)</source>
        <translation>Nie zmieniaj mapy gdy ktoś ją ukończy (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="179"/>
        <source>Spawn players as far as possible (DM)</source>
        <translation>Spawnuj graczy jak najdalej od siebie (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="181"/>
        <source>Automatically respawn dead players (DM)</source>
        <translation>Automatycznie respawnuj martwych graczy (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="183"/>
        <source>Don&apos;t spawn armor (DM)</source>
        <translation>Nie spawnuj zbroi (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="185"/>
        <source>Kill anyone who tries to exit the level (DM)</source>
        <translation>Zabijaj graczy gdy aktywują wyjście (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="187"/>
        <source>Infinite ammo</source>
        <translation>Nieskończona amunicja</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="189"/>
        <source>No monsters</source>
        <translation>Brak potworów</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="191"/>
        <source>Monsters respawn</source>
        <translation>Potwory odradzają się</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="193"/>
        <source>Items other than invuln. and invis. respawn</source>
        <translation>Przedmioty (oprócz nieśmier. i niewidz.) się odnawiają</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="195"/>
        <source>Fast monsters</source>
        <translation>Szybkie potwory</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="197"/>
        <source>No jumping</source>
        <translation>Brak skoku</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="199"/>
        <source>No freelook</source>
        <translation>Brak patrzenia góra-dół</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="201"/>
        <source>Respawn invulnerability and invisibility</source>
        <translation>Odradzaj nieśmiertelność i niewidzialność</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="203"/>
        <source>Arbitrator FOV</source>
        <translation>FOV ustalony z góry</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="205"/>
        <source>No multiplayer weapons in cooperative</source>
        <translation>Wyłącz bronie multiplayer w kooperacji</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="207"/>
        <source>No crouching</source>
        <translation>Brak kucania</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="209"/>
        <source>Lose all old inventory on respawn (COOP)</source>
        <translation>Zabieraj cały ekwipunek po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="211"/>
        <source>Lose keys on respawn (COOP)</source>
        <translation>Zabieraj klucze po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="213"/>
        <source>Lose weapons on respawn (COOP)</source>
        <translation>Zabieraj bronie po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="215"/>
        <source>Lose armor on respawn (COOP)</source>
        <translation>Zabieraj zbroję po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="217"/>
        <source>Lose powerups on respawn (COOP)</source>
        <translation>Zabieraj powerupy po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="219"/>
        <source>Lose ammo on respawn (COOP)</source>
        <translation>Zabieraj amunicję po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="221"/>
        <source>Lose half your ammo on respawn (COOP)</source>
        <translation>Zabieraj połowę amunicji po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="224"/>
        <source>Jumping allowed</source>
        <translation>Skakanie dozwolone</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="226"/>
        <source>Crouching allowed</source>
        <translation>Kucanie dozwolone</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="234"/>
        <source>Drop weapons upon death</source>
        <translation>Upuszczaj bronie po śmierci</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="236"/>
        <source>Don&apos;t spawn runes</source>
        <translation>Nie spawnuj run</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="238"/>
        <source>Instantly return flags (ST/CTF)</source>
        <translation>Natychmiastowo zwracaj flagi (ST/CTF)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="240"/>
        <source>Don&apos;t allow players to switch teams</source>
        <translation>Nie pozwalaj graczom na zmianę drużyn</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="242"/>
        <source>Players are automatically assigned teams</source>
        <translation>Gracze są automatycznie przydzielani do drużyn</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="244"/>
        <source>Double the amount of ammo given</source>
        <translation>Podwójna amunicja</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="246"/>
        <source>Players slowly lose health over 100% like Quake</source>
        <translation>Gracze powoli tracą zdrowie, gdy powyżej 100%</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="248"/>
        <source>Allow BFG freeaiming</source>
        <translation>Zezwól na celowanie BFG</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="250"/>
        <source>Barrels respawn</source>
        <translation>Odradzaj beczki</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="252"/>
        <source>No respawn protection</source>
        <translation>Nie chroń po odrodzeniu</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="254"/>
        <source>All players start with a shotgun</source>
        <translation>Wszyscy gracze dostają shotguna</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="256"/>
        <source>Players respawn where they died (COOP)</source>
        <translation>Gracze odradzają się tam, gdzie zginęli (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="258"/>
        <source>Don&apos;t clear frags after each level</source>
        <translation>Nie resetuj fragów po ukończeniu poziomu</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="260"/>
        <source>Player can&apos;t respawn</source>
        <translation>Gracz nie może się odrodzić</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="262"/>
        <source>Lose a frag when killed</source>
        <translation>Zabieraj fraga w momencie śmierci</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="264"/>
        <source>Infinite inventory</source>
        <translation>Nieskończony ekwipunek</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="266"/>
        <source>All monsters must be killed before exiting</source>
        <translation>Wszystkie potwory muszą zostać zabite przed wyjściem</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="268"/>
        <source>Players can&apos;t see the automap</source>
        <translation>Gracze nie widzą automapy</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="270"/>
        <source>Allies can&apos;t be seen on the automap</source>
        <translation>Sojusznicy są niewidoczni na automapie</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="272"/>
        <source>You can&apos;t spy allies</source>
        <translation>Nie możesz podglądać sojuszników</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="274"/>
        <source>Players can use chase cam</source>
        <translation>Można używać kamery z trzeciej osoby</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="276"/>
        <source>Players can&apos;t suicide</source>
        <translation>Gracze nie mogą popełniać samobójstwa</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="278"/>
        <source>Players can&apos;t use autoaim</source>
        <translation>Gracze nie mogą używać autocelowania</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="280"/>
        <source>Don&apos;t check ammo when switching weapons</source>
        <translation>Nie sprawdzaj amunicji podczas zmiany broni</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="282"/>
        <source>Kill all monsters spawned by a boss cube when the boss dies</source>
        <translation>Zabij wszystkie potwory spawnięte przez kostkę bossa gdy boss zginie</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="290"/>
        <source>Clients can&apos;t identify targets</source>
        <translation>Klienci nie mogą identyfikować celów</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="293"/>
        <source>lmsspectatorsettings applied in all game modes</source>
        <translation>Zastosuj lmsspectatorsettings we wszystkich trybach gry</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="295"/>
        <source>Clients can&apos;t draw coop info</source>
        <translation>Klienci nie mogą rysować informacji o kooperacji</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="297"/>
        <source>Unlagged is disabled</source>
        <translation>Unlagged jest wyłączone</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="299"/>
        <source>Players don&apos;t block each other</source>
        <translation>Gracze nie mogą się nawzajem blokować</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="301"/>
        <source>Clients don&apos;t show medals</source>
        <translation>Klienci nie pokazują medali</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="303"/>
        <source>Keys are shared between players</source>
        <translation>Klucze są współdzielone pomiędzy graczami</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="305"/>
        <source>Player teams are preserved between maps</source>
        <translation>Gracze są w tych samych drużynach po zmianie mapy</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="307"/>
        <source>Force OpenGL defaults</source>
        <translation>Wymuś domyślne ustawienia OpenGL</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="309"/>
        <source>No rocket jumping</source>
        <translation>Wyłącz skakanie na rakietach</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="311"/>
        <source>Award damage instead of kills</source>
        <translation>Nagradzaj obrażenia, nie zabójstwa</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="313"/>
        <source>Force drawing alpha</source>
        <translation>Wymuś rysowanie przezroczystości</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="315"/>
        <source>Don&apos;t spawn multiplayer things</source>
        <translation>Nie spawnuj aktorów wieloosobowych</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="318"/>
        <source>Force blood screen brightness on clients to emulate vanilla</source>
        <translation>Wymuszaj na klientach jasność zakrwawionego ekranu taką, jak w vanilli</translation>
    </message>
    <message>
        <location filename="../zandronum2dmflags.cpp" line="320"/>
        <source>Teammates don&apos;t block each other</source>
        <translation>Członkowie drużyny nie mogą się nawzajem blokować</translation>
    </message>
</context>
<context>
    <name>Zandronum3::Dmflags</name>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="40"/>
        <source>Use Doom&apos;s shortest texture behavior</source>
        <translation>Użyj Doomowego zachowania najkrótszej tekstury</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="42"/>
        <source>Don&apos;t fix loop index for stair building</source>
        <translation>Nie naprawiaj indeksu pętli do budowania schodów</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="44"/>
        <source>Pain elemental is limited to 20 lost souls</source>
        <translation>Pain elemental jest ograniczony do 20 lost souls</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="46"/>
        <source>Pickups are only heard locally</source>
        <translation>Podniesienia przedmiotów słychać tylko lokalnie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="48"/>
        <source>Infinitely tall actors</source>
        <translation>Nieskończenie wysocy aktorzy</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="50"/>
        <source>Limit actors to only one sound</source>
        <translation>Ogranicz aktorów do tylko jednego dźwięku</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="52"/>
        <source>Enable wallrunning</source>
        <translation>Włącz wallrunning</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="54"/>
        <source>Dropped items spawn on floor</source>
        <translation>Upuszczane przedmioty tworzone są na podłodze</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="56"/>
        <source>Special lines block use line</source>
        <translation>Linie specjalne blokują użycie linii</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="58"/>
        <source>Disable BOOM local door light effect</source>
        <translation>Wyłącz efekt lokalnego światła drzwi z BOOMa</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="60"/>
        <source>Raven&apos;s scrollers use their original speed</source>
        <translation>Przewijacze Ravena używają oryginalnych prędkości</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="62"/>
        <source>Use sector based sound target code</source>
        <translation>Używaj kodu celowania dźwiękiem opartego na sektorach</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="64"/>
        <source>Limit dehacked MaxHealth to health bonus</source>
        <translation>Ogranicz maks. zdrowie z dehacked do bonusowego zdrowia</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="66"/>
        <source>Trace ignores lines with the same sector on both sides</source>
        <translation>Śledzenie ignoruje linie z tym samym sektorem po obu stronach</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="68"/>
        <source>Monsters can not move when hanging over a drop off</source>
        <translation>Potwory utykają gdy wiszą nad przepaścią</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="70"/>
        <source>Scrolling sectors are additive like Boom</source>
        <translation>Przesuwające sektory dodają się jak w Boomie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="72"/>
        <source>Monsters can see semi-invisible players</source>
        <translation>Potwory mogą widzieć na-wpół-niewidzialnych graczy</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="74"/>
        <source>Instantly moving floors are not silent</source>
        <translation>Natychmiastowo ruszające się podłogi nie są ciche</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="76"/>
        <source>Sector sounds use original method for sound origin</source>
        <translation>Dźwięki sektorów używają oryginalnej metody pozycjonowania dźwięku</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="78"/>
        <source>Use original Doom heights for clipping against projectiles</source>
        <translation>Używaj oryginalnych wysokości z Dooma podczas kolizji z pociskami</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="80"/>
        <source>Monsters can&apos;t be pushed over dropoffs</source>
        <translation>Potwory nie mogą być wypchnięte przez przepaść</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="82"/>
        <source>Any monster which calls BOSSDEATH counts for level specials</source>
        <translation>Każdy potwór, który wywołuje BOSSDEATH liczy się dla specjali na poziomie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="84"/>
        <source>Minotaur&apos;s floor flame is exploded immediately when feet are clipped</source>
        <translation>Ogień podłogowy Minotaura eksploduje natychmiast gdy stopy są przycięte</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="86"/>
        <source>Force original velocity calculations for A_Mushroom in Dehacked mods</source>
        <translation>Oryginalne obliczanie prędkości dla A_Mushroom w Dehacked</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="88"/>
        <source>Monsters are affected by friction and pushers/pullers</source>
        <translation>Tarcie oraz popychacze i ciągniki wpływają na potwory</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="90"/>
        <source>Crushed monsters are turned into gibs, rather than replaced by gibs</source>
        <translation>Zgniecione potwory zamieniają się we flaki, a nie są przez nie zastępowane</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="92"/>
        <source>Friendly monsters aren&apos;t blocked by monster-blocking lines</source>
        <translation>Linie blokujące potwory ignorują przyjazne potwory</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="94"/>
        <source>Invert sprite sorting order for sprites of equal distance</source>
        <translation>Odwrócone sortowanie sprite&apos;ów gdy są w tej samej odległości</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="96"/>
        <source>Hitscans use original blockmap and hit check code</source>
        <translation>Hitscany używają oryginalnego kodu blockmap i detekcji trafień</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="98"/>
        <source>Find neighboring light level like like Doom</source>
        <translation>Szukaj sąsiadującego poziomu światła jak w Doomie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="100"/>
        <source>Draw polyobjects the old fashioned way</source>
        <translation>Rysuj polyobjecty w stary sposób</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="102"/>
        <source>Ignore compositing when drawing masked midtextures</source>
        <translation>Ignoruj kompozycję podczas rysowania maskowanych midtekstur</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="112"/>
        <source>It is impossible to directly face cardinal direction</source>
        <translation>Nie można obrócić się bezpośrednio w kierunku kardynalnym</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="114"/>
        <source>Use the same floor motion behavior as Doom</source>
        <translation>Użyj takiego zachowania ruchu podłóg, jak w Doomie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="116"/>
        <source>Trigger bump actions on pass-over linedefs</source>
        <translation>Można popychać nieblokujące linie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="126"/>
        <source>Net scripts are client side</source>
        <translation>Skrypty NET są po stronie klienta</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="128"/>
        <source>Clients send full button info</source>
        <translation>Klienci wysyłają pełne info o przyciskach</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="130"/>
        <source>Players can&apos;t use &apos;land&apos; CCMD</source>
        <translation>Gracze nie mogą używać komendy &apos;land&apos;</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="133"/>
        <source>Use Doom&apos;s original random number generator</source>
        <translation>Używaj oryginalnego generatora liczb losowych z Dooma</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="135"/>
        <source>Spheres have NOGRAVITY flag</source>
        <translation>Kule mają flagę NOGRAVITY</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="138"/>
        <source>Don&apos;t stop player scripts on disconnect</source>
        <translation>Nie zatrzymuj skryptów gracza po rozłączeniu</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="141"/>
        <source>Use horizontal explosion thrust of old ZDoom versions</source>
        <translation>Eksplozje popychają w poziomie jak w starym ZDoomie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="144"/>
        <source>Non-SOLID things fall through invisible bridges</source>
        <translation>Nie-SOLIDne obiekty przenikają przez aktorów mostu</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="146"/>
        <source>Use old ZDoom jump physics</source>
        <translation>Fizyka skoku ze starego ZDooma</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="149"/>
        <source>Disallow weapon change when in mid raise/lower</source>
        <translation>Nie można zmieniać broni w trakcie wyciągania/chowania</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="151"/>
        <source>Use vanilla&apos;s autoaim tracer behavior</source>
        <translation>Trasowanie autocelowania takie jak w vanilli</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="153"/>
        <source>West spawns are silent</source>
        <translation>Zachodnie spawny są ciche</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="155"/>
        <source>Jumping works as in Skulltag</source>
        <translation>Skakanie jak w Skulltagu</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="157"/>
        <source>Reset the world/global ACS variables when resetting the map</source>
        <translation>Resetuj zmienne ACS global/world wraz z mapą</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="159"/>
        <source>Don&apos;t show obituaries</source>
        <translation>Nie wyświetlaj tekstów pośmiertnych</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="161"/>
        <source>Limited movement in the air</source>
        <translation>Ograniczone poruszanie w powietrzu</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="163"/>
        <source>Allow map01 &quot;plasma bump&quot; bug</source>
        <translation>Pozwól na błąd &quot;plasma bump&quot; z map01</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="165"/>
        <source>Allow instant respawn after death</source>
        <translation>Pozwól na natychmiastowy respawn po śmierci</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="167"/>
        <source>Disable taunting</source>
        <translation>Wyłącz okrzyki</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="169"/>
        <source>Use doom2.exe&apos;s original sound curve</source>
        <translation>Używaj krzywej dźwięku z doom2.exe</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="171"/>
        <source>Use original doom2 intermission music</source>
        <translation>Używaj oryginalnej (doom2) muzyki podczas intermisji</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="173"/>
        <source>Disable stealth monsters</source>
        <translation>Wyłącz potwory &quot;stealth&quot;</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="175"/>
        <source>Radius damage has infinite height</source>
        <translation>Obrażenia obszarowe mają nieskończoną wysokość</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="177"/>
        <source>Disable crosshair</source>
        <translation>Wyłącz celownik</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="179"/>
        <source>Force weapon switch</source>
        <translation>Wymuś zmianę broni</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="187"/>
        <source>Do not spawn health items (DM)</source>
        <translation>Nie spawnuj przedmiotów ze zdrowiem (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="189"/>
        <source>Do not spawn powerups (DM)</source>
        <translation>Nie spawnuj powerupów (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="191"/>
        <source>Weapons remain after pickup (DM)</source>
        <translation>Bronie zostają po podniesieniu (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="193"/>
        <source>Falling damage (old ZDoom)</source>
        <translation>Upadek boli (stary ZDoom)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="195"/>
        <source>Falling damage (Hexen)</source>
        <translation>Upadek boli (Hexen)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="197"/>
        <source>Falling damage (Strife)</source>
        <translation>Upadek boli (Strife)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="199"/>
        <source>Stay on same map when someone exits (DM)</source>
        <translation>Nie zmieniaj mapy gdy ktoś ją ukończy (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="201"/>
        <source>Spawn players as far as possible (DM)</source>
        <translation>Spawnuj graczy jak najdalej od siebie (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="203"/>
        <source>Automatically respawn dead players (DM)</source>
        <translation>Automatycznie respawnuj martwych graczy (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="205"/>
        <source>Don&apos;t spawn armor (DM)</source>
        <translation>Nie spawnuj zbroi (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="207"/>
        <source>Kill anyone who tries to exit the level (DM)</source>
        <translation>Zabijaj graczy gdy aktywują wyjście (DM)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="209"/>
        <source>Infinite ammo</source>
        <translation>Nieskończona amunicja</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="211"/>
        <source>No monsters</source>
        <translation>Brak potworów</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="213"/>
        <source>Monsters respawn</source>
        <translation>Potwory odradzają się</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="215"/>
        <source>Items other than invuln. and invis. respawn</source>
        <translation>Przedmioty (oprócz nieśmier. i niewidz.) się odnawiają</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="217"/>
        <source>Fast monsters</source>
        <translation>Szybkie potwory</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="219"/>
        <source>No jumping</source>
        <translation>Brak skoku</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="221"/>
        <source>No freelook</source>
        <translation>Brak patrzenia góra-dół</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="223"/>
        <source>Respawn invulnerability and invisibility</source>
        <translation>Odradzaj nieśmiertelność i niewidzialność</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="225"/>
        <source>Arbitrator FOV</source>
        <translation>FOV ustalony z góry</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="227"/>
        <source>No multiplayer weapons in cooperative</source>
        <translation>Wyłącz bronie multiplayer w kooperacji</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="229"/>
        <source>No crouching</source>
        <translation>Brak kucania</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="231"/>
        <source>Lose all old inventory on respawn (COOP)</source>
        <translation>Zabieraj cały ekwipunek po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="233"/>
        <source>Lose keys on respawn (COOP)</source>
        <translation>Zabieraj klucze po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="235"/>
        <source>Lose weapons on respawn (COOP)</source>
        <translation>Zabieraj bronie po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="237"/>
        <source>Lose armor on respawn (COOP)</source>
        <translation>Zabieraj zbroję po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="239"/>
        <source>Lose powerups on respawn (COOP)</source>
        <translation>Zabieraj powerupy po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="241"/>
        <source>Lose ammo on respawn (COOP)</source>
        <translation>Zabieraj amunicję po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="243"/>
        <source>Lose half your ammo on respawn (COOP)</source>
        <translation>Zabieraj połowę amunicji po odrodzeniu (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="245"/>
        <source>Jumping allowed</source>
        <translation>Skakanie dozwolone</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="247"/>
        <source>Crouching allowed</source>
        <translation>Kucanie dozwolone</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="255"/>
        <source>Drop weapons upon death</source>
        <translation>Upuszczaj bronie po śmierci</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="257"/>
        <source>Don&apos;t spawn runes</source>
        <translation>Nie spawnuj run</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="259"/>
        <source>Instantly return flags (ST/CTF)</source>
        <translation>Natychmiastowo zwracaj flagi (ST/CTF)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="261"/>
        <source>Don&apos;t allow players to switch teams</source>
        <translation>Nie pozwalaj graczom na zmianę drużyn</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="263"/>
        <source>Players are automatically assigned teams</source>
        <translation>Gracze są automatycznie przydzielani do drużyn</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="265"/>
        <source>Double the amount of ammo given</source>
        <translation>Podwójna amunicja</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="267"/>
        <source>Players slowly lose health over 100% like Quake</source>
        <translation>Gracze powoli tracą zdrowie, gdy powyżej 100%</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="269"/>
        <source>Allow BFG freeaiming</source>
        <translation>Zezwól na celowanie BFG</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="271"/>
        <source>Barrels respawn</source>
        <translation>Odradzaj beczki</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="273"/>
        <source>No respawn protection</source>
        <translation>Nie chroń po odrodzeniu</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="275"/>
        <source>All players start with a shotgun</source>
        <translation>Wszyscy gracze dostają shotguna</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="277"/>
        <source>Players respawn where they died (COOP)</source>
        <translation>Gracze odradzają się tam, gdzie zginęli (COOP)</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="279"/>
        <source>Don&apos;t clear frags after each level</source>
        <translation>Nie resetuj fragów po ukończeniu poziomu</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="281"/>
        <source>Player can&apos;t respawn</source>
        <translation>Gracz nie może się odrodzić</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="283"/>
        <source>Lose a frag when killed</source>
        <translation>Zabieraj fraga w momencie śmierci</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="285"/>
        <source>Infinite inventory</source>
        <translation>Nieskończony ekwipunek</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="287"/>
        <source>All monsters must be killed before exiting</source>
        <translation>Wszystkie potwory muszą zostać zabite przed wyjściem</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="289"/>
        <source>Players can&apos;t see the automap</source>
        <translation>Gracze nie widzą automapy</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="291"/>
        <source>Allies can&apos;t be seen on the automap</source>
        <translation>Sojusznicy są niewidoczni na automapie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="293"/>
        <source>You can&apos;t spy allies</source>
        <translation>Nie możesz podglądać sojuszników</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="295"/>
        <source>Players can use chase cam</source>
        <translation>Można używać kamery z trzeciej osoby</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="297"/>
        <source>Players can&apos;t suicide</source>
        <translation>Gracze nie mogą popełniać samobójstwa</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="299"/>
        <source>Players can&apos;t use autoaim</source>
        <translation>Gracze nie mogą używać autocelowania</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="301"/>
        <source>Don&apos;t check ammo when switching weapons</source>
        <translation>Nie sprawdzaj amunicji podczas zmiany broni</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="303"/>
        <source>Kill all monsters spawned by a boss cube when the boss dies</source>
        <translation>Zabij wszystkie potwory spawnięte przez kostkę bossa gdy boss zginie</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="306"/>
        <source>Do not count monsters in &apos;end level when dying&apos; sectors towards kill count</source>
        <translation>Nie licz zabójstw potworów znajdujących się w sektorach typu &apos;zakończ poziom przy śmierci&apos;</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="316"/>
        <source>Clients can&apos;t identify targets</source>
        <translation>Klienci nie mogą identyfikować celów</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="319"/>
        <source>lmsspectatorsettings applied in all game modes</source>
        <translation>Zastosuj lmsspectatorsettings we wszystkich trybach gry</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="321"/>
        <source>Clients can&apos;t draw coop info</source>
        <translation>Klienci nie mogą rysować informacji o kooperacji</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="323"/>
        <source>Unlagged is disabled</source>
        <translation>Unlagged jest wyłączone</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="325"/>
        <source>Players don&apos;t block each other</source>
        <translation>Gracze nie mogą się nawzajem blokować</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="327"/>
        <source>Clients don&apos;t show medals</source>
        <translation>Klienci nie pokazują medali</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="329"/>
        <source>Keys are shared between players</source>
        <translation>Klucze są współdzielone pomiędzy graczami</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="331"/>
        <source>Player teams are preserved between maps</source>
        <translation>Gracze są w tych samych drużynach po zmianie mapy</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="333"/>
        <source>Force renderer defaults</source>
        <translation>Wymuś domyślne ustawienia rendera</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="354"/>
        <source>No unlagged on BFG tracers</source>
        <translation>Wyłącz unlagged dla tracerów BFG</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="356"/>
        <source>No door closing</source>
        <translation>Zamykanie drzwi zabronione</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="358"/>
        <source>Limit vertical look range to the limit of the software renderer</source>
        <translation>Limituj patrzenie góra-dół do rendera software&apos;owego</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="360"/>
        <source>Players can shoot through allies</source>
        <translation>Strzały przechodzą przez kolegów z drużyny</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="362"/>
        <source>Players don&apos;t push allies when shooting them</source>
        <translation>Strzały nie popychają kolegów z drużyny</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="335"/>
        <source>No rocket jumping</source>
        <translation>Wyłącz skakanie na rakietach</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="337"/>
        <source>Award damage instead of kills</source>
        <translation>Nagradzaj obrażenia, nie zabójstwa</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="339"/>
        <source>Force drawing alpha</source>
        <translation>Wymuś rysowanie przezroczystości</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="341"/>
        <source>Don&apos;t spawn multiplayer things</source>
        <translation>Nie spawnuj aktorów wieloosobowych</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="344"/>
        <source>Force blood screen brightness on clients to emulate vanilla</source>
        <translation>Wymuszaj na klientach jasność zakrwawionego ekranu taką, jak w vanilli</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="346"/>
        <source>Teammates don&apos;t block each other</source>
        <translation>Członkowie drużyny nie mogą się nawzajem blokować</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="348"/>
        <source>No dropping allowed</source>
        <translation>Nie wolno upuszczać przedmiotów</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="350"/>
        <source>No map reset on death in survival</source>
        <translation>Mapa nie jest resetowana gdy gracze zginą w surwiwalu</translation>
    </message>
    <message>
        <location filename="../zandronum3dmflags.cpp" line="352"/>
        <source>Dead players can keep inventory</source>
        <translation>Martwi gracze mogą zachować ekwipunek</translation>
    </message>
</context>
<context>
    <name>ZandronumAboutProvider</name>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="110"/>
        <source>This plugin is distributed under the terms of the LGPL v2.1 or later.

</source>
        <translation>Ta wtyczka jest rozprowadzana na zasadach licencji LGPL v2.1 lub późniejszej.

</translation>
    </message>
</context>
<context>
    <name>ZandronumBroadcast</name>
    <message>
        <location filename="../zandronumbroadcast.cpp" line="117"/>
        <source>Listening to Zandronum&apos;s LAN servers broadcasts on port %1.</source>
        <translation>Nasłuchuję rozgłoszeń serwerów LAN Zandronum na porcie %1.</translation>
    </message>
    <message>
        <location filename="../zandronumbroadcast.cpp" line="123"/>
        <source>Failed to bind Zandronum&apos;s LAN broadcasts listening socket on port %1. Will keep retrying silently.</source>
        <translation>Nie udało się rozpocząć nasłuchu rozgłoszeń LAN dla Zandronum na porcie %1. Będą podejmowane ciche próby naprawy.</translation>
    </message>
</context>
<context>
    <name>ZandronumClientExeFile</name>
    <message>
        <location filename="../zandronumbinaries.cpp" line="94"/>
        <source>client</source>
        <translation>klient</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="116"/>
        <source>No testing directory specified for Zandronum</source>
        <translation>Nie podano katalogu testowego dla Zandronum</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="122"/>
        <source>Unable to create directory:
%1</source>
        <translation>Nie można utworzyć katalogu:
%1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="126"/>
        <source>Unable to create directory:
%1/%2</source>
        <translation>Nie można utworzyć katalogu:
%1/%2</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="149"/>
        <source>Doomseeker</source>
        <translation>Doomseeker</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="150"/>
        <source>Please install now version &quot;%1&quot; into:
%2</source>
        <translation>Proszę teraz zainstalować wersję &quot;%1&quot; do:
%2</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="165"/>
        <source>&lt;p&gt;Zandronum testing version %1 can be installed, but files already exist at the target directory.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Istnieje możliwość instalacji testowej wersji Zandronum %1, ale w docelowym katalogu istnieją już pliki.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="169"/>
        <source>&lt;p&gt;Installation of testing binaries for version %1 can potentially &lt;b&gt;overwrite&lt;/b&gt; your files.&lt;/p&gt;&lt;p&gt;Game will be installed to:&lt;br&gt;%2&lt;/p&gt;&lt;p&gt;Do you want Doomseeker to extract Zandronum files, potentially &lt;b&gt;overwriting existing ones&lt;/b&gt;, and to copy all your configuration files from your base directory?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Instalacja binarek testowych dla wersji %1 może potencjalnie &lt;b&gt;nadpisać&lt;/b&gt; Twoje pliki.&lt;/p&gt;&lt;p&gt;Gra zostanie zainstalowana pod ścieżką:&lt;br&gt;%2&lt;/p&gt;&lt;p&gt;Czy chcesz aby Doomseeker wypakował pliki Zandronum, potencjalnie &lt;b&gt;nadpisując już istniejące&lt;/b&gt;, oraz aby skopiował wszystkie Twoje pliki konfiguracyjne z katalogu bazowego?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="180"/>
        <source>Zandronum testing version %1 can be installed.

Game will be installed to:
%2

Do you want Doomseeker to install that version and copy all your configuration files from your base directory?</source>
        <translation>Wersja testowa Zandronum %1 może zostać zainstalowana.

Gra zostanie zainstalowana do:
%2

Czy chcesz, aby Doomseeker zainstalował tą wersję i skopiował wszystkie Twoje pliki konfiguracyjne z katalogu bazowego?</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="187"/>
        <source>Doomseeker - install Zandronum testing version</source>
        <translation>Doomseeker - instalacja testowej wersji Zandronum</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="213"/>
        <source>No testing directory configured for Zandronum</source>
        <translation>Nie skonfigurowano katalogu testowego dla Zandronum</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="223"/>
        <source>%1
doesn&apos;t exist.
You need to install new testing binaries.</source>
        <translation>%1
nie istnieje.
Musisz zainstalować nowe binarki testowe.</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="230"/>
        <source>%1
exists but is NOT a directory.
Cannot proceed.</source>
        <translation>%1
istnieje ale NIE JEST katalogiem.
Nie można kontynuować.</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="239"/>
        <source>%1
exists but doesn&apos;t contain a Zandronum executable.

Doomseeker can still install the game there if you want.</source>
        <translation>%1
istnieje, ale nie zawiera pliku wykonywalnego Zandronum.

Doomseeker może zainstalować grę, jeśli chcesz.</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="310"/>
        <source>Downloading Zandronum testing binary from URL: %1</source>
        <translation>Pobieram binarkę testową Zandronum z URLa: %1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="315"/>
        <source>Doomseeker - download failed</source>
        <translation>Doomseeker - pobieranie nie powiodło się</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="316"/>
        <source>Failed to download testing binary.

%1</source>
        <translation>Nie udało się pobrać binarek testowych.

%1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="340"/>
        <source>Unpacking file: %1</source>
        <translation>Wypakowuję plik: %1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="352"/>
        <source>Doomseeker - unpack failed</source>
        <translation>Doomseeker - błąd wypakowania</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="353"/>
        <source>Failed to unpack: %1</source>
        <translation>Nie udało się wypakować: %1</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="379"/>
        <source>%1
 should be a script file but is a directory!</source>
        <translation>%1
 powinien być plikiem skryptowym, ale jest katalogiem!</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="388"/>
        <source>You don&apos;t have permissions to execute file: %1
</source>
        <translation>Nie masz uprawnień aby uruchomić plik: %1
</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="429"/>
        <source>Couldn&apos;t open batch file &quot;%1&quot; for writing</source>
        <translation>Nie można otworzyć pliku skryptowego &quot;%1&quot; do zapisu</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="436"/>
        <source>Error while writing batch file &quot;%1&quot;</source>
        <translation>Błąd podczas zapisywania pliku skryptowego &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../zandronumbinaries.cpp" line="446"/>
        <source>Cannot set permissions for file:
%1</source>
        <translation>Nie można ustawić uprawnień plikowi:
%1</translation>
    </message>
</context>
<context>
    <name>ZandronumGameInfo</name>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="37"/>
        <source>Time limit</source>
        <translation>Limit czasu</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="45"/>
        <source>Frag limit</source>
        <translation>Limit fragów</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="56"/>
        <source>Point limit</source>
        <translation>Limit punktów</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="63"/>
        <source>Win limit</source>
        <translation>Limit zwycięstw</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="68"/>
        <source>Duel limit</source>
        <translation>Limit pojedynków</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="71"/>
        <source>Max. lives</source>
        <translation>Maks. żyć</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="80"/>
        <source>Survival</source>
        <translation>Surwiwal</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="81"/>
        <source>Invasion</source>
        <translation>Inwazja</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="84"/>
        <source>Duel</source>
        <translation>Pojedynek</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="85"/>
        <source>Terminator</source>
        <translation>Terminator</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="86"/>
        <source>LMS</source>
        <translation>LMS</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="87"/>
        <source>Team LMS</source>
        <translation>Drużynowy LMS</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="88"/>
        <source>Possession</source>
        <translation>Posesja</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="89"/>
        <source>Team Poss</source>
        <translation>Drużynowa Posesja</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="90"/>
        <source>Team Game</source>
        <translation>Gra Drużynowa</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="92"/>
        <source>One Flag CTF</source>
        <translation>CTF z jedną flagą</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="93"/>
        <source>Skulltag</source>
        <translation>Skulltag</translation>
    </message>
    <message>
        <location filename="../zandronumgameinfo.cpp" line="94"/>
        <source>Domination</source>
        <translation>Dominacja</translation>
    </message>
</context>
<context>
    <name>ZandronumMasterClient</name>
    <message>
        <location filename="../zandronummasterclient.cpp" line="70"/>
        <source>You can contact Zandronum staff about this on Discord or on the forum: &lt;a href=&quot;https://zandronum.com/forum&quot;&gt;https://zandronum.com/forum&lt;/a&gt;.</source>
        <translation>Możesz uzyskać pomoc w tej sprawie poprzez kontakt z załogą Zandronum na Discordzie lub na forum: &lt;a href=&quot;https://zandronum.com/forum&quot;&gt;https://zandronum.com/forum&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>ZandronumRConProtocol</name>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="69"/>
        <source>Connection attempt ...</source>
        <translation>Próba połączenia ...</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="77"/>
        <source>Too many failed connection attempts. Aborting.</source>
        <translation>Zbyt wiele nieudanych prób połączenia. Przerywam.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="112"/>
        <source>Authenticating ...</source>
        <translation>Uwierzytelnianie ...</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="131"/>
        <source>Too many failed authentication attempts. Aborting.</source>
        <translation>Zbyt wiele nieudanych prób uwierzytelnienia. Przerywam.</translation>
    </message>
    <message numerus="yes">
        <location filename="../zandronumrconprotocol.cpp" line="173"/>
        <source>Delaying for about %n seconds before next authentication attempt.</source>
        <translation>
            <numerusform>Opóźniam na około %n sekundę przed następną próbą uwierzytelnienia.</numerusform>
            <numerusform>Opóźniam na około %n sekundy przed następną próbą uwierzytelnienia.</numerusform>
            <numerusform>Opóźniam na około %n sekund przed następną próbą uwierzytelnienia.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="220"/>
        <source>Failed to establish connection.</source>
        <translation>Nie udało się zestawić połączenia.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="223"/>
        <source>Timeout on authentication.</source>
        <translation>Przekroczono czas uwierzytelnienia.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="241"/>
        <source>You have been banned from this server.</source>
        <translation>Masz bana na tym serwerze.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="246"/>
        <source>The protocol appears to be outdated.</source>
        <translation>Protokół wydaje się być przestarzały.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="284"/>
        <source>Authentication failure.</source>
        <translation>Błąd uwierzytelnienia.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="289"/>
        <source>Remote console connection established.</source>
        <translation>Połączenie ze zdalną konsolą zestawione.</translation>
    </message>
    <message>
        <location filename="../zandronumrconprotocol.cpp" line="290"/>
        <source>-----</source>
        <translation>-----</translation>
    </message>
</context>
<context>
    <name>ZandronumServer</name>
    <message>
        <location filename="../zandronumserver.cpp" line="163"/>
        <source>Blue</source>
        <translation>Niebieska</translation>
    </message>
    <message>
        <location filename="../zandronumserver.cpp" line="164"/>
        <source>Red</source>
        <translation>Czerwona</translation>
    </message>
    <message>
        <location filename="../zandronumserver.cpp" line="165"/>
        <source>Green</source>
        <translation>Zielona</translation>
    </message>
    <message>
        <location filename="../zandronumserver.cpp" line="166"/>
        <source>Gold</source>
        <translation>Złota</translation>
    </message>
</context>
</TS>
