<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="122"/>
        <source>Copyright %1 %2 The Doomseeker Team</source>
        <translation>Copyright %1 %2 L&apos;Equip de Doomseeker (The Doomseeker Team)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="124"/>
        <source>This program is distributed under the terms of the LGPL v2.1 or later.</source>
        <translation>Aquest programa es distribueix sota els termes de la llicència LGPL v2.1 o posterior.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="127"/>
        <source>Doomseeker translations contributed by:
</source>
        <translation>Traduccions de Doomseeker contribuïdes per:
</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="128"/>
        <source>- Polish: Zalewa</source>
        <translation>- Polonès: Zalewa</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="129"/>
        <source>- Spanish: Pol Marcet Sard%1</source>
        <translation>- Espanyol: Pol Marcet Sard%1</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="130"/>
        <source>- Catalan: Pol Marcet Sard%1</source>
        <translation>- Català: Pol Marcet Sard%1</translation>
    </message>
    <message>
        <source>This program uses GeoLite2 data for IP-to-Country (IP2C) purposes, available from https://www.maxmind.com</source>
        <translation type="vanished">Aquest programa fa servir la informació de GeoLite2 per localitzar països per IP (IP2C), i la pot trobar en https://www.maxmind.com</translation>
    </message>
    <message>
        <source>Database and Contents Copyright (c) 2018 MaxMind, Inc.</source>
        <translation type="vanished">La base de dades i el seu contingut sota Copyright (c) 2018 Maxmind, Inc.</translation>
    </message>
    <message>
        <source>GeoLite2 License:
This work is licensed under the Creative Commons Attribution - ShareAlike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.</source>
        <translation type="vanished">Llicència de GeoLite2:
Es troba sota la llicència de la Creative Commons Attribution-ShareAlike 4.0 Unported License. Per veure una còpia d&apos;aquesta llicència, visita http://creativecommons.org/licenses/by-sa/4.0/.</translation>
    </message>
    <message>
        <source>GeoLite2 available at:
https://dev.maxmind.com/geoip/geoip2/geolite2/</source>
        <translation type="vanished">GeoLite2 està disponible en:
https://dev.maxmind.com/geoip/geoip2/geolite2/</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="66"/>
        <source>&lt;i&gt;No URL available&lt;/i&gt;</source>
        <translation>&lt;i&gt;No URL disponible&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="142"/>
        <source>- Aha-Soft</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="143"/>
        <source>- Crystal Clear by Everaldo Coelho</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="144"/>
        <source>- Fugue Icons (C) 2013 Yusuke Kamiyamane. All rights reserved.</source>
        <translation>- Fugue Icons (C) 2013 Yusuke Kamiyamane. Tots els drets reservats.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="145"/>
        <source>- Nuvola 1.0 (KDE 3.x icon set)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="146"/>
        <source>- Oxygen Icons 4.3.1 (KDE)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="147"/>
        <source>- Silk Icon Set (C) Mark James (famfamfam.com)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="148"/>
        <source>- Tango Icon Library / Tango Desktop Project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="149"/>
        <source>This program uses icons (or derivates of) from following sources:
</source>
        <translation>Aquest programa fa servir icones (o derivats) de les següents fonts:
</translation>
    </message>
    <message>
        <source>JSON library license</source>
        <translation type="vanished">Llicència de la biblioteca JSON</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="14"/>
        <source>About Doomseeker</source>
        <translation>Sobre Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="24"/>
        <location filename="../gui/aboutdialog.ui" line="43"/>
        <source>Doomseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="77"/>
        <location filename="../gui/aboutdialog.ui" line="406"/>
        <source>&lt;Version&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="90"/>
        <source>(ABI: &lt;DOOMSEEKER_ABI_VERSION&gt;)</source>
        <translation>(ABI: &lt;DOOMSEEKER_ABI_VERSION&gt;)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="112"/>
        <source>&lt;Changeset&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="140"/>
        <source>Revision:</source>
        <translation>Revisió:</translation>
    </message>
    <message>
        <source>This value is relevant to auto updater feature.</source>
        <translation type="vanished">Aquest valor és rellevant per a la funció d&apos;actualització automàtica.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="147"/>
        <source>The auto updater compares the versions by this revision number.</source>
        <translation>L&apos;actualitzador automàtic compara altres versions amb aquest nombre de versió.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="150"/>
        <source>&lt;Revision&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="175"/>
        <location filename="../gui/aboutdialog.ui" line="419"/>
        <source>&lt;a href=&quot;https://doomseeker.drdteam.org/&quot;&gt;https://doomseeker.drdteam.org/&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Contact information:</source>
        <translation type="vanished">Informació de contacte:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="232"/>
        <source>Zalewa:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="239"/>
        <source>Blzut3:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="290"/>
        <source>Additional contributions from:</source>
        <translation>Contribuïdors addicionals:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="300"/>
        <source>- Hyper_Eye, Nece228, Linda &quot;WubTheCaptain&quot; Lapinlampi, Pol Marcet Sardà</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="310"/>
        <source>- Doomseeker logo and main icon design by Rachael &quot;Eruanna&quot; Alexanderson</source>
        <translation>- El logotip de Doomseeker i la icona principal dissenyats per Rachael &quot;Eruanna&quot; Alexanderson</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="320"/>
        <source>- Doomseeker icons by MazterQyoun-ae and CarpeDiem</source>
        <translation>- Icones de Doomseeker fetes per MazterQyoun-ae i CarpeDiem</translation>
    </message>
    <message>
        <source>- Improved Doomseeker icons by MazterQyoun-ae and CarpeDiem</source>
        <translation type="vanished">- Icones de Doomseeker millorats per MazterQyoun-ae i CarpeDiem</translation>
    </message>
    <message>
        <source>Show JSON library license</source>
        <translation type="vanished">Mostrar llicència de la llibreria JSON</translation>
    </message>
    <message>
        <source>GeoLite2 Database:</source>
        <translation type="vanished">Base de dades GeoLite2:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="223"/>
        <source>Doomseeker maintainers:</source>
        <translation>Responsables de Doomseeker:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="246"/>
        <source>&lt;a href=&quot;mailto:admin@maniacsvault.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;admin@maniacsvault.net&lt;/span&gt;&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="262"/>
        <source>&lt;a href=&quot;mailto:zalewapl@gmail.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;zalewapl@gmail.com&lt;/span&gt;&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="340"/>
        <source>IP2C Database:</source>
        <translation>Base de dades IP2C:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="350"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&amp;lt;URL&amp;gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="371"/>
        <location filename="../gui/aboutdialog.ui" line="390"/>
        <source>Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="437"/>
        <source>Copyright ©</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="447"/>
        <source>&lt;YearSpan&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="457"/>
        <source>&lt;Author&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="482"/>
        <source>&lt;Description&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="538"/>
        <source>Version: 0.0.0.0</source>
        <translation>Versió: 0.0.0.0</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="551"/>
        <source>(ABI: DOOMSEEKER_ABI_VERSION)</source>
        <translation>(ABI: DOOMSEEKER_ABI_VERSION)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="573"/>
        <source>&lt;Plugin Author&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="509"/>
        <source>Plugins</source>
        <translation>Complements</translation>
    </message>
</context>
<context>
    <name>AddBuddyDlg</name>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="300"/>
        <source>Invalid Pattern</source>
        <translation>Patró invàlid</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="300"/>
        <source>The pattern you have specified is invalid.</source>
        <translation>El patró que heu especificat no és vàlid.</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="319"/>
        <source>The asterisk (*) can be used as a wild card.</source>
        <translation>L&apos;asterisc (*) representa qualsevol lletra.</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="323"/>
        <source>Use the Regular Expression format.</source>
        <translation>Utilitza el format d&apos;expressions regulars.</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="14"/>
        <source>Add Buddy</source>
        <translation>Afegir Company</translation>
    </message>
    <message>
        <source>Type the name of your buddy here.  If the pattern type is set to basic you may use an asterisk (*) as a wild card.</source>
        <translation type="vanished">Escriu aquí el nom del seu company. Si el tipus de patró és bàsic, pot utilitzar l&apos;asterisc (*) com a caràcter comodí.</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="26"/>
        <source>Buddy name:</source>
        <translation>Nom de company:</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="36"/>
        <source>Pattern Type</source>
        <translation>Tipus de patró</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="42"/>
        <source>Basic</source>
        <translation>Bàsic</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="52"/>
        <source>Advanced</source>
        <translation>Avançat</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="62"/>
        <source>&lt;tip&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppRunner</name>
    <message>
        <location filename="../apprunner.cpp" line="55"/>
        <source>Could not read bundle plist. (%1)</source>
        <translation>No s&apos;ha pogut llegir el paquet plist. (%1)</translation>
    </message>
    <message>
        <location filename="../apprunner.cpp" line="83"/>
        <location filename="../apprunner.cpp" line="114"/>
        <source>Starting (working dir %1): %2</source>
        <translation>Començant (directori de treball %1): %2</translation>
    </message>
    <message>
        <location filename="../apprunner.cpp" line="102"/>
        <source>Cannot run file: %1</source>
        <translation>No es pot executar l&apos;arxiu: %1</translation>
    </message>
    <message>
        <source>File: %1
cannot be run</source>
        <translation type="vanished">El fitxer %1
no pot ser executat</translation>
    </message>
</context>
<context>
    <name>AutoUpdater</name>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="91"/>
        <source>Missing main &quot;update&quot; node.</source>
        <translation>Falta node principal &quot;update&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="126"/>
        <source>Missing &quot;install&quot; element.</source>
        <translation>Falta element &quot;install&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="136"/>
        <source>Missing text node for &quot;package&quot; element for &quot;file&quot; element %1</source>
        <translation>Manca node de text per a l&apos;element &quot;package&quot; per l&apos;element &quot;file&quot; %1</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="158"/>
        <source>Missing &quot;packages&quot; element.</source>
        <translation>Falta element &quot;packages&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="172"/>
        <source>More than one &quot;package&quot; element found.</source>
        <translation>Més d&apos;un element &quot;package&quot; trobat.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="174"/>
        <source>Missing &quot;package&quot; element.</source>
        <translation>Falta element &quot;package&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="183"/>
        <source>Failed to find &quot;name&quot; text node.</source>
        <translation>No s&apos;ha trobat el node de text &quot;name&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="122"/>
        <source>Failed to parse updater XML script: %1, l: %2, c: %3</source>
        <translation>No s&apos;ha pogut analitzar l&apos;script actualitzador XML: %1, l: %2, c: %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="132"/>
        <source>Failed to modify package name in updater script: %1</source>
        <translation>No s&apos;ha pogut modificar el nom del paquet en l&apos;script actualitzador: %1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="160"/>
        <source>Detected update for package &quot;%1&quot; from version &quot;%2&quot; to version &quot;%3&quot;.</source>
        <translation>S&apos;ha detectat una actualització per al paquet &quot;%1&quot; de la versió &quot;%2&quot; a &quot;%3&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="195"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="197"/>
        <source>Update was aborted.</source>
        <translation>Actualització avortada.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="199"/>
        <source>Update channel is not configured. Please check your configuration.</source>
        <translation>Canal d&apos;actualització no configurat. Si us plau comprovi la configuració.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="201"/>
        <source>Failed to download updater info file.</source>
        <translation>No s&apos;ha pogut descarregar la informació del actualitzador.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="203"/>
        <source>Cannot parse updater info file.</source>
        <translation>No es pot processar el fitxer amb les dades d&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="205"/>
        <source>Main program node is missing from updater info file.</source>
        <translation>Falta el node principal del programa a l&apos;arxiu de dades d&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="207"/>
        <source>Revision info on one of the packages is missing from the updater info file. Check the log for details.</source>
        <translation>Les dades sobre la revisió d&apos;un dels paquets no existeixen a l&apos;arxiu de dades d&apos;actualització. Comproveu el registre per més detalls.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="210"/>
        <source>Download URL for one of the packages is missing from the updater info file. Check the log for details.</source>
        <translation>L&apos;URL de descàrrega d&apos;un dels paquets no existeix en el fitxer de dades d&apos;actualització. Comproveu el registre per més detalls.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="213"/>
        <source>Download URL for one of the packages is invalid. Check the log for details.</source>
        <translation>L&apos;URL de descàrrega d&apos;un dels paquets és invàlida. Comproveu el registre per més detalls.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="216"/>
        <source>Update package download failed. Check the log for details.</source>
        <translation>Error al descarregar el paquet d&apos;actualització. Comproveu el registre per més detalls.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="218"/>
        <source>Failed to create directory for updates packages storage.</source>
        <translation>No s&apos;ha pogut crear el directori per guardar paquets d&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="220"/>
        <source>Failed to save update package.</source>
        <translation>No s&apos;ha pogut desar el paquet d&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="222"/>
        <source>Failed to save update script.</source>
        <translation>No s&apos;ha pogut desar l&apos;script d&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="224"/>
        <source>Unknown error.</source>
        <translation>Error desconegut.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="268"/>
        <source>Finished downloading package &quot;%1&quot;.</source>
        <translation>Descàrrega completa del paquet &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="274"/>
        <source>Network error when downloading package &quot;%1&quot;: [%2] %3</source>
        <translation>S&apos;ha produït un error de xarxa mentre es descarregava el paquet &quot;%1&quot;: [%2] %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="297"/>
        <source>Finished downloading package script &quot;%1&quot;.</source>
        <translation>S&apos;ha acabat de descarregar l&apos;script del paquet &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="314"/>
        <source>All packages downloaded. Building updater script.</source>
        <translation>Tots els paquets s&apos;han descarregat. Creant l&apos;script d&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="321"/>
        <source>Network error when downloading package script &quot;%1&quot;: [%2] %3</source>
        <translation>S&apos;ha produït un error de xarxa mentre es descarregava el script del paquet &quot;%1&quot;: [%2] %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="350"/>
        <source>Requesting update confirmation.</source>
        <translation>Sol·licitant confirmació d&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="351"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="362"/>
        <source>No new program updates detected.</source>
        <translation>No s&apos;han trobat actualitzacions.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="365"/>
        <source>Some update packages were ignored. To install them select &quot;Check for updates&quot; option from &quot;Help&quot; menu.</source>
        <translation>Alguns paquets d&apos;actualització han estat ignorats. Per instal·lar-los, seleccioneu &quot;Cerca actualitzacions&quot; al menú &quot;Ajuda&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="430"/>
        <source>Failed to create directory for updates storage: %1</source>
        <translation>No s&apos;ha pogut crear el directori per guardar l&apos;actualització: %1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="448"/>
        <source>Update info</source>
        <translation>Informació de l&apos;actualització</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="465"/>
        <source>Invalid download URL for package &quot;%1&quot;: %2</source>
        <translation>L&apos;URL de descàrrega no és vàlida per al paquet &quot;%1&quot;:%2</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="470"/>
        <source>Package: %1</source>
        <translation>Paquet: %1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="471"/>
        <source>Downloading package &quot;%1&quot; from URL: %2.</source>
        <translation>S&apos;està baixant paquet &quot;%1&quot; de la URL: %2.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="486"/>
        <source>Couldn&apos;t save file in path: %1</source>
        <translation>No s&apos;ha pogut desar el fitxer en la ruta:%1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="516"/>
        <source>Invalid download URL for package script &quot;%1&quot;: %2</source>
        <translation>L&apos;URL de descàrrega no és vàlida per al script del paquet &quot;%1&quot;:%2</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="521"/>
        <source>Downloading package script &quot;%1&quot; from URL: %2.</source>
        <translation>S&apos;està baixant script del paquet &quot;%1&quot; de la URL: %2.</translation>
    </message>
</context>
<context>
    <name>BroadcastManager</name>
    <message>
        <location filename="../serverapi/broadcastmanager.cpp" line="50"/>
        <source>%1 LAN server gone: %2, %3:%4</source>
        <translation>Servidor LAN %1 desaparegut: %2, %3:%4</translation>
    </message>
    <message>
        <location filename="../serverapi/broadcastmanager.cpp" line="63"/>
        <source>New %1 LAN server detected: %2:%3</source>
        <translation>Nou servidor LAN %1 detectat: %2:%3</translation>
    </message>
</context>
<context>
    <name>CFGAppearance</name>
    <message>
        <location filename="../gui/configuration/cfgappearance.h" line="45"/>
        <source>Appearance</source>
        <translation>Aparença</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="95"/>
        <source>Use system language</source>
        <translation>Utilitzar llengua del sistema</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="187"/>
        <source>Unknown language definition &quot;%1&quot;</source>
        <translation>Definició de llengua desconeguda &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="213"/>
        <source>Loading translation &quot;%1&quot;</source>
        <translation>Carregant traducció &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="217"/>
        <source>Program needs to be restarted to fully apply the translation</source>
        <translation>El programa necessita reiniciar-se per aplicar completament la traducció</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="34"/>
        <source>Language:</source>
        <translation>Llengua:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="87"/>
        <source>Full retranslation requires a restart.</source>
        <translation>La traducció completa requereix de reiniciar el programa.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="99"/>
        <source>Player slots style:</source>
        <translation>Estil del nombre de jugadors:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="113"/>
        <source>Marines</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="118"/>
        <source>Blocks</source>
        <translation>Blocs</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="123"/>
        <source>Numeric</source>
        <translation>Numèric</translation>
    </message>
    <message>
        <source>Custom servers color:</source>
        <translation type="vanished">Color servidors customitzats:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="148"/>
        <source>LAN servers color:</source>
        <translation>Color servidors LAN:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="165"/>
        <source>Servers with buddies color:</source>
        <translation>Color servidors amb companys:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="229"/>
        <source>Bots are not players</source>
        <translation>Bots no són jugadors</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="208"/>
        <source>Hide passwords</source>
        <translation>Amaga contrasenyes</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="131"/>
        <source>Pinned servers color:</source>
        <translation>Color dels servidors fixats:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="215"/>
        <source>Lookup server hosts</source>
        <translation>Cercar servidors host</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="222"/>
        <source>Colorize server console</source>
        <translation>Pintar consola del servidor</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="236"/>
        <source>Draw grid in server table</source>
        <translation>Dibuixar quadrícula en la taula de servidors</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="243"/>
        <source>Use tray icon</source>
        <translation>Utilitza icona de la safata del sistema</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="252"/>
        <source>When close button is pressed, minimize to tray icon.</source>
        <translation>Quan es pressioni el botó &quot;tancar&quot;, minimitzar a la safata del sistema.</translation>
    </message>
</context>
<context>
    <name>CFGAutoUpdates</name>
    <message>
        <source>Auto Updates</source>
        <translation type="vanished">Actualitzacions automàtiques</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="44"/>
        <source>Disabled</source>
        <translation>Desactivat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="51"/>
        <source>Notify me but don&apos;t install</source>
        <translation>Notifica però no instal·lis actualitzacions</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="61"/>
        <source>Install automatically</source>
        <translation>Instal·la automàticament</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="76"/>
        <source>Update channel:</source>
        <translation>Canal d&apos;actualització:</translation>
    </message>
    <message>
        <source>&lt;p&gt;New IP2C database will be downloaded if locally stored one is missing or has a different checksum than the one stored on Doomseeker&apos;s web page.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Es descarregarà una nova base de dades IP2C si falta una emmagatzemada localment o té una suma de verificació diferent a la emmagatzemada en la pàgina web de Doomseeker.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>IP2C auto update</source>
        <translation type="vanished">Actualitzar automàticament IP2C</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.h" line="44"/>
        <source>Auto updates</source>
        <translation>Actualitzacions automàtiques</translation>
    </message>
</context>
<context>
    <name>CFGCustomServers</name>
    <message>
        <source>Pinned Servers</source>
        <translation type="vanished">Servidors fixats</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="87"/>
        <source>Toggle enabled state</source>
        <translation>Commutar l&apos;estat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="112"/>
        <source>Doomseeker - pinned servers</source>
        <translation>Doomseeker - servidors fixats</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="126"/>
        <source>Port must be within range 1 - 65535</source>
        <translation>El port ha d&apos;estar en el rang 1-65535</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="130"/>
        <source>Unimplemented behavior!</source>
        <translation>Comportament no implementat!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="205"/>
        <source>Host</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="205"/>
        <source>Port</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Adds a new entry to the list. To specify the address and port of the custom server double-click on the cells in the table. Domain names are supported.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Afegeix una nova entrada a la llista. Per especificar l&apos;adreça i el port del servidor fixat, feu doble clic a les cel·les de la taula. Els noms de domini estan suportats.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="46"/>
        <source>&lt;p&gt;Add a new entry to the list. To specify the address and port of the custom server double-click on the cells in the table. Domain names are supported.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Afegir una entrada a la llista. Per a especificar l&apos;adreça i port del servidor, fes doble clic a les cel·les de la taula. També pots usar noms de domini (DNS).&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="49"/>
        <source>Add</source>
        <translation>Afegir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="60"/>
        <source>Remove</source>
        <translation>Suprimir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="78"/>
        <source>Enable selected</source>
        <translation>Habilitar seleccionats</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="81"/>
        <source>Enable</source>
        <translation>Habilitar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="92"/>
        <source>Disable selected</source>
        <translation>Desactivar seleccionats</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="95"/>
        <source>Disable</source>
        <translation>Desactivar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="113"/>
        <source>Change the game on the selected servers.</source>
        <translation>Canvia el joc en els servidors seleccionats.</translation>
    </message>
    <message>
        <source>Changes engine on selected servers.</source>
        <translation type="vanished">Canvia el motor en els servidors seleccionats.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="116"/>
        <source>Set game</source>
        <translation>Establir joc</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.h" line="50"/>
        <source>Pinned servers</source>
        <translation>Servidors fixats</translation>
    </message>
</context>
<context>
    <name>CFGFilePaths</name>
    <message>
        <source>File Paths</source>
        <translation type="vanished">Rutes d&apos;arxiu</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="61"/>
        <source>Path</source>
        <translation>Ruta</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="61"/>
        <source>Recurse</source>
        <translation>Recursiu</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="69"/>
        <source>Doomseeker supports special characters in user-configurable paths. This affects all paths everywhere, not only the file paths configured on this page. The placeholders are:</source>
        <translation>Doomseeker admet caràcters especials en rutes configurades per l&apos;usuari. Això afecta *totes* les rutes, no només les rutes configurades en aquesta pàgina. Els marcadors són:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="75"/>
        <source>&lt;b&gt;~&lt;/b&gt; - if at the beginning of the path, resolved to the home directory of the current user</source>
        <translation>&lt;b&gt;~&lt;/b&gt; - Si està al principi de la ruta, es tradueix al directori principal de l&apos;usuari actual</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="79"/>
        <source>&lt;b&gt;~&lt;i&gt;user&lt;/i&gt;&lt;/b&gt; - if at the beginning of the path, resolved to the home directory of the specified &lt;i&gt;user&lt;/i&gt;</source>
        <translation>&lt;b&gt;~&lt;i&gt;usuari&lt;/i&gt;&lt;/b&gt; - Si està al principi de la ruta, es tradueix al directori principal del &lt;i&gt;usuari&lt;/i&gt; especificat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="83"/>
        <source>&lt;b&gt;$PROGDIR&lt;/b&gt; - resolved to the directory where Doomseeker&apos;s executable is located; this may create invalid paths if used in the middle of the path</source>
        <translation>&lt;b&gt;$PROGDIR&lt;/b&gt; - es tradueix al directori on l&apos;executable de Doomseeker es troba; si s&apos;usa enmig d&apos;una ruta, potser creeu rutes invàlides</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="87"/>
        <source>&lt;b&gt;$&lt;i&gt;NAME&lt;/i&gt;&lt;/b&gt; - any &lt;i&gt;NAME&lt;/i&gt; is resolved to the environment variable of the same &lt;i&gt;NAME&lt;/i&gt;, or to empty if the environment variable is absent</source>
        <translation>&lt;b&gt;$&lt;i&gt;NOM&lt;/i&gt;&lt;/b&gt; - qualsevol altre &lt;i&gt;NOM&lt;/i&gt; es tradueix a la variable d&apos;entorn amb el mateix &lt;i&gt;NOM&lt;/i&gt;, o es deixa buida si no existeix</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="93"/>
        <source>This option controls whether the &lt;b&gt;$PROGDIR&lt;/b&gt; and &lt;b&gt;$&lt;i&gt;NAME&lt;/i&gt;&lt;/b&gt; will be resolved or ignored. If this option is &lt;b&gt;checked&lt;/b&gt;, both are resolved as explained. If this option is &lt;b&gt;unchecked&lt;/b&gt;, both will be taken literally. The &lt;b&gt;~&lt;/b&gt; placeholders are always resolved regardless.</source>
        <translation>Aquesta opció controla si els marcadors &lt;b&gt;$PROGDIR&lt;/b&gt; i &lt;b&gt;$&lt;i&gt;NOM&lt;/i&gt;&lt;/b&gt; es resolen. Si està &lt;b&gt;marcat&lt;/b&gt;, es resoldran com s&apos;ha explicat. Si està &lt;b&gt;desmarcat&lt;/b&gt;, es prendran literalment. El marcador &lt;b&gt;~&lt;/b&gt; es resol sempre.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="261"/>
        <source>Path empty.</source>
        <translation>Ruta buida.</translation>
    </message>
    <message>
        <source>Doomseeker - Add wad path</source>
        <translation type="vanished">Doomseeker - Afegir ruta a WAD</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="151"/>
        <source>Doomseeker - Add game mod path</source>
        <translation>Doomseeker - Afegir ruta de modificacions del joc</translation>
    </message>
    <message>
        <source>No path specified.</source>
        <translation type="vanished">Ruta no especificada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="265"/>
        <source>Path doesn&apos;t exist.</source>
        <translation>La ruta no existeix.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="268"/>
        <source>Path is not a directory.</source>
        <translation>La ruta no és un directori.</translation>
    </message>
    <message>
        <source>IWAD and PWAD paths:</source>
        <translation type="vanished">Rutes cap IWADs i PWADs:</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;WARNING: &lt;/span&gt;It&apos;s highly discouraged to enable recursion for directories with lots of subdirectories. You may experience heavy performance loss and high hard drive usage if recursion is used recklessly. When recursion is enabled, file search operations will go the entire way down to the bottom of the directory tree if necessary.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot;font-weight: 600;&quot;&gt;NOTA: &lt;/span&gt;No es recomana habilitar la recursivitat per directoris amb molts subdirectoris. Pot experimentar una pèrdua severa de rendiment i un desgast excessiu del disc dur si la recursió s&apos;usa imprudentment. Quan la recursivitat està habilitada, l&apos;operació de recerca d&apos;arxius anirà fins al fons de l&apos;arbre de directoris, si cal.&lt;/P&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;WARNING: &lt;/span&gt;It&apos;s not recommended to enable recursion for directories with lots of subdirectories. With such directories you may experience heavy performance loss and high hard drive usage. When recursion is enabled, file search operations will go the entire way down to the bottom of the directory tree if necessary.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot;font-weight: 600;&quot;&gt;NOTA: &lt;/span&gt;No es recomana habilitar la recursivitat per directoris amb molts subdirectoris. Pot experimentar una pèrdua severa de rendiment i un ús excessiu del disc. Quan la recursivitat està habilitada, l&apos;operació de recerca d&apos;arxius anirà fins al fons de l&apos;arbre de directoris, si cal.&lt;/P&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="29"/>
        <source>Where are the game mods, IWADs, PWADs:</source>
        <translation>On es troben les modificacions, IWADs i PWADs:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="64"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;WARNING: &lt;/span&gt;It&apos;s not recommended to enable recursion for directories with lots of subdirectories. With such directories you may experience heavy performance loss and high hard drive usage. When recursion is enabled, the file search will go the entire way down to the bottom of the directory tree if necessary.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot;font-weight: 600;&quot;&gt;NOTA: &lt;/span&gt;No es recomana habilitar la recursivitat per directoris amb molts subdirectoris. Pot experimentar una pèrdua severa de rendiment i un ús excessiu del disc. Quan la recursivitat està habilitada, l&apos;operació de recerca d&apos;arxius anirà fins al fons de l&apos;arbre de directoris, si cal.&lt;/P&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="92"/>
        <source>Browse for a file path.</source>
        <translation>Buscar una ruta de fitxer.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="95"/>
        <source>Browse</source>
        <translation>Cercar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="102"/>
        <source>Add an empty path to the list.</source>
        <translation>Afegir una ruta buida a la llista.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="105"/>
        <source>Add empty</source>
        <translation>Afegir vuit</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="116"/>
        <source>Remove the selected paths from the list.</source>
        <translation>Eliminar las rutes seleccionades de la llista.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="119"/>
        <source>Remove</source>
        <translation>Suprimir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="132"/>
        <source>&lt;p&gt;If selected Doomseeker will attempt to find the files used on a particular server when the mouse cursor is hovered over the WADs and IWAD columns. This requires a hard drive access to search in all the directories specified above each time a new tooltip is generated. If it takes too long to create such tooltip, you may try disabling this option.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si se selecciona, Doomseeker intentarà buscar els arxius que usa aquest servidor quan el teu cursor estigui a sobre de la columna de WADs i IWAD. Això requereix buscar en els directoris que has especificat cada cop que es genera una finestra. Si això pren molt temps, prova de desactivar aquesta opció.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="142"/>
        <source>&lt;p&gt;If selected Doomseeker will attempt to check the integrity of the local WADs, compared to the ones used on a particular server. This requires a hard drive access to read the entirety of the WADs. It also requires the checksums to be downloaded from the servers, causing more network traffic. If it takes too long to join servers, or there are problems with the server refresh, you may want to disable this feature.&lt;/p&gt;
&lt;p&gt;This setting is used when joining a server and when clicking the &amp;quot;Find missing WADs&amp;quot; button in the context menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si se selecciona Doomseeker intentarà comprovar la integritat dels WADs locals, en comparació amb els utilitzats en un servidor concret. Això requereix un accés al disc dur per llegir la totalitat dels WADs. També requereix que les sumes de control es descarreguin dels servidors, provocant més trànsit de xarxa. Si es triga massa a unir-se als servidors o hi ha problemes amb l&apos;actualització del servidor, és possible que vulgueu desactivar aquesta característica.&lt;/p&gt;
&lt;p&gt;Aquesta configuració s&apos;utilitza quan s&apos;uneix a un servidor i quan es fa clic al botó &amp;quot;Troba els WADs que falten&amp;quot; al menú contextual.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;If selected Doomseeker will attempt to check the integrity of the local WADs, compared to the ones used on a particular server. This requires a hard drive access to read the entirety of the WADs. If it takes too long to join servers, you may want to disable this feature.&lt;/p&gt;
&lt;p&gt;This setting is used when joining a server and when clicking the &amp;quot;Find missing WADs&amp;quot; button in the context menu.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Si se selecciona, Doomseeker intentarà comprovar la integritat dels WADs locals, en comparació amb les utilitzades en un servidor concret. Això requereix accés al disc dur per llegir la totalitat dels WADs. Si això requereix massa temps per a connectar-se als servidors, és possible que vulgueu desactivar aquesta funció.&lt;/p&gt;
&lt;p&gt;Aquest paràmetre s’utilitza quan s’uneix a un servidor i quan feu clic al botó &amp;quot;Troba els WADs faltants&amp;quot; al menú contextual del ratolí.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="153"/>
        <source>Resolve $PLACEHOLDERS in paths</source>
        <translation>Evaluar $MARCADORS en les rutes</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;If selected Doomseeker will attempt to find files used on a particular server if mouse cursor is hovered over the WADS column. This requires hard drive access and search in all directories specified above each time a new tooltip is generated. If it takes too long to create such tooltip you may try disabling this option.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This also applies to IWAD column.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Si es selecciona Doomseeker intentarà trobar els arxius utilitzats en un servidor particular si el cursor està a la columna WADs d&apos;aquest. Això requereix d&apos;accés al disc dur i buscar en tots els directoris especificats cada vegada que es genera la llista. Si porta massa temps crear aquesta llista, pot intentar desactivar aquesta opció.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Això també s&apos;aplica a la columna IWAD.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="135"/>
        <source>Tell me where are my WADs located</source>
        <translation>Digues-me on es troben els meus WADs</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Regular&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;If selected Doomseeker will attempt to check the integrity of the local WADs, compared to the ones used on a particular server. This requires hard drive access to read the entirety of the WADs. If it takes too long to join servers you may want to disable this feature.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;This setting is used when joining a server and when clicking the &amp;quot;Find missing WADs&amp;quot; button on the right-click menu.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>He de determinar quin serà el nom de &quot;Trobar WADs faltants&quot;</translatorcomment>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Regular&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;Si se selecciona, Doomseeker intentarà comprovar la integritat dels WADs locals, en comparació amb les utilitzades en un servidor concret. Això requereix accés al disc dur per llegir la totalitat dels WADs. Si això requereix massa temps per a connectar-se als servidors, és possible que vulgueu desactivar aquesta funció.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;Aquest paràmetre s’utilitza quan s’uneix a un servidor i quan feu clic al botó &amp;quot;Troba els WADs faltants&amp;quot; al menú contextual del ratolí.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="146"/>
        <source>Check the integrity of local WADs</source>
        <translation>Comprovar la integritat dels WADs locals</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.h" line="49"/>
        <source>File paths</source>
        <translation>Rutes d&apos;arxiu</translation>
    </message>
</context>
<context>
    <name>CFGGames</name>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="29"/>
        <source>Player name:</source>
        <translation>Nom del jugador:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="36"/>
        <source>Use Operating System&apos;s username</source>
        <translation>Utilitzar el nom d&apos;usuari del sistema operatiu</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="46"/>
        <source>Custom:</source>
        <translation>Customitzat:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="56"/>
        <source>Player</source>
        <translation>Jugador</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="63"/>
        <source>This is currently used only for recording demos. It&apos;s attached to each demo you record.</source>
        <translation>Actualment només s&apos;utilitza per gravar demos. S&apos;adjunta a cada demo que graveu.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.h" line="43"/>
        <source>Games</source>
        <translation>Jocs</translation>
    </message>
</context>
<context>
    <name>CFGIP2C</name>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="29"/>
        <source>&lt;p&gt;A new IP2C database will be downloaded if the local one is missing or has a different checksum than the one stored on Doomseeker&apos;s web page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Es descarregarà una nova base de dades IP2C si falta una emmagatzemada localment o té una suma de verificació diferent a la emmagatzemada en la pàgina web de Doomseeker.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="32"/>
        <source>IP2C auto update</source>
        <translation>Actualitzar automàticament IP2C</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Servers of some games are able to directly tell Doomseeker in which country they are located in. If this is checked, Doomseeker will honor this information. If unchecked, Doomseeker will always try to detect the country by server&apos;s IP. &lt;/p&gt;&lt;p&gt;Neither of these methods may be accurate - the server can tell false info and the IP2C database may contain inaccuracies. Either method can lead to the wrong country being displayed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Alguns servidors poden dir a Doomseeker en quin país es troben directament. Si aquesta opció està activada, Doomseeker usarà la informació que aquest li proporciona. Si no, Doomseeker intentarà detectar-ho segons la IP del servidor.&lt;/p&gt;&lt;p&gt;Ambdues opcions poden tenir errors: el servidor pot donar informació falsa, i la base de dades IP2C pot tenir entrades sense actualitzar. Les dues opcions poden acabar amb un país erroni&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="42"/>
        <source>Honor countries set by servers</source>
        <translation>Usar el país definit pel servidor</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.h" line="44"/>
        <source>IP2C</source>
        <translation>IP2C</translation>
    </message>
</context>
<context>
    <name>CFGIRCAppearance</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.h" line="44"/>
        <source>Appearance</source>
        <translation>Aparença</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.h" line="49"/>
        <source>IRC - Appearance</source>
        <translation>IRC - Aparença</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="37"/>
        <source>Default text color:</source>
        <translation>Color de text predefinit:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="54"/>
        <source>Channel action color:</source>
        <translation>Color d&apos;acció al canal:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="71"/>
        <source>Network action color:</source>
        <translation>Color d&apos;acció a la xarxa:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="88"/>
        <source>CTCP color:</source>
        <translation>Color CTCP:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="105"/>
        <source>Error color:</source>
        <translation>Color errors:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="125"/>
        <source>Background color:</source>
        <translation>Color de fons:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="142"/>
        <source>URL color:</source>
        <translation>Color de links:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="159"/>
        <source>Main font:</source>
        <translation>Font principal:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="185"/>
        <source>User list</source>
        <translation>Llista d&apos;usuaris</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="194"/>
        <source>Font:</source>
        <translation>Font:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="211"/>
        <source>Selected text color:</source>
        <translation>Color de text seleccionat:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="228"/>
        <source>Selected background:</source>
        <translation>Fons seleccionat:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="248"/>
        <source>Prepends all entries with [hh:mm:ss] timestamps.</source>
        <translation>Precedeix totes les entrades amb marques de temps [hh:mm:ss].</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="251"/>
        <source>Enable timestamps</source>
        <translation>Activa marques de temps</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="258"/>
        <source>Window/task bar alert on important event (private msg, callsign)</source>
        <translation>Alertar a la finestra/barra de tasques quan succeeixin esdeveniments importants (missatge privat, trucada)</translation>
    </message>
</context>
<context>
    <name>CFGIRCDefineNetworkDialog</name>
    <message>
        <source>Following commands have violated the IRC maximum byte number limit (%1):

</source>
        <translation type="vanished">Les següents comandes han sobrepassat el límit IRC de màxim bytes (%1):

</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="82"/>
        <source>These commands have violated the IRC maximum byte number limit (%1):

</source>
        <translation>Aquestes comandes han sobrepassat el límit IRC de màxim bytes (%1):

</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="84"/>
        <source>

If saved, the script may not run properly.

Do you wish to save the script anyway?</source>
        <translation>

Si es guarda, l&apos;script pot no executar-se correctament.

Voleu desar el script de totes maneres?</translation>
    </message>
    <message numerus="yes">
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="89"/>
        <source>

... and %n more ...</source>
        <translation>
            <numerusform>

... i %n més ...</numerusform>
            <numerusform>

... i %n més ...</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="92"/>
        <source>Doomseeker - IRC Commands Problem</source>
        <translation>Doomseeker - Problema Ordres IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="117"/>
        <source>	%1 (...)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="219"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="225"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="234"/>
        <source>Invalid IRC network description</source>
        <translation>Descripció de la xarxa IRC no vàlida</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="220"/>
        <source>Network description cannot be empty.</source>
        <translation>La descripció de xarxa no pot estar buida.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="226"/>
        <source>There already is a network with such description.</source>
        <translation>Ja hi ha una xarxa amb tal descripció.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="231"/>
        <source>Network description is invalid.

Only letters, digits, spaces and &quot;%1&quot; are allowed.</source>
        <translation>La descripció de la xarxa no és vàlida.

Només s&apos;admeten lletres, dígits, espais i &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="14"/>
        <source>Define IRC Network</source>
        <translation>Definir la xarxa IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="30"/>
        <source>Description:</source>
        <translation>Descripció:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="40"/>
        <source>Address:</source>
        <translation>Direcció:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="50"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="93"/>
        <source>Server password (usually this should remain empty):</source>
        <translation>Contrasenya del servidor (generalment això hauria de romandre buida):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="24"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="109"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="157"/>
        <source>Hide</source>
        <translation>Amagar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="141"/>
        <source>Nickserv password:</source>
        <translation>Contrasenya Nickserv:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="169"/>
        <source>Nickserv command:</source>
        <translation>Comanda Nickserv:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="179"/>
        <source>/privmsg nickserv identify %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="199"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;%1 is substituted with nickserv password.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Usually nickserv command shouldn&apos;t be changed.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;%1 se substitueix per la contrasenya Nickserv.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;En general, la comanda Nickserv no ha de modificar-se.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="135"/>
        <source>Nickserv</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="221"/>
        <source>Specify channels that you wish to join by default when connecting to this network:</source>
        <translation>Especifiqueu els canals als que desitja unir-se per defecte quan es connecti a aquesta xarxa:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="234"/>
        <source>SPACEBAR or ENTER separated</source>
        <translation>BARRA D&apos;ESPAI o ENTER separats</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="215"/>
        <source>Channels</source>
        <translation>Canals</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="251"/>
        <source>Execute following commands on network join:</source>
        <translation>Executeu les ordres següents en unir-se a una xarxa:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="274"/>
        <source>Put each command on a separate line.</source>
        <translation>Posa cada comanda en una línia separada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="281"/>
        <source>Max. 512 characters per command.</source>
        <translation>Max. 512 caràcters per comandament.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="245"/>
        <source>Join Script</source>
        <translation>Script en unir-se</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="292"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Warning:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; passwords are stored as plain text in configuration file.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Alerta:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; les contrasenyes s&apos;emmagatzemen en el fitxer de configuració com a text pla.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CFGIRCNetworks</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.h" line="49"/>
        <source>Networks</source>
        <translation>Xarxes</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.h" line="55"/>
        <source>IRC - Networks</source>
        <translation>IRC - Xarxes</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.cpp" line="189"/>
        <source>Description</source>
        <translation>Descripció</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.cpp" line="189"/>
        <source>Address</source>
        <translation>Direcció</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="64"/>
        <source>Add</source>
        <translation>Afegir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="75"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="86"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="99"/>
        <source>Channel/network quit message:</source>
        <translation>Missatge en sortir de canal/xarxa:</translation>
    </message>
</context>
<context>
    <name>CFGIRCSounds</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.h" line="46"/>
        <source>Sounds</source>
        <translation>Sons</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.h" line="51"/>
        <source>IRC - Sounds</source>
        <translation>IRC - Sons</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="62"/>
        <source>Pick Sound File</source>
        <translation>Seleccionar arxiu de so</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="64"/>
        <source>WAVE (*.wav)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="137"/>
        <source>No path specified.</source>
        <translation>Cap ruta especificada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="141"/>
        <source>File doesn&apos;t exist.</source>
        <translation>El fitxer no existeix.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="144"/>
        <source>This is not a file.</source>
        <translation>Això no és un arxiu.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="31"/>
        <source>Nickname used:</source>
        <translation>Alias usat:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="38"/>
        <source>Private message:</source>
        <translation>Missatge privat:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="57"/>
        <source>Sound played when your nickname is used in a channel.</source>
        <translation>So reproduït quan el seu àlies s&apos;usa en un canal.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="71"/>
        <source>Browse the sound played when your nickname is used in a channel.</source>
        <translation>Cerca el so a reproduir quan siguis mencionat pel teu àlies a un canal.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="100"/>
        <source>Sound played when a private message is received.</source>
        <translation>So reproduït quan es rep un missatge privat.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="114"/>
        <source>Browse the sound played when a private message is received.</source>
        <translation>Cerca el so a reproduir quan es rep un missatge privat.</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Sound played when private message is received.</source>
        <translation type="vanished">So reproduït quan es rep un missatge privat.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="139"/>
        <source>&lt;b&gt;Note:&lt;/b&gt; Use sounds in .WAV format.</source>
        <translation>&lt;b&gt;Nota:&lt;/b&gt; fes servir sons en format .WAV.</translation>
    </message>
</context>
<context>
    <name>CFGQuery</name>
    <message>
        <location filename="../gui/configuration/cfgquery.h" line="45"/>
        <source>Query</source>
        <translation>Consulta</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="20"/>
        <source>Refresh servers on startup</source>
        <translation>Actualitzar servidors en arrencar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="27"/>
        <source>Refresh before launch</source>
        <translation>Actualitzar abans de llançar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="37"/>
        <source>Servers autorefresh</source>
        <translation>Autorefresc de servidors</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="48"/>
        <location filename="../gui/configuration/cfgquery.ui" line="58"/>
        <location filename="../gui/configuration/cfgquery.ui" line="71"/>
        <source>Minimum value: 30 seconds.</source>
        <translation>Valor mínim: 30 segons.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="51"/>
        <source>Obtain new server list every:</source>
        <translation>Obtenir una nova llista de servidors cada:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="74"/>
        <source>seconds</source>
        <translation>segons</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="96"/>
        <source>Enabling this will prevent server list from disappearing when you are browsing through it.</source>
        <translation>Habilitar això evitarà que la llista de servidors desaparegui mentre estigui navegant a través d&apos;ella.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="99"/>
        <source>Don&apos;t refresh if Doomseeker window is active.</source>
        <translation>No actualitzi si la finestra de Doomseeker està activa.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="109"/>
        <source>Refresh speed</source>
        <translation>Velocitat de refresc</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="117"/>
        <location filename="../gui/configuration/cfgquery.ui" line="127"/>
        <source>How many times Doomseeker will attempt to query each server before deeming it to be not responding.</source>
        <translation>Quantes vegades Doomseeker intentarà consultar cada servidor abans de determinar que no respon.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="120"/>
        <source>Number of attempts per server:</source>
        <translation>Nombre d&apos;intents per servidor:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="149"/>
        <location filename="../gui/configuration/cfgquery.ui" line="159"/>
        <source>Delay in miliseconds between each query attempt.</source>
        <translation>Retard en mil·lisegons entre cada intent de consulta.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="152"/>
        <source>Delay between attemps (ms):</source>
        <translation>Retard entre intents (ms):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="181"/>
        <source>Interval between different servers (ms):</source>
        <translation>Interval entre diferents servidors (ms):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="188"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sleep delay that occurs after querying a server and before querying a next one. Increasing this value may negatively affect the speed of server list refresh, but will free up CPU and decrease bandwidth usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Retard que passa després de consultar un servidor i abans de consultar el següent. Augmentar aquest valor pot afectar negativament la velocitat d&apos;actualització de la llista de servidors, però consumirà menys CPU i reduirà l&apos;ús d&apos;ample de banda.&lt;/P&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="211"/>
        <source>Cautious</source>
        <translation>Cautelós</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="218"/>
        <source>Moderate</source>
        <translation>Moderat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="225"/>
        <source>Aggressive</source>
        <translation>Agressiu</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="232"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Be warned: in this mode correctly working servers may appear as &amp;quot;not responding&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Aneu amb compte: en aquesta manera, els servidors que funcionen correctament poden mostrar-se com &amp;quot;no respon&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="235"/>
        <source>Very aggressive</source>
        <translation>Molt agressiu</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="244"/>
        <source>These buttons change refresh speed settings to different values. Speed settings determine how long will it take to populate the server list and how accurate the refresh will be. Higher speed can mean lesser accuracy.

If you&apos;re experiencing problems with refreshing servers, you might prefer &quot;Cautious&quot; preset, while &quot;Aggressive&quot; presets should be preferred with good connection quality.

These values can be also modified by hand.</source>
        <translation>Aquests botons canvien la configuració de velocitat de refresc a diferents valors. La configuració de velocitat determina quant de temps durà omplir la llista de servidors i què tan precisa serà l&apos;actualització. Una velocitat més alta pot significar una menor precisió.

Si té problemes amb el refresc de servidors, és possible que prefereixi la configuració predefinida &quot;Cautelós&quot;, mentre que els valors predefinits &quot;agressius&quot; es recomanen per a connexions de bona qualitat.

Aquests valors també es poden modificar a mà.</translation>
    </message>
</context>
<context>
    <name>CFGServerPasswords</name>
    <message>
        <source>Server Passwords</source>
        <translation type="vanished">Contrasenyes de servidors</translation>
    </message>
    <message>
        <source>Reveal</source>
        <translation type="vanished">Mostrar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="31"/>
        <source>Hide</source>
        <translation>Amagar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="48"/>
        <source>Add password</source>
        <translation>Afegir contrasenya</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="92"/>
        <source>Password</source>
        <translation>Contrasenya</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="97"/>
        <source>Last Game</source>
        <translation>Últim joc</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="102"/>
        <source>Last Server</source>
        <translation>Últim servidor</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="107"/>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="194"/>
        <source>Last Time</source>
        <translation>Últim cop</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="115"/>
        <source>Remove selected passwords</source>
        <translation>Suprimir contrasenyes seleccionades</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="141"/>
        <source>Servers the selected password was used on:</source>
        <translation>Servidors on la contrasenya seleccionada es va fer servir:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="179"/>
        <source>Game</source>
        <translation>Joc</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="184"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="189"/>
        <source>Address</source>
        <translation>Direcció</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="202"/>
        <source>Remove selected servers</source>
        <translation>Suprimir servidors seleccionats</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Afegir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="228"/>
        <source>Max number of servers to remember per password:</source>
        <translation>Nombre màxim de servidors a recordar per contrasenya:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="247"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Warning:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Some servers may be lost.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Advertència:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Alguns servidors poden perdre&apos;s.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.h" line="48"/>
        <source>Server passwords</source>
        <translation>Contrasenyes de servidors</translation>
    </message>
</context>
<context>
    <name>CFGWadAlias</name>
    <message>
        <source>WAD Aliases</source>
        <translation type="vanished">Àlies de WADs</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="66"/>
        <source>Left-to-Right will use the alias files instead of the main file but not vice-versa.</source>
        <translation>Esquerra-a-Dreta utilitzarà els àlies d&apos;arxius en lloc de l&apos;arxiu principal, però no a l&apos;inrevés.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="69"/>
        <source>All Equal will treat all files as equal and try to match them in any combination.</source>
        <translation>Tot Equivalent tractarà a tots els arxius com iguals i intentarà combinar-los en qualsevol combinació.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="186"/>
        <source>Left-to-Right</source>
        <translation>Esquerra-a-Dreta</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="187"/>
        <source>All Equal</source>
        <translation>Tot Equivalent</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="32"/>
        <source>Aliases listed here will be used if WADs are not found.</source>
        <translation>Els àlies mostrats aquí es faran servir si no es troben els WADs.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="55"/>
        <source>WAD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="60"/>
        <source>Aliases</source>
        <translation>Àlies</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="65"/>
        <source>Match</source>
        <translation>Coincidència</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="99"/>
        <source>Add</source>
        <translation>Afegir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="75"/>
        <source>Add defaults</source>
        <translation>Afegir predefinits</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="110"/>
        <source>Remove</source>
        <translation>Suprimir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="123"/>
        <source>Multiple aliases can be separated with semicolon &apos;;&apos;</source>
        <translation>Es poden separar múltiples àlies amb un punt i coma &apos;;&apos;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.h" line="44"/>
        <source>WAD aliases</source>
        <translation>Àlies de WADs</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerAppearance</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.h" line="44"/>
        <source>Appearance</source>
        <translation>Aparença</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.h" line="49"/>
        <source>Wadseeker - Appearance</source>
        <translation>Wadseeker - Aparença</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="31"/>
        <source>Message colors:</source>
        <translation>Color missatges:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="51"/>
        <source>Notice:</source>
        <translation>Notificació:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="61"/>
        <source>Error:</source>
        <translation>Error:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="71"/>
        <source>Critical error:</source>
        <translation>Error crític:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="81"/>
        <source>Navigation:</source>
        <translation>Navegació:</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.h" line="44"/>
        <source>Wadseeker</source>
        <translation>Wadseeker</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.h" line="48"/>
        <source>Wadseeker - General</source>
        <translation>Wadseeker - General</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="131"/>
        <source>No path specified.</source>
        <translation>Cap ruta especificada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="134"/>
        <source>This path doesn&apos;t exist.</source>
        <translation>Aquesta ruta no existeix.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="137"/>
        <source>This is not a directory.</source>
        <translation>Això no és un directori.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="140"/>
        <source>This directory cannot be written to.</source>
        <translation>No es pot escriure en aquest directori.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="29"/>
        <source>Directory where Wadseeker will put WADs into:</source>
        <translation>Directori on Wadseeker col·locarà els WADs:</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Time after which Wadseeker stops connecting if no response is received.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(min: 20 seconds, max: 360 seconds)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Temps després del qual Wadseeker deixa de connectar-se si no es rep resposta.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(Min: 20 segons, max: 360 segons)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Connect timeout (seconds):</source>
        <translation type="vanished">Temps d&apos;espera de connexió (segons):</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Time after which Wadseeker stops downloading if data stops coming.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(min: 60 seconds, max: 360 seconds)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Temps després del qual Wadseeker deixa de descarregar si no es rep informació.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(Min: 60 segons, max: 360 segons)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Download timeout (seconds):</source>
        <translation type="vanished">Temps d&apos;espera de descàrrega (segons):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="48"/>
        <source>Max concurrent site seeks:</source>
        <translation>Màxim nombre de recerques simultànies en pàgines:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="83"/>
        <source>Max concurrent WAD downloads:</source>
        <translation>Màxim nombre de descàrregues simultànies:</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerIdgames</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.h" line="44"/>
        <source>Archives</source>
        <translation>Arxius</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.h" line="49"/>
        <source>Wadseeker - Archives</source>
        <translation>Wadseeker - Arxius</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="29"/>
        <source>Idgames</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="35"/>
        <source>Use the /idgames Archive</source>
        <translation>Utilitza Arxiu /idgames</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="63"/>
        <source>The /idgames Archive URL (changing this is NOT recommended):</source>
        <translation>URL de l&apos;Arxiu /idgames (NO es recomana canviar això):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="75"/>
        <source>Default</source>
        <translation>Predefinit</translation>
    </message>
    <message>
        <source>Use Wad Archive</source>
        <translation type="vanished">Utilitzar Wad Archive</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerSites</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.h" line="46"/>
        <source>Sites</source>
        <translation>Llocs</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.h" line="51"/>
        <source>Wadseeker - Sites</source>
        <translation>Wadseeker - Llocs</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="104"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wadseeker library comes with its own list of file storage sites. As these sites come and go the list is modified with new updates of the library. Leave this option to be always up-to-date with the current list. &lt;/p&gt;&lt;p&gt;When enabled the list of hardcoded default sites will always be used in addition to the list above.&lt;/p&gt;&lt;p&gt;When disabled only the list above is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;La biblioteca Wadseeker ve amb la seva pròpia llista de llocs d&apos;emmagatzematge d&apos;arxius. A mesura que aquests llocs van i vénen, la llista es modifica amb noves actualitzacions de la biblioteca. Deixeu aquest element activat per estar sempre actualitzat amb la llista actual. &lt;/p&gt;&lt;p&gt;Quan s&apos;habiliti, la llista de llocs predeterminats sempre es farà servir a més de la llista anterior.&lt;/p&gt;&lt;p&gt;Quan està desactivat, només s&apos;utilitza la llista anterior.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="107"/>
        <source>Always use Wadseeker&apos;s default sites</source>
        <translation>Sempre utilitzeu els llocs predeterminats d&apos;Wadseeker</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="53"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adds sites hardcoded within the Wadseeker library to the list. No URL that is currently on the list will be removed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Afegeix llocs predeterminats a la biblioteca de Wadseeker a la llista. No s&apos;eliminarà cap URL que estigui actualment a la llista.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="56"/>
        <source>Add defaults</source>
        <translation>Afegir predefinits</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="80"/>
        <source>Add</source>
        <translation>Afegir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="91"/>
        <source>Remove</source>
        <translation>Suprimir</translation>
    </message>
</context>
<context>
    <name>CfgChatLogsPage</name>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.h" line="46"/>
        <source>Logging</source>
        <translation>Registre</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.h" line="51"/>
        <source>IRC - Logging</source>
        <translation>IRC - Registre</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="61"/>
        <source>Browse chat logs storage directory</source>
        <translation>Examinar el directori d&apos;emmagatzematge de registres de xat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="74"/>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="81"/>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="87"/>
        <source>Directory error</source>
        <translation>Error de directori</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="74"/>
        <source>Directory not specified.</source>
        <translation>Directori no especificat.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="81"/>
        <source>Directory doesn&apos;t exist.</source>
        <translation>El directori no existeix.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="136"/>
        <source>The specified path isn&apos;t a directory.</source>
        <translation>La ruta especificada no és un directori.</translation>
    </message>
    <message>
        <source>Specified path isn&apos;t a directory.</source>
        <translation type="vanished">La ruta especificada no és un directori.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="32"/>
        <source>Store chat logs</source>
        <translation>Emmagatzemar registres de xat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="39"/>
        <source>Restore logs when re-entering chat</source>
        <translation>Restaurar registres quan es torna a ingressar a l&apos;xat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="59"/>
        <source>Remove old log archives</source>
        <translation>Suprimir arxius de registre antics</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="68"/>
        <source>Remove all older than:</source>
        <translation>Suprimir tot anterior a:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="81"/>
        <source> days</source>
        <translation> dies</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="126"/>
        <source>Logs storage directory:</source>
        <translation>Directori d&apos;emmagatzematge de registres:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="151"/>
        <source>Browse</source>
        <translation>Cercar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="166"/>
        <source>Explore</source>
        <translation>Explorar</translation>
    </message>
</context>
<context>
    <name>ChatLogs</name>
    <message>
        <source>Won&apos;t transfer chat logs from &quot;%1&quot; to &quot;%2&quot; as directory &quot;%2&quot;already exists.</source>
        <translation type="vanished">No transferirà els registres de xat de &quot;%1&quot; a &quot;%2&quot; ja que el directori &quot;%2&quot; ja existeix.</translation>
    </message>
    <message>
        <location filename="../irc/chatlogs.cpp" line="90"/>
        <source>Won&apos;t transfer chat logs from &quot;%1&quot; to &quot;%2&quot; as directory &quot;%2&quot; already exists.</source>
        <translation>No es transferirà els registres de xat de &quot;%1&quot; a &quot;%2&quot; ja que el directori &quot;%2&quot; ja existeix.</translation>
    </message>
    <message>
        <location filename="../irc/chatlogs.cpp" line="94"/>
        <source>Failed to transfer chat from &quot;%1&quot; to &quot;%2&quot;</source>
        <translation>Error en transferir al xat de &quot;%1&quot; a &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../irc/chatlogs.cpp" line="98"/>
        <source>Chat logs transfer</source>
        <translation>Transferència de registres de xat</translation>
    </message>
</context>
<context>
    <name>CheckWadsDlg</name>
    <message>
        <source>Doomseeker - Checking wads</source>
        <translation type="vanished">Doomseeker - Comprovant WADs</translation>
    </message>
    <message>
        <source>Checking the location and integrity of your wads</source>
        <translation type="vanished">Comprovant la ubicació i la integritat dels WADs</translation>
    </message>
    <message>
        <location filename="../gui/checkwadsdlg.ui" line="17"/>
        <source>Doomseeker - Checking WADs</source>
        <translation>Doomseeker - Comprovant WADs</translation>
    </message>
    <message>
        <location filename="../gui/checkwadsdlg.ui" line="32"/>
        <source>Checking the location and integrity of your WADs</source>
        <translation>Comprovant la ubicació i la integritat dels WADs</translation>
    </message>
</context>
<context>
    <name>CmdArgsHelp</name>
    <message>
        <location filename="../cmdargshelp.cpp" line="40"/>
        <source>--connect &lt;protocol://ip[:port]&gt;
    Attempts to connect to the specified server.
</source>
        <translation>--connect &lt;protocol://ip[:port]&gt;
    Intenta connectar-se al servidor especificat.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="43"/>
        <source>--create-game
    Launches Doomseeker in &quot;Create Game&quot; mode.
</source>
        <translation>--create-game
    Arrenca Doomseeker en mode &quot;Crea Joc&quot;.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="46"/>
        <source>--datadir &lt;directory&gt;
    Sets an explicit search location for
    IP2C data along with plugins.
    Can be specified multiple times.
</source>
        <translation>--datadir &lt;directory&gt;
    Estableix una ubicació de búsqueda explícita
    per a dades IP2C juntament amb complements.
    Es pot especificar diverses vegades.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="62"/>
        <source>--help
    Prints this list of command line arguments.
</source>
        <translation>--help
    mostra aquesta llista d&apos;arguments de línia de
    comandes.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="65"/>
        <source>--rcon [plugin] [ip]
    Launch the rcon client for the specified ip.
</source>
        <translation>--rcon [plugin] [ip]
    Inicia el client rcon per a la ip especificada.
</translation>
    </message>
    <message>
        <source>--portable
    Starts application in portable mode.
    In portable mode Doomseeker saves all configuration files
    to the directory where its executable resides.
    Normally, configuration is saved to user&apos;s home directory.
</source>
        <translation type="vanished">--portable
    Inicia l&apos;aplicació en mode portàtil.
    En mode portàtil, Doomseeker guarda tots els arxius de
    configuració en el directori on resideix l&apos;executable.
    Normalment, la configuració es guarda en el directori
    d&apos;inici de l&apos;usuari.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="35"/>
        <source>--basedir &lt;directory&gt;
    Changes the default storage directory to the specified
    one (also in --portable mode). Doomseeker stores all of its
    settings, cache and managed files in this directory.
</source>
        <translation>--basedir &lt;directory&gt;
     Canvia la ruta per defecte d&apos;emmagatzematge a l&apos;especificada
     (fins i tot en mode --portable). Doomseeker guarda tota la
     configuració, memòria cau, i fitxers gestionats en aquest directori.
</translation>
    </message>
    <message>
        <source>--portable
    Starts application in a portable mode.
    In the portable mode Doomseeker saves all the configuration
    files to the directory where its executable resides.
    Normally, configuration is saved to user&apos;s home directory.
    See also: --basedir.
</source>
        <translation type="vanished">--portable
    Inicia l&apos;aplicació en mode portàtil.
    En mode portàtil, Doomseeker guarda tots els arxius de
    configuració en el directori on resideix l&apos;executable.
    Normalment, la configuració es guarda en el directori
    d&apos;inici de l&apos;usuari. Miri també: --basedir.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="28"/>
        <source>being assigned a different letter</source>
        <translation>sent assignat a una lletra diferent</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="31"/>
        <source>being mounted on a different path</source>
        <translation>sent muntat a una ruta diferent</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="51"/>
        <source>--disable-plugin &lt;plugin&gt;
    Ban a &lt;plugin&gt; from loading even if it is normally loaded.
    Specify the plugin name without the &apos;lib&apos; prefix or the file
    extension, for ex. &apos;vavoom&apos;, not &apos;libvavoom%1&apos;.
</source>
        <translation>--disable-plugin &lt;plugin&gt;
     Prohibeix que &lt;plugin&gt; sigui carregat encara que normalment
     s&apos;hauria carregat. Especifiqueu el nom del plugin sense el
     prefix &quot;lib&quot; o la seva extensió, per exemple &apos;vavoom&apos;, no
     &apos;libvavoom%1&apos;.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="56"/>
        <source>--enable-plugin &lt;plugin&gt;
    Load a &lt;plugin&gt; even if it is normally disabled/banned.
    Specify the plugin name without the &apos;lib&apos; prefix or the file
    extension, for ex. &apos;vavoom&apos;, not &apos;libvavoom%1&apos;.
</source>
        <translation>--enable-plugin &lt;plugin&gt;
     Carrega un &lt;plugin&gt; encara que normalment estigui desactivat
     Especifiqueu el nom del connector sense el prefix &quot;lib&quot; o el vostre
     extensió, per exemple &apos;vavoom&apos;, no &apos;libvavoom%1&apos;.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="68"/>
        <source>--portable
    Starts the application in the portable mode:
    - Doomseeker saves all the configuration files to the directory
      where its executable resides. Normally, configuration is saved to
      user&apos;s home directory. This directory can be changed with --basedir.
    - The current working directory is forced to the directory where
      Doomseeker&apos;s executable resides.
    - Doomseeker will save in the configuration all paths as relative
      in anticipation that the absolute paths may change between the runs,
      for ex. due to the portable device %1.
</source>
        <translation>--portable
     Inicia l&apos;aplicació en mode portàtil:
     - Doomseeker desa tota la configuració al directori
       on hi ha l&apos;executable. Normalment, la
       configuració es desa al directori d&apos;inici de l&apos;usuari.
       Aquest directori es pot canviar amb --basedir.
     - El directori de treball es força al directori on es
       troba lexecutable.
     - Doomseeker guardarà totes les rutes de forma relativa,
       en anticipació que les rutes globals canviïn entre
       execucions, degut a, per exemple, un pendrive
       %1.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="79"/>
        <source>--version-json [file|-]
    Prints version info on Doomseeker and all
    plugins in JSON format to specified file,
    then closes the program. If file is not
    specified or specified as &apos;-&apos;, version info
    is printed to stdout.
</source>
        <translation>--version-json [file|-]
    Imprimeix la informació de la versió de
    Doomseeker i tots els complements en
    format JSON al fitxer indicat, després
    tanca el programa. Si l&apos;arxiu no s&apos;especifica
    o s&apos;especifica com &apos;-&apos;, la informació de la
    versió s&apos;imprimeix en stdout.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="86"/>
        <source>--verbose
    Forces verbose logging to stderr.
    This is the default in most cases.
</source>
        <translation>--verbose
    Força el registre detallat a stderr.
    Aquest és el predeterminat en la majoria
    dels casos.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="90"/>
        <source>--quiet
    Forces no logging to stderr.
    This is the default when dumping versions.
</source>
        <translation>--quiet
    Prohibeix el registre a stderr.
    Aquest és el predeterminat en versions
    llançades.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="99"/>
        <source>Available command line parameters:
</source>
        <translation>Paràmetres de la línia d&apos;ordres disponibles:
</translation>
    </message>
    <message numerus="yes">
        <location filename="../cmdargshelp.cpp" line="106"/>
        <source>doomseeker: expected %n argument(s) in option %1

</source>
        <translation>
            <numerusform>doomseeker: se esperaba un argumento en la opción %1

</numerusform>
            <numerusform>doomseeker: se esperaba %n argumentos en la opción %1

</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="113"/>
        <source>doomseeker: unrecognized option &apos;%1&apos;

</source>
        <translation>doomseeker: opció no reconeguda &apos;%1&apos;

</translation>
    </message>
</context>
<context>
    <name>ConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/configurationdialog.ui" line="14"/>
        <source>Doomseeker - Configuration</source>
        <translation>Doomseeker - Configuració</translation>
    </message>
</context>
<context>
    <name>ConnectionHandler</name>
    <message>
        <location filename="../connectionhandler.cpp" line="83"/>
        <location filename="../connectionhandler.cpp" line="87"/>
        <source>Doomseeker - join server</source>
        <translation>Doomseeker - unir-se al servidor</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="84"/>
        <source>Connection to server timed out.</source>
        <translation>S&apos;ha excedit el límit de temps de connexió al servidor.</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="88"/>
        <source>An error occured while trying to connect to server.</source>
        <translation>S&apos;ha produït un error en connectar al servidor.</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="165"/>
        <source>Doomseeker - join game</source>
        <translation>Doomseeker - unir-se al joc</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="177"/>
        <source>Error while launching game &quot;%2&quot; for server &quot;%1&quot;: %3</source>
        <translation>Error en llançar el joc &quot;%2&quot; per al servidor &quot;%1&quot;: %3</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="179"/>
        <source>Doomseeker - launch game</source>
        <translation>Doomseeker - executar joc</translation>
    </message>
    <message>
        <source>Error while launching executable for server &quot;%1&quot;, game &quot;%2&quot;: %3</source>
        <translation type="vanished">Error en executar executable per al servidor &quot;%1&quot;, joc &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <source>Doomseeker - launch executable</source>
        <translation type="vanished">Doomseeker - executar executable</translation>
    </message>
</context>
<context>
    <name>CopyTextDlg</name>
    <message>
        <location filename="../gui/copytextdlg.ui" line="13"/>
        <source>Doomseeker - Copy Text</source>
        <translation>Doomseeker - Copiar text</translation>
    </message>
    <message>
        <location filename="../gui/copytextdlg.ui" line="19"/>
        <source>Text to copy:</source>
        <translation>Text a copiar:</translation>
    </message>
    <message>
        <location filename="../gui/copytextdlg.ui" line="31"/>
        <source>Copy to clipboard</source>
        <translation>Copia al porta-retalls</translation>
    </message>
</context>
<context>
    <name>CreateServerDialog</name>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="119"/>
        <source>Host server</source>
        <translation>Hostejar servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="123"/>
        <source>Play</source>
        <translation>Jugar</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="126"/>
        <source>Doomseeker - Setup Remote Game</source>
        <translation>Doomseeker - Configurar joc remot</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="118"/>
        <source>Doomseeker - Host Online Game</source>
        <translation>Doomseeker - Hostejar joc en línea</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="114"/>
        <source>Doomseeker - Play Demo</source>
        <translation>Doomseeker - Reproduir Demo</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="115"/>
        <source>Play demo</source>
        <translation>Reproduir Demo</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="122"/>
        <source>Doomseeker - Play Offline Game</source>
        <translation>Doomseeker - Jugar fora de línia</translation>
    </message>
    <message>
        <source>Doomseeker - [Unhandled Host Mode]</source>
        <translation type="vanished">Doomseeker - [Mode desconegut]</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="153"/>
        <location filename="../gui/createserverdialog.cpp" line="389"/>
        <source>Doomseeker - create game</source>
        <translation>Doomseeker - crear joc</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="156"/>
        <location filename="../gui/createserverdialog.cpp" line="392"/>
        <source>No game selected</source>
        <translation>Cap joc seleccionat</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="173"/>
        <source>Doomseeker - error</source>
        <translation>Doomseeker - error</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="69"/>
        <location filename="../gui/createserverdialog.cpp" line="254"/>
        <source>Flags</source>
        <translation>Indicadors</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="127"/>
        <source>Play online</source>
        <translation>Jugar en línia</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="130"/>
        <source>Doomseeker - Create Game</source>
        <translation>Doomseeker - Crear Joc</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="131"/>
        <source>Create game</source>
        <translation>Crear joc</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="449"/>
        <source>&amp;Mode</source>
        <translation>&amp;Mode</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="450"/>
        <source>&amp;Host server</source>
        <translation>&amp;Hostejar servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="453"/>
        <source>&amp;Play offline</source>
        <translation>&amp;Jugar fora de línia</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="457"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuració</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="459"/>
        <source>&amp;Load game configuration</source>
        <translation>&amp;Carregar la configuració del joc</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="464"/>
        <source>&amp;Save game configuration</source>
        <translation>&amp;Guargar la configuració del joc</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="470"/>
        <source>&amp;Program settings</source>
        <translation>Configuració del &amp;Programa</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="494"/>
        <source>Run game command line:</source>
        <translation>Comanda per executar joc:</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="495"/>
        <source>Host server command line:</source>
        <translation>Comanda per executar el servidor:</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="504"/>
        <source>Doomseeker - load game setup config</source>
        <translation>Doomseeker - carregar la configuració del joc</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="504"/>
        <location filename="../gui/createserverdialog.cpp" line="518"/>
        <source>Config files (*.ini)</source>
        <translation>Arxius de configuració (* .ini)</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="518"/>
        <location filename="../gui/createserverdialog.cpp" line="528"/>
        <source>Doomseeker - save game setup config</source>
        <translation>Doomseeker - guarda la configuració del joc</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="528"/>
        <source>Unable to save game setup configuration!</source>
        <translation>No es pot guardar la configuració del joc!</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="14"/>
        <source>Create Game</source>
        <translation>Crear joc</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="39"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="49"/>
        <source>Rules</source>
        <translation>Regles</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="59"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="79"/>
        <source>Custom parameters</source>
        <translation>Paràmetres personalitzats</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="135"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="158"/>
        <source>Obtain command line required to launch this game.</source>
        <translation>Obtenir la línia d&apos;ordres requerida per iniciar aquest joc.</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="161"/>
        <source>Command line</source>
        <translation>Línia d&apos;ordres</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Tancar</translation>
    </message>
</context>
<context>
    <name>CustomParamsPanel</name>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="23"/>
        <source>Custom parameters (ENTER or SPACEBAR separated):</source>
        <translation>Paràmetres personalitzats (ENTER o ESPAI separats):</translation>
    </message>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="33"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Note: these are added to the command line as visible on the list.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Console commands usually begin with &apos;+&apos; character. For example: +sv_cheats 1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Nota: aquests paràmetres s&apos;agreguen a la llista exactament com els pot veure.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Els comandaments de la consola generalment comencen amb el caràcter &apos;+&apos;. Per exemple: + sv_cheats 1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomServers</name>
    <message>
        <location filename="../customservers.cpp" line="163"/>
        <source>Unknown game for custom server %1:%2</source>
        <translation>Joc desconegut per al servidor personalitzat %1:%2</translation>
    </message>
    <message>
        <location filename="../customservers.cpp" line="175"/>
        <source>Failed to resolve address for custom server %1:%2</source>
        <translation>No s&apos;ha pogut resoldre l&apos;adreça del servidor personalitzat %1:%2</translation>
    </message>
    <message>
        <location filename="../customservers.cpp" line="186"/>
        <source>Plugin returned nullptr &quot;Server*&quot; for custom server %1:%2. This is a problem with the plugin.</source>
        <translation>El complement ha retornat nullptr &quot;Server*&quot; per al servidor personalitzat %1: %2. Això és un problema amb el complement.</translation>
    </message>
</context>
<context>
    <name>DefaultDifficultyProvider</name>
    <message>
        <location filename="../plugins/enginedefaults.h" line="41"/>
        <source>1 - I&apos;m too young to die</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="42"/>
        <source>2 - Hey, not too rough</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="43"/>
        <source>3 - Hurt me plenty</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="44"/>
        <source>4 - Ultra-violence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="45"/>
        <source>5 - NIGHTMARE!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DemoManagerDlg</name>
    <message>
        <source>Unable to delete</source>
        <translation type="vanished">No es pot esborrar</translation>
    </message>
    <message>
        <source>Could not delete the selected demo.</source>
        <translation type="vanished">No es pot esborrar la demo seleccionada.</translation>
    </message>
    <message>
        <source>Delete demo?</source>
        <translation type="vanished">Esborrar demo?</translation>
    </message>
    <message>
        <source>Are you sure you want to delete the selected demo?</source>
        <translation type="vanished">¿Segur que vols eliminar la demo seleccionada?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="229"/>
        <source>Export plain demo file</source>
        <translation>Exporta fitxer &amp;quot;demo&amp;quot; senzill</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="231"/>
        <source>Export with Doomseeker metadata</source>
        <translation>Exporta amb metadades de Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="273"/>
        <source>Unable to remove</source>
        <translation>Incapaç de suprimir</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="273"/>
        <source>Could not remove the selected demo.</source>
        <translation>No s&apos;ha pogut esborrar la demo seleccionada.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="284"/>
        <source>Remove demo?</source>
        <translation>Suprimir demo?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="285"/>
        <source>Are you sure you want to remove the selected demo?</source>
        <translation>¿Segur que vols suprimir la demo seleccionada?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="329"/>
        <source>Demo export</source>
        <translation>Exportar Demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="330"/>
        <source>The exported files already exist at the specified directory. Overwrite?

%1</source>
        <translation>Els fitxers exportats ja existeixen al directori especificat. Sobreescriure?

%1</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="361"/>
        <location filename="../gui/demomanager.cpp" line="384"/>
        <source>Unable to save</source>
        <translation>Error d&apos;escriptura</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="361"/>
        <location filename="../gui/demomanager.cpp" line="385"/>
        <source>Could not write to the specified location.</source>
        <translation>No s&apos;ha pogut escriure en la ubicació especificada.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="395"/>
        <source>Import demo</source>
        <translation>Importar Demo</translation>
    </message>
    <message>
        <source>Demos (%1)</source>
        <translation type="vanished">Arxius demo (%1)</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="407"/>
        <source>Import game demo</source>
        <translation>Importar joc Demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="426"/>
        <source>It looks like this demo is already imported. Overwrite anyway?</source>
        <translation>Sembla que aquesta demo ja està importada. Sobreescriure?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="430"/>
        <source>Another demo with this metadata already exists. Overwrite?</source>
        <translation>Ja existeix una altra demo amb aquestes metadades. Sobreescriure?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="432"/>
        <source>Existing: %1
New: %2
</source>
        <translation>Existent: %1
Nou: %2
</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="438"/>
        <location filename="../gui/demomanager.cpp" line="448"/>
        <source>Demo import</source>
        <translation>Importar Demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="439"/>
        <source>%1

%2</source>
        <translation>%1

%2</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="449"/>
        <source>Could not import the selected demo.</source>
        <translation>No s&apos;ha pogut importar la demo seleccionada.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="462"/>
        <source>%1 %L2 B (MD5: %3)</source>
        <translation>%1 %L2 B (MD5: %3)</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="490"/>
        <source>No plugin</source>
        <translation>Sense complements</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="491"/>
        <source>The &quot;%1&quot; plugin does not appear to be loaded.</source>
        <translation>El complement &quot;%1&quot; no sembla estar carregat.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="526"/>
        <source>Files not found</source>
        <translation>Arxius no trobats</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="527"/>
        <source>The following files could not be located: </source>
        <translation>Els següents fitxers no han estat localitzats: </translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="574"/>
        <source>Game</source>
        <translation>Joc</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="580"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="582"/>
        <source>WADs / mods</source>
        <translation>WADs / mods</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="17"/>
        <source>Demo Manager</source>
        <translation>Administrador de demos</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="134"/>
        <source>Play</source>
        <translation>Reproduir</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="161"/>
        <source>Remove</source>
        <translation>Suprimir</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="185"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="192"/>
        <source>Export ...</source>
        <translation>Exportar...</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Eliminar</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">Exportar</translation>
    </message>
</context>
<context>
    <name>DemoMetaDataDialog</name>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="14"/>
        <source>Game Demo</source>
        <translation>Demo del joc</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="22"/>
        <source>Game:</source>
        <translation>Joc:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="29"/>
        <source>Created time:</source>
        <translation>Temps de creació:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="49"/>
        <source>yyyy-MM-dd HH:mm:ss</source>
        <translation>yyyy-MM-dd HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="59"/>
        <source>IWAD:</source>
        <translation>IWAD:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="69"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="76"/>
        <source>Game version:</source>
        <translation>Versió del joc:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="88"/>
        <source>WADs and other game files:</source>
        <translation>WADs i altres fitxers del joc:</translation>
    </message>
</context>
<context>
    <name>DemoModel</name>
    <message>
        <location filename="../gui/demomanager.cpp" line="84"/>
        <source>Created Time</source>
        <translation>Temps de creació</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="84"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="84"/>
        <source>IWAD</source>
        <translation>IWAD</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="84"/>
        <source>WADs</source>
        <translation>WADs</translation>
    </message>
</context>
<context>
    <name>DockBuddiesList</name>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="84"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="85"/>
        <source>Buddy</source>
        <translation>Company</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="86"/>
        <source>Location</source>
        <translation>Ubicació</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="136"/>
        <source>Remove Buddy</source>
        <translation>Suprimir Company</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="136"/>
        <source>Are you sure you want to remove this pattern?</source>
        <translation>¿Segur que vols suprimir aquest patró?</translation>
    </message>
    <message>
        <source>Delete Buddy</source>
        <translation type="vanished">Eliminar Company</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this pattern?</source>
        <translation type="vanished">¿Segur que vols eliminar aquest patró?</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="149"/>
        <location filename="../gui/dockBuddiesList.cpp" line="198"/>
        <source>Add</source>
        <translation>Afegir</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="20"/>
        <source>Buddies</source>
        <translation>Companys</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="51"/>
        <source>List</source>
        <translation>Llista</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="103"/>
        <source>Manage</source>
        <translation>Gestionar</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="160"/>
        <location filename="../gui/dockBuddiesList.cpp" line="204"/>
        <source>Remove</source>
        <translation>Suprimir</translation>
    </message>
</context>
<context>
    <name>DoomseekerConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/doomseekerconfigurationdialog.cpp" line="112"/>
        <source>Settings saved!</source>
        <translation>Configuració guardada!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/doomseekerconfigurationdialog.cpp" line="114"/>
        <source>Settings save failed!</source>
        <translation>Guardat fallit de la configuració!</translation>
    </message>
    <message>
        <source>Games</source>
        <translation type="vanished">Jocs</translation>
    </message>
</context>
<context>
    <name>EngineConfigPage</name>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="165"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - cercar executable</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="267"/>
        <source>Failed to automatically find file.
You may need to use the browse button.</source>
        <translation>Error en buscar l&apos;arxiu automàticament
És possible que necessiti utilitzar el botó Cerca.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="272"/>
        <source>Game - %1</source>
        <translation>Joc - %1</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="65"/>
        <source>Custom parameters:</source>
        <translation>Paràmetres personalitzats:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="90"/>
        <source>Add these parameters to the saved list.</source>
        <translation>Afegir aquests paràmetres a la llista desada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="101"/>
        <source>Remove these parameters from the saved list.</source>
        <translation>Suprimir aquests paràmetres a la llista desada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="129"/>
        <source>Masterserver address:</source>
        <translation>Direcció de Masterserver:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="141"/>
        <source>Restore the master server address to the default.</source>
        <translation>Restablir la direcció mestra a la per defecte.</translation>
    </message>
    <message>
        <source>Restore the master server address to default.</source>
        <translation type="vanished">Restablir la direcció mestra a la per defecte.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="144"/>
        <source>Default</source>
        <translation>Predefinit</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="160"/>
        <source>[error]</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EnginePluginComboBox</name>
    <message>
        <location filename="../gui/widgets/engineplugincombobox.cpp" line="81"/>
        <source>%1 (unknown)</source>
        <translation>%1 (desconegut)</translation>
    </message>
</context>
<context>
    <name>ExeFile</name>
    <message>
        <source>No %1 executable specified for %2</source>
        <translation type="vanished">No s&apos;ha especificat %1 executable per %2</translation>
    </message>
    <message>
        <source>Executable for %1 %2:
%3
is a directory or doesn&apos;t exist.</source>
        <translation type="vanished">Executable per %1 %2:
%3
és un directori o no existeix.</translation>
    </message>
    <message>
        <source>No %1 executable configured for %2</source>
        <translation type="vanished">No s&apos;ha configurat %1 executable per %2</translation>
    </message>
    <message>
        <source>The executable path for %1 %2:
%3
is a directory or doesn&apos;t exist.</source>
        <translation type="vanished">L&apos;executable per %1 %2:
%3
és un directori o no existeix.</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="79"/>
        <source>The %1 executable is not set for %2.</source>
        <translation>El programa %1 per a %2 no està especificat.</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="87"/>
        <source>The %1 executable for %2 doesn&apos;t exist:
%3</source>
        <translation>El programa %1 per a %2 no existeix:
%3</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="97"/>
        <source>The path to %1 executable for %2 is a directory:
%3</source>
        <translation>La ruta al programa %1 per a %2 és un directori:
%3</translation>
    </message>
</context>
<context>
    <name>FileFilter</name>
    <message>
        <source>All files(*)</source>
        <translation type="vanished">Tots els fitxers (*)</translation>
    </message>
    <message>
        <location filename="../filefilter.cpp" line="29"/>
        <source>All files (*)</source>
        <translation>Tots els fitxers (*)</translation>
    </message>
    <message>
        <location filename="../filefilter.cpp" line="35"/>
        <location filename="../filefilter.cpp" line="43"/>
        <source>Demos (%1)</source>
        <translation>Demos (%1)</translation>
    </message>
    <message>
        <location filename="../filefilter.cpp" line="49"/>
        <source>Executables (*.exe *.bat *.com)</source>
        <translation>Arxius executables (*.exe *.bat *.com)</translation>
    </message>
</context>
<context>
    <name>FilePickWidget</name>
    <message>
        <source>Doomseeker - choose executable file</source>
        <translation type="vanished">Doomseeker - triï el fitxer executable</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="113"/>
        <source>Path to %1 executable:</source>
        <translation>Ruta cap a l&apos;executable %1:</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="174"/>
        <source>File doesn&apos;t exist.</source>
        <translation>El fitxer no existeix.</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="177"/>
        <source>This is a directory.</source>
        <translation>Això és un directori.</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="32"/>
        <source>Path to file:</source>
        <translation>Ruta al fitxer:</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="54"/>
        <source>Browse</source>
        <translation>Cercar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="67"/>
        <source>Find</source>
        <translation>Buscar</translation>
    </message>
</context>
<context>
    <name>FileUtilsTr</name>
    <message>
        <location filename="../fileutils.cpp" line="38"/>
        <source>cannot create directory</source>
        <translation>no es pot crear el directori</translation>
    </message>
    <message>
        <location filename="../fileutils.cpp" line="43"/>
        <source>the parent path is not a directory: %1</source>
        <translation>la ruta pare no és un directori: %1</translation>
    </message>
    <message>
        <location filename="../fileutils.cpp" line="48"/>
        <source>lack of necessary permissions to the parent directory: %1</source>
        <translation>falta de permisos necessaris per al directori pare: %1</translation>
    </message>
</context>
<context>
    <name>FreedoomDialog</name>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="96"/>
        <source>Install Freedoom</source>
        <translation>Instal·lar Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="97"/>
        <source>Select at least one file.</source>
        <translation>Seleccioneu com a mínim un arxiu.</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="101"/>
        <source>Downloading &amp; installing ...</source>
        <translation>Baixant i instal·lant ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="128"/>
        <source>Downloading Freedoom version info ...</source>
        <translation>S&apos;està baixant la informació de la versió de Freedom ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="134"/>
        <source>Working ...</source>
        <translation>Treballant...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="145"/>
        <source>Error: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="174"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="179"/>
        <source>Missing</source>
        <translation>Faltant</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="188"/>
        <source>Different</source>
        <translation>Diferent</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="206"/>
        <source>&lt;p&gt;File: %1&lt;br&gt;Version: %2&lt;br&gt;Description: %3&lt;br&gt;Location: %4&lt;/p&gt;</source>
        <translation>&lt;p&gt;Arxiu: %1&lt;br&gt;Versió: %2&lt;br&gt;Descripció: %3&lt;br&gt;Ubicació: %4&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="283"/>
        <source>%1 %p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="14"/>
        <source>Freedoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="30"/>
        <source>Freedoom is a free-of-charge data set replacement for commercial Doom games. It will allow you to play on most servers that host modded content without having to purchase Doom games. It won&apos;t allow you to join unmodded servers.</source>
        <translation>Freedoom és un reemplaçament de dades gratuït per al Doom comercial. Et permetrà jugar a la majoria dels servidors que allotgen contingut modificat sense la necessitat de comprar jocs de la sèrie Doom. No li permetrà unir-se a servidors no modificats.</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="40"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Webpage: &lt;a href=&quot;https://freedoom.github.io&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://freedoom.github.io&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pàgina web: &lt;a href=&quot;https://freedoom.github.io&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://freedoom.github.io&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="66"/>
        <source>Install path:</source>
        <translation>Ruta d&apos;instal·lació:</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="108"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="113"/>
        <source>Status</source>
        <translation>Estat</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="118"/>
        <source>Install?</source>
        <translation>Instal·lar?</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="169"/>
        <source>Work in progress ...</source>
        <translation>Treball en curs ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="75"/>
        <source>Install</source>
        <translation>Instal·lar</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Reintentar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Tancar</translation>
    </message>
</context>
<context>
    <name>GameClientRunner</name>
    <message>
        <source>Path to working directory for game &quot;%1&quot; is empty.

Make sure the configuration for the client executable is set properly.</source>
        <translation type="vanished">La ruta al directori de treball per al joc &quot;%1&quot; està buida.

Assegureu-vos que la configuració de l&apos;executable del client estigui configurada correctament.</translation>
    </message>
    <message>
        <source>%1

This directory cannot be used as working directory for game: %2

Executable: %3</source>
        <translation type="vanished">%1

Aquest directori no es pot fer servir com a directori de treball per al joc: %2

Executable: %3</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="217"/>
        <source>Cannot determine the working directory for &quot;%1&quot;. Check the game configuration.</source>
        <translation>No s&apos;ha pogut determinar el directori de treball per a %1. Comproveu la configuració del joc.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="225"/>
        <source>This path cannot be used as the working directory for %1:
%2.</source>
        <translation>Aquest directori no es pot fer servir com a directori de treball per al joc %1:

%2.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="242"/>
        <source>BUG: Plugin doesn&apos;t specify argument for in-game password, but the server requires such password.</source>
        <translation>BUG: El complement no especifica l&apos;argument per a la contrasenya dins del joc, però el servidor requereix aquesta contrasenya.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="294"/>
        <source>BUG: Plugin doesn&apos;t specify argument for connect password, but the server is passworded.</source>
        <translation>BUG: El complement no especifica l&apos;argument per a la contrasenya de connexió, però el servidor requereix tal contrasenya.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="496"/>
        <source>The game can be installed by Doomseeker.</source>
        <translation>El joc pot ser instal·lat per Doomseeker.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="508"/>
        <source>Couldn&apos;t find game %1.</source>
        <translation>El joc %1 no es pot trobar.</translation>
    </message>
    <message>
        <source>Game %1 cannot be found.</source>
        <translation type="vanished">El joc %1 no es pot trobar.</translation>
    </message>
    <message>
        <source>Client binary cannot be obtained for %1, please check the location given in the configuration.</source>
        <translation type="vanished">L&apos;executable del client no es pot obtenir per %1, verifiqui la ubicació donada en la configuració.</translation>
    </message>
</context>
<context>
    <name>GameConfigErrorBox</name>
    <message>
        <location filename="../gui/configuration/gameconfigerrorbox.cpp" line="41"/>
        <source>Configure &amp;game</source>
        <translation>Configurar &amp;joc</translation>
    </message>
</context>
<context>
    <name>GameDemoTr</name>
    <message>
        <location filename="../gamedemo.cpp" line="53"/>
        <source>Doomseeker - Record Demo</source>
        <translation>Doomseeker - Grabar Demo</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="61"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="63"/>
        <source>The demo storage directory doesn&apos;t exist and cannot be created!%1

%2</source>
        <translation>La ruta on desar les demos no existeix i no es pot crear! %1

%2</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="69"/>
        <source>The demo storage directory exists but lacks the necessary permissions!

%1</source>
        <translation>La ruta on desar les demos existeix, però no té prou permisos!

%1</translation>
    </message>
</context>
<context>
    <name>GameExeFactory</name>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="72"/>
        <source>game</source>
        <translation>joc</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="78"/>
        <source>client</source>
        <translation>client</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="81"/>
        <source>server</source>
        <translation>servidor</translation>
    </message>
</context>
<context>
    <name>GameExeRetriever</name>
    <message>
        <location filename="../serverapi/gameexeretriever.cpp" line="41"/>
        <source>Game doesn&apos;t define offline executable.</source>
        <translation>El joc no defineix cap executable fora de línia.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexeretriever.cpp" line="48"/>
        <source>Game offline executable is not configured.</source>
        <translation>El joc executable fora de línia no està configurat.</translation>
    </message>
</context>
<context>
    <name>GameExecutablePicker</name>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="73"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - cercar executable</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="115"/>
        <source>Plugin doesn&apos;t support configuration.</source>
        <translation>El complement no suporta configuracions.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="122"/>
        <source>Game doesn&apos;t define any executables for this game setup.</source>
        <translation>El joc no defineix cap executable per a la configuració d&apos;aquest joc.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="135"/>
        <source>Default executable for this game isn&apos;t configured.</source>
        <translation>El executable per defecte per a aquest joc no està configurat.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="161"/>
        <source>Game plugin not set.</source>
        <translation>El complement del joc no està configurat.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="45"/>
        <source>Browse</source>
        <translation>Cercar</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="58"/>
        <source>Default</source>
        <translation>Predefinit</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="81"/>
        <source>This map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>Aquest mapa no està present a la llista de mapes. El joc pot comportar-se de manera estranya.</translation>
    </message>
</context>
<context>
    <name>GameHost</name>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="151"/>
        <source>IWAD is not set</source>
        <translation>IWAD no està definit</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="159"/>
        <source>IWAD Path error:
&quot;%1&quot; doesn&apos;t exist or is a directory!</source>
        <translation>Error de ruta IWAD:
&quot;%1&quot; no existeix o és un directori!</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="440"/>
        <source>The game executable is not set.</source>
        <translation>No s&apos;ha configurat l&apos;executable del joc.</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="447"/>
        <source>The executable
%1
doesn&apos;t exist or is not a file.</source>
        <translation>L&apos;executable
%1
no existeix o no és un fitxer.</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="463"/>
        <source>PWAD path error:
&quot;%1&quot; doesn&apos;t exist!</source>
        <translation>Error de ruta PWAD:
&quot;%1&quot; no existeix!</translation>
    </message>
    <message>
        <source>%1
doesn&apos;t exist or is not a file.</source>
        <translation type="vanished">%1
 no existeix o no és un arxiu.</translation>
    </message>
    <message>
        <source>%1
 doesn&apos;t exist or is not a file.</source>
        <translation type="vanished">%1
 no existeix o és un directori.</translation>
    </message>
    <message>
        <source>PWAD path error:
&quot;%1&quot; doesn&apos;t exist or is a directory!</source>
        <translation type="vanished">Error de ruta PWAD:
&quot;%1&quot; no existeix o és un directori!</translation>
    </message>
</context>
<context>
    <name>GameInfoTip</name>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="67"/>
        <source>Players</source>
        <translation>Jugadors</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="72"/>
        <source>%1 / %2 (%3 can join)</source>
        <translation>%1 / %2 (%3 es poden unir)</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="96"/>
        <source>Scorelimit</source>
        <translation>Límit puntuació</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="131"/>
        <source>Timelimit</source>
        <translation>Límit temps</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="138"/>
        <source>(%1 left)</source>
        <translation>(resten %1)</translation>
    </message>
</context>
<context>
    <name>GameRulesPanel</name>
    <message>
        <location filename="../gui/createserver/gamerulespanel.cpp" line="249"/>
        <source>&lt; NONE &gt;</source>
        <translation>&lt; RES &gt;</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.cpp" line="283"/>
        <source>%1:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="41"/>
        <source>Map list</source>
        <translation>Lista de mapes</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="84"/>
        <source>Hosting limits</source>
        <translation>Límits d’allotjament</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="90"/>
        <source>Max. clients:</source>
        <translation>Max. clients:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="107"/>
        <source>Max. players:</source>
        <translation>Max. jugadors:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="127"/>
        <source>Modifier</source>
        <translation>Modificador</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="146"/>
        <source>Extra settings</source>
        <translation>Opcions addicionals</translation>
    </message>
    <message>
        <source>Game limits</source>
        <translation type="vanished">Límits del joc</translation>
    </message>
</context>
<context>
    <name>GeneralGameSetupPanel</name>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="283"/>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="313"/>
        <source>&lt; NONE &gt;</source>
        <translation>&lt; RES &gt;</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="480"/>
        <source>Doomseeker - load server config</source>
        <translation>Doomseeker - carregar la configuració del servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="481"/>
        <source>Plugin for engine &quot;%1&quot; is not present!</source>
        <translation>El complement per al motor &quot;%1&quot; no està present!</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="34"/>
        <source>Game:</source>
        <translation>Joc:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="44"/>
        <source>Executable:</source>
        <translation>Executable:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="105"/>
        <source>If locked, executable won&apos;t change when new config for the same engine is loaded</source>
        <translation>Si està bloquejat, l&apos;executable no canviarà quan es carreguin noves configuracions per al mateix motor</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="108"/>
        <source>Lock</source>
        <translation>Bloquear</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="117"/>
        <source>Logging:</source>
        <translation>Registre:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="140"/>
        <source>Server name:</source>
        <translation>Nom del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="147"/>
        <source>Started from Doomseeker</source>
        <translation>Començat des Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="154"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="195"/>
        <source>Allow the game to choose port</source>
        <translation>Permetre que el joc triï el port</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="205"/>
        <source>Game mode:</source>
        <translation>Mode de joc:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="215"/>
        <source>Difficulty:</source>
        <translation>Dificultat:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="232"/>
        <source>Map:</source>
        <translation>Mapa:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="271"/>
        <source>Demo:</source>
        <translation>Demo:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="311"/>
        <source>This map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>Aquest mapa no està present a la llista de mapes. El joc pot comportar-se de forma estranya.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="239"/>
        <source>IWAD:</source>
        <translation>IWAD:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="329"/>
        <source>Additional WADs and files (check the required ones):</source>
        <translation>WADs i arxius addicionals (Comprova els que són requerits):</translation>
    </message>
    <message>
        <source>Additional WADs and files (check required):</source>
        <translation type="vanished">WADs i arxius addicionals (es requereix verificació):</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="361"/>
        <source>Broadcast to LAN</source>
        <translation>Emetre a la LAN</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="371"/>
        <source>Broadcast to master</source>
        <translation>Emetre a master</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="396"/>
        <source>If checked, the game will try to tell your router to forward necessary ports.</source>
        <translation>Si està marcat, el joc intentarà dir-li al seu router que reenviï els ports necessaris.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="399"/>
        <source>UPnP</source>
        <translation>UPnP</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="409"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;UPnP port. Set this to 0 to let Doomseeker or game decide.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;port UPnP. Establiu això en 0 per permetre que Doomseeker o el joc decideixin.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>GeneralInfoTip</name>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="51"/>
        <source>Version</source>
        <translation>Versió</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="52"/>
        <source>E-mail</source>
        <translation>Correu</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="53"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="59"/>
        <source>Location</source>
        <translation>Ubicació</translation>
    </message>
    <message>
        <source>Location: %1
</source>
        <translation type="vanished">Ubicació: %1
</translation>
    </message>
</context>
<context>
    <name>IP2C</name>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="228"/>
        <source>No flag for country: %1</source>
        <translation>No hi ha bandera per al país: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="173"/>
        <source>Localhost</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="176"/>
        <source>LAN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="219"/>
        <source>Unknown country: %1</source>
        <translation>País desconegut: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="234"/>
        <source>Unknown</source>
        <translation>Desconegut</translation>
    </message>
</context>
<context>
    <name>IP2CCountryTr</name>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="57"/>
        <source>Aruba</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="58"/>
        <source>Afghanistan</source>
        <translation>Afganistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="59"/>
        <source>Angola</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="60"/>
        <source>Anguilla</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="61"/>
        <source>Albania</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="62"/>
        <source>Andorra</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="63"/>
        <source>Netherlands Antilles</source>
        <translation>Antilles Holandeses</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="64"/>
        <source>United Arab Emirates</source>
        <translation>Emirats Àrabs Units</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="65"/>
        <source>Argentina</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="66"/>
        <source>Armenia</source>
        <translation>Armènia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="67"/>
        <source>American Samoa</source>
        <translation>Samoa Americana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="68"/>
        <source>Antarctica</source>
        <translation>Antàrtida</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="69"/>
        <source>French Southern Territories</source>
        <translation>Territoris Francesos del Sud</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="70"/>
        <source>Antigua and Barbuda</source>
        <translation>Antiga i Barbuda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="71"/>
        <source>Australia</source>
        <translation>Austràlia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="72"/>
        <source>Austria</source>
        <translation>Àustria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="73"/>
        <source>Azerbaijan</source>
        <translation>Azerbaidjan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="74"/>
        <source>Burundi</source>
        <translation>Burundi</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="75"/>
        <source>Belgium</source>
        <translation>Bèlgica</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="76"/>
        <source>Benin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="77"/>
        <source>Burkina Faso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="78"/>
        <source>Bangladesh</source>
        <translation>Bangla Desh</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="79"/>
        <source>Bulgaria</source>
        <translation>Bulgària</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="80"/>
        <source>Bahrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="81"/>
        <source>Bahamas</source>
        <translation>Bahames</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="82"/>
        <source>Bosnia and Herzegovina</source>
        <translation>Bòsnia i Hercegovina</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="83"/>
        <source>Belarus</source>
        <translation>Bielorússia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="84"/>
        <source>Belize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="85"/>
        <source>Bermuda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="86"/>
        <source>Bolivia</source>
        <translation>Bolívia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="87"/>
        <source>Brazil</source>
        <translation>Brasil</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="88"/>
        <source>Barbados</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="89"/>
        <source>Brunei</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="90"/>
        <source>Bhutan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="91"/>
        <source>Bouvet Island</source>
        <translation>Illa Bouvet</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="92"/>
        <source>Botswana</source>
        <translation>Botswana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="93"/>
        <source>Central African Republic</source>
        <translation>República Centreafricana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="94"/>
        <source>Canada</source>
        <translation>Canadà</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="95"/>
        <source>Cocos (Keeling) Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="96"/>
        <source>Switzerland</source>
        <translation>Suïssa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="97"/>
        <source>Chile</source>
        <translation>Chile</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="98"/>
        <source>China</source>
        <translation>Xina</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="99"/>
        <source>Ivory Coast</source>
        <translation>Costa d&apos;Ivori</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="100"/>
        <source>Cameroon</source>
        <translation>Camerun</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="101"/>
        <source>Congo, the Democratic Republic of the</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="102"/>
        <source>Congo</source>
        <translation>Congo</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="103"/>
        <source>Cook Islands</source>
        <translation>Illes Cook</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="104"/>
        <source>Colombia</source>
        <translation>Colòmbia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="105"/>
        <source>Comoros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="106"/>
        <source>Cape Verde</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="107"/>
        <source>Costa Rica</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="108"/>
        <source>Cuba</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="109"/>
        <source>Christmas Island</source>
        <translation>Illa de Nadal</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="110"/>
        <source>Cayman Islands</source>
        <translation>Illes Caiman</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="111"/>
        <source>Cyprus</source>
        <translation>Xipre</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="112"/>
        <source>Czech Republic</source>
        <translation>República Txeca</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="113"/>
        <source>Germany</source>
        <translation>Alemanya</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="114"/>
        <source>Djibouti</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="115"/>
        <source>Dominica</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="116"/>
        <source>Denmark</source>
        <translation>Dinamarca</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="117"/>
        <source>Dominican Republic</source>
        <translation>República Dominicana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="118"/>
        <source>Algeria</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="119"/>
        <source>Ecuador</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="120"/>
        <source>Egypt</source>
        <translation>Egipte</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="121"/>
        <source>Eritrea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="122"/>
        <source>Western Sahara</source>
        <translation>Sàhara Occidental</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="123"/>
        <source>Spain</source>
        <translation>Espanya</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="124"/>
        <source>Estonia</source>
        <translation>Estònia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="125"/>
        <source>Ethiopia</source>
        <translation>Etiòpia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="126"/>
        <source>European Union</source>
        <translation>Unió Europea</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="127"/>
        <source>Finland</source>
        <translation>Finlàndia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="128"/>
        <source>Fiji</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="129"/>
        <source>Falkland Islands (Malvinas)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="130"/>
        <source>France</source>
        <translation>França</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="131"/>
        <source>Faroe Islands</source>
        <translation>Illes Faroe</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="132"/>
        <source>Micronesia, Federated States of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="133"/>
        <source>Gabon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="134"/>
        <source>United Kingdom</source>
        <translation>Regne Unit</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="135"/>
        <source>Georgia</source>
        <translation>Geòrgia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="136"/>
        <source>Guernsey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="137"/>
        <source>Ghana</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="138"/>
        <source>Gibraltar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="139"/>
        <source>Guinea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="140"/>
        <source>Guadeloupe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="141"/>
        <source>Gambia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="142"/>
        <source>Guinea-Bissau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="143"/>
        <source>Equatorial Guinea</source>
        <translation>Guinea Equatorial</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="144"/>
        <source>Greece</source>
        <translation>Grècia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="145"/>
        <source>Grenada</source>
        <translation>Granada</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="146"/>
        <source>Greenland</source>
        <translation>Groenlàndia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="147"/>
        <source>Guatemala</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="148"/>
        <source>French Guiana</source>
        <translation>Guaiana Francesa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="149"/>
        <source>Guam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="150"/>
        <source>Guyana</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="151"/>
        <source>Hong Kong</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="152"/>
        <source>Heard Island and McDonald Islands</source>
        <translation>Illa Heard i illes McDonald</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="153"/>
        <source>Honduras</source>
        <translation>Hondures</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="154"/>
        <source>Croatia</source>
        <translation>Croàcia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="155"/>
        <source>Haiti</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="156"/>
        <source>Hungary</source>
        <translation>Hongria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="157"/>
        <source>Indonesia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="158"/>
        <source>Isle of Man</source>
        <translation>illa de l&apos;home</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="159"/>
        <source>India</source>
        <translation>Índia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="160"/>
        <source>British Indian Ocean Territory</source>
        <translation>Territori Britànic de l&apos;Oceà Índic</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="161"/>
        <source>Ireland</source>
        <translation>Irlanda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="162"/>
        <source>Iran, Islamic Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="163"/>
        <source>Iraq</source>
        <translation>Iraq</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="164"/>
        <source>Iceland</source>
        <translation>Islàndia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="165"/>
        <source>Israel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="166"/>
        <source>Italy</source>
        <translation>Itàlia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="167"/>
        <source>Jamaica</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="168"/>
        <source>Jersey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="169"/>
        <source>Jordan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="170"/>
        <source>Japan</source>
        <translation>Japó</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="171"/>
        <source>Kazakhstan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="172"/>
        <source>Kenya</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="173"/>
        <source>Kyrgyzstan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="174"/>
        <source>Cambodia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="175"/>
        <source>Kiribati</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="176"/>
        <source>Saint Kitts and Nevis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="177"/>
        <source>South Korea</source>
        <translation>Corea del Sud</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="178"/>
        <source>Kuwait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="179"/>
        <source>Lao People&apos;s Democratic Republic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="180"/>
        <source>Lebanon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="181"/>
        <source>Liberia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="182"/>
        <source>Libya</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="183"/>
        <source>Saint Lucia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="184"/>
        <source>Liechtenstein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="185"/>
        <source>Sri Lanka</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="186"/>
        <source>Lesotho</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="187"/>
        <source>Lithuania</source>
        <translation>Lituània</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="188"/>
        <source>Luxembourg</source>
        <translation>Luxemburg</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="189"/>
        <source>Latvia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="190"/>
        <source>Macao</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="191"/>
        <source>Morocco</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="192"/>
        <source>Monaco</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="193"/>
        <source>Moldova, Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="194"/>
        <source>Madagascar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="195"/>
        <source>Maldives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="196"/>
        <source>Mexico</source>
        <translation>Mèxic</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="197"/>
        <source>Marshall Islands</source>
        <translation>Illes Marshall</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="198"/>
        <source>Macedonia, the former Yugoslav Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="199"/>
        <source>Mali</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="200"/>
        <source>Malta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="201"/>
        <source>Myanmar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="202"/>
        <source>Montenegro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="203"/>
        <source>Mongolia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="204"/>
        <source>Northern Mariana Islands</source>
        <translation>Illes Mariannes del Nord</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="205"/>
        <source>Mozambique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="206"/>
        <source>Mauritania</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="207"/>
        <source>Montserrat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="208"/>
        <source>Martinique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="209"/>
        <source>Mauritius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="210"/>
        <source>Malawi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="211"/>
        <source>Malaysia</source>
        <translation>Malàisia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="212"/>
        <source>Mayotte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="213"/>
        <source>Namibia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="214"/>
        <source>New Caledonia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="215"/>
        <source>Niger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="216"/>
        <source>Norfolk Island</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="217"/>
        <source>Nigeria</source>
        <translation>Nigèria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="218"/>
        <source>Nicaragua</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="219"/>
        <source>Niue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="220"/>
        <source>Netherlands</source>
        <translation>Països Baixos</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="221"/>
        <source>Norway</source>
        <translation>Noruega</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="222"/>
        <source>Nepal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="223"/>
        <source>Nauru</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="224"/>
        <source>New Zealand</source>
        <translation>Nova Zelanda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="225"/>
        <source>Oman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="226"/>
        <source>Pakistan</source>
        <translation>Pakistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="227"/>
        <source>Panama</source>
        <translation>Panamà</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="228"/>
        <source>Pitcairn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="229"/>
        <source>Peru</source>
        <translation>Perú</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="230"/>
        <source>Philippines</source>
        <translation>Filipines</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="231"/>
        <source>Palau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="232"/>
        <source>Papua New Guinea</source>
        <translation>Papua Nova Guinea</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="233"/>
        <source>Poland</source>
        <translation>Polònia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="234"/>
        <source>Puerto Rico</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="235"/>
        <source>Korea, Democratic People&apos;s Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="236"/>
        <source>Portugal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="237"/>
        <source>Paraguay</source>
        <translation>Paraguai</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="238"/>
        <source>Palestinian Territory, Occupied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="239"/>
        <source>French Polynesia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="240"/>
        <source>Qatar</source>
        <translation>Qatar</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="241"/>
        <source>Réunion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="242"/>
        <source>Romania</source>
        <translation>Romania</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="243"/>
        <source>Russian Federation</source>
        <translation>Rússia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="244"/>
        <source>Rwanda</source>
        <translation>Ruanda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="245"/>
        <source>Saudi Arabia</source>
        <translation>Aràbia Saudita</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="246"/>
        <source>Sudan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="247"/>
        <source>Senegal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="248"/>
        <source>Singapore</source>
        <translation>Singapur</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="249"/>
        <source>South Georgia and the South Sandwich Islands</source>
        <translation>Geòrgia del Sud i les illes Sandwich del Sud</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="250"/>
        <source>Saint Helena, Ascension and Tristan da Cunha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="251"/>
        <source>Svalbard and Jan Mayen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="252"/>
        <source>Solomon Islands</source>
        <translation>Illes Salomó</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="253"/>
        <source>Sierra Leone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="254"/>
        <source>El Salvador</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="255"/>
        <source>San Marino</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="256"/>
        <source>Somalia</source>
        <translation>Somàlia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="257"/>
        <source>Saint Pierre and Miquelon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="258"/>
        <source>Serbia</source>
        <translation>Sèrbia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="259"/>
        <source>South Sudan</source>
        <translation>Sudan del Sud</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="260"/>
        <source>Sao Tome and Principe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="261"/>
        <source>Suriname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="262"/>
        <source>Slovakia</source>
        <translation>Eslovàquia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="263"/>
        <source>Slovenia</source>
        <translation>Eslovènia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="264"/>
        <source>Sweden</source>
        <translation>Suècia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="265"/>
        <source>Swaziland</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="266"/>
        <source>Seychelles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="267"/>
        <source>Syrian Arab Republic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="268"/>
        <source>Turks and Caicos Islands</source>
        <translation>Illes Turks i Caicos</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="269"/>
        <source>Chad</source>
        <translation>Txad</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="270"/>
        <source>Togo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="271"/>
        <source>Thailand</source>
        <translation>Tailàndia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="272"/>
        <source>Tajikistan</source>
        <translation>Tadjikistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="273"/>
        <source>Tokelau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="274"/>
        <source>Turkmenistan</source>
        <translation>Turkmenistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="275"/>
        <source>Timor-Leste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="276"/>
        <source>Tonga</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="277"/>
        <source>Trinidad and Tobago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="278"/>
        <source>Tunisia</source>
        <translation>Tunísia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="279"/>
        <source>Turkey</source>
        <translation>Turquia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="280"/>
        <source>Tuvalu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="281"/>
        <source>Taiwan</source>
        <translation>Taiwan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="282"/>
        <source>Tanzania, United Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="283"/>
        <source>Uganda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="284"/>
        <source>Ukraine</source>
        <translation>Ucraïna</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="285"/>
        <source>United States Minor Outlying Islands</source>
        <translation>Illes menors perifèriques dels Estats Units</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="286"/>
        <source>Uruguay</source>
        <translation>Uruguai</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="287"/>
        <source>United States</source>
        <translation>Estats Units</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="288"/>
        <source>Uzbekistan</source>
        <translation>Uzbekistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="289"/>
        <source>Holy See (Vatican City State)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="290"/>
        <source>Saint Vincent and the Grenadines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="291"/>
        <source>Venezuela</source>
        <translation>Veneçuela</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="292"/>
        <source>Virgin Islands, British</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="293"/>
        <source>Virgin Islands, U.S.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="294"/>
        <source>Vietnam</source>
        <translation>Vietnam</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="295"/>
        <source>Vanuatu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="296"/>
        <source>Wallis and Futuna</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="297"/>
        <source>Samoa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="298"/>
        <source>Yemen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="299"/>
        <source>South Africa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="300"/>
        <source>Zambia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="301"/>
        <source>Zimbabwe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IP2CLoader</name>
    <message>
        <source>IP2C parser is still working, awaiting stop...</source>
        <translation type="vanished">L&apos;analitzador IP2C segueix funcionant, esperant que s&apos;aturi ...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="144"/>
        <source>IP2C update not needed.</source>
        <translation>Actualització de IP2C no necessària.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="147"/>
        <source>IP2C update errored. See log for details.</source>
        <translation>L&apos;actualització de IP2C va fallar. Veure registre per més detalls.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="150"/>
        <source>IP2C update bugged out.</source>
        <translation>L&apos;actualització de IP2C mostra errors.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="162"/>
        <source>Starting IP2C update.</source>
        <translation>Iniciant l&apos;actualització de IP2C.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="200"/>
        <source>IP2C unable to retrieve the parsing result.</source>
        <translation>IP2C incapaç de recuperar el resultat de lanàlisi.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="210"/>
        <source>Failed to read the IP2C fallback. Stopping.</source>
        <translation>Error en llegir la base de dades IP2C de recanvi. Parant.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="214"/>
        <source>Failed to read the IP2C database. Reverting...</source>
        <translation>Error en llegir la base de dades IP2C. Revertint ...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="228"/>
        <source>Trying to use the preinstalled IP2C database.</source>
        <translation>Intentant utilitzar la base de dades IP2C preinstal·lada.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="267"/>
        <source>IP2C database is being read. This may take some time.</source>
        <translation>La base de dades IP2C està sent llegida. Això pot prendre una mica de temps.</translation>
    </message>
    <message>
        <source>IP2C update must wait until parsing of current database finishes. Waiting 1 second</source>
        <translation type="vanished">L&apos;actualització de IP2C ha d&apos;esperar fins que finalitzi l&apos;anàlisi de la base de dades actual. Esperant 1 segon</translation>
    </message>
    <message>
        <source>IP2C update must wait until parsing of current database finishes. Waiting 1 second.</source>
        <translation type="vanished">L&apos;actualització de IP2C ha d&apos;esperar fins que finalitzi l&apos;anàlisi de la base de dades actual. Esperant 1 segon.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="173"/>
        <source>IP2C database finished downloading.</source>
        <translation>La base de dades IP2C s&apos;ha acabat de descarregar.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="177"/>
        <source>Unable to save IP2C database at path: %1</source>
        <translation>No es pot desar la base de dades IP2C a la ruta: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="182"/>
        <source>IP2C download has failed.</source>
        <translation>La descàrrega de IP2C ha fallat.</translation>
    </message>
    <message>
        <source>Failed to read IP2C fallback. Stopping.</source>
        <translation type="vanished">Error en llegir la base de dades IP2C de recanvi. Parant.</translation>
    </message>
    <message>
        <source>Failed to read IP2C database. Reverting...</source>
        <translation type="vanished">Error en llegir la base de dades IP2C. Revertint ...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="219"/>
        <source>IP2C revert attempt failed. Nothing to go back to.</source>
        <translation>Intent de revertir IP2C fallit. Res al que tornar.</translation>
    </message>
    <message>
        <source>Trying to use preinstalled IP2C database.</source>
        <translation type="vanished">Intentant utilitzar la base de dades IP2C preinstal·lada.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="248"/>
        <source>IP2C parsing finished.</source>
        <translation>L&apos;anàlisi de IP2C ha acabat.</translation>
    </message>
    <message>
        <source>Please wait. IP2C database is being read. This may take some time.</source>
        <translation type="vanished">Si us plau espera. La base de dades IP2C està sent llegida. Això pot prendre una mica de temps.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="126"/>
        <source>Did not find any IP2C database. IP2C functionality will be disabled.</source>
        <translation>No s&apos;ha trobat cap base de dades IP2C. La funcionalitat de IP2C estarà desactivada.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="127"/>
        <source>You may install an IP2C database from the &quot;File&quot; menu.</source>
        <translation>Pot instal·lar una base de dades IP2C des del menú &quot;Arxiu&quot;.</translation>
    </message>
</context>
<context>
    <name>IP2CParser</name>
    <message>
        <source>Parsing IP2C database: %1</source>
        <translation type="vanished">Analitzant base de dades IP2C: %1</translation>
    </message>
    <message>
        <source>Unable to open IP2C file.</source>
        <translation type="vanished">No es pot obrir el fitxer IP2C.</translation>
    </message>
    <message>
        <source>IP2C database read in %1 ms; IP ranges: %2</source>
        <translation type="vanished">Base de dades IP2C llegida a %1 ms. Rangs de IP: %2</translation>
    </message>
    <message>
        <source>IP2C database read in %1 ms. Entries read: %2</source>
        <translation type="vanished">Base de dades IP2C llegida a %1 ms. Entrades llegides: %2</translation>
    </message>
    <message>
        <source>IP2C parsing thread has finished.</source>
        <translation type="vanished">El fil de l&apos;anàlisi IP2C ha acabat.</translation>
    </message>
</context>
<context>
    <name>IP2CParserThread</name>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="63"/>
        <source>IP2C database read in %1 ms; IP ranges: %2</source>
        <translation>Base de dades IP2C llegida a %1 ms. Rangs de IP: %2</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="68"/>
        <source>Unable to open IP2C file: %1</source>
        <translation>No es pot obrir el fitxer IP2C: %1</translation>
    </message>
</context>
<context>
    <name>IP2CUpdateBox</name>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="70"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="79"/>
        <source>File is already downloaded.</source>
        <translation>L&apos;arxiu ja està descarregat.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="80"/>
        <source>File doesn&apos;t exist yet or location doesn&apos;t point to a file.</source>
        <translation>L&apos;arxiu encara no existeix o l&apos;ubicació no apunta a un arxiu.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="86"/>
        <source>Verifying checksum ...</source>
        <translation>Verificant suma de verificació ...</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="94"/>
        <source>The IP2C database file was not found. Use the &quot;Download&quot; button if you want to download the newest database.</source>
        <translation>No s&apos;ha trobat el fitxer de base de dades IP2C. Utilitzeu el botó d&apos;actualització si voleu descarregar la base de dades més recent.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="96"/>
        <source>Download</source>
        <translation>Descarregar</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="109"/>
        <source>Update available.</source>
        <translation>Actualitzacions disponibles.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="118"/>
        <source>Database status check failed. See the log for details.</source>
        <translation>Error en la comprovació de l&apos;estat de la base de dades. Veure registre per més detalls.</translation>
    </message>
    <message>
        <source>IP2C database file was not found. Use the update button if you want to download the newest database.</source>
        <translation type="vanished">No s&apos;ha trobat el fitxer de base de dades IP2C. Utilitzeu el botó d&apos;actualització si voleu descarregar la base de dades més recent.</translation>
    </message>
    <message>
        <source>Update required.</source>
        <translation type="vanished">Actualització necessària.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="113"/>
        <source>Database is up-to-date.</source>
        <translation>La base de dades està actualitzada.</translation>
    </message>
    <message>
        <source>Database status check failed. See log for details.</source>
        <translation type="vanished">Error en la comprovació de l&apos;estat de la base de dades. Veure registre per més detalls.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="122"/>
        <source>Unhandled update check status.</source>
        <translation>Estat de comprovació d&apos;actualització no controlat.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="20"/>
        <source>Doomseeker - Update IP2C</source>
        <translation>Doomseeker - Actualització de IP2C</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="26"/>
        <source>IP2C file location:</source>
        <translation>Ubicació del fitxer IP2C:</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="186"/>
        <source>The downloaded database will be placed in the following location:</source>
        <translation>La base de dades descarregada estarà ubicada a la següent ruta:</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="250"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;The IP2C update is performed in the background. Doomseeker will notify you of download progress through a progress bar and log messages.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Recordeu que l&apos;actualització de IP2C es realitza en segon pla. Doomseeker li notificarà del progrés de la descàrrega a través d&apos;una barra de progrés i missatges al registre.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="127"/>
        <source>Update</source>
        <translation>Actualització</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>IP2CUpdater</name>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="67"/>
        <source>IP2C checksum check network error: %1</source>
        <translation>Error de xarxa en verificar la suma de verificació de IP2C: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="87"/>
        <source>Comparing IP2C hashes: local = %1, remote = %2</source>
        <translation>Comparació dels valors hash de IP2C: local = %1, remot = %2</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="91"/>
        <source>IP2C update needed.</source>
        <translation>Actualització de IP2C necessària.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="207"/>
        <source>Checking if IP2C database at &apos;%1&apos; needs updating.</source>
        <translation>Comprovant si la base de dades IP2C a &apos;%1&apos; necessita una actualització.</translation>
    </message>
</context>
<context>
    <name>IRCChannelAdapter</name>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="157"/>
        <source>%1 is now known as %2</source>
        <translation>%1 és conegut com a%2</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="166"/>
        <source>User %1 [%2] has joined the channel.</source>
        <translation>L&apos;usuari %1 [%2] s&apos;ha unit al canal.</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="183"/>
        <source>User %1 has left the channel. (PART: %2)</source>
        <translation>L&apos;usuari %1 ha sortit del canal. (PART: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="188"/>
        <source>Connection for user %1 has been killed. (KILL: %2)</source>
        <translation>La connexió per a l&apos;usuari %1 ha estat eliminada. (KILL: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="193"/>
        <source>User %1 has quit the network. (QUIT: %2)</source>
        <translation>L&apos;usuari %1 ha abandonat la xarxa. (QUIT: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="198"/>
        <source>Unknown quit type from user %1.</source>
        <translation>Tipus d&apos;abandonament desconegut de l&apos;usuari %1.</translation>
    </message>
</context>
<context>
    <name>IRCClient</name>
    <message>
        <location filename="../irc/ircclient.cpp" line="45"/>
        <source>IRC: Connecting: %1:%2</source>
        <translation>IRC: Conectant: %1:%2</translation>
    </message>
</context>
<context>
    <name>IRCConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="35"/>
        <source>Doomseeker - IRC Options</source>
        <translation>Doomseeker - Opcions IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="41"/>
        <source>Settings saved!</source>
        <translation>Configuració guardada!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="43"/>
        <source>Settings save failed!</source>
        <translation>Guardat fallit de la configuració!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="81"/>
        <source>Config validation</source>
        <translation>Validació de configuració</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="81"/>
        <source>You have chosen one or more networks for autojoin startup but you have not defined any nickname. Please define it now.</source>
        <translation>Ha triat una o més xarxes per a l&apos;inici automàtic, però no ha definit cap àlies. Si us plau, defineixla ara.</translation>
    </message>
</context>
<context>
    <name>IRCCtcpParser</name>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="85"/>
        <source>CTCP %1: [%2] %3 %4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="89"/>
        <source>%1 %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="147"/>
        <source>REQUEST</source>
        <translation>SOL·LICITUD</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="149"/>
        <source>REPLY</source>
        <translation>RESPOSTA</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="151"/>
        <source>????</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IRCDelayedOperationIgnore</name>
    <message>
        <location filename="../irc/ops/ircdelayedoperationignore.cpp" line="85"/>
        <source>Ignore user %1 (username=%2) on network %3:</source>
        <translation>Ignorar usuari %1 (username=%2) en xarxa %3:</translation>
    </message>
    <message>
        <location filename="../irc/ops/ircdelayedoperationignore.cpp" line="87"/>
        <source>IRC - Ignore user</source>
        <translation>IRC - Ignorar usuari</translation>
    </message>
</context>
<context>
    <name>IRCDock</name>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="170"/>
        <source>Connect</source>
        <translation>Connectar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="171"/>
        <location filename="../gui/irc/ircdock.cpp" line="261"/>
        <source>Open chat window</source>
        <translation>Obrir la finestra de xat</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="256"/>
        <location filename="../gui/irc/ircdock.cpp" line="258"/>
        <source>Doomseeker IRC - Open chat window</source>
        <translation>Doomseeker IRC - Obrir finestra de xat</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="256"/>
        <source>Cannot obtain network connection adapter.</source>
        <translation>No es pot obtenir l&apos;adaptador de connexió de xarxa.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="258"/>
        <source>You are not connected to this network.</source>
        <translation>No està connectat a aquesta xarxa.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="261"/>
        <source>Specify a channel or user name:</source>
        <translation>Especifiqueu una cadena o nom d&apos;usuari:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.ui" line="26"/>
        <source>IRC</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IRCDockTabContents</name>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="360"/>
        <source>&lt;&lt;&lt;DATE&gt;&gt;&gt; Date on this computer changes to %1</source>
        <translation>&lt;&lt;&lt;FECHA&gt;&gt;&gt; La data en aquest ordinador canvia a %1</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="454"/>
        <source>Failed to create chat log directory:
&apos;%1&apos;</source>
        <translation>Error en crear un directori de registre de xat:
&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="460"/>
        <source>&lt;&lt;&lt;DATE&gt;&gt;&gt; Chat log started on %1

</source>
        <translation>&lt;&lt;&lt;DATA&gt;&gt;&gt; Registre de xat iniciat a %1

</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="487"/>
        <source>Error: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="559"/>
        <source>---- All lines above were loaded from log ----</source>
        <translation>---- Totes les línies de dalt han estat carregades del registre ----</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="711"/>
        <source>Manage ignores</source>
        <translation>Administrar ignorats</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="717"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="931"/>
        <source>Whois</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="718"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="933"/>
        <source>CTCP Ping</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="719"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="932"/>
        <source>CTCP Time</source>
        <translation>CTCP Temps</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="720"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="934"/>
        <source>CTCP Version</source>
        <translation>CTCP Versió</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="721"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="943"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="842"/>
        <source>Ban user</source>
        <translation>Banejar usuari</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="842"/>
        <source>Input reason for banning user %1 from channel %2</source>
        <translation>Introduïu el motiu per banear a l&apos;usuari %1 de la cadena %2</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="866"/>
        <source>Kick user</source>
        <translation>Fer fora usuari</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="866"/>
        <source>Input reason for kicking user %1 from channel %2</source>
        <translation>Introduïu el motiu per fer fora a l&apos;usuari %1 de la cadena %2</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="929"/>
        <source>Open chat window</source>
        <translation>Obrir la finestra de xat</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="936"/>
        <source>Op</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="937"/>
        <source>Deop</source>
        <translation>DesOP</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="938"/>
        <source>Half op</source>
        <translation>Casi OP</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="939"/>
        <source>De half op</source>
        <translation>Des Casi OP</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="940"/>
        <source>Voice</source>
        <translation>Desilenciar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="941"/>
        <source>Devoice</source>
        <translation>Silenciar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="944"/>
        <source>Kick</source>
        <translation>Fer fora</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="945"/>
        <source>Ban</source>
        <translation>Banejar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="69"/>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="76"/>
        <source>Do not scroll text area</source>
        <translation>No moure la àrea del text</translation>
    </message>
</context>
<context>
    <name>IRCIgnoresManager</name>
    <message>
        <location filename="../gui/irc/ircignoresmanager.ui" line="14"/>
        <source>IRC - ignores manager</source>
        <translation>IRC - administrador d&apos;ignorats</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircignoresmanager.ui" line="30"/>
        <source>Remove selected</source>
        <translation>Suprimir seleccionats</translation>
    </message>
    <message>
        <source>Delete selected</source>
        <translation type="vanished">Eliminar seleccionat</translation>
    </message>
</context>
<context>
    <name>IRCNetworkAdapter</name>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="196"/>
        <source>You are not connected to the network.</source>
        <translation>No està connectat a la xarxa.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="206"/>
        <source>Insufficient parameters.</source>
        <translation>Paràmetres insuficients.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="210"/>
        <source>This is a server window. All commands must be prepended with a &apos;/&apos; character.</source>
        <translation>Aquesta és una finestra del servidor. Totes les comandes han d&apos;anar precedits d&apos;un caràcter &apos;/&apos;.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="214"/>
        <source>Attempted to send empty message.</source>
        <translation>Intent d&apos;enviar un missatge en blanc.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="218"/>
        <source>Command is too long.</source>
        <translation>La comanda és massa llarg.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="222"/>
        <source>Not a chat window.</source>
        <translation>No és una finestra de xat.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="231"/>
        <source>Quit</source>
        <translation>Sortir</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="318"/>
        <source>IRC: Successfully registered on network %1 [%2:%3]</source>
        <translation>IRC: Es va registrar correctament a la xarxa %1 [%2:%3]</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="358"/>
        <source>Invalid parse result for message: %1</source>
        <translation>Resultat d&apos;anàlisi invàlid per mesage: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="402"/>
        <source>You have been kicked from channel %1 by %2 (Reason: %3)</source>
        <translation>Has estat expulsat del canal %1 per %2 (Raó: %3)</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="407"/>
        <source>%1 was kicked by %2 (%3)</source>
        <translation>%1 ha estat expulsat per %2 (%3)</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="459"/>
        <source>%1 sets mode: %2</source>
        <translation>%1 posa la manera: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="482"/>
        <source>Nickname %1 is already taken.</source>
        <translation>L&apos;àlies %1 ja està en ús.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="488"/>
        <source>Both nickname and alternate nickname are taken on this network.</source>
        <translation>Tant l&apos;àlies com l&apos;àlies alternatiu estan en ús en aquesta xarxa.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="490"/>
        <source>No alternate nickname specified.</source>
        <translation>No s&apos;ha especificat un àlies alternatiu.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="495"/>
        <source>Using alternate nickname %1 to join.</source>
        <translation>Utilitzant l&apos;àlies alternatiu %1 per a unir-se.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="504"/>
        <source>User %1 is not logged in.</source>
        <translation>L&apos;usuari %1 no ha iniciat sessió.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="526"/>
        <source>IRC parse error: %1</source>
        <translation>Error d&apos;anàlisi IRC: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="549"/>
        <source>FROM %1: %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="631"/>
        <source>Updated own nickname to %1.</source>
        <translation>Actualitzat el propi sobrenom a %1.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="676"/>
        <source>Last activity of user %1 was %2 ago.</source>
        <translation>L&apos;última activitat de l&apos;usuari %1 va ser fa %2.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="693"/>
        <source>%1 joined the network on %2</source>
        <translation>%1 es va unir a la xarxa a %2</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="706"/>
        <source>You left channel %1.</source>
        <translation>Has sortit del canal %1.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="736"/>
        <source>Ping to user %1: %2ms</source>
        <translation>Ping a l&apos;usuari %1: %2ms</translation>
    </message>
</context>
<context>
    <name>IRCNetworkSelectionBox</name>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="90"/>
        <source>Doomseeker - edit IRC network</source>
        <translation>Doomseeker - edita la xarxa IRC</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="91"/>
        <source>Cannot edit as no valid network is selected.</source>
        <translation>No es pot editar ja que no està seleccionada una xarxa vàlida.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="209"/>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="213"/>
        <source>Doomseeker - remove IRC network</source>
        <translation>Doomseeker - suprimeix la xarxa IRC0</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="210"/>
        <source>Cannot remove as no valid network is selected.</source>
        <translation>No es pot suprimir ja que no està seleccionada una xarxa vàlida.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="214"/>
        <source>Are you sure you wish to remove network &apos;%1&apos;?</source>
        <translation>Segur que vols suprimir la xarxa &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="240"/>
        <source>IRC connection error</source>
        <translation>Error de connexió IRC</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="245"/>
        <source>You must specify a nick.</source>
        <translation>Heu d&apos;especificar un àlies.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="251"/>
        <source>You must specify a network address.</source>
        <translation>Cal especificar una direcció de xarxa.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="14"/>
        <source>IRC - Connect to Network</source>
        <translation>IRC - Connectar a una xarxa</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="25"/>
        <source>Nick:</source>
        <translation>Alias:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="32"/>
        <source>DSTest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="39"/>
        <source>Alternate nick:</source>
        <translation>Alias alternatiu:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="46"/>
        <source>DSTestA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="53"/>
        <source>Real name:</source>
        <translation>Nom real:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="60"/>
        <source>Doomseeker Tester</source>
        <translation></translation>
    </message>
    <message>
        <source>User name</source>
        <translation type="vanished">Nom d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="67"/>
        <source>Username:</source>
        <translation>Nom d&apos;usuari:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="74"/>
        <source>Sent to the IRC network during login. If not specified, defaults to nick.</source>
        <translation>Enviat a la xarxa IRC durant l&apos;inici de sessió. Si no s&apos;especifica, el valor predeterminat és nick.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="77"/>
        <source>DSTestUser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="116"/>
        <source>Add network</source>
        <translation>Afegir xarxa</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="133"/>
        <source>Edit network</source>
        <translation>Edita xarxa</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="150"/>
        <source>Remove network</source>
        <translation>Suprimir xarxa</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="170"/>
        <source>Server address:</source>
        <translation>Direcció del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="177"/>
        <source>74.207.247.18</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="187"/>
        <source>Port:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="210"/>
        <source>Password:</source>
        <translation>Contrasenya:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="226"/>
        <source>Hide</source>
        <translation>Amagar</translation>
    </message>
</context>
<context>
    <name>IRCPrivAdapter</name>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="37"/>
        <source>This user changed nickname from %1 to %2</source>
        <translation>Aquest usuari ha canviat el seu Àlies de %1 a %2</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="61"/>
        <source>This user connection has been killed. (KILL: %1)</source>
        <translation>Aquesta connexió d&apos;usuari ha estat eliminada. (KILL: %1)</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="65"/>
        <source>This user has left the network. (QUIT: %1)</source>
        <translation>Aquest usuari ha abandonat la xarxa. (QUIT: %1)</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="69"/>
        <source>Unhandled IRCQuitType in IRCPrivAdapter::userLeaves()</source>
        <translation>IRCQuitType no controlat en IRCPrivAdapter::userLeaves()</translation>
    </message>
</context>
<context>
    <name>IRCResponseParser</name>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="138"/>
        <source>User %1 is away: %2</source>
        <translation>L&apos;usuari %1 està absent: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="197"/>
        <source>%1 is on channels: %2</source>
        <translation>%1 està en els canals: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="225"/>
        <source>Topic: %1</source>
        <translation>Tema: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="237"/>
        <source>Topic set by %1 on %2.</source>
        <translation>Tema establert per %1 el %2.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="247"/>
        <source>URL: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="256"/>
        <source>Created time: %1</source>
        <translation>Temps creat: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="278"/>
        <source>RPLNamReply: Received names list but no channel name.</source>
        <translation>RPLNamReply: Llista de noms rebuts però no el nom de canal.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="368"/>
        <source>Erroneous nickname: %1</source>
        <translation>Sobrenom erroni: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="370"/>
        <source> (%1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="404"/>
        <source>%1: %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="511"/>
        <source>New topic set by user %1:
%2</source>
        <translation>Nou tema establert per l&apos;usuari%1:
%2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="530"/>
        <source>IRCResponseParser: Type &apos;%1&apos; was recognized but there has been no parse code implemented for it.(yep, it&apos;s a bug in the application!)</source>
        <translation>IRCResponseParser: S&apos;ha reconegut el tipus &apos;%1&apos; però no s&apos;ha implementat cap codi d&apos;anàlisi per a aquest. (Sip, això és un error en l&apos;aplicació!)</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="589"/>
        <source>[%1]: %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="591"/>
        <source>Type &apos;%1&apos; was incorrectly parsed in PrivMsg block.</source>
        <translation>El tipus &apos;%1&apos; es va analitzar incorrectament en el bloc PRIVMSG.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="606"/>
        <source>MODE flags string from IRC server are incorrect: &quot;%1&quot;. Information for channel &quot;%2&quot; might not be correct anymore.</source>
        <translation>Les cadenes d&apos;indicadors MODE del servidor IRC són incorrectes: &quot;%1&quot;. És possible que la informació per al canal &quot;%2&quot; ja no sigui correcta.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="638"/>
        <source>IRCResponseParser::parseUserModeMessage(): wrong FlagMode. Information for channel &quot;%2&quot; might not be correct anymore.</source>
        <translation>IRCResponseParser::parseUserModeMessage(): FlagMode incorrecte. És possible que la informació per al canal &quot;%2&quot; ja no sigui correcta.</translation>
    </message>
</context>
<context>
    <name>IRCSocketSignalsAdapter</name>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="752"/>
        <source>Connected. Sending registration messages.</source>
        <translation>Connectat. Enviant missatges de registre.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="772"/>
        <source>IRC: Disconnected from network %1</source>
        <translation>IRC: Desconnectat de la xarxa %1</translation>
    </message>
</context>
<context>
    <name>ImportantMessagesWidget</name>
    <message>
        <location filename="../gui/widgets/importantmessageswidget.ui" line="40"/>
        <source>Clear</source>
        <translation>Buidar</translation>
    </message>
</context>
<context>
    <name>IwadAndWadsPickerDialog</name>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.cpp" line="65"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - cercar executable</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Diàleg</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="22"/>
        <source>Executable:</source>
        <translation>Executable:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="39"/>
        <source>IWAD:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="61"/>
        <source>Browse</source>
        <translation>Cercar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="72"/>
        <source>Additional WADs and files:</source>
        <translation>WADs i arxius addicionals:</translation>
    </message>
</context>
<context>
    <name>IwadPicker</name>
    <message>
        <location filename="../gui/createserver/iwadpicker.cpp" line="74"/>
        <source>Doomseeker - select IWAD</source>
        <translation>Doomseeker - selecciona IWAD</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadpicker.ui" line="42"/>
        <source>Browse</source>
        <translation>Cercar</translation>
    </message>
</context>
<context>
    <name>JoinCommandLineBuilder</name>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="116"/>
        <source>Demo set for recording, but couldn&apos;t prepare its save path.

%1</source>
        <translation>Demo llesta per a gravar, però no s&apos;ha pogut preparar la ruta de guardat.

%1</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="133"/>
        <source>This server is still refreshing.
Please wait until it is finished.</source>
        <translation>Aquest servidor encara s&apos;està refrescant.
Espereu fins que acabi.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="134"/>
        <source>Attempted to obtain a join command line for a &quot;%1&quot; server that is under refresh.</source>
        <translation>S&apos;ha intentat obtenir una línia de comanda per unir-se a un servidor &quot;%1&quot; que s&apos;està refrescant.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="141"/>
        <source>Data for this server is not available.
Operation failed.</source>
        <translation>La informació per a aquest servidor no està disponible.
Operació fallida.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="142"/>
        <source>Attempted to obtain a join command line for an unknown server &quot;%1&quot;</source>
        <translation>S&apos;ha intentat obtenir una línia de comanda per unir-se a un servidor desconegut &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="183"/>
        <source>Unknown error.</source>
        <translation>Error desconegut.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="187"/>
        <source>Error when obtaining join parameters for server &quot;%1&quot;, game &quot;%2&quot;: %3</source>
        <translation>Error en obtenir els paràmetres per unir-se al servidor &quot;%1&quot;, joc &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="358"/>
        <source>Game installation failure</source>
        <translation>Error d&apos;instal·lació del joc</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../fileutils.cpp" line="175"/>
        <source>Failed to remove: %1</source>
        <translation>Error al suprimir: %1</translation>
    </message>
</context>
<context>
    <name>LogDirectoryPicker</name>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.cpp" line="76"/>
        <source>Doomseeker - select Log path</source>
        <translation>Doomseeker - seleccioneu la ruta de registre</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="35"/>
        <source>This path could not be found. It will not be used.</source>
        <translation>No s’ha trobat la ruta. No s’utilitzarà.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="51"/>
        <source>Browse</source>
        <translation>Cercar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="64"/>
        <source>Enabled</source>
        <translation>Habilitat</translation>
    </message>
</context>
<context>
    <name>LogDock</name>
    <message>
        <location filename="../gui/logdock.ui" line="20"/>
        <source>Log</source>
        <translation>Registre</translation>
    </message>
    <message>
        <location filename="../gui/logdock.ui" line="67"/>
        <source>Copy all to clipboard</source>
        <translation>Copia-ho tot al porta-retalls</translation>
    </message>
    <message>
        <location filename="../gui/logdock.ui" line="74"/>
        <source>Clear</source>
        <translation>Buidar</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../main.cpp" line="240"/>
        <source>Init finished.</source>
        <translation>Init finalitzat.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="255"/>
        <source>Doomseeker - Updates Install Failure</source>
        <translation>Doomseeker - Instal·lació d&apos;actualitzacions falles</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="301"/>
        <source>Failed to open file &apos;%1&apos;.</source>
        <translation>Error en obrir el fitxer &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="309"/>
        <source>Failed to open stdout.</source>
        <translation>Error en obrir stdout.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="319"/>
        <source>Dumping version info to file in JSON format.</source>
        <translation>Dipositant la informació de la versió a l&apos;arxiu en format JSON.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="331"/>
        <source>Preparing GUI.</source>
        <translation>Preparant GUI.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="344"/>
        <source>Starting Create Game box.</source>
        <translation>Iniciant la finestra de Creació del Joc.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="352"/>
        <source>Starting RCon client.</source>
        <translation>Iniciant el client RCon.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="367"/>
        <source>None of the currently loaded game plugins supports RCon.</source>
        <translation>Cap dels complements carregats actualment admet RCon.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="369"/>
        <source>Doomseeker RCon</source>
        <translation></translation>
    </message>
    <message>
        <source>Couldn&apos;t find specified plugin: </source>
        <translation type="vanished">No s&apos;ha pogut trobar el complement especificat: </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="383"/>
        <source>Couldn&apos;t find the specified plugin: </source>
        <translation>No s&apos;ha pogut trobar el complement especificat: </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="393"/>
        <source>Plugin does not support RCon.</source>
        <translation>El complement no és compatible amb RCon.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="410"/>
        <source>Loading extra CA certificates from &apos;%1&apos;.</source>
        <translation>Carregant certificats CA addicionals de &apos;%1&apos;.</translation>
    </message>
    <message numerus="yes">
        <location filename="../main.cpp" line="415"/>
        <source>Appending %n extra CA certificate(s).</source>
        <translation>
            <numerusform>Annexant un certificat CA addicional.</numerusform>
            <numerusform>Annexant %n certificats CA addicionals.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="427"/>
        <source>Running in the portable mode. Forcing current directory to: %1</source>
        <translation>Executant en mode portable. Forçant el directori actual a: %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="431"/>
        <source>Forcing the current directory failed! Path detection may misbehave.</source>
        <translation>El canvi forçat de directori ha fallat! La detecció de rutes pot fallar.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="449"/>
        <source>Doomseeker will not run because some directories cannot be used properly.
</source>
        <translation>Doomseeker no s&apos;executarà perquè alguns directoris no es poden fer servir correctament.
</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="457"/>
        <source>Doomseeker startup error</source>
        <translation>Error d&apos;inici de Doomseeker</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="481"/>
        <source>Initializing IP2C database.</source>
        <translation>Inicialitzant la base de dades IP2C.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="489"/>
        <source>Initializing IRC configuration file.</source>
        <translation>Inicialitzant el fitxer de configuració d&apos;IRC.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="507"/>
        <source>Loading translations definitions</source>
        <translation>Carregant definicions de traducció</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="515"/>
        <source>Loading translation &quot;%1&quot;.</source>
        <translation>Carregant la traducció &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="519"/>
        <source>Translation loaded.</source>
        <translation>Traducció carregada.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="523"/>
        <source>Failed to load translation.</source>
        <translation>Error en carregar la traducció.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="530"/>
        <source>Initializing configuration file.</source>
        <translation>Inicialitzant fitxer de configuració.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="539"/>
        <source>Could not get an access to the settings directory. Configuration will not be saved.</source>
        <translation>No s&apos;ha pogut obtenir accés al directori de configuracions. La configuració no es guardarà.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="558"/>
        <source>Initializing passwords configuration file.</source>
        <translation>Inicialitzant el fitxer de configuració de contrasenyes.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="571"/>
        <source>Initializing configuration for plugins.</source>
        <translation>Inicialitzant la configuració per a complements.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="725"/>
        <source>doomseeker: `--connect`, `--create-game` and `--rcon` are mutually exclusive</source>
        <translation>doomseeker: `--connect`,` --create-game` i `--rcon` s&apos;exclouen entre si</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="738"/>
        <source>Starting refreshing thread.</source>
        <translation>Iniciant fil de refresc.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/mainwindow.cpp" line="239"/>
        <source>&amp;Buddies</source>
        <translation>&amp;Companys</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="240"/>
        <source>CTRL+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="412"/>
        <source>Doomseeker - Auto Update</source>
        <translation>Doomseeker - Actualització automàtica</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="413"/>
        <source>Update is already in progress.</source>
        <translation>L&apos;actualització ja està en progrés.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="422"/>
        <source>Removing old update packages from local temporary space.</source>
        <translation>Eliminació de paquets d&apos;actualització antics de l&apos;espai temporal local.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="427"/>
        <source>Checking for updates...</source>
        <translation>Comprovant actualitzacions...</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="636"/>
        <source>All WADs found</source>
        <translation>Tots els WADs trobats</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="636"/>
        <source>All of the WADs used by this server are present.</source>
        <translation>Tots els WAD utilitzats per aquest servidor estan presents.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="670"/>
        <source>Doomseeker needs to be restarted for some changes to be applied.</source>
        <translation>Doomseeker necessita reiniciar perquè alguns dels canvis s&apos;apliquin.</translation>
    </message>
    <message>
        <source>Doomseeker is unable to proceed with the refresh operation because the following problem has occured:

</source>
        <translation type="vanished">Doomseeker no pot continuar amb l&apos;operació d&apos;actualització perquè s&apos;ha produït el següent problema:

</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="717"/>
        <source>Plugins are missing from the &quot;engines/&quot; directory.</source>
        <translation>Els complements falten en el directori &quot;engines/&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="719"/>
        <source>No master servers are enabled in the &quot;Query&quot; menu.</source>
        <translation>No hi ha servidors mestres habilitats en el menú &quot;Consulta&quot;.</translation>
    </message>
    <message>
        <source>Unknown error occured.</source>
        <translation type="vanished">S&apos;ha produït un error desconegut.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="713"/>
        <source>Doomseeker is unable to proceed with the refresh operation because the following problem has occurred:

</source>
        <translation>Doomseeker no pot continuar amb l&apos;operació d&apos;actualització perquè s&apos;ha produït el següent problema:

</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="721"/>
        <source>Unknown error occurred.</source>
        <translation>S&apos;ha produït un error desconegut.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="724"/>
        <source>Doomseeker - refresh problem</source>
        <translation>Doomseeker - problema d&apos;actualització</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="730"/>
        <source>Total refresh initialized!</source>
        <translation>Actualització total inicialitzada!</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="740"/>
        <source>Warning: No master servers were enabled for this refresh. Check your Query menu or &quot;engines/&quot; directory.</source>
        <translation>Avís: No s&apos;han habilitat servidors mestres per a aquesta actualització. Verifiqui el seu menú de consulta o directori &quot;engines/&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="803"/>
        <source>Auto Updater:</source>
        <translation>Actualitzador automàtic:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="818"/>
        <source>Abort update.</source>
        <translation>Cancel·lar actualització.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="830"/>
        <source>IP2C Update</source>
        <translation>Actualització de IP2C</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="841"/>
        <source>&amp;IRC</source>
        <translation>&amp;IRC</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="842"/>
        <source>CTRL+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="857"/>
        <source>&amp;Log</source>
        <translation>&amp;Registre</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="858"/>
        <source>CTRL+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="874"/>
        <source>Servers</source>
        <translation>Servidors</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="887"/>
        <source>Server &amp;details</source>
        <translation>&amp;Detalls Servidor</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="888"/>
        <source>CTRL+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1001"/>
        <source>Master server for %1: %2</source>
        <translation>Servidor mestre per a %1:%2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1005"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1014"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1045"/>
        <location filename="../gui/mainwindow.cpp" line="1053"/>
        <source>Help error</source>
        <translation>Error en ajuda</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1045"/>
        <source>No help found.</source>
        <translation>No s&apos;ha trobat ajuda.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1053"/>
        <source>Failed to open URL:
%1</source>
        <translation>Error en obrir la URL:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1136"/>
        <source>Welcome to Doomseeker</source>
        <translation>Benvingut a Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1137"/>
        <source>Before you start browsing for servers, please ensure that Doomseeker is properly configured.</source>
        <translation>Abans de començar a buscar servidors, assegureu-vos que Doomseeker estigui configurat correctament.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1154"/>
        <source>Program update detection &amp; download finished with status: [%1] %2</source>
        <translation>La detecció i descàrrega de l&apos;actualització del programa va acabar amb l&apos;estat: [%1] %2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1164"/>
        <source>Updates will be installed on next program start.</source>
        <translation>Les actualitzacions s&apos;instal·laran en el següent inici del programa.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1170"/>
        <source>Update channel was changed during update process. Discarding update.</source>
        <translation>El canal d&apos;actualització ha canviat durant el procés d&apos;actualització. Descartant l&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1209"/>
        <source>Query on startup warning: No master servers are enabled in the Query menu.</source>
        <translation>Notes de consulta a l&apos;inici: No hi ha servidors mestres habilitats en el menú Consulta.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1226"/>
        <source>Doomseeker was unable to find any plugin libraries.
Although the application will still work it will not be possible to fetch any server info or launch any game.

Please check if there are any files in &quot;engines/&quot; directory.
To fix this problem you may try downloading Doomseeker again from the site specified in the Help|About box and reinstalling Doomseeker.</source>
        <translation>Doomseeker no ha pogut trobar cap biblioteca de complements.
Encara que l&apos;aplicació seguirà funcionant, no serà possible obtenir cap informació de servidors o iniciar cap joc.

Comproveu si hi ha arxius en el directori &quot;engines/&quot;.
Per solucionar aquest problema, podria descarregar Doomseeker novament des del lloc especificat en el quadre Ajuda | Sobre i tornar a instal·lar Doomseeker.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1233"/>
        <source>Doomseeker critical error</source>
        <translation>Error crític de Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1273"/>
        <source>Querying...</source>
        <translation>Consultant...</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1282"/>
        <source>Done</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1364"/>
        <source>Main Toolbar</source>
        <translation>Barra d&apos;eines principal</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1369"/>
        <source>Get Servers</source>
        <translation>Obtenir servidors</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1403"/>
        <source>Search:</source>
        <translation>Buscar:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1474"/>
        <source>Doomseeker - show join command line</source>
        <translation>Doomseeker - mostrar comanda per unir-se</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1475"/>
        <source>Failed to build the command line:
%1</source>
        <translation>La línia d&apos;ordres no es pot construir:
%1</translation>
    </message>
    <message>
        <source>Command line cannot be built:
%1</source>
        <translation type="vanished">La línia d&apos;ordres no es pot construir:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1489"/>
        <source>Update installation problem:
%1</source>
        <translation>Problema d&apos;instal·lació d&apos;actualització:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1492"/>
        <source>Update installation failed.</source>
        <translation>Error en instal·lar l&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1493"/>
        <location filename="../gui/mainwindow.cpp" line="1502"/>
        <source>Doomseeker - Auto Update problem</source>
        <translation>Doomseeker - Problema d&apos;actualització automàtica</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1494"/>
        <source>%1

Remaining updates have been discarded.</source>
        <translation>%1

Les actualitzacions restants s&apos;han descartat.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1501"/>
        <source>Update install problem:
%1

Remaining updates have been discarded.</source>
        <translation>Problema de l&apos;actualització:
%1

Les actualitzacions restants s&apos;han descartat.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1584"/>
        <source>Generic servers: %1
</source>
        <translation>Servidors genèrics: %1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1585"/>
        <source>Custom servers: %1
</source>
        <translation>Servidors personalitzats:%1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1586"/>
        <source>LAN servers: %1
</source>
        <translation>Servidors LAN:%1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1587"/>
        <source>Human players: %1</source>
        <translation>Jugadors humans: %1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1599"/>
        <source>Finished refreshing. Servers on the list: %1 (+%2 custom, +%3 LAN). Players: %4.</source>
        <translation>Actualització finalitzada. Servidors a la llista:%1 (+%2 personalitzats, +%3 LAN). Jugadors: %4.</translation>
    </message>
</context>
<context>
    <name>MainWindowWnd</name>
    <message>
        <location filename="../gui/mainwindow.ui" line="14"/>
        <source>Doomseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="350"/>
        <source>&amp;Configure</source>
        <translation>&amp;Configurar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="353"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="361"/>
        <source>&amp;About</source>
        <translation>&amp;Sobre</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="369"/>
        <source>&amp;Quit</source>
        <translation>&amp;Sortir</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="372"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="383"/>
        <source>Server Info</source>
        <translation>Informació del servidor</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="386"/>
        <source>I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="395"/>
        <source>&amp;Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="398"/>
        <source>Ctrl+Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="406"/>
        <source>&amp;Buddies</source>
        <translation>&amp;Companys</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="409"/>
        <source>Ctrl+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="418"/>
        <source>&amp;Create game</source>
        <translation>&amp;Crear una partida</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="421"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="429"/>
        <source>&amp;Log</source>
        <translation>&amp;Registre</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="432"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="437"/>
        <source>&amp;Help (Online)</source>
        <translation>&amp;Ajuda (Online)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="440"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="449"/>
        <source>&amp;Update IP2C</source>
        <translation>&amp;Actualitzar IP2C</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="457"/>
        <source>&amp;IRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="460"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="465"/>
        <source>&amp;IRC options</source>
        <translation>Opcions &amp;IRC</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="468"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="473"/>
        <source>About &amp;Qt</source>
        <translation>Sobre &amp;Qt</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="488"/>
        <source>&amp;Record demo</source>
        <translation>&amp;Gravar demo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="497"/>
        <source>&amp;Demo manager</source>
        <translation>&amp;Gestor de &amp;demos</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="500"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="509"/>
        <source>&amp;Check for updates</source>
        <translation>&amp;Cerca actualitzacions</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="518"/>
        <source>&amp;Program args</source>
        <translation>&amp;Arguments del programa</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="523"/>
        <source>Install &amp;Freedoom</source>
        <translation>Instal·lar &amp;Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="78"/>
        <source>New updates are available:</source>
        <translation>Noves actualitzacions estan disponibles:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="97"/>
        <source>Download &amp;&amp; Install</source>
        <translation>Descarregar i Instal·lar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="120"/>
        <source>Discard</source>
        <translation>Descarta</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="185"/>
        <source>Updates have been downloaded:</source>
        <translation>Les actualitzacions han estat descarregades:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="204"/>
        <source>Restart &amp;&amp; Install now</source>
        <translation>Reiniciar i Instal·lar ara</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="223"/>
        <source>Restart &amp;&amp; Install later</source>
        <translation>Reiniciar i Instal·lar més tard</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="252"/>
        <source>Server filter is applied</source>
        <translation>Els filtres de servidor estan aplicats</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="297"/>
        <source>&amp;Options</source>
        <translation>&amp;Opcions</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="304"/>
        <source>&amp;Help</source>
        <translation>&amp;Ajuda</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="315"/>
        <source>&amp;File</source>
        <translation>&amp;Archiu</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="329"/>
        <source>&amp;View</source>
        <translation>&amp;Veure</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="334"/>
        <source>&amp;Query</source>
        <translation>&amp;Consulta</translation>
    </message>
</context>
<context>
    <name>MapListPanel</name>
    <message>
        <location filename="../gui/createserver/maplistpanel.cpp" line="57"/>
        <source>The current map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>El mapa actual no està present a la llista de mapes. El joc pot comportar-se de forma estranya.</translation>
    </message>
    <message>
        <source>Add from loaded wads</source>
        <translation type="vanished">Afegir des dels WADs carregats</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="51"/>
        <source>Add from loaded WADs</source>
        <translation>Afegir des dels WADs carregats</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="64"/>
        <source>Add</source>
        <translation>Afegir</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="75"/>
        <source>Remove</source>
        <translation>Suprimir</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="91"/>
        <source>Random map rotation</source>
        <translation>Rotació aleatòria del mapa</translation>
    </message>
</context>
<context>
    <name>MapListSelector</name>
    <message>
        <location filename="../gui/createserver/maplistselector.cpp" line="188"/>
        <source>Invert selection</source>
        <translation>Inverteix la selecció</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="26"/>
        <source>Doomseeker - Select Maps</source>
        <translation>Doomseeker - Seleccioni Mapas</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="35"/>
        <source>All selected maps will get added to the map list</source>
        <translation>Tots els mapes seleccionats s&apos;afegiran a la llista de mapes</translation>
    </message>
    <message>
        <source>Reading wads...</source>
        <translation type="vanished">LLegint WADs...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="71"/>
        <source>Reading WADs...</source>
        <translation>LLegint WADs...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="86"/>
        <source>Select All</source>
        <translation>Seleccionar tot</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="121"/>
        <source>Add</source>
        <translation>Afegeix</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>MasterClient</name>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="108"/>
        <source>%1 %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="156"/>
        <source>Could not fetch a new server list from the master because not enough time has passed.</source>
        <translation>No s&apos;ha pogut obtenir una nova llista de servidors de màster perquè no ha passat prou temps.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="161"/>
        <source>Bad response from master server.</source>
        <translation>Mala resposta del servidor màster.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="166"/>
        <source>Could not fetch a new server list. The protocol you are using is too old. An update may be available.</source>
        <translation>No s&apos;ha pogut obtenir una nova llista de servidors. El protocol que estàs fent servir és molt vell. Una actualització pot estar disponible.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="263"/>
        <source>Reloading master server results from cache for %1!</source>
        <translation>¡Recarregant els resultats del servidor màster de la memòria cau per a %1!</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="354"/>
        <source>Master server timeout</source>
        <translation>Temps d&apos;espera per a servidor màster</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="354"/>
        <source>Connection timeout (%1:%2).</source>
        <translation>Temps d&apos;espera de connexió (%1:%2).</translation>
    </message>
</context>
<context>
    <name>MiscServerSetupPanel</name>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="34"/>
        <source>URL:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="44"/>
        <source>E-mail:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="54"/>
        <source>Connect password:</source>
        <translation>Contrasenya de connexió:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="68"/>
        <source>Join password:</source>
        <translation>Contrasenya d&apos;unió:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="82"/>
        <source>RCon password:</source>
        <translation>Contrasenya de RCon:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="99"/>
        <source>Hide passwords</source>
        <translation>Amaga contrasenyes</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="111"/>
        <source>MOTD:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MissingWadsDialog</name>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="14"/>
        <source>Doomseeker - files are missing</source>
        <translation>Doomseeker - falten fitxers</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="41"/>
        <source>You don&apos;t have all the files required by this server and an instance of Wadseeker is already running.</source>
        <translation>No té tots els fitxers rellevants per aquest servidor i ja s&apos;està executant Wadseeker.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="57"/>
        <source>Press &apos;Ignore&apos; to join anyway.</source>
        <translation>Premeu &apos;Ignorar&apos; per a unir-se de totes maneres.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="103"/>
        <source>These files belong to a commercial game or are otherwise blocked from download:</source>
        <translation>Aquests arxius pertanyen a un joc comercial o estan bloquejats per a descarregar:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="116"/>
        <location filename="../gui/missingwadsdialog.ui" line="195"/>
        <source>&lt;files&gt;</source>
        <translation>&lt;archius&gt;</translation>
    </message>
    <message>
        <source>Make sure that this file is in one of the paths specified in Options -&gt; File Paths.

If you don&apos;t have this file, and it belongs to a commercial game, you need to purchase the game associated with this file. Wadseeker will not download commercial IWADs or modifications.</source>
        <translation type="vanished">Assegureu-vos que aquest arxiu estigui en una de les rutes especificades a Opcions -&gt; Rutes d&apos;arxius.

Si no té aquest arxiu, i pertany a un joc comercial, necessita comprar el joc associat a aquest fitxer. Wadseeker no descarregarà IWADs o modificacions comercials.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="135"/>
        <source>Make sure that this file is in one of the paths specified in Options -&gt; File paths.

If you don&apos;t have this file, and it belongs to a commercial game, you need to purchase the game associated with this file. Wadseeker will not download commercial IWADs or modifications.</source>
        <translation>Assegureu-vos que aquest arxiu estigui en una de les rutes especificades a Opcions -&gt; Rutes d&apos;arxius.

Si no té aquest arxiu, i pertany a un joc comercial, necessita comprar el joc associat a aquest fitxer. Wadseeker no descarregarà IWADs o modificacions comercials.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="147"/>
        <source>You can also install a free replacement IWAD with &quot;Install Freedoom&quot; button.</source>
        <translation>També pot instal·lar un IWAD de reemplaçament gratuït amb el botó &quot;Instal·lar Freedoom&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="182"/>
        <source>Following files can be downloaded:</source>
        <translation>Els següents arxius es poden descarregar:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="236"/>
        <source>Incompatible files:</source>
        <translation>Fitxers incompatibles:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="243"/>
        <source>(Your current files will be overwritten)</source>
        <translation>(Els vostres fitxers actuals seran sobreescrits)</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="281"/>
        <source>Optional files:</source>
        <translation>Arxius opcionals:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="304"/>
        <source>Do you want Wadseeker to find the missing WADs?</source>
        <translation>Vols que Wadseeker trobi els WADs que falten?</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="311"/>
        <source>Alternatively use ignore to connect anyways.</source>
        <translation>Alternativament, premi &quot;Ignora&quot; per connectar-se de totes maneres.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="342"/>
        <source>Install Freedoom</source>
        <translation>Instal·lar Freedom</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.cpp" line="63"/>
        <source>Install</source>
        <translation>Instal·lar</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Ignorar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../gui/passworddlg.ui" line="32"/>
        <source>Connection Password</source>
        <translation>Contrasenya de connexió</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="38"/>
        <source>This server requires a password in order to connect, please enter this password below.</source>
        <translation>Aquest servidor requereix una clau per connectar-se, si us plau entreu la contrasenya a continuació.</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="63"/>
        <source>Connect password:</source>
        <translation>Contrasenya de connexió:</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="112"/>
        <source>Ingame password:</source>
        <translation>Contrasenya en el joc:</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="146"/>
        <source>Hide passwords</source>
        <translation>Amaga contrasenyes</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="156"/>
        <source>Remember password</source>
        <translation>Recordar contrasenya</translation>
    </message>
</context>
<context>
    <name>PlayerTable</name>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="210"/>
        <source>BOT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="214"/>
        <source>SPECTATOR</source>
        <translation>ESPECTADOR</translation>
    </message>
    <message>
        <source>Team</source>
        <translation type="vanished">Equip</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="146"/>
        <source>Bots</source>
        <translation>Bots</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="151"/>
        <source>Spectators</source>
        <translation>Espectadors</translation>
    </message>
    <message numerus="yes">
        <location filename="../serverapi/tooltips/playertable.cpp" line="238"/>
        <source>(and %n more ...)</source>
        <translation>
            <numerusform>(i %n més...)</numerusform>
            <numerusform>(i %n més...)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="253"/>
        <source>Player</source>
        <translation>Jugador</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="254"/>
        <source>Score</source>
        <translation>Puntuació</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="255"/>
        <source>Ping</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="256"/>
        <source>Status</source>
        <translation>Estat</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="287"/>
        <source>Team %1</source>
        <translation>Equip %1</translation>
    </message>
</context>
<context>
    <name>PlayersDiagram</name>
    <message>
        <location filename="../gui/helpers/playersdiagram.cpp" line="64"/>
        <source>Numeric</source>
        <translation>Numèric</translation>
    </message>
    <message>
        <location filename="../gui/helpers/playersdiagram.cpp" line="65"/>
        <source>Blocks</source>
        <translation>Blocs</translation>
    </message>
</context>
<context>
    <name>PluginUrlHandler</name>
    <message>
        <location filename="../connectionhandler.cpp" line="226"/>
        <source>Connect to server</source>
        <translation>Connectar amb el servidor</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="227"/>
        <source>Do you want to connect to the server at %1?</source>
        <translation>Voleu connectar-se al servidor %1?</translation>
    </message>
</context>
<context>
    <name>ProgramArgsHelpDialog</name>
    <message>
        <location filename="../gui/programargshelpdialog.ui" line="14"/>
        <source>Doomseeker - Program arguments</source>
        <translation>Doomseeker - Arguments del programa</translation>
    </message>
    <message>
        <location filename="../gui/programargshelpdialog.ui" line="20"/>
        <source>These arguments can be passed to Doomseeker&apos;s executable:</source>
        <translation>Aquests arguments es poden passar a l&apos;executable de Doomseeker:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="36"/>
        <source>Unlimited</source>
        <translation>Il·limitat</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="404"/>
        <source>Cooperative</source>
        <translation>Cooperatiu</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="409"/>
        <source>Deathmatch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="414"/>
        <source>Team DM</source>
        <translation>DM per equips</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="419"/>
        <source>CTF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="424"/>
        <source>Unknown</source>
        <translation>Desconegut</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="224"/>
        <source>Skipping loading of forbidden plugin: %1</source>
        <translation>Saltant la càrrega d&apos;un complement prohibit: %1</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="67"/>
        <source>plugin ABI version mismatch; plugin: %1, Doomseeker: %2</source>
        <translation>diferències en l&apos;ABI del complement; complement:%1, Doomseeker:%2</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="73"/>
        <source>plugin doesn&apos;t report its ABI version</source>
        <translation>el complement no proporciona la seva versió ABI</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="75"/>
        <source>Cannot load plugin %1, reason: %2.</source>
        <translation>No es pot carregar el complement %1, Motiu: %2.</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="94"/>
        <source>Loaded plugin: &quot;%1&quot;!</source>
        <translation>Complement carregat: &quot;%1&quot;!</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="99"/>
        <source>Failed to open plugin: %1</source>
        <translation>Error en obrir el complement: %1</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="182"/>
        <source>Failed to locate plugins.</source>
        <translation>No s&apos;ha pogut localitzar complements.</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="93"/>
        <source>Add plugin&apos;s IRC channel?</source>
        <translation>Afegeix el canal IRC del complement?</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="94"/>
        <source>Would you like the %1 plugin to add its channel to %2&apos;s auto join?</source>
        <translation>Voleu que el complement %1 afegeixi el seu canal en unir-se automàticament a %2?</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="166"/>
        <source>Setting IRC INI file: %1</source>
        <translation>Configurant l&apos;arxiu IRC INI: %1</translation>
    </message>
    <message>
        <source>parent node is not a directory: %1</source>
        <translation type="vanished">node pare no és un directori: %1</translation>
    </message>
    <message>
        <source>lack of necessary permissions to the parent directory: %1</source>
        <translation type="vanished">falta de permisos necessaris per al directori pare: %1</translation>
    </message>
    <message>
        <source>cannot create directory</source>
        <translation type="vanished">no es pot crear el directori</translation>
    </message>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="114"/>
        <source>DoomseekerConfig.iniSectionForPlugin(): empty plugin name has been specified, returning dummy IniSection.</source>
        <translation>DoomseekerConfig.iniSectionForPlugin (): el nom del complement ha estat especificat buit, tornant el IniSection fictici.</translation>
    </message>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="120"/>
        <source>DoomseekerConfig.iniSectionForPlugin(): plugin name is invalid: %1</source>
        <translation>DoomseekerConfig.iniSectionForPlugin (): el nom del complement no és vàlid: %1</translation>
    </message>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="213"/>
        <source>Setting INI file: %1</source>
        <translation>Configurant arxiu INI: %1</translation>
    </message>
</context>
<context>
    <name>RconPasswordDialog</name>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="20"/>
        <source>Connect to Remote Console</source>
        <translation>Connectar-se a consola remota</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="26"/>
        <source>Connection</source>
        <translation>Connexió</translation>
    </message>
    <message>
        <source>Source Port:</source>
        <translation type="vanished">Joc:</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="32"/>
        <source>Game:</source>
        <translation>Joc:</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="42"/>
        <source>Server Address:</source>
        <translation>Direcció del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="55"/>
        <source>Please enter your remote console password.</source>
        <translation>Si us plau ingressi la contrasenya de la seva consola remota.</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="67"/>
        <source>Hide password</source>
        <translation>Amaga contrasenya</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="70"/>
        <source>Hide</source>
        <translation>Amagar</translation>
    </message>
</context>
<context>
    <name>RemoteConsole</name>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="68"/>
        <source>No RCon support</source>
        <translation>Sense suport RCon</translation>
    </message>
    <message>
        <source>The selected source port has no RCon support.</source>
        <translation type="vanished">El source port seleccionat no té suport per a RCon.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="68"/>
        <source>The selected game has no RCon support.</source>
        <translation>El joc seleccionat no té suport per a RCon.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="83"/>
        <source>RCon failure</source>
        <translation>Error de RCon</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="83"/>
        <source>Failed to create RCon protocol for the server.</source>
        <translation>No s&apos;ha pogut crear el protocol RCon per al servidor.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="110"/>
        <source>RCon Failure</source>
        <translation>Error RCon</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="111"/>
        <source>Failed to connect RCon to server %1:%2</source>
        <translation>Ha fallat la connexió RCon al servidor %1:%2</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="134"/>
        <source> - Remote Console</source>
        <translation> - Consola remota</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="146"/>
        <source>Invalid Password</source>
        <translation>Contrasenya invàlida</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="146"/>
        <source>The password you entered appears to be invalid.</source>
        <translation>La contrasenya que heu introduït sembla ser invàlida.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="14"/>
        <source>Remote Console</source>
        <translation>Consola remota</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="90"/>
        <source>Disconnect</source>
        <translation>Desconnectar</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="21"/>
        <source>Scoreboard</source>
        <translation>Marcador</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="52"/>
        <source>Player Name</source>
        <translation>Nom del jugador</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="63"/>
        <source>Console</source>
        <translation>Consola</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="81"/>
        <source>File</source>
        <translation>Arxiu</translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <location filename="../serverapi/server.cpp" line="148"/>
        <source>&lt;&lt; ERROR &gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../serverapi/server.cpp" line="263"/>
        <source>client</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../serverapi/server.cpp" line="296"/>
        <source>Undefined</source>
        <translation>Indefinit</translation>
    </message>
</context>
<context>
    <name>ServerConsole</name>
    <message>
        <location filename="../gui/widgets/serverconsole.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
</context>
<context>
    <name>ServerDetailsDock</name>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="14"/>
        <source>Server details</source>
        <translation>Detalls del servidor</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="47"/>
        <source>Server:</source>
        <translation>Servidor:</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="54"/>
        <source>&lt;server&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="70"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="96"/>
        <source>Players</source>
        <translation>Jugadors</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="122"/>
        <source>Flags</source>
        <translation>Indicadors</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="72"/>
        <source>No server selected.</source>
        <translation>Cap servidor seleccionat.</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="92"/>
        <source>&lt;b&gt;Address:&lt;/b&gt; %1
</source>
        <translation>&lt;b&gt;Adreça:&lt;/b&gt; %1
</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="105"/>
        <source>No players on this server.</source>
        <translation>No hi ha jugadors en aquest servidor.</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="111"/>
        <source>DMFlags unknown or no DMFlags set.</source>
        <translation>DMFlags desconeguts o cap DMFlags establert.</translation>
    </message>
</context>
<context>
    <name>ServerFilterBuilderMenu</name>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="48"/>
        <source>Build server filter ...</source>
        <translation>Generar filtre de servidor ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="55"/>
        <source>Show only servers with ping lower than %1</source>
        <translation>Mostra només servidors amb ping inferior a %1</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="59"/>
        <source>Filter by game mode &quot;%1&quot;</source>
        <translation>Filtrar per mode de joc &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="64"/>
        <source>Hide game mode &quot;%1&quot;</source>
        <translation>Amaga mode de joc &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="68"/>
        <source>Include WAD ...</source>
        <translation>Inclou WAD ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="69"/>
        <source>Exclude WAD ...</source>
        <translation>Exclou WAD ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="85"/>
        <source>Filter by address</source>
        <translation>Filtra per direcció</translation>
    </message>
</context>
<context>
    <name>ServerFilterDock</name>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="151"/>
        <source>Server &amp;filter</source>
        <translation>&amp;Filtre de Servidors</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="152"/>
        <source>CTRL+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="296"/>
        <source>[no filter]</source>
        <translation>[sense filtre]</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="348"/>
        <source>Doomseeker - Remove filter preset</source>
        <translation>Doomseeker - Suprimir filtre</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="349"/>
        <source>Are you sure you wish to remove the filter preset &quot;%1&quot;?</source>
        <translation>Esteu segur que voleu suprimir el filtre &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="362"/>
        <source>Doomseeker - Save filter preset</source>
        <translation>Doomseeker - Guardar filtre</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="363"/>
        <source>Enter name for the filter preset (will overwrite if exists):</source>
        <translation>Introdueix el nom per a aquest filtre (si n&apos;hi ha, el sobreescriurà):</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="26"/>
        <source>Server filter</source>
        <translation>Filtre de servidors</translation>
    </message>
    <message>
        <source>Server Name:</source>
        <translation type="vanished">Nom del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="112"/>
        <source>Put populated servers on top</source>
        <translation>Posar servidors plens a la part superior</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="132"/>
        <source>Filtering enabled</source>
        <translation>Filtrat habilitat</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="95"/>
        <source>Server name:</source>
        <translation>Nom del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="51"/>
        <location filename="../gui/serverfilterdock.cpp" line="296"/>
        <source>[custom]</source>
        <translation>[customitzat]</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="65"/>
        <source>Save the current server filter as a preset.</source>
        <translation>Desar el filtre de servidors com una nova configuració.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="82"/>
        <source>Remove the server filter preset.</source>
        <translation>Suprimir la configuració de filtre de servidors.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="184"/>
        <source>Max. ping:</source>
        <translation>Max. Ping:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="191"/>
        <source>Set &apos;0&apos; to disable.</source>
        <translation>Establir a &apos;0&apos; per a desactivar.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="207"/>
        <source>WADs:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="214"/>
        <source>Use &apos;,&apos; (a comma) to separate multiple WADs.</source>
        <translation>Utilitzi &apos;,&apos; (una coma) per separar múltiples WADs.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="221"/>
        <source>Exclude WADs:</source>
        <translation>Excloure WADs:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="228"/>
        <source>Servers with WADs on this list won&apos;t be displayed. Use &apos;,&apos; (a comma) to separate multiple wads.</source>
        <translation>Els servidors amb WADs en aquesta llista no es mostraran. Utilitzeu &apos;,&apos; (una coma) per separar múltiples WADs.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="235"/>
        <source>Game modes:</source>
        <translation>Modes de joc:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="245"/>
        <source>Exclude game modes:</source>
        <translation>Excloure modes de joc:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="252"/>
        <source>Selected game modes won&apos;t appear on the server list.</source>
        <translation>Les maneres de joc seleccionats no apareixeran a la llista de servidors.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="259"/>
        <source>Addresses:</source>
        <translation>Direcció:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="266"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only servers that match these addresses will be displayed. Multiple addresses can be separated by commas (&lt;span style=&quot; font-weight:600;&quot;&gt;,&lt;/span&gt;). Subnets are supported by using the &lt;span style=&quot; font-weight:600;&quot;&gt;ADDR/N&lt;/span&gt; format where &lt;span style=&quot; font-weight:600;&quot;&gt;N&lt;/span&gt; can be a subnet mask or a numerical value for the subnet mask. For IPv4 the subnet can also be specified by omitting the octets. &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/24&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/255.255.255.0&lt;/span&gt; are all the same.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Només es mostraran els servidors que coincideixin amb aquestes adreces. Es poden separar diverses adreces per comes (&lt;span style=&quot; font-weight:600;&quot;&gt;,&lt;/span&gt;).Les subxarxes es poden definir com &lt;span style=&quot; font-weight:600;&quot;&gt;ADDR/N&lt;/span&gt; on &lt;span style=&quot; font-weight:600;&quot;&gt;N&lt;/span&gt; pot ser una màscara de subxarxa o un valor numèric per a la màscara de la subxarxa. Per a IPv4 la subxarxa també es pot definir omitint octets finals. &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/24&lt;/span&gt; i &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/255.255.255.0&lt;/span&gt; són equivalents.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="273"/>
        <source>Show full servers</source>
        <translation>Mostra servidors plens</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="283"/>
        <source>Show empty servers</source>
        <translation>Mostra servidors buits</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="293"/>
        <source>Show password protected servers</source>
        <translation>Mostra servidors protegits per contrasenya</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="303"/>
        <source>Show only valid servers</source>
        <translation>Mostra només servidors vàlids</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="325"/>
        <source>Show the servers you were banned from.</source>
        <translation>Mostra els servidors on has estat banejat.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="328"/>
        <source>Show banned servers</source>
        <translation>Mostrar servidors banejats</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="338"/>
        <source>Show the &quot;Refreshed too soon&quot; servers.</source>
        <translation>Mostra els servidors &quot;Refrescat massa de pressa&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="341"/>
        <source>Show &quot;too soon&quot; servers</source>
        <translation>Mostra servidors &quot;massa de pressa&quot;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="351"/>
        <source>&lt;p&gt;Show the servers that didn&apos;t respond at all or returned a bad response.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Mostra els servidors que no responen o responen incorrectament&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="354"/>
        <source>Show not responding servers</source>
        <translation>Mostra servidors que no responen</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="367"/>
        <source>Show testing servers</source>
        <translation>Mostra servidors de prova</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="390"/>
        <source>Reset filter</source>
        <translation>Reiniciar filtre</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Buidar</translation>
    </message>
</context>
<context>
    <name>ServerList</name>
    <message>
        <location filename="../gui/serverlist.cpp" line="222"/>
        <source>Doomseeker - context menu warning</source>
        <translation>Doomseeker - menú contextual d&apos;advertència</translation>
    </message>
    <message>
        <location filename="../gui/serverlist.cpp" line="223"/>
        <source>Unhandled behavior in ServerList::contextMenuTriggered()</source>
        <translation>Comportament no controlat en ServerList::contextMenuTriggered ()</translation>
    </message>
</context>
<context>
    <name>ServerListColumns</name>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="55"/>
        <source>Players</source>
        <translation>Jugadors</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="57"/>
        <source>Ping</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="59"/>
        <source>Server Name</source>
        <translation>Nom del servidor</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="61"/>
        <source>Address</source>
        <translation>Direcció</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="63"/>
        <source>IWAD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="65"/>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="67"/>
        <source>WADs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="69"/>
        <source>Game Type</source>
        <translation>Tipus de joc</translation>
    </message>
</context>
<context>
    <name>ServerListContextMenu</name>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="106"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="107"/>
        <source>Copy Address</source>
        <translation>Copiar Direcció</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="110"/>
        <source>Copy E-Mail</source>
        <translation>Copiar E-Mail</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="113"/>
        <source>Copy URL</source>
        <translation>Copiar URL</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="115"/>
        <source>Copy Name</source>
        <translation>Copiar Nom</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="126"/>
        <source>Refresh</source>
        <translation>Refrescar</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="127"/>
        <source>Join</source>
        <translation>Unir-se</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="128"/>
        <source>Show join command line</source>
        <translation>Mostra la comanda per unir-se</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="129"/>
        <source>Find missing WADs</source>
        <translation>Troba els WADs que falten</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="136"/>
        <source>Open URL in browser</source>
        <translation>Obrir URL al navegador</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="139"/>
        <source>Pin</source>
        <translation>Marcar</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="139"/>
        <source>Unpin</source>
        <translation>Desmarcar</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="155"/>
        <source>Remote console</source>
        <translation>Consola remota</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="162"/>
        <source>Sort additionally ascending</source>
        <translation>Addicionalment ordre ascendent</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="163"/>
        <source>Sort additionally descending</source>
        <translation>Addicionalment ordre descendent</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="168"/>
        <source>Remove additional sorting for column</source>
        <translation>Treure ordre addicional per a la columna</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="171"/>
        <source>Clear additional sorting</source>
        <translation>Esborrar ordre addicional</translation>
    </message>
</context>
<context>
    <name>ServerListRowHandler</name>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="269"/>
        <source>&lt;ERROR&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="281"/>
        <source>You are banned from this server!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="340"/>
        <source>&lt;REFRESHING&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="350"/>
        <source>&lt;NO RESPONSE&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="362"/>
        <source>&lt;Refreshed too soon, wait a while and try again&gt;</source>
        <translation>&lt;Refrescat massa de pressa, esperi un moment i intenti-ho de nou&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="416"/>
        <source>Unknown server response (%1): %2:%3</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ServerTooltip::L10n</name>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="58"/>
        <source>(alias of: %1)</source>
        <translation>(àlies de: %1)</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="64"/>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="175"/>
        <source>MISSING</source>
        <translation>FALTANT</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="170"/>
        <source>OPTIONAL</source>
        <translation>OPCIONAL</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="180"/>
        <source>ALIAS</source>
        <translation>ÀLIES</translation>
    </message>
</context>
<context>
    <name>ServersStatusWidget</name>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="83"/>
        <source>Players (Humans + Bots) / Servers Refreshed%</source>
        <translation>Jugadors (
Humans + Bots) / Servidors actualitzats%</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="114"/>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="137"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="116"/>
        <source>%1%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="130"/>
        <source>%1 (%2+%3) / %4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="133"/>
        <source> %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>StaticMessages</name>
    <message>
        <location filename="../serverapi/message.cpp" line="30"/>
        <source>You have been banned from the master server.</source>
        <translation>Vostè ha estat banejat del servidor master.</translation>
    </message>
</context>
<context>
    <name>UpdateChannelTr</name>
    <message>
        <location filename="../updater/updatechannel.cpp" line="110"/>
        <source>Beta versions have newer features but they are untested. Releases on this update channel are more often and are suggested for users who want newest functionalities and minor bug fixes as soon as they become implemented and available.</source>
        <translation>Les versions Beta tenen característiques més noves però no han estat provades. Les versions d&apos;aquest canal d&apos;actualització són més freqüents i es suggereixen per a usuaris que volen funcionalitats més recents i correccions d&apos;errors menors tan aviat com s&apos;implementen i estan disponibles.</translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="120"/>
        <source>Stable versions are released rarely. They cover many changes at once and these changes are more certain to work correctly. Critical bug fixes are also provided through this channel.</source>
        <translation>Les versions estables es llancen rarament. Cobreixen molts canvis alhora i és més probable que aquests canvis funcionin correctament. Les correccions d&apos;errors crítiques també es proporcionen a través d&apos;aquest canal.</translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="138"/>
        <source>Beta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="142"/>
        <source>Stable</source>
        <translation>Estable</translation>
    </message>
</context>
<context>
    <name>UpdateInstaller</name>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="82"/>
        <source>Failed to copy the updater executable to a temporary space: &quot;%1&quot; -&gt; &quot;%2&quot;.</source>
        <translation>Error en copiar l&apos;executable del actualitzador en un espai temporal: &quot;%1&quot; -&gt; &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="92"/>
        <location filename="../updater/updateinstaller.cpp" line="127"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="94"/>
        <source>Nothing to update.</source>
        <translation>Res per actualitzar.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="96"/>
        <source>Update package or script are not found. Check log for details.</source>
        <translation>No es troben el paquet d&apos;actualització o l&apos;script. Revisi el registre per més detalls.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="98"/>
        <source>Failed to start updater process.</source>
        <translation>Error al iniciar el procés d&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="100"/>
        <source>Unknown error: %1.</source>
        <translation>Error desconegut: %1.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="129"/>
        <source>Unable to read the update script.</source>
        <translation>No es pot llegir l&apos;script de l&apos;actualització.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="131"/>
        <source>No installation directory specified.</source>
        <translation>No s&apos;ha especificat el directori d&apos;instal·lació.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="133"/>
        <source>Unable to determine the path of the updater.</source>
        <translation>No es pot determinar la ruta de l&apos;actualitzador.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="135"/>
        <source>General failure.</source>
        <translation>Fallada general.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="137"/>
        <source>Unknown process error code: %1.</source>
        <translation>Codi d&apos;error desconegut per al procés: %1.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="147"/>
        <source>Installing update.</source>
        <translation>Instal·lant actualització.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="162"/>
        <source>Package directory &quot;%1&quot; doesn&apos;t exist.</source>
        <translation>El directori de paquets &quot;%1&quot; no existeix.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="168"/>
        <source>Update was about to be installed but update script &quot;%1&quot; is missing.</source>
        <translation>L&apos;actualització estava a punt d&apos;instal·lar-però falta l&apos;script d&apos;actualització &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="205"/>
        <source>Failed to start updater process: %1 %2</source>
        <translation>Error en iniciar el procés d&apos;actualització: %1 %2</translation>
    </message>
</context>
<context>
    <name>UpdatePackageFilter</name>
    <message>
        <location filename="../updater/updatepackagefilter.cpp" line="164"/>
        <source>-BROKEN</source>
        <translation>-TRENCAT</translation>
    </message>
</context>
<context>
    <name>UpdaterInfoParser</name>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="98"/>
        <source>Missing update revision info for package %1.</source>
        <translation>Manca informació de l&apos;actualització per al paquet %1.</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="126"/>
        <source>Invalid update download URL for package %1: %2</source>
        <translation>L&apos;URL de descàrrega de l&apos;actualització no és vàlida per al paquet %1: %2</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="133"/>
        <source>Missing update download URL for package %1.</source>
        <translation>Falta la URL de descàrrega de l&apos;actualització per al paquet %1.</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="143"/>
        <source>Invalid update script download URL for package %1, %2</source>
        <translation>La URL de descàrrega de l&apos;script d&apos;actualització no és vàlida per al paquet %1, %2</translation>
    </message>
</context>
<context>
    <name>WadsPicker</name>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="62"/>
        <source>Check paths</source>
        <translation>Comprovar rutes</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="114"/>
        <source>Doomseeker - Add file(s)</source>
        <translation>Doomseeker - Afegir arxiu(s)</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="164"/>
        <source>%1 MISSING</source>
        <translation>%1 FALTANT</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="116"/>
        <source>Clear</source>
        <translation>Buidar</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="53"/>
        <source>Browse</source>
        <translation>Cercar</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="50"/>
        <source>Browse for a file.</source>
        <translation>Buscar un fitxer.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="66"/>
        <source>Add an empty path to the list.</source>
        <translation>Afegir una ruta buida a la llista.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="69"/>
        <source>Add empty</source>
        <translation>Afegir vuit</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="86"/>
        <source>Remove the selected files from the list.</source>
        <translation>Suprimeix els fitxers seleccionats de la llista.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="89"/>
        <source>Remove</source>
        <translation>Suprimir</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="113"/>
        <source>Remove all files from the list.</source>
        <translation>Suprimeix tots els fitxers de la llista.</translation>
    </message>
</context>
<context>
    <name>WadseekerInterface</name>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="123"/>
        <source>Downloading WADs for server &quot;%1&quot;</source>
        <translation>Descarregant WADs per al servidor &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="134"/>
        <source>Aborting service: %1</source>
        <translation>Avortant servei: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="140"/>
        <source>Aborting site: %1</source>
        <translation>Avortant lloc: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="194"/>
        <source>All done. Success.</source>
        <translation>Tot llest. Èxit.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="213"/>
        <source>All done. Fail.</source>
        <translation>Tot llest. Fracàs.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="349"/>
        <source>CRIT</source>
        <translation>FATAL</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="353"/>
        <source>ERROR</source>
        <translation>ERROR</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="357"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="361"/>
        <source>NOTI</source>
        <translation>NOTIF</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="365"/>
        <source>NAVI</source>
        <translation>NAVI</translation>
    </message>
    <message>
        <source>CRITICAL ERROR: %1</source>
        <translation type="vanished">ERROR CRÍTIC: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="14"/>
        <location filename="../gui/wadseekerinterface.cpp" line="224"/>
        <location filename="../gui/wadseekerinterface.cpp" line="398"/>
        <source>Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="22"/>
        <source>Seek WADs, comma (&apos;,&apos;) separated:</source>
        <translation>Buscar WADs, separats per coma &apos;,&apos;:</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="59"/>
        <source>Abort</source>
        <translation>Abortar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Tancar</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="557"/>
        <source>[%1%] Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="605"/>
        <source>Context menu error</source>
        <translation>Error del menú contextual</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="605"/>
        <source>Unknown action selected.</source>
        <translation>Acció desconeguda seleccionada.</translation>
    </message>
    <message>
        <source>Seek WAD or multiple WADs, comma (&apos;,&apos;) separated:</source>
        <translation type="vanished">Cercar WAD o WADs múltiples, separats per comes (&apos;,&apos;):</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="49"/>
        <location filename="../gui/wadseekerinterface.ui" line="91"/>
        <source>URL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="54"/>
        <location filename="../gui/wadseekerinterface.ui" line="96"/>
        <source>Progress</source>
        <translation>Progrés</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="86"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="101"/>
        <source>Speed</source>
        <translation>Velocitat</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="106"/>
        <source>ETA</source>
        <translation>temps estimat</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="111"/>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="159"/>
        <source>Start game</source>
        <translation>Començar joc</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="173"/>
        <source>Download</source>
        <translation>Descarregar</translation>
    </message>
</context>
<context>
    <name>WadseekerShow</name>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="52"/>
        <location filename="../gui/wadseekershow.cpp" line="67"/>
        <source>Wadseeker cannot be launched</source>
        <translation>Wadseeker no pot ser executat</translation>
    </message>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="53"/>
        <source>Another instance of Wadseeker is already running.</source>
        <translation>Una altra instància de Wadseeker ja s&apos;està executant.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="63"/>
        <source>Wadseeker will not work correctly:

The target directory is either not configured, is invalid or cannot be written to.

Please review your Configuration and/or refer to the online help available from the Help menu.</source>
        <translation>Wadseeker no funcionarà correctament:

El directori de destinació no està configurat, no és vàlid o no es pot escriure.

Revisi la seva configuració i/o consulteu l&apos;ajuda en línia disponible al menú Ajuda.</translation>
    </message>
    <message>
        <source>Wadseeker will not work correctly:

Target directory is either not set, is invalid or cannot be written to.

Please review your Configuration and/or refer to online help available from the Help menu.</source>
        <translation type="vanished">Wadseeker no funcionarà correctament:

El directori de destinació no està configurat, no és vàlid o no es pot escriure.

Revisi la seva configuració i/o consulteu l&apos;ajuda en línia disponible al menú Ajuda.</translation>
    </message>
</context>
<context>
    <name>WadseekerSitesTable</name>
    <message>
        <location filename="../gui/widgets/wadseekersitestable.cpp" line="93"/>
        <source>Abort</source>
        <translation>Avortar</translation>
    </message>
</context>
<context>
    <name>WadseekerWadsTable</name>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="68"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="69"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="129"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="130"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="307"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="315"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="132"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="133"/>
        <source>Awaiting URLs</source>
        <translation>Esperant URLs</translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="192"/>
        <source>Done</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="324"/>
        <source>Skip current URL</source>
        <translation>Ometre la URL actual</translation>
    </message>
</context>
</TS>
