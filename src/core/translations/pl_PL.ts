<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../gui/aboutdialog.ui" line="14"/>
        <source>About Doomseeker</source>
        <translation>O programie Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="24"/>
        <location filename="../gui/aboutdialog.ui" line="43"/>
        <source>Doomseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="77"/>
        <location filename="../gui/aboutdialog.ui" line="406"/>
        <source>&lt;Version&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="90"/>
        <source>(ABI: &lt;DOOMSEEKER_ABI_VERSION&gt;)</source>
        <translation>(ABI: &lt;DOOMSEEKER_ABI_VERSION&gt;)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="112"/>
        <source>&lt;Changeset&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="140"/>
        <source>Revision:</source>
        <translation>Rewizja:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="150"/>
        <source>&lt;Revision&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="175"/>
        <location filename="../gui/aboutdialog.ui" line="419"/>
        <source>&lt;a href=&quot;https://doomseeker.drdteam.org/&quot;&gt;https://doomseeker.drdteam.org/&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="340"/>
        <source>IP2C Database:</source>
        <translation>Baza IP2C:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="350"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&amp;lt;URL&amp;gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Contact information:</source>
        <translation type="vanished">Informacje kontaktowe:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="232"/>
        <source>Zalewa:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="239"/>
        <source>Blzut3:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="290"/>
        <source>Additional contributions from:</source>
        <translation>Dodatkowy wkład:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="300"/>
        <source>- Hyper_Eye, Nece228, Linda &quot;WubTheCaptain&quot; Lapinlampi, Pol Marcet Sardà</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="310"/>
        <source>- Doomseeker logo and main icon design by Rachael &quot;Eruanna&quot; Alexanderson</source>
        <translation>- Logo Doomseeker i projekt głównej ikony - Rachael &quot;Eruanna&quot; Alexanderson</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="147"/>
        <source>The auto updater compares the versions by this revision number.</source>
        <translation>Numer rewizji jest porównywany do wykrycia automatycznych aktualizacji.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="223"/>
        <source>Doomseeker maintainers:</source>
        <translation>Twórcy Doomseekera:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="246"/>
        <source>&lt;a href=&quot;mailto:admin@maniacsvault.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;admin@maniacsvault.net&lt;/span&gt;&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="262"/>
        <source>&lt;a href=&quot;mailto:zalewapl@gmail.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;zalewapl@gmail.com&lt;/span&gt;&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="320"/>
        <source>- Doomseeker icons by MazterQyoun-ae and CarpeDiem</source>
        <translation>- Ikony Doomseeker - MazterQyoun-ae i CarpeDiem</translation>
    </message>
    <message>
        <source>GeoLite2 Database:</source>
        <translation type="vanished">Baza danych GeoLite2:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="371"/>
        <location filename="../gui/aboutdialog.ui" line="390"/>
        <source>Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="437"/>
        <source>Copyright ©</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="447"/>
        <source>&lt;YearSpan&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="457"/>
        <source>&lt;Author&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="482"/>
        <source>&lt;Description&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="538"/>
        <source>Version: 0.0.0.0</source>
        <translation>Wersja: 0.0.0.0</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="551"/>
        <source>(ABI: DOOMSEEKER_ABI_VERSION)</source>
        <translation>(ABI: &lt;DOOMSEEKER_ABI_VERSION&gt;)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="573"/>
        <source>&lt;Plugin Author&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="509"/>
        <source>Plugins</source>
        <translation>Wtyczki</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="122"/>
        <source>Copyright %1 %2 The Doomseeker Team</source>
        <translation>Prawa autorskie %1 %2 Zespół Doomseeker (The Doomseeker Team)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="124"/>
        <source>This program is distributed under the terms of the LGPL v2.1 or later.</source>
        <translation>Ten program jest rozprowadzany na zasadach licencji LGPL v2.1 lub późniejszej.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="127"/>
        <source>Doomseeker translations contributed by:
</source>
        <translation>Autorzy tłumaczeń Doomseekera:
</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="128"/>
        <source>- Polish: Zalewa</source>
        <translation>- Polski: Zalewa</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="129"/>
        <source>- Spanish: Pol Marcet Sard%1</source>
        <translation>- Hiszpański: Pol Marcet Sard%1</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="130"/>
        <source>- Catalan: Pol Marcet Sard%1</source>
        <translation>- Kataloński: Pol Marcet Sard%1</translation>
    </message>
    <message>
        <source>This program uses GeoLite2 data for IP-to-Country (IP2C) purposes, available from https://www.maxmind.com</source>
        <translation type="vanished">Ten program używa bazy danych GeoLite2 aby zapewnić rozpoznawanie krajów po adresach IP (IP2C). Baza dostępna pod adresem https://www.maxmind.com</translation>
    </message>
    <message>
        <source>Database and Contents Copyright (c) 2018 MaxMind, Inc.</source>
        <translation type="vanished">Baza Danych i Zawartość, Prawo autorskie (c) 2018 MaxMind, Inc.</translation>
    </message>
    <message>
        <source>GeoLite2 License:
This work is licensed under the Creative Commons Attribution - ShareAlike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.</source>
        <translation type="vanished">Licencja GeoLite2:
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.</translation>
    </message>
    <message>
        <source>GeoLite2 available at:
https://dev.maxmind.com/geoip/geoip2/geolite2/</source>
        <translation type="vanished">GeoLite2 dostępne pod adresem:
https://dev.maxmind.com/geoip/geoip2/geolite2/</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="66"/>
        <source>&lt;i&gt;No URL available&lt;/i&gt;</source>
        <translation>&lt;i&gt;URL niedostępny&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="142"/>
        <source>- Aha-Soft</source>
        <translation>- Aha-Soft</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="143"/>
        <source>- Crystal Clear by Everaldo Coelho</source>
        <translation>- Crystal Clear przez Everaldo Coelho</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="144"/>
        <source>- Fugue Icons (C) 2013 Yusuke Kamiyamane. All rights reserved.</source>
        <translation>- Fugue Icons (C) 2013 Yusuke Kamiyamane. Wszelkie prawa zastrzeżone.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="145"/>
        <source>- Nuvola 1.0 (KDE 3.x icon set)</source>
        <translation>- Nuvola 1.0 (zestaw ikon KDE 3.x)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="146"/>
        <source>- Oxygen Icons 4.3.1 (KDE)</source>
        <translation>- Oxygen Icons 4.3.1 (KDE)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="147"/>
        <source>- Silk Icon Set (C) Mark James (famfamfam.com)</source>
        <translation>- Silk Icon Set (C) Mark James (famfamfam.com)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="148"/>
        <source>- Tango Icon Library / Tango Desktop Project</source>
        <translation>- Tango Icon Library / Tango Desktop Project</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="149"/>
        <source>This program uses icons (or derivates of) from following sources:
</source>
        <translation>Ten program używa ikon (lub opartych na nich) z następujących źródeł:
</translation>
    </message>
</context>
<context>
    <name>AddBuddyDlg</name>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="300"/>
        <source>Invalid Pattern</source>
        <translation>Nieprawidłowy wzorzec</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="300"/>
        <source>The pattern you have specified is invalid.</source>
        <translation>Podany wzorzec nie jest prawidłowy.</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="319"/>
        <source>The asterisk (*) can be used as a wild card.</source>
        <translation>Gwiazdka (*) dopasowuje dowolny tekst.</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="323"/>
        <source>Use the Regular Expression format.</source>
        <translation>Użyj Wyrażenia Regularnego (RegExp).</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="14"/>
        <source>Add Buddy</source>
        <translation>Dodaj znajomego</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="26"/>
        <source>Buddy name:</source>
        <translation>Nick znajomego:</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="36"/>
        <source>Pattern Type</source>
        <translation>Typ wzorca</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="42"/>
        <source>Basic</source>
        <translation>Podstawowy</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="52"/>
        <source>Advanced</source>
        <translation>Zaawansowany</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="62"/>
        <source>&lt;tip&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppRunner</name>
    <message>
        <location filename="../apprunner.cpp" line="55"/>
        <source>Could not read bundle plist. (%1)</source>
        <translation>Nie można odczytać plist z pakietu. (%1)</translation>
    </message>
    <message>
        <location filename="../apprunner.cpp" line="83"/>
        <location filename="../apprunner.cpp" line="114"/>
        <source>Starting (working dir %1): %2</source>
        <translation>Startuję (katalog roboczy %1): %2</translation>
    </message>
    <message>
        <location filename="../apprunner.cpp" line="102"/>
        <source>Cannot run file: %1</source>
        <translation>Nie można uruchomić pliku: %1</translation>
    </message>
</context>
<context>
    <name>AutoUpdater</name>
    <message>
        <location filename="../updater/autoupdater.cpp" line="160"/>
        <source>Detected update for package &quot;%1&quot; from version &quot;%2&quot; to version &quot;%3&quot;.</source>
        <translation>Wykryto aktualizację pakietu &quot;%1&quot; z wersji &quot;%2&quot; do wersji &quot;%3&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="195"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="197"/>
        <source>Update was aborted.</source>
        <translation>Aktualizacja przerwana.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="122"/>
        <source>Failed to parse updater XML script: %1, l: %2, c: %3</source>
        <translation>Błąd parsowania skryptu XML aktualizatora: %1, l: %2, k: %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="132"/>
        <source>Failed to modify package name in updater script: %1</source>
        <translation>Błąd podmiany nazwy pakietu w skrypcie aktualizatora: %1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="199"/>
        <source>Update channel is not configured. Please check your configuration.</source>
        <translation>Kanał aktualizacji nie jest skonfigurowany. Sprawdź swoją konfigurację.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="201"/>
        <source>Failed to download updater info file.</source>
        <translation>Pobranie informacji o aktualizacjach nie powiodło się.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="203"/>
        <source>Cannot parse updater info file.</source>
        <translation>Nie można odczytać pliku z danymi o aktualizacjach.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="205"/>
        <source>Main program node is missing from updater info file.</source>
        <translation>W pliku z danymi o aktualizacjach brakuje informacji o programie głównym.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="207"/>
        <source>Revision info on one of the packages is missing from the updater info file. Check the log for details.</source>
        <translation>Brakuje danych o rewizji jednego z pakietów w pliku z danymi o aktualizacjach. Szczegóły w logu.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="210"/>
        <source>Download URL for one of the packages is missing from the updater info file. Check the log for details.</source>
        <translation>Brak URLa do pobrania jednego z pakietów w pliku z danymi o aktualizacjach. Szczegóły w logu.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="213"/>
        <source>Download URL for one of the packages is invalid. Check the log for details.</source>
        <translation>URL do pobrania jednego z pakietów jest nieprawidłowy. Szczegóły w logu.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="216"/>
        <source>Update package download failed. Check the log for details.</source>
        <translation>Nie powiodło się pobranie pakietu aktualizacji. Szczegóły w logu.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="218"/>
        <source>Failed to create directory for updates packages storage.</source>
        <translation>Nie można stworzyć katalogu do zapisu pakietów aktualizacyjnych.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="220"/>
        <source>Failed to save update package.</source>
        <translation>Nie można zapisać pakietu aktualizacyjnego.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="222"/>
        <source>Failed to save update script.</source>
        <translation>Nie można zapisać skryptu aktualizacyjnego.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="351"/>
        <source>Confirm</source>
        <translation>Zatwierdź</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="448"/>
        <source>Update info</source>
        <translation>Informacje o aktualizacji</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="470"/>
        <source>Package: %1</source>
        <translation>Pakiet: %1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="516"/>
        <source>Invalid download URL for package script &quot;%1&quot;: %2</source>
        <translation>Nieprawidłowy URL do pobrania skryptu dla pakietu &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="521"/>
        <source>Downloading package script &quot;%1&quot; from URL: %2.</source>
        <translation>Pobieranie skryptu dla pakietu &quot;%1&quot; z URL: %2.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="224"/>
        <source>Unknown error.</source>
        <translation>Nieznany błąd.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="268"/>
        <source>Finished downloading package &quot;%1&quot;.</source>
        <translation>Zakończono pobieranie pakietu: &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="274"/>
        <source>Network error when downloading package &quot;%1&quot;: [%2] %3</source>
        <translation>Błąd sieciowy podczas pobierania pakietu &quot;%1&quot;: [%2] %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="297"/>
        <source>Finished downloading package script &quot;%1&quot;.</source>
        <translation>Zakończono pobieranie skryptu dla pakietu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="314"/>
        <source>All packages downloaded. Building updater script.</source>
        <translation>Wszystkie pakiety pobrane. Tworzenie skryptu aktualizacyjnego.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="321"/>
        <source>Network error when downloading package script &quot;%1&quot;: [%2] %3</source>
        <translation>Błąd sieciowy podczas pobierania skryptu dla pakietu &quot;%1&quot;: [%2] %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="350"/>
        <source>Requesting update confirmation.</source>
        <translation>Potwierdź aktualizację.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="362"/>
        <source>No new program updates detected.</source>
        <translation>Nie wykryto nowych aktualizacji dla programu.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="365"/>
        <source>Some update packages were ignored. To install them select &quot;Check for updates&quot; option from &quot;Help&quot; menu.</source>
        <translation>Niektóre pakiety aktualizacyjne zotały zignorowane. Aby je zainstalować, wybierz &quot;Sprawdź aktualizacje&quot; z menu &quot;Pomoc&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="430"/>
        <source>Failed to create directory for updates storage: %1</source>
        <translation>Nie udało się stworzyć katalogu dla zapisu aktualizacji: %1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="465"/>
        <source>Invalid download URL for package &quot;%1&quot;: %2</source>
        <translation>Nieprawidłowy URL dla pobrania pakietu &quot;%1&quot;: %2</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="471"/>
        <source>Downloading package &quot;%1&quot; from URL: %2.</source>
        <translation>Pobieranie pakietu &quot;%1&quot; z URL: %2.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="486"/>
        <source>Couldn&apos;t save file in path: %1</source>
        <translation>Nie można zapisać pliku w ścieżce: %1</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="91"/>
        <source>Missing main &quot;update&quot; node.</source>
        <translation>Brakuje głównego węzła &quot;update&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="126"/>
        <source>Missing &quot;install&quot; element.</source>
        <translation>Brakuje elementu &quot;install&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="136"/>
        <source>Missing text node for &quot;package&quot; element for &quot;file&quot; element %1</source>
        <translation>Brakuje tekstu w elemencie &quot;package&quot; w elemencie &quot;file&quot; %1</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="158"/>
        <source>Missing &quot;packages&quot; element.</source>
        <translation>Brakuje elementu &quot;packages&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="172"/>
        <source>More than one &quot;package&quot; element found.</source>
        <translation>Znaleziono więcej niż jeden element &quot;package&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="174"/>
        <source>Missing &quot;package&quot; element.</source>
        <translation>Brakuje elementu &quot;package&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="183"/>
        <source>Failed to find &quot;name&quot; text node.</source>
        <translation>Brakuje węzła tekstowego dla elementu &quot;name&quot;.</translation>
    </message>
</context>
<context>
    <name>BroadcastManager</name>
    <message>
        <location filename="../serverapi/broadcastmanager.cpp" line="50"/>
        <source>%1 LAN server gone: %2, %3:%4</source>
        <translation>Serwer LAN dla gry %1 przepadł: %2, %3:%4</translation>
    </message>
    <message>
        <location filename="../serverapi/broadcastmanager.cpp" line="63"/>
        <source>New %1 LAN server detected: %2:%3</source>
        <translation>Nowy serwer LAN dla gry %1 wykryty: %2:%3</translation>
    </message>
</context>
<context>
    <name>CFGAppearance</name>
    <message>
        <location filename="../gui/configuration/cfgappearance.h" line="45"/>
        <source>Appearance</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="99"/>
        <source>Player slots style:</source>
        <translation>Styl slotów graczy:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="113"/>
        <source>Marines</source>
        <translation>Marines</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="118"/>
        <source>Blocks</source>
        <translation>Bloczki</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="123"/>
        <source>Numeric</source>
        <translation>Liczbowy</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="229"/>
        <source>Bots are not players</source>
        <translation>Boty nie są graczami</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="208"/>
        <source>Hide passwords</source>
        <translation>Ukryj hasła</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="215"/>
        <source>Lookup server hosts</source>
        <translation>Pobieraj nazwy hostów serwerów</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="34"/>
        <source>Language:</source>
        <translation>Język:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="222"/>
        <source>Colorize server console</source>
        <translation>Koloruj konsolę serwerową</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="236"/>
        <source>Draw grid in server table</source>
        <translation>Rysuj siatkę w tabelce serwerów</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="165"/>
        <source>Servers with buddies color:</source>
        <translation>Kolor serwerów ze znajomymi:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="87"/>
        <source>Full retranslation requires a restart.</source>
        <translation>Pełne przetłumaczenie wymaga restartu.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="131"/>
        <source>Pinned servers color:</source>
        <translation>Kolor przypiętych serwerów:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="148"/>
        <source>LAN servers color:</source>
        <translation>Kolor serwerów LAN:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="243"/>
        <source>Use tray icon</source>
        <translation>Ikona na tacce systemowej</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="252"/>
        <source>When close button is pressed, minimize to tray icon.</source>
        <translation>Przycisk zamykania minimalizuje do tacki.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="95"/>
        <source>Use system language</source>
        <translation>Używaj języka systemu</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="187"/>
        <source>Unknown language definition &quot;%1&quot;</source>
        <translation>Nieznana definicja języka &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="213"/>
        <source>Loading translation &quot;%1&quot;</source>
        <translation>Wczytuję tłumaczenie &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="217"/>
        <source>Program needs to be restarted to fully apply the translation</source>
        <translation>Należy zrestartować program aby tłumaczenie zadziałało w pełni</translation>
    </message>
</context>
<context>
    <name>CFGAutoUpdates</name>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.h" line="44"/>
        <source>Auto updates</source>
        <translation>Automatyczne aktualizacje</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="44"/>
        <source>Disabled</source>
        <translation>Wyłączone</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="51"/>
        <source>Notify me but don&apos;t install</source>
        <translation>Powiadamiaj mnie, ale nie instaluj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="61"/>
        <source>Install automatically</source>
        <translation>Instaluj automatycznie</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="76"/>
        <source>Update channel:</source>
        <translation>Kanał aktualizacji:</translation>
    </message>
</context>
<context>
    <name>CFGCustomServers</name>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="87"/>
        <source>Toggle enabled state</source>
        <translation>Włącz/wyłącz</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="112"/>
        <source>Doomseeker - pinned servers</source>
        <translation>Doomseeker - serwery przypięte</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="126"/>
        <source>Port must be within range 1 - 65535</source>
        <translation>Port musi być w zakresie 1 - 65535</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="130"/>
        <source>Unimplemented behavior!</source>
        <translation>Niezaimplementowana funkcja!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="205"/>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="205"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="46"/>
        <source>&lt;p&gt;Add a new entry to the list. To specify the address and port of the custom server double-click on the cells in the table. Domain names are supported.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Dodaj nowy wpis do listy. Adres i port serwera podaje się poprzez dwuklik na komórkach w tabelce. Można też używać nazw domenowych.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="49"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="60"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="78"/>
        <source>Enable selected</source>
        <translation>Włącz zaznaczone</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="81"/>
        <source>Enable</source>
        <translation>Włącz</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="92"/>
        <source>Disable selected</source>
        <translation>Wyłącz zaznaczone</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="95"/>
        <source>Disable</source>
        <translation>Wyłącz</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="113"/>
        <source>Change the game on the selected servers.</source>
        <translation>Zmień grę dla zaznaczonych serwerów.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="116"/>
        <source>Set game</source>
        <translation>Ustaw grę</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.h" line="50"/>
        <source>Pinned servers</source>
        <translation>Serwery przypięte</translation>
    </message>
</context>
<context>
    <name>CFGFilePaths</name>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="61"/>
        <source>Path</source>
        <translation>Ścieżka</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="61"/>
        <source>Recurse</source>
        <translation>Rekursywnie</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="69"/>
        <source>Doomseeker supports special characters in user-configurable paths. This affects all paths everywhere, not only the file paths configured on this page. The placeholders are:</source>
        <translation>Doomseeker posiada wsparcie dla znaków specjalnych w ścieżkach konfigurowalnych przez użytkownika. Dotyczy to wszystkich takich ścieżek, wszędzie, nie tylko tych konfigurowalnych na tej stronie. Te znaki specjalne to:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="75"/>
        <source>&lt;b&gt;~&lt;/b&gt; - if at the beginning of the path, resolved to the home directory of the current user</source>
        <translation>&lt;b&gt;~&lt;/b&gt; - jeżeli znajduje się na początku ścieżki, jest zamieniany na katalog domowy aktualnego użytkownika</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="79"/>
        <source>&lt;b&gt;~&lt;i&gt;user&lt;/i&gt;&lt;/b&gt; - if at the beginning of the path, resolved to the home directory of the specified &lt;i&gt;user&lt;/i&gt;</source>
        <translation>&lt;b&gt;~&lt;i&gt;użytkownik&lt;/i&gt;&lt;/b&gt; - jeżeli znajduje się na początku ścieżki, jest zamieniany na katalog domowy podanego &lt;i&gt;użytkownika&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="83"/>
        <source>&lt;b&gt;$PROGDIR&lt;/b&gt; - resolved to the directory where Doomseeker&apos;s executable is located; this may create invalid paths if used in the middle of the path</source>
        <translation>&lt;b&gt;$PROGDIR&lt;/b&gt; - zamieniany na katalog w którym znajduje się plik wykonywalny Doomseekera; może prowadzić do tworzenia nieprawidłowych ścieżek, jeżeli zostanie umieszczony w środku ścieżki</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="87"/>
        <source>&lt;b&gt;$&lt;i&gt;NAME&lt;/i&gt;&lt;/b&gt; - any &lt;i&gt;NAME&lt;/i&gt; is resolved to the environment variable of the same &lt;i&gt;NAME&lt;/i&gt;, or to empty if the environment variable is absent</source>
        <translation>&lt;b&gt;$&lt;i&gt;NAZWA&lt;/i&gt;&lt;/b&gt; - dowolna &lt;i&gt;NAZWA&lt;/i&gt; jest zamieniana na zmienną środowiskową o tej samej &lt;i&gt;NAZWIE&lt;/i&gt;, albo jest usuwana jeżeli zmienna jest nieobecna</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="93"/>
        <source>This option controls whether the &lt;b&gt;$PROGDIR&lt;/b&gt; and &lt;b&gt;$&lt;i&gt;NAME&lt;/i&gt;&lt;/b&gt; will be resolved or ignored. If this option is &lt;b&gt;checked&lt;/b&gt;, both are resolved as explained. If this option is &lt;b&gt;unchecked&lt;/b&gt;, both will be taken literally. The &lt;b&gt;~&lt;/b&gt; placeholders are always resolved regardless.</source>
        <translation>Ta opcja decyduje czy &lt;b&gt;$PROGDIR&lt;/b&gt; i &lt;b&gt;$&lt;i&gt;NAZWA&lt;/i&gt;&lt;/b&gt; będą zamieniane czy ignorowane. Jeżeli ta opcja zostanie &lt;b&gt;zaznaczona&lt;/b&gt;, obydwa wzorce będą zastępowane tak jak wytłumaczono. Jeżeli zostanie &lt;b&gt;odznaczona&lt;/b&gt;, obydwa wzorce będą brane dosłownie. Wzorce &lt;b&gt;~&lt;/b&gt; są zawsze zastępowane niezależnie od tej opcji.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="151"/>
        <source>Doomseeker - Add game mod path</source>
        <translation>Doomseeker - Dodaj ścieżkę do modów</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="261"/>
        <source>Path empty.</source>
        <translation>Pusta ścieżka.</translation>
    </message>
    <message>
        <source>No path specified.</source>
        <translation type="vanished">Nie podano ścieżki.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="265"/>
        <source>Path doesn&apos;t exist.</source>
        <translation>Ścieżka nie istnieje.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="268"/>
        <source>Path is not a directory.</source>
        <translation>Ścieżka nie jest katalogiem.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.h" line="49"/>
        <source>File paths</source>
        <translation>Ścieżki do plików</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="29"/>
        <source>Where are the game mods, IWADs, PWADs:</source>
        <translation>Gdzie są mody, IWADy, PWADy:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="64"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;WARNING: &lt;/span&gt;It&apos;s not recommended to enable recursion for directories with lots of subdirectories. With such directories you may experience heavy performance loss and high hard drive usage. When recursion is enabled, the file search will go the entire way down to the bottom of the directory tree if necessary.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;UWAGA: &lt;/span&gt;Nie jest zalecane włączanie rekursji dla katalogów z wieloma podkatalogami. Przeszukiwanie takich katalogów może być mało wydajne i powodować duże zużycie dysku twardego. Przy włączonej rekursji, pliki będą wyszukiwane w całym drzewie katalogów, aż do ostatniego podkatalogu w razie potrzeby.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="92"/>
        <source>Browse for a file path.</source>
        <translation>Przeglądaj ścieżkę do plików.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="95"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="102"/>
        <source>Add an empty path to the list.</source>
        <translation>Dodaj pustą ścieżkę do listy.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="105"/>
        <source>Add empty</source>
        <translation>Dodaj pustą</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="116"/>
        <source>Remove the selected paths from the list.</source>
        <translation>Usuń zaznaczone ścieżki z listy.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="119"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="132"/>
        <source>&lt;p&gt;If selected Doomseeker will attempt to find the files used on a particular server when the mouse cursor is hovered over the WADs and IWAD columns. This requires a hard drive access to search in all the directories specified above each time a new tooltip is generated. If it takes too long to create such tooltip, you may try disabling this option.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Po włączeniu tej opcji, Doomseeker będzie wyszukiwał na dysku pliki używane na poszczególnych serwerach zawsze gdy zawiesisz kursor myszy nad kolumną WADów albo IWADa. To wymaga dostępu do dysku twardego i będzie przeszukiwać wszystkie katalogi podane powyżej, za każdym razem, gdy będzie trzeba wygenerować nowy dymek z podpowiedzią. Jeżeli generowanie tych dymków okaże się zbyt długotrwałe, możesz spróbować wyłączyć tą opcję.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;If selected Doomseeker will attempt to check the integrity of the local WADs, compared to the ones used on a particular server. This requires a hard drive access to read the entirety of the WADs. If it takes too long to join servers, you may want to disable this feature.&lt;/p&gt;
&lt;p&gt;This setting is used when joining a server and when clicking the &amp;quot;Find missing WADs&amp;quot; button in the context menu.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Po włączeniu tej opcji, Doomseeker będzie sprawdzać spójność lokalnych WADów, porównując je do tych używanych na serwerze gry. To wymaga dostępu do dysku twardego i odczytania każdego WADa w całości. Jeżeli okaże się to zbyt długotrwałe, możesz spróbować wyłączyć tą opcję.&lt;/p&gt;
&lt;p&gt;Doomseeker robi to gdy dołączasz się do serwera, a także gdy używasz akcji &quot;Odszukaj brakujące WADy&quot; z menu kontekstowego serwera.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="153"/>
        <source>Resolve $PLACEHOLDERS in paths</source>
        <translation>Zamieniaj $WZORCE w ścieżkach</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="135"/>
        <source>Tell me where are my WADs located</source>
        <translation>Mów mi, gdzie znajdują się moje WADy</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="142"/>
        <source>&lt;p&gt;If selected Doomseeker will attempt to check the integrity of the local WADs, compared to the ones used on a particular server. This requires a hard drive access to read the entirety of the WADs. It also requires the checksums to be downloaded from the servers, causing more network traffic. If it takes too long to join servers, or there are problems with the server refresh, you may want to disable this feature.&lt;/p&gt;
&lt;p&gt;This setting is used when joining a server and when clicking the &amp;quot;Find missing WADs&amp;quot; button in the context menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Po włączeniu tej opcji, Doomseeker będzie sprawdzać spójność lokalnych WADów, porównując je do tych używanych na serwerze gry. To wymaga dostępu do dysku twardego i odczytywania każdego WADa w całości. Musi też pobierać sumy kontrolne plików z serwerów gry, co zwiększa zużycie łącza. Jeżeli uznasz, że wpływa to negatywnie na wydajność dołączania do serwera, albo powoduje problemy z odświeżaniem serwerów, to możesz spróbować wyłączyć tą opcję.&lt;/p&gt;
&lt;p&gt;Doomseeker używa tej opcji, gdy dołączasz się do serwera, a także gdy używasz akcji &quot;Odszukaj brakujące WADy&quot; z menu kontekstowego serwera.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="146"/>
        <source>Check the integrity of local WADs</source>
        <translation>Sprawdzaj spójność WADów na dysku</translation>
    </message>
</context>
<context>
    <name>CFGGames</name>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="29"/>
        <source>Player name:</source>
        <translation>Nazwa gracza:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="36"/>
        <source>Use Operating System&apos;s username</source>
        <translation>Użyj nazwy użytkownika z Systemu</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="46"/>
        <source>Custom:</source>
        <translation>Własna:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="56"/>
        <source>Player</source>
        <translation>Gracz</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="63"/>
        <source>This is currently used only for recording demos. It&apos;s attached to each demo you record.</source>
        <translation>Nazwa gracza jest póki co używana jedynie podczas nagrywania demo. Doomseeker dopisuje ją do nagrania.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.h" line="43"/>
        <source>Games</source>
        <translation>Gry</translation>
    </message>
</context>
<context>
    <name>CFGIP2C</name>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="29"/>
        <source>&lt;p&gt;A new IP2C database will be downloaded if the local one is missing or has a different checksum than the one stored on Doomseeker&apos;s web page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Nowa baza IP2C zostanie pobrana jeśli nie ma jej na dysku, albo jeśli ta na dysku ma inną sumę kontrolną niż ta, która jest przechowywana na stronie Internetowej Doomseekera.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="32"/>
        <source>IP2C auto update</source>
        <translation>Auto aktualizacja bazy IP2C</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Servers of some games are able to directly tell Doomseeker in which country they are located in. If this is checked, Doomseeker will honor this information. If unchecked, Doomseeker will always try to detect the country by server&apos;s IP. &lt;/p&gt;&lt;p&gt;Neither of these methods may be accurate - the server can tell false info and the IP2C database may contain inaccuracies. Either method can lead to the wrong country being displayed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Serwery niektórych gier potrafią powiedzieć Doomseekerowi w jakim kraju się znajdują. Jeśli zaznaczysz tą opcję, Doomseeker będzie z tego korzystał. Jeśli ją odznaczysz, Doomseeker będzie wykrywał kraj na podstawie adresu IP serwera.&lt;/p&gt;&lt;p&gt;Obie te metody mogą być niedokładne. Serwery mogą specjalnie podawać inny kraj, a baza adresów IP2C jest niedoskonała. Doomseeker może czasami wyświetlić niewłaściwy kraj niezależnie od wybranej metody.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="42"/>
        <source>Honor countries set by servers</source>
        <translation>Akceptuj kraje podawane przez serwery</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.h" line="44"/>
        <source>IP2C</source>
        <translation>IP2C</translation>
    </message>
</context>
<context>
    <name>CFGIRCAppearance</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.h" line="44"/>
        <source>Appearance</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.h" line="49"/>
        <source>IRC - Appearance</source>
        <translation>IRC - Wygląd</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="37"/>
        <source>Default text color:</source>
        <translation>Domyślny kolor tekstu:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="54"/>
        <source>Channel action color:</source>
        <translation>Kolor akcji kanału:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="71"/>
        <source>Network action color:</source>
        <translation>Kolor akcji sieci:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="105"/>
        <source>Error color:</source>
        <translation>Kolor błędu:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="125"/>
        <source>Background color:</source>
        <translation>Kolor tła:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="159"/>
        <source>Main font:</source>
        <translation>Czcionka główna:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="142"/>
        <source>URL color:</source>
        <translation>Kolor linków:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="88"/>
        <source>CTCP color:</source>
        <translation>Kolor CTCP:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="185"/>
        <source>User list</source>
        <translation>Lista użytkowników</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="194"/>
        <source>Font:</source>
        <translation>Czcionka:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="211"/>
        <source>Selected text color:</source>
        <translation>Kolor tekstu zaznaczenia:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="228"/>
        <source>Selected background:</source>
        <translation>Tło zaznaczenia:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="248"/>
        <source>Prepends all entries with [hh:mm:ss] timestamps.</source>
        <translation>Dodawaj sygnatury czasowe [hh:mm:ss] do tekstu.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="251"/>
        <source>Enable timestamps</source>
        <translation>Włącz sygnatury czasowe</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="258"/>
        <source>Window/task bar alert on important event (private msg, callsign)</source>
        <translation>Alert w oknie/na pasku zadań o ważnych wydarzeniach (wiadomość prywatna, wywołanie)</translation>
    </message>
</context>
<context>
    <name>CFGIRCDefineNetworkDialog</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="14"/>
        <source>Define IRC Network</source>
        <translation>Zdefiniuj sieć IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="30"/>
        <source>Description:</source>
        <translation>Opis:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="40"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="50"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="93"/>
        <source>Server password (usually this should remain empty):</source>
        <translation>Hasło serwera (zazwyczaj powinno pozostać puste):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="24"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="109"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="157"/>
        <source>Hide</source>
        <translation>Ukryj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="141"/>
        <source>Nickserv password:</source>
        <translation>Hasło nickserv:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="169"/>
        <source>Nickserv command:</source>
        <translation>Komenda nickserv:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="179"/>
        <source>/privmsg nickserv identify %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="199"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;%1 is substituted with nickserv password.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Usually nickserv command shouldn&apos;t be changed.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Pod %1 podstawiane jest hasło nickserv.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;W większości przypadków komenda nickserv nie powinna być zmieniana.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="135"/>
        <source>Nickserv</source>
        <translation>Nickserv</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="221"/>
        <source>Specify channels that you wish to join by default when connecting to this network:</source>
        <translation>Podaj kanały do których chcesz dołączyć zawsze po podłączeniu do sieci:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="234"/>
        <source>SPACEBAR or ENTER separated</source>
        <translation>Rozdzielaj ENTEREM lub SPACJĄ</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="215"/>
        <source>Channels</source>
        <translation>Kanały</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="251"/>
        <source>Execute following commands on network join:</source>
        <translation>Wykonaj te komendy po podłączeniu do sieci:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="274"/>
        <source>Put each command on a separate line.</source>
        <translation>Wprowadź każdą komendę w osobnej linii.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="281"/>
        <source>Max. 512 characters per command.</source>
        <translation>Maks. 512 znaków na komendę.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="245"/>
        <source>Join Script</source>
        <translation>Skrypt dołączeniowy</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="292"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Warning:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; passwords are stored as plain text in configuration file.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Uwaga:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; hasła są przechowywane w pliku konfiguracyjnym jako zwykły tekst.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="82"/>
        <source>These commands have violated the IRC maximum byte number limit (%1):

</source>
        <translation>Te komendy przekroczyły maksymalny limit IRCa na ilość bajtów (%1):

</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="84"/>
        <source>

If saved, the script may not run properly.

Do you wish to save the script anyway?</source>
        <translation>

Ten skrypt może nie zadziałać poprawnie.

Czy chcesz zapisać skrypt mimo wszystko?</translation>
    </message>
    <message numerus="yes">
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="89"/>
        <source>

... and %n more ...</source>
        <translation>
            <numerusform>

... i %n więcej ...</numerusform>
            <numerusform>

... i %n więcej ...</numerusform>
            <numerusform>

... i%n więcej ...</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="92"/>
        <source>Doomseeker - IRC Commands Problem</source>
        <translation>Doomseeker - problem z komendami IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="117"/>
        <source>	%1 (...)</source>
        <translation>	%1 (...)</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="219"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="225"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="234"/>
        <source>Invalid IRC network description</source>
        <translation>Nieprawidłowy opis sieci IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="220"/>
        <source>Network description cannot be empty.</source>
        <translation>Opis sieci nie może być pusty.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="226"/>
        <source>There already is a network with such description.</source>
        <translation>Już istnieje sieć o takim opisie.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="231"/>
        <source>Network description is invalid.

Only letters, digits, spaces and &quot;%1&quot; are allowed.</source>
        <translation>Opis sieci zawiera błąd.

Dozwolone są tylko litery, cyfry, spacje i &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>CFGIRCNetworks</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.cpp" line="189"/>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.cpp" line="189"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.h" line="49"/>
        <source>Networks</source>
        <translation>Sieci</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.h" line="55"/>
        <source>IRC - Networks</source>
        <translation>IRC - Sieci</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="64"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="75"/>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="86"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="99"/>
        <source>Channel/network quit message:</source>
        <translation>Wiadomość wyjścia z kanału/sieci:</translation>
    </message>
</context>
<context>
    <name>CFGIRCSounds</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="62"/>
        <source>Pick Sound File</source>
        <translation>Wybierz plik dźwiękowy</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="64"/>
        <source>WAVE (*.wav)</source>
        <translation>WAVE (*.wav)</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="137"/>
        <source>No path specified.</source>
        <translation>Nie podano ścieżki.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="141"/>
        <source>File doesn&apos;t exist.</source>
        <translation>Plik nie istnieje.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="144"/>
        <source>This is not a file.</source>
        <translation>To nie jest plik.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.h" line="46"/>
        <source>Sounds</source>
        <translation>Dźwięki</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.h" line="51"/>
        <source>IRC - Sounds</source>
        <translation>IRC - Dźwięki</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="31"/>
        <source>Nickname used:</source>
        <translation>Użyto nicka:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="38"/>
        <source>Private message:</source>
        <translation>Wiadomość prywatna:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="57"/>
        <source>Sound played when your nickname is used in a channel.</source>
        <translation>Dźwięk odtwarzany gdy na kanale zostanie użyty twój nick.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="71"/>
        <source>Browse the sound played when your nickname is used in a channel.</source>
        <translation>Przeglądaj dźwięk odtwarzany gdy na kanale zostanie użyty twój nick.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="100"/>
        <source>Sound played when a private message is received.</source>
        <translation>Dźwięk odtwarzany, gdy otrzymasz prywatną wiadomość.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="114"/>
        <source>Browse the sound played when a private message is received.</source>
        <translation>Przeglądaj dźwięk odtwarzany, gdy otrzymasz prywatną wiadomość.</translation>
    </message>
    <message>
        <source>Sound played when private message is received.</source>
        <translation type="vanished">Dźwięk odtwarzany, gdy otrzymano prywatną wiadomość.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="139"/>
        <source>&lt;b&gt;Note:&lt;/b&gt; Use sounds in .WAV format.</source>
        <translation>&lt;b&gt;Uwaga:&lt;/b&gt; Używaj dźwięków tylko w formacie .WAV.</translation>
    </message>
</context>
<context>
    <name>CFGQuery</name>
    <message>
        <location filename="../gui/configuration/cfgquery.h" line="45"/>
        <source>Query</source>
        <translation>Odświeżanie</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="117"/>
        <location filename="../gui/configuration/cfgquery.ui" line="127"/>
        <source>How many times Doomseeker will attempt to query each server before deeming it to be not responding.</source>
        <translation>Ile razy Doomseeker powinien odpytać serwer, zanim stwierdzi, że ten nie odpowiada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="149"/>
        <location filename="../gui/configuration/cfgquery.ui" line="159"/>
        <source>Delay in miliseconds between each query attempt.</source>
        <translation>Opóźnienie w milisekundach pomiędzy każdą próbą odpytania.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="20"/>
        <source>Refresh servers on startup</source>
        <translation>Odśwież serwery na starcie</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="27"/>
        <source>Refresh before launch</source>
        <translation>Odśwież przed wystartowaniem gry</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="37"/>
        <source>Servers autorefresh</source>
        <translation>Auto-odświeżanie serwerów</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="48"/>
        <location filename="../gui/configuration/cfgquery.ui" line="58"/>
        <location filename="../gui/configuration/cfgquery.ui" line="71"/>
        <source>Minimum value: 30 seconds.</source>
        <translation>Minimum 30 sekund.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="51"/>
        <source>Obtain new server list every:</source>
        <translation>Pobierz nową listę serwerów co:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="74"/>
        <source>seconds</source>
        <translation>sekund</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="96"/>
        <source>Enabling this will prevent server list from disappearing when you are browsing through it.</source>
        <translation>Włączenie tego zapobiegnie znikaniu listy serwerów w momencie, gdy ją przeglądasz.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="99"/>
        <source>Don&apos;t refresh if Doomseeker window is active.</source>
        <translation>Nie odświeżaj, gdy okno Doomseekera jest aktywne.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="109"/>
        <source>Refresh speed</source>
        <translation>Prędkość odświeżania</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="120"/>
        <source>Number of attempts per server:</source>
        <translation>Ilość prób na serwer:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="152"/>
        <source>Delay between attemps (ms):</source>
        <translation>Opóźnienie pomiędzy próbami (ms):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="181"/>
        <source>Interval between different servers (ms):</source>
        <translation>Opóźnienie pomiędzy poszczególnymi serwerami (ms):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="188"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sleep delay that occurs after querying a server and before querying a next one. Increasing this value may negatively affect the speed of server list refresh, but will free up CPU and decrease bandwidth usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Opóźnienie pomiędzy odpytywaniem kolejnych serwerów. Im to opóźnienie będzie większe, tym wolniej będzie odświeżana cała lista serwerów, ale obniży to też zużycie łącza.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="211"/>
        <source>Cautious</source>
        <translation>Ostrożny</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="218"/>
        <source>Moderate</source>
        <translation>Umiarkowany</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="225"/>
        <source>Aggressive</source>
        <translation>Agresywny</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="232"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Be warned: in this mode correctly working servers may appear as &amp;quot;not responding&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uwaga: w tym trybie poprawnie działające serwery mogą pojawiać się jako &amp;quot;bez odpowiedzi&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="235"/>
        <source>Very aggressive</source>
        <translation>Wściekły</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="244"/>
        <source>These buttons change refresh speed settings to different values. Speed settings determine how long will it take to populate the server list and how accurate the refresh will be. Higher speed can mean lesser accuracy.

If you&apos;re experiencing problems with refreshing servers, you might prefer &quot;Cautious&quot; preset, while &quot;Aggressive&quot; presets should be preferred with good connection quality.

These values can be also modified by hand.</source>
        <translation>Możesz zmienić prędkość odświeżania za pomocą tych przycisków. Większa prędkość może obniżać dokładność.

Jeżeli wystąpią problemy z odświeżaniem serwerów, wybierz tryb &quot;Ostrożny&quot;. Tryby &quot;Agresywne&quot; zalecane są przy dobrych połączeniach internetowych.

Poszczególne parametry możesz też dostosowywać własnoręcznie.</translation>
    </message>
</context>
<context>
    <name>CFGServerPasswords</name>
    <message>
        <source>Reveal</source>
        <translation type="vanished">Odkryj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="31"/>
        <source>Hide</source>
        <translation>Ukryj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="48"/>
        <source>Add password</source>
        <translation>Dodaj hasło</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="92"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="97"/>
        <source>Last Game</source>
        <translation>Ostatnia gra</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="102"/>
        <source>Last Server</source>
        <translation>Ostatni serwer</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="107"/>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="194"/>
        <source>Last Time</source>
        <translation>Ostatni czas</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="115"/>
        <source>Remove selected passwords</source>
        <translation>Usuń zaznaczone hasła</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="141"/>
        <source>Servers the selected password was used on:</source>
        <translation>Serwery na których wykorzystano zaznaczone hasło:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="179"/>
        <source>Game</source>
        <translation>Gra</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="184"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="189"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="202"/>
        <source>Remove selected servers</source>
        <translation>Usuń zaznaczone serwery</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Dodaj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="228"/>
        <source>Max number of servers to remember per password:</source>
        <translation>Maks. liczba serwerów do zapamiętania na hasło:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="247"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Warning:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Some servers may be lost.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Uwaga:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; niektóre serwery mogą zostać utracone.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.h" line="48"/>
        <source>Server passwords</source>
        <translation>Hasła serwerów</translation>
    </message>
</context>
<context>
    <name>CFGWadAlias</name>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="32"/>
        <source>Aliases listed here will be used if WADs are not found.</source>
        <translation>Zamienniki są używane, gdy oryginalne WADy nie zostaną odnalezione.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="55"/>
        <source>WAD</source>
        <translation>WAD</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="60"/>
        <source>Aliases</source>
        <translation>Zamienniki</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="65"/>
        <source>Match</source>
        <translation>Dopasowanie</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="99"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="75"/>
        <source>Add defaults</source>
        <translation>Dodaj domyślne</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="110"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="123"/>
        <source>Multiple aliases can be separated with semicolon &apos;;&apos;</source>
        <translation>Oddzielaj wiele zamienników średnikami &apos;;&apos;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="66"/>
        <source>Left-to-Right will use the alias files instead of the main file but not vice-versa.</source>
        <translation>Lewy-do-Prawych - użyj zamienników zamiast głównego pliku, ale nie odwrotnie.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="69"/>
        <source>All Equal will treat all files as equal and try to match them in any combination.</source>
        <translation>Wszystkie Równe - potrakuj wszystkie pliki jako równoznaczne i używaj ich w dowolnej kombinacji.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="186"/>
        <source>Left-to-Right</source>
        <translation>Lewy-do-Prawych</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="187"/>
        <source>All Equal</source>
        <translation>Wszytkie Równe</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.h" line="44"/>
        <source>WAD aliases</source>
        <translation>Zamienniki WADów</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerAppearance</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.h" line="44"/>
        <source>Appearance</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.h" line="49"/>
        <source>Wadseeker - Appearance</source>
        <translation>Wadseeker - Wygląd</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="31"/>
        <source>Message colors:</source>
        <translation>Kolory wiadomości:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="51"/>
        <source>Notice:</source>
        <translation>Powiadomienie:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="61"/>
        <source>Error:</source>
        <translation>Błąd:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="71"/>
        <source>Critical error:</source>
        <translation>Błąd krytyczny:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="81"/>
        <source>Navigation:</source>
        <translation>Nawigacja:</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerGeneral</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="131"/>
        <source>No path specified.</source>
        <translation>Nie podano ścieżki.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="134"/>
        <source>This path doesn&apos;t exist.</source>
        <translation>Ta ścieżka nie istnieje.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="137"/>
        <source>This is not a directory.</source>
        <translation>To nie jest katalog.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="140"/>
        <source>This directory cannot be written to.</source>
        <translation>Do tego katalogu nie można zapisywać.</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">Ogólne</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.h" line="44"/>
        <source>Wadseeker</source>
        <translation>Wadseeker</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.h" line="48"/>
        <source>Wadseeker - General</source>
        <translation>Wadseeker - Ogólne</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="29"/>
        <source>Directory where Wadseeker will put WADs into:</source>
        <translation>Katalog do którego Wadseeker będzie zapisywał WADy:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="48"/>
        <source>Max concurrent site seeks:</source>
        <translation>Maks. liczba jednoczesnych poszukiwań:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="83"/>
        <source>Max concurrent WAD downloads:</source>
        <translation>Maks. liczba jednoczesnych pobierań WADów:</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerIdgames</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="29"/>
        <source>Idgames</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="35"/>
        <source>Use the /idgames Archive</source>
        <translation>Używaj Archiwum /idgames</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="63"/>
        <source>The /idgames Archive URL (changing this is NOT recommended):</source>
        <translation>Adres Archiwum /idgames (zmienianie tego NIE jest zalecane):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="75"/>
        <source>Default</source>
        <translation>Domyślnie</translation>
    </message>
    <message>
        <source>Wad Archive</source>
        <translation type="vanished">Wad Archive</translation>
    </message>
    <message>
        <source>Use Wad Archive</source>
        <translation type="vanished">Używaj Wad Archive</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.h" line="44"/>
        <source>Archives</source>
        <translation>Archiwa</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.h" line="49"/>
        <source>Wadseeker - Archives</source>
        <translation>Wadseeker - Archiwa</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerSites</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.h" line="46"/>
        <source>Sites</source>
        <translation>Źródła</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.h" line="51"/>
        <source>Wadseeker - Sites</source>
        <translation>Wadseeker - Źródła</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="56"/>
        <source>Add defaults</source>
        <translation>Dodaj domyślne</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="104"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wadseeker library comes with its own list of file storage sites. As these sites come and go the list is modified with new updates of the library. Leave this option to be always up-to-date with the current list. &lt;/p&gt;&lt;p&gt;When enabled the list of hardcoded default sites will always be used in addition to the list above.&lt;/p&gt;&lt;p&gt;When disabled only the list above is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wadseeker posiada swoją własną listę stron z plikami. Ta lista jest aktualizowana wraz z Wadseekerem wtedy, gdy strony przestają działać lub pojawiają się nowe. Pozostaw tą opcję zaznaczoną, aby zawsze korzystać z aktualnej listy.&lt;/p&gt;&lt;p&gt;Gdy ta opcja jest włączona, lista stron zakodowanych w programie będzie zawsze dodawana do listy powyżej.&lt;/p&gt;&lt;p&gt;Gdy ta opcja jest wyłączona, tylko ta lista powyżej będzie używana.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="107"/>
        <source>Always use Wadseeker&apos;s default sites</source>
        <translation>Zawsze używaj domyślnych źródeł Wadseekera</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="53"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adds sites hardcoded within the Wadseeker library to the list. No URL that is currently on the list will be removed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj źródła zakodowane w Wadseekerze do listy. Żaden URL będący obecnie na liście nie zostanie usunięty.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="80"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="91"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
</context>
<context>
    <name>CfgChatLogsPage</name>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="61"/>
        <source>Browse chat logs storage directory</source>
        <translation>Przeglądaj katalog przechowujący logi z chata</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="74"/>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="81"/>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="87"/>
        <source>Directory error</source>
        <translation>Błąd katalogu</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="74"/>
        <source>Directory not specified.</source>
        <translation>Katalog nie został podany.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="81"/>
        <source>Directory doesn&apos;t exist.</source>
        <translation>Katalog nie istnieje.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="136"/>
        <source>The specified path isn&apos;t a directory.</source>
        <translation>Podana ścieżka nie jest katalogiem.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.h" line="46"/>
        <source>Logging</source>
        <translation>Logowanie</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.h" line="51"/>
        <source>IRC - Logging</source>
        <translation>IRC - Logowanie</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="32"/>
        <source>Store chat logs</source>
        <translation>Przechowuj logi z chata</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="39"/>
        <source>Restore logs when re-entering chat</source>
        <translation>Przywracaj logi podczas ponownego wejścia na chata</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="59"/>
        <source>Remove old log archives</source>
        <translation>Usuwanie starych archiwów logów</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="68"/>
        <source>Remove all older than:</source>
        <translation>Usuń wszystkie starsze niż:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="81"/>
        <source> days</source>
        <translation> dni</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="126"/>
        <source>Logs storage directory:</source>
        <translation>Katalog przechowujący logi:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="151"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="166"/>
        <source>Explore</source>
        <translation>Eksploruj</translation>
    </message>
</context>
<context>
    <name>ChatLogs</name>
    <message>
        <location filename="../irc/chatlogs.cpp" line="90"/>
        <source>Won&apos;t transfer chat logs from &quot;%1&quot; to &quot;%2&quot; as directory &quot;%2&quot; already exists.</source>
        <translation>Logi chata nie zostaną przeniesione z &quot;%1&quot; do &quot;%2&quot;, ponieważ katalog &quot;%2&quot; już istnieje.</translation>
    </message>
    <message>
        <location filename="../irc/chatlogs.cpp" line="94"/>
        <source>Failed to transfer chat from &quot;%1&quot; to &quot;%2&quot;</source>
        <translation>Błąd przenoszenia chata z &quot;%1&quot; do &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../irc/chatlogs.cpp" line="98"/>
        <source>Chat logs transfer</source>
        <translation>Przenoszenie logów z chata</translation>
    </message>
</context>
<context>
    <name>CheckWadsDlg</name>
    <message>
        <location filename="../gui/checkwadsdlg.ui" line="17"/>
        <source>Doomseeker - Checking WADs</source>
        <translation>Doomseeker - Sprawdzanie WADów</translation>
    </message>
    <message>
        <location filename="../gui/checkwadsdlg.ui" line="32"/>
        <source>Checking the location and integrity of your WADs</source>
        <translation>Sprawdzanie położenia i spójności Twoich WADów</translation>
    </message>
</context>
<context>
    <name>CmdArgsHelp</name>
    <message>
        <location filename="../cmdargshelp.cpp" line="40"/>
        <source>--connect &lt;protocol://ip[:port]&gt;
    Attempts to connect to the specified server.
</source>
        <translation>--connect &lt;protokół://ip[:port]&gt;
    Połącz się z podanym serwerem.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="43"/>
        <source>--create-game
    Launches Doomseeker in &quot;Create Game&quot; mode.
</source>
        <translation>--create-game
    Uruchamia Doomseeker w trybie &quot;Utwórz Grę&quot;.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="46"/>
        <source>--datadir &lt;directory&gt;
    Sets an explicit search location for
    IP2C data along with plugins.
    Can be specified multiple times.
</source>
        <translation>--datadir &lt;katalog&gt;
    Ustawia katalog, w którym program
    będzie szukał wtyczek oraz bazy IP2C.
    Może zostać podany wiele razy.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="62"/>
        <source>--help
    Prints this list of command line arguments.
</source>
        <translation>--help
    Wypisuje tą pomoc.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="65"/>
        <source>--rcon [plugin] [ip]
    Launch the rcon client for the specified ip.
</source>
        <translation>--rcon [wtyczka] [ip]
    Uruchamia klienta rcon pod podane ip.
</translation>
    </message>
    <message>
        <source>--portable
    Starts application in portable mode.
    In portable mode Doomseeker saves all configuration files
    to the directory where its executable resides.
    Normally, configuration is saved to user&apos;s home directory.
</source>
        <translation type="vanished">--portable
    Uruchamia aplikację w tybie przenośnym.
    W trybie przenośnym Doomseeker zapisuje wszystkie pliki
    konfiguracyjne do katalogu, w którym znajduje się
    jego plik wykonywalny.
    Normalnie, te pliki są zapisywane w katalogu domowym
    użytkownika.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="28"/>
        <source>being assigned a different letter</source>
        <translation>zostanie przypisane pod inną literą</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="31"/>
        <source>being mounted on a different path</source>
        <translation>zostanie zamontowane pod inną ścieżką</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="35"/>
        <source>--basedir &lt;directory&gt;
    Changes the default storage directory to the specified
    one (also in --portable mode). Doomseeker stores all of its
    settings, cache and managed files in this directory.
</source>
        <translation>--basedir &lt;katalog&gt;
    Zmienia domyślny katalog przechowywania na podany
    (także w trybie --portable). Doomseeker zapisuje wszystkie
    swoje ustawienia, pamięć podręczną i pliki zarządzalne
    w tym katalogu.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="51"/>
        <source>--disable-plugin &lt;plugin&gt;
    Ban a &lt;plugin&gt; from loading even if it is normally loaded.
    Specify the plugin name without the &apos;lib&apos; prefix or the file
    extension, for ex. &apos;vavoom&apos;, not &apos;libvavoom%1&apos;.
</source>
        <translation>--disable-plugin &lt;wtyczka&gt;
    Zabroń ładować &lt;wtyczkę&gt; nawet jeśli normalnie byłaby
    ona załadowana. Podaj nazwę wtyczki bez przedrostka &apos;lib&apos;
    oraz bez rozszerzenia, np. &apos;vavoom&apos;, nie &apos;libvavoom%1&apos;.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="56"/>
        <source>--enable-plugin &lt;plugin&gt;
    Load a &lt;plugin&gt; even if it is normally disabled/banned.
    Specify the plugin name without the &apos;lib&apos; prefix or the file
    extension, for ex. &apos;vavoom&apos;, not &apos;libvavoom%1&apos;.
</source>
        <translation>--enable-plugin &lt;wtyczka&gt;
    Załaduj &lt;wtyczkę&gt; nawet jeśli normalnie nie jest ona
    ładowana. Podaj nazwę wtyczki bez przedrostka &apos;lib&apos;
    oraz bez rozszerzenia, np. &apos;vavoom&apos;, nie &apos;libvavoom%1&apos;.
</translation>
    </message>
    <message>
        <source>--portable
    Starts the application in the portable mode:
    - Doomseeker saves all the configuration files to the directory
      where its executable resides. Normally, configuration is saved to
      user&apos;s home directory. This directory can be changed with --basedir.
    - The current working directory is forced to the directory where
      Doomseeker&apos;s executable resides.
    - Doomseeker will save in the configuration all paths as relative
      in anticipation that they may change between the runs, for ex.
      due to the portable device %1.
</source>
        <translation type="vanished">--portable
    Startuje aplikację w trybie przenośnym:
    - Doomseeker zapisze wszystkie pliki konfiguracyjne w katalogu,
      w którym znajduje się plik wykonywalny. Zwyczajnie konfiguracja
      jest zapisywana w katalogu domowym użytkownika. Oprócz tego
      można ten katalog zmienić opcją --basedir.
    - Katalog roboczy zostanie wymuszony na katalog gdzie znajduje się
      plik wykonywalny Doomseekera.
    - Doomseeker będzie zapisywać wszystkie konfigurowalne ścieżki jako
      względne, oczekując, że ścieżki bezwzględne mogą ulec zmianie
      pomiędzy uruchomieniami programu, na przykład wtedy gdy urządzenie
      %1.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="68"/>
        <source>--portable
    Starts the application in the portable mode:
    - Doomseeker saves all the configuration files to the directory
      where its executable resides. Normally, configuration is saved to
      user&apos;s home directory. This directory can be changed with --basedir.
    - The current working directory is forced to the directory where
      Doomseeker&apos;s executable resides.
    - Doomseeker will save in the configuration all paths as relative
      in anticipation that the absolute paths may change between the runs,
      for ex. due to the portable device %1.
</source>
        <translation>--portable
    Startuje aplikację w trybie przenośnym:
    - Doomseeker zapisze wszystkie pliki konfiguracyjne w katalogu,
      w którym znajduje się plik wykonywalny. Zwyczajnie konfiguracja
      jest zapisywana w katalogu domowym użytkownika. Oprócz tego
      można ten katalog zmienić opcją --basedir.
    - Katalog roboczy zostanie wymuszony na katalog gdzie znajduje się
      plik wykonywalny Doomseekera.
    - Doomseeker będzie zapisywać wszystkie konfigurowalne ścieżki jako
      względne, oczekując, że ścieżki bezwzględne mogą ulec zmianie
      pomiędzy uruchomieniami programu, na przykład wtedy gdy urządzenie
      %1.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="79"/>
        <source>--version-json [file|-]
    Prints version info on Doomseeker and all
    plugins in JSON format to specified file,
    then closes the program. If file is not
    specified or specified as &apos;-&apos;, version info
    is printed to stdout.
</source>
        <translation>--version-json [plik|-]
    Wypisuje wersje Doomseekera oraz
    wszystkich wtyczek w formacie JSON
    do podanego pliku, a następnie zamyka
    program. Jeżeli plik nie zostanie podany,
    lub zamiast niego zostanie podany znak &apos;-&apos;, wersje
    zostaną wypisane na standardowe wyjście.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="86"/>
        <source>--verbose
    Forces verbose logging to stderr.
    This is the default in most cases.
</source>
        <translation>--verbose
    Wymusza szczegółowe logowanie na stderr.
    Ta opcja jest domyślna w większości przypadków.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="90"/>
        <source>--quiet
    Forces no logging to stderr.
    This is the default when dumping versions.
</source>
        <translation>--quiet
    Wymusza brak logowania na stderr.
    Ta opcja jest domyślna podczas zrzucania wersji.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="99"/>
        <source>Available command line parameters:
</source>
        <translation>Dostępne parametry wiersza poleceń:
</translation>
    </message>
    <message numerus="yes">
        <location filename="../cmdargshelp.cpp" line="106"/>
        <source>doomseeker: expected %n argument(s) in option %1

</source>
        <translation>
            <numerusform>doomseeker: spodziewano się %n argumentu w opcji %1

</numerusform>
            <numerusform>doomseeker: spodziewano się %n argumentów w opcji %1

</numerusform>
            <numerusform>doomseeker: spodziewano się %n argumentów w opcji %1

</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="113"/>
        <source>doomseeker: unrecognized option &apos;%1&apos;

</source>
        <translation>doomseeker: nierozpoznana opcja &apos;%1

</translation>
    </message>
</context>
<context>
    <name>ConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/configurationdialog.ui" line="14"/>
        <source>Doomseeker - Configuration</source>
        <translation>Doomseeker - Konfiguracja</translation>
    </message>
</context>
<context>
    <name>ConnectionHandler</name>
    <message>
        <location filename="../connectionhandler.cpp" line="83"/>
        <location filename="../connectionhandler.cpp" line="87"/>
        <source>Doomseeker - join server</source>
        <translation>Doomseeker- dołączanie do serwera</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="84"/>
        <source>Connection to server timed out.</source>
        <translation>Przekroczono limit czasu połączenia z serwerem.</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="88"/>
        <source>An error occured while trying to connect to server.</source>
        <translation>Wystąpił błąd w trakcie łączenia z serwerem.</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="165"/>
        <source>Doomseeker - join game</source>
        <translation>Doomseeker - dołączanie do gry</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="177"/>
        <source>Error while launching game &quot;%2&quot; for server &quot;%1&quot;: %3</source>
        <translation>Błąd uruchamiania gry &quot;%2&quot; dla serwera &quot;%1&quot;: %3</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="179"/>
        <source>Doomseeker - launch game</source>
        <translation>Doomseeker - uruchamianie gry</translation>
    </message>
</context>
<context>
    <name>CopyTextDlg</name>
    <message>
        <location filename="../gui/copytextdlg.ui" line="13"/>
        <source>Doomseeker - Copy Text</source>
        <translation>Doomseeker - Kopiuj tekst</translation>
    </message>
    <message>
        <location filename="../gui/copytextdlg.ui" line="19"/>
        <source>Text to copy:</source>
        <translation>Tekst do skopiowania:</translation>
    </message>
    <message>
        <location filename="../gui/copytextdlg.ui" line="31"/>
        <source>Copy to clipboard</source>
        <translation>Kopiuj do schowka</translation>
    </message>
</context>
<context>
    <name>CreateServerDialog</name>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="504"/>
        <location filename="../gui/createserverdialog.cpp" line="518"/>
        <source>Config files (*.ini)</source>
        <translation>Pliki konfiguracyjne (*.ini)</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="504"/>
        <source>Doomseeker - load game setup config</source>
        <translation>Doomseeker - wczytaj konfigurację gry</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="518"/>
        <location filename="../gui/createserverdialog.cpp" line="528"/>
        <source>Doomseeker - save game setup config</source>
        <translation>Doomseeker - zapisz konfigurację gry</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="528"/>
        <source>Unable to save game setup configuration!</source>
        <translation>Nie można zapisać konfiguracji gry!</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="153"/>
        <location filename="../gui/createserverdialog.cpp" line="389"/>
        <source>Doomseeker - create game</source>
        <translation>Doomseeker - tworzenie gry</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="114"/>
        <source>Doomseeker - Play Demo</source>
        <translation>Doomseeker - odtwórz demo</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="115"/>
        <source>Play demo</source>
        <translation>Odtwórz demo</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="119"/>
        <source>Host server</source>
        <translation>Załóż serwer</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="123"/>
        <source>Play</source>
        <translation>Graj</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="126"/>
        <source>Doomseeker - Setup Remote Game</source>
        <translation>Doomseeker - skonfiguruj zdalną grę</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="118"/>
        <source>Doomseeker - Host Online Game</source>
        <translation>Doomseeker - załóż grę online</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="122"/>
        <source>Doomseeker - Play Offline Game</source>
        <translation>Doomseeker - graj offline</translation>
    </message>
    <message>
        <source>Doomseeker - [Unhandled Host Mode]</source>
        <translation type="vanished">Doomseeker - [nieobsługiwany tryb hostowania]</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="156"/>
        <location filename="../gui/createserverdialog.cpp" line="392"/>
        <source>No game selected</source>
        <translation>Nie wybrano gry</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="69"/>
        <location filename="../gui/createserverdialog.cpp" line="254"/>
        <source>Flags</source>
        <translation>Flagi</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="127"/>
        <source>Play online</source>
        <translation>Graj online</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="130"/>
        <source>Doomseeker - Create Game</source>
        <translation>Doomseeker - tworzenie gry</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="131"/>
        <source>Create game</source>
        <translation>Stwórz grę</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="449"/>
        <source>&amp;Mode</source>
        <translation>&amp;Tryb</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="450"/>
        <source>&amp;Host server</source>
        <translation>&amp;Hostuj serwer</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="453"/>
        <source>&amp;Play offline</source>
        <translation>&amp;Graj offline</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="457"/>
        <source>&amp;Settings</source>
        <translation>&amp;Ustawienia</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="459"/>
        <source>&amp;Load game configuration</source>
        <translation>&amp;Wczytaj konfigurację gry</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="464"/>
        <source>&amp;Save game configuration</source>
        <translation>&amp;Zapisz konfigurację gry</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="470"/>
        <source>&amp;Program settings</source>
        <translation>&amp;Ustawienia programu</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="494"/>
        <source>Run game command line:</source>
        <translation>Wiersz poleceń do uruchomienia gry:</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="495"/>
        <source>Host server command line:</source>
        <translation>Wiersz poleceń do uruchomienia serwera:</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="173"/>
        <source>Doomseeker - error</source>
        <translation>Doomseeker - błąd</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="14"/>
        <source>Create Game</source>
        <translation>Stwórz grę</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="59"/>
        <source>Server</source>
        <translation>Serwer</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="135"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="161"/>
        <source>Command line</source>
        <translation>Wiersz poleceń</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="39"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="49"/>
        <source>Rules</source>
        <translation>Zasady</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="79"/>
        <source>Custom parameters</source>
        <translation>Parametry własne</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="158"/>
        <source>Obtain command line required to launch this game.</source>
        <translation>Wyświetl wiersz poleceń do wystartowania gry.</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
</context>
<context>
    <name>CustomParamsPanel</name>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="23"/>
        <source>Custom parameters (ENTER or SPACEBAR separated):</source>
        <translation>Własne parametry (rozdzielone ENTEREM lub SPACJĄ):</translation>
    </message>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="33"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Note: these are added to the command line as visible on the list.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Console commands usually begin with &apos;+&apos; character. For example: +sv_cheats 1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Uwaga: te parametry są dodawane do listy dokładnie tak, jak widać.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Komendy konsoli gry zazwyczaj zaczynają się od znaku +. Na przykład: +sv_cheats 1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomServers</name>
    <message>
        <location filename="../customservers.cpp" line="163"/>
        <source>Unknown game for custom server %1:%2</source>
        <translation>Nieznana gra dla serwera własnego %1:%2</translation>
    </message>
    <message>
        <location filename="../customservers.cpp" line="175"/>
        <source>Failed to resolve address for custom server %1:%2</source>
        <translation>Nie udało się przetworzyć adresu dla serwera własnego %1:%2</translation>
    </message>
    <message>
        <location filename="../customservers.cpp" line="186"/>
        <source>Plugin returned nullptr &quot;Server*&quot; for custom server %1:%2. This is a problem with the plugin.</source>
        <translation>Wtyczka zwróciła nullptr  &quot;Server*&quot; dla serwera własnego %1:%2. Jest to problem z wtyczką.</translation>
    </message>
</context>
<context>
    <name>DefaultDifficultyProvider</name>
    <message>
        <location filename="../plugins/enginedefaults.h" line="41"/>
        <source>1 - I&apos;m too young to die</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="42"/>
        <source>2 - Hey, not too rough</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="43"/>
        <source>3 - Hurt me plenty</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="44"/>
        <source>4 - Ultra-violence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="45"/>
        <source>5 - NIGHTMARE!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DemoManagerDlg</name>
    <message>
        <location filename="../gui/demomanager.ui" line="134"/>
        <source>Play</source>
        <translation>Odtwarzaj</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="161"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="185"/>
        <source>Import</source>
        <translation>Importuj</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="192"/>
        <source>Export ...</source>
        <translation>Eksportuj ...</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">Eksportuj</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="360"/>
        <location filename="../gui/demomanager.cpp" line="383"/>
        <source>Unable to save</source>
        <translation>Błąd zapisu</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="360"/>
        <location filename="../gui/demomanager.cpp" line="384"/>
        <source>Could not write to the specified location.</source>
        <translation>Nie można zapisywać do podanego miejsca.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="283"/>
        <source>Remove demo?</source>
        <translation>Usunąć demo?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="284"/>
        <source>Are you sure you want to remove the selected demo?</source>
        <translation>Czy jesteś pewien, że chcesz usunąć wybrane demo?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="272"/>
        <source>Unable to remove</source>
        <translation>Błąd usuwania</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="228"/>
        <source>Export plain demo file</source>
        <translation>Eksportuj zwykły plik demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="230"/>
        <source>Export with Doomseeker metadata</source>
        <translation>Eksportuj z metadanymi Doomseekera</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="272"/>
        <source>Could not remove the selected demo.</source>
        <translation>Nie można usunąć wybranego dema.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="328"/>
        <source>Demo export</source>
        <translation>Eksport demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="329"/>
        <source>The exported files already exist at the specified directory. Overwrite?

%1</source>
        <translation>Eksportowane pliki już istnieją w podanym katalogu. Nadpisać?

%1</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="398"/>
        <source>Import demo</source>
        <translation>Importuj demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="400"/>
        <source>Demos (%1)</source>
        <translation>Dema (%1)</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="410"/>
        <source>Import game demo</source>
        <translation>Importuj demo gry</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="429"/>
        <source>It looks like this demo is already imported. Overwrite anyway?</source>
        <translation>Zdaje się, że to demo jest już zaimportowane. Czy nadpisać mimo wszystko?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="433"/>
        <source>Another demo with this metadata already exists. Overwrite?</source>
        <translation>Inne demo z tymi samymi metadanymi już istnieje. Nadpisać?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="435"/>
        <source>Existing: %1
New: %2
</source>
        <translation>Istniejące: %1
Nowe: %2
</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="441"/>
        <location filename="../gui/demomanager.cpp" line="451"/>
        <source>Demo import</source>
        <translation>Import demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="442"/>
        <source>%1

%2</source>
        <translation>%1

%2</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="452"/>
        <source>Could not import the selected demo.</source>
        <translation>Import wybranego demo nie powiódł się.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="465"/>
        <source>%1 %L2 B (MD5: %3)</source>
        <translation>%1 %L2 B (MD5: %3)</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="493"/>
        <source>No plugin</source>
        <translation>Brak wtyczki</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="494"/>
        <source>The &quot;%1&quot; plugin does not appear to be loaded.</source>
        <translation>Wtyczka &quot;%1&quot; nie jest załadowana.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="529"/>
        <source>Files not found</source>
        <translation>Pliki nie odnalezione</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="530"/>
        <source>The following files could not be located: </source>
        <translation>Następujące pliki nie mogły zostać odnalezione: </translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="577"/>
        <source>Game</source>
        <translation>Gra</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="583"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="585"/>
        <source>WADs / mods</source>
        <translation>WADy / mody</translation>
    </message>
    <message>
        <source>Doomseeker - error</source>
        <translation type="vanished">Doomseeker - błąd</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">Port</translation>
    </message>
    <message>
        <source>WADs</source>
        <translation type="vanished">WADy</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="17"/>
        <source>Demo Manager</source>
        <translation>Menadżer dem</translation>
    </message>
</context>
<context>
    <name>DemoMetaDataDialog</name>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="14"/>
        <source>Game Demo</source>
        <translation>Demo gry</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="22"/>
        <source>Game:</source>
        <translation>Gra:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="29"/>
        <source>Created time:</source>
        <translation>Czas utworzenia:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="49"/>
        <source>yyyy-MM-dd HH:mm:ss</source>
        <translation>yyyy-MM-dd HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="59"/>
        <source>IWAD:</source>
        <translation>IWAD:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="69"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="76"/>
        <source>Game version:</source>
        <translation>Wersja gry:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="88"/>
        <source>WADs and other game files:</source>
        <translation>WADy i inne pliki:</translation>
    </message>
</context>
<context>
    <name>DemoModel</name>
    <message>
        <location filename="../gui/demomanager.cpp" line="83"/>
        <source>Created Time</source>
        <translation>Czas utworzenia</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="83"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="83"/>
        <source>IWAD</source>
        <translation>IWAD</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="83"/>
        <source>WADs</source>
        <translation>WADy</translation>
    </message>
</context>
<context>
    <name>DockBuddiesList</name>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="84"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="85"/>
        <source>Buddy</source>
        <translation>Znajomy</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="86"/>
        <source>Location</source>
        <translation>Miejsce</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="136"/>
        <source>Remove Buddy</source>
        <translation>Usuń znajomego</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="136"/>
        <source>Are you sure you want to remove this pattern?</source>
        <translation>Czy jesteś pewien, że chcesz usunąć ten wzorzec?</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="149"/>
        <location filename="../gui/dockBuddiesList.cpp" line="198"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="160"/>
        <location filename="../gui/dockBuddiesList.cpp" line="204"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="20"/>
        <source>Buddies</source>
        <translation>Znajomi</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="51"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="103"/>
        <source>Manage</source>
        <translation>Zarządzaj</translation>
    </message>
</context>
<context>
    <name>DoomseekerConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/doomseekerconfigurationdialog.cpp" line="112"/>
        <source>Settings saved!</source>
        <translation>Zapisano ustawienia!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/doomseekerconfigurationdialog.cpp" line="114"/>
        <source>Settings save failed!</source>
        <translation>Zapis ustawień nie powiódł się!</translation>
    </message>
    <message>
        <source>Games</source>
        <translation type="vanished">Gry</translation>
    </message>
</context>
<context>
    <name>EngineConfigPage</name>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="165"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - przeglądaj plik wykonywalny</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="267"/>
        <source>Failed to automatically find file.
You may need to use the browse button.</source>
        <translation>Nie udało się automatycznie znaleźć pliku.
Musisz użyć przycisku przeglądaj.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="272"/>
        <source>Game - %1</source>
        <translation>Gra - %1</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="65"/>
        <source>Custom parameters:</source>
        <translation>Parametry własne:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="90"/>
        <source>Add these parameters to the saved list.</source>
        <translation>Zapisz te parametry na liście.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="101"/>
        <source>Remove these parameters from the saved list.</source>
        <translation>Usuń te parametry z zapisanej listy.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="141"/>
        <source>Restore the master server address to the default.</source>
        <translation>Przywróć adres master serwera do wartości domyślnej.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="144"/>
        <source>Default</source>
        <translation>Domyślnie</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="160"/>
        <source>[error]</source>
        <translation>[błąd]</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="129"/>
        <source>Masterserver address:</source>
        <translation>Adres masterserwera:</translation>
    </message>
</context>
<context>
    <name>EnginePluginComboBox</name>
    <message>
        <location filename="../gui/widgets/engineplugincombobox.cpp" line="81"/>
        <source>%1 (unknown)</source>
        <translation>%1 (nieznany)</translation>
    </message>
</context>
<context>
    <name>ExeFile</name>
    <message>
        <source>No %1 executable configured for %2</source>
        <translation type="vanished">Nie skonfigurowano pliku wykonywalnego typu %1 dla %2</translation>
    </message>
    <message>
        <source>The executable path for %1 %2:
%3
is a directory or doesn&apos;t exist.</source>
        <translation type="vanished">Plik wykonywalny dla %1 typu %2
%3
jest katalogiem lub nie istnieje.</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="79"/>
        <source>The %1 executable is not set for %2.</source>
        <translation>Plik wykonywalny typu %1 nie został skonfigurowany dla %2.</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="87"/>
        <source>The %1 executable for %2 doesn&apos;t exist:
%3</source>
        <translation>Plik wykonywalny typu %1 dla %2 nie istnieje:
%3</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="97"/>
        <source>The path to %1 executable for %2 is a directory:
%3</source>
        <translation>Ścieżka do pliku wykonywalnego typu %1 dla %2 prowadzi do katalogu:
%3</translation>
    </message>
</context>
<context>
    <name>FileFilter</name>
    <message>
        <location filename="../filefilter.cpp" line="29"/>
        <source>All files (*)</source>
        <translation>Wszystkie pliki (*)</translation>
    </message>
    <message>
        <location filename="../filefilter.cpp" line="35"/>
        <source>Demos (%1)</source>
        <translation>Dema (%1)</translation>
    </message>
    <message>
        <location filename="../filefilter.cpp" line="41"/>
        <source>Executables (*.exe *.bat *.com)</source>
        <translation>Pliki wykonywalne (*.exe *.bat *.com)</translation>
    </message>
</context>
<context>
    <name>FilePickWidget</name>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="32"/>
        <source>Path to file:</source>
        <translation>Ścieżka do pliku:</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="54"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="67"/>
        <source>Find</source>
        <translation>Znajdź</translation>
    </message>
    <message>
        <source>Doomseeker - choose executable file</source>
        <translation type="vanished">Doomseeker - wybierz plik wykonywalny</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="113"/>
        <source>Path to %1 executable:</source>
        <translation>Ścieżka do pliku wykonywalnego - %1:</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="174"/>
        <source>File doesn&apos;t exist.</source>
        <translation>Plik nie istnieje.</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="177"/>
        <source>This is a directory.</source>
        <translation>To jest katalog.</translation>
    </message>
</context>
<context>
    <name>FileUtilsTr</name>
    <message>
        <location filename="../fileutils.cpp" line="38"/>
        <source>cannot create directory</source>
        <translation>nie można utworzyć katalogu</translation>
    </message>
    <message>
        <location filename="../fileutils.cpp" line="43"/>
        <source>the parent path is not a directory: %1</source>
        <translation>ścieżka nadrzędna nie jest katalogiem: %1</translation>
    </message>
    <message>
        <location filename="../fileutils.cpp" line="48"/>
        <source>lack of necessary permissions to the parent directory: %1</source>
        <translation>brak odpowiednich uprawnień do nadrzędnego katalogu: %1</translation>
    </message>
</context>
<context>
    <name>FreedoomDialog</name>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="96"/>
        <source>Install Freedoom</source>
        <translation>Zainstaluj Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="97"/>
        <source>Select at least one file.</source>
        <translation>Wybierz przynajmniej jeden plik.</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="101"/>
        <source>Downloading &amp; installing ...</source>
        <translation>Pobieranie i instalowanie ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="128"/>
        <source>Downloading Freedoom version info ...</source>
        <translation>Pobieranie informacji o wersji Freedooma ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="134"/>
        <source>Working ...</source>
        <translation>Pracuję ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="145"/>
        <source>Error: %1</source>
        <translation>Błąd: %1</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="174"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="179"/>
        <source>Missing</source>
        <translation>Nie ma</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="188"/>
        <source>Different</source>
        <translation>Różny</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="206"/>
        <source>&lt;p&gt;File: %1&lt;br&gt;Version: %2&lt;br&gt;Description: %3&lt;br&gt;Location: %4&lt;/p&gt;</source>
        <translation>&lt;p&gt;Plik: %1&lt;br&gt;Wersja: %2&lt;br&gt;Opis: %3&lt;br&gt;Położenie: %4&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="283"/>
        <source>%1 %p%</source>
        <translation>%1 %p%</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="14"/>
        <source>Freedoom</source>
        <translation>Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="30"/>
        <source>Freedoom is a free-of-charge data set replacement for commercial Doom games. It will allow you to play on most servers that host modded content without having to purchase Doom games. It won&apos;t allow you to join unmodded servers.</source>
        <translation>Freedoom jest darmowy i zastępuje oficjalne pliki z danymi gier Doom. Będziesz mógł grać z nim na większości serwerów z modami bez potrzeby zakupu gier z serii Doom. Ale Freedoom nie pozwoli ci dołączać się do serwerów bez modów.</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="40"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Webpage: &lt;a href=&quot;https://freedoom.github.io&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://freedoom.github.io&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Strona WWW: &lt;a href=&quot;https://freedoom.github.io&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://freedoom.github.io&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="66"/>
        <source>Install path:</source>
        <translation>Ścieżka do instalacji:</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="108"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="113"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="118"/>
        <source>Install?</source>
        <translation>Instaluj?</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="169"/>
        <source>Work in progress ...</source>
        <translation>Praca w toku ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="75"/>
        <source>Install</source>
        <translation>Instaluj</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Spróbuj ponownie</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
</context>
<context>
    <name>GameClientRunner</name>
    <message>
        <source>Path to working directory for game &quot;%1&quot; is empty.

Make sure the configuration for the client executable is set properly.</source>
        <translation type="vanished">Ścieżka do katalogu roboczego dla gry &quot;%1&quot; jest pusta.

Upewnij się, że klient gry jest skonfigurowany poprawnie.</translation>
    </message>
    <message>
        <source>%1

This directory cannot be used as working directory for game: %2

Executable: %3</source>
        <translation type="vanished">%1

Ten katalog nie może zostać użyty jako katalog roboczy dla gry: %2

Plik wykonywalny: %3</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="217"/>
        <source>Cannot determine the working directory for &quot;%1&quot;. Check the game configuration.</source>
        <translation>Nie można określić katalogu roboczego dla &quot;%1&quot;. Sprawdź konfigurację gry.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="225"/>
        <source>This path cannot be used as the working directory for %1:
%2.</source>
        <translation>Ta ścieżka nie może zostać użyta jako katalog roboczy dla %1:
%2.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="242"/>
        <source>BUG: Plugin doesn&apos;t specify argument for in-game password, but the server requires such password.</source>
        <translation>BUG: Wtyczka nie definiuje argumentu dla hasła w grze, a serwer takiego hasła wymaga.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="294"/>
        <source>BUG: Plugin doesn&apos;t specify argument for connect password, but the server is passworded.</source>
        <translation>BUG: Wtyczka nie definiuje argumentu dla hasła do połączenia, a serwer takie hasło posiada.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="496"/>
        <source>The game can be installed by Doomseeker.</source>
        <translation>Doomseeker może zainstalować tą grę.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="508"/>
        <source>Couldn&apos;t find game %1.</source>
        <translation>Nie odnaleziono gry %1.</translation>
    </message>
    <message>
        <source>Game %1 cannot be found.</source>
        <translation type="vanished">Nie znaleziono gry %1.</translation>
    </message>
</context>
<context>
    <name>GameConfigErrorBox</name>
    <message>
        <location filename="../gui/configuration/gameconfigerrorbox.cpp" line="41"/>
        <source>Configure &amp;game</source>
        <translation>Skonfiguruj &amp;grę</translation>
    </message>
</context>
<context>
    <name>GameDemoTr</name>
    <message>
        <location filename="../gamedemo.cpp" line="53"/>
        <source>Doomseeker - Record Demo</source>
        <translation>Doomseeker - Nagraj demo</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="61"/>
        <source>Error: %1</source>
        <translation>Błąd: %1</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="63"/>
        <source>The demo storage directory doesn&apos;t exist and cannot be created!%1

%2</source>
        <translation>Katalog do przechowywania dem nie istnieje i nie może zostać stworzony!%1

%2</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="69"/>
        <source>The demo storage directory exists but lacks the necessary permissions!

%1</source>
        <translation>Katalog do przechowywania dem istnieje, ale brakuje do niego niezbędnych uprawnień!

%1</translation>
    </message>
</context>
<context>
    <name>GameExeFactory</name>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="72"/>
        <source>game</source>
        <translation>gra</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="78"/>
        <source>client</source>
        <translation>klient</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="81"/>
        <source>server</source>
        <translation>serwer</translation>
    </message>
</context>
<context>
    <name>GameExeRetriever</name>
    <message>
        <location filename="../serverapi/gameexeretriever.cpp" line="41"/>
        <source>Game doesn&apos;t define offline executable.</source>
        <translation>Gra nie posiada pliku wykonywalnego do gry offline.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexeretriever.cpp" line="48"/>
        <source>Game offline executable is not configured.</source>
        <translation>Plik wykonywalny do gry offline nie jest skonfigurowany.</translation>
    </message>
</context>
<context>
    <name>GameExecutablePicker</name>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="73"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - przeglądaj plik wykonywalny</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="115"/>
        <source>Plugin doesn&apos;t support configuration.</source>
        <translation>Plugin nie wspiera konfiguracji.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="122"/>
        <source>Game doesn&apos;t define any executables for this game setup.</source>
        <translation>Gra nie definiuje żadnych plików wykonywalnych dla takiego trybu gry.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="135"/>
        <source>Default executable for this game isn&apos;t configured.</source>
        <translation>Domyślny plik wykonywalny dla tej gry nie jest skonfigurowany.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="161"/>
        <source>Game plugin not set.</source>
        <translation>Plugin gry nie ustawiony.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="45"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="58"/>
        <source>Default</source>
        <translation>Domyślnie</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="81"/>
        <source>This map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>Tej mapy nie ma na liście map. Gra może zachować się dziwnie.</translation>
    </message>
</context>
<context>
    <name>GameHost</name>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="151"/>
        <source>IWAD is not set</source>
        <translation>IWAD nie jest ustawiony</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="159"/>
        <source>IWAD Path error:
&quot;%1&quot; doesn&apos;t exist or is a directory!</source>
        <translation>Błąd ściezki do IWADa:
&quot;%1&quot; nie istnieje lub jest katalogiem!</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="440"/>
        <source>The game executable is not set.</source>
        <translation>Nie skonfigurowano pliku wykonywalnego gry.</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="447"/>
        <source>The executable
%1
doesn&apos;t exist or is not a file.</source>
        <translation>Ścieżka do pliku wykonywalnego
%1
nie istnieje, bądź nie prowadzi do pliku.</translation>
    </message>
    <message>
        <source>%1
doesn&apos;t exist or is not a file.</source>
        <translation type="vanished">%1
nie istnieje lub nie jest plikiem.</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="463"/>
        <source>PWAD path error:
&quot;%1&quot; doesn&apos;t exist!</source>
        <translation>Błąd ścieżki do PWADa:
&quot;%1&quot; nie istnieje!</translation>
    </message>
</context>
<context>
    <name>GameInfoTip</name>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="67"/>
        <source>Players</source>
        <translation>Graczy</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="72"/>
        <source>%1 / %2 (%3 can join)</source>
        <translation>%1 / %2 (%3 może się dołączyć)</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="96"/>
        <source>Scorelimit</source>
        <translation>Limit punktów</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="131"/>
        <source>Timelimit</source>
        <translation>Limit czasu</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="138"/>
        <source>(%1 left)</source>
        <translation>(pozostało %1)</translation>
    </message>
</context>
<context>
    <name>GameRulesPanel</name>
    <message>
        <location filename="../gui/createserver/gamerulespanel.cpp" line="249"/>
        <source>&lt; NONE &gt;</source>
        <translation>&lt; BRAK &gt;</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.cpp" line="283"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="41"/>
        <source>Map list</source>
        <translation>Lista map</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="84"/>
        <source>Hosting limits</source>
        <translation>Limity hostowania</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="90"/>
        <source>Max. clients:</source>
        <translation>Maks. klientów:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="107"/>
        <source>Max. players:</source>
        <translation>Maks. graczy:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="127"/>
        <source>Modifier</source>
        <translation>Modyfikator</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="146"/>
        <source>Extra settings</source>
        <translation>Dodatkowe ustawienia</translation>
    </message>
</context>
<context>
    <name>GeneralGameSetupPanel</name>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="283"/>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="313"/>
        <source>&lt; NONE &gt;</source>
        <translation>&lt; BRAK &gt;</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="480"/>
        <source>Doomseeker - load server config</source>
        <translation>Doomseeker - wczytaj konfigurację serwera</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="481"/>
        <source>Plugin for engine &quot;%1&quot; is not present!</source>
        <translation>Wtyczka dla gry &quot;%1&quot; nie jest załadowana!</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="34"/>
        <source>Game:</source>
        <translation>Gra:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="44"/>
        <source>Executable:</source>
        <translation>Plik wykonywalny:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="105"/>
        <source>If locked, executable won&apos;t change when new config for the same engine is loaded</source>
        <translation>Zaklucz, aby zachować ten plik wykonywalny po załadowaniu innej konfiguracji dla tej samej gry</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="108"/>
        <source>Lock</source>
        <translation>Zaklucz</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="117"/>
        <source>Logging:</source>
        <translation>Logowanie:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="140"/>
        <source>Server name:</source>
        <translation>Nazwa serwera:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="147"/>
        <source>Started from Doomseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="154"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="195"/>
        <source>Allow the game to choose port</source>
        <translation>Pozwól grze wybrać port</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="205"/>
        <source>Game mode:</source>
        <translation>Tryb gry:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="232"/>
        <source>Map:</source>
        <translation>Mapa:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="271"/>
        <source>Demo:</source>
        <translation>Demo:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="311"/>
        <source>This map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>Tej mapy nie ma na liście map. Gra może zachować się dziwnie.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="239"/>
        <source>IWAD:</source>
        <translation>IWAD:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="329"/>
        <source>Additional WADs and files (check the required ones):</source>
        <translation>Dodatkowe WADy i pliki (zaznacz te wymagane):</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="215"/>
        <source>Difficulty:</source>
        <translation>Poziom trudności:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="396"/>
        <source>If checked, the game will try to tell your router to forward necessary ports.</source>
        <translation>Zaznacz by gra poprosiła Twój router o automatyczne przekierowanie niezbędnych portów.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="399"/>
        <source>UPnP</source>
        <translation>UPnP</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="409"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;UPnP port. Set this to 0 to let Doomseeker or game decide.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Port UPnP. Ustaw tutaj 0 aby pozwolić Doomseekerowi albo grze wybrać własny.&lt;/p&gt;&lt;/body&gt;&lt;/html</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="361"/>
        <source>Broadcast to LAN</source>
        <translation>Opublikuj po LANie</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="371"/>
        <source>Broadcast to master</source>
        <translation>Opublikuj na masterze</translation>
    </message>
</context>
<context>
    <name>GeneralInfoTip</name>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="51"/>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="52"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="53"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="59"/>
        <source>Location</source>
        <translation>Miejsce</translation>
    </message>
    <message>
        <source>Location: %1
</source>
        <translation type="vanished">Lokacja: %1
</translation>
    </message>
</context>
<context>
    <name>IP2C</name>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="228"/>
        <source>No flag for country: %1</source>
        <translation>Brak flagi dla kraju: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="173"/>
        <source>Localhost</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="176"/>
        <source>LAN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="219"/>
        <source>Unknown country: %1</source>
        <translation>Nieznany kraj: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="234"/>
        <source>Unknown</source>
        <translation>Nieznany</translation>
    </message>
</context>
<context>
    <name>IP2CCountryTr</name>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="57"/>
        <source>Aruba</source>
        <translation>Aruba</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="58"/>
        <source>Afghanistan</source>
        <translation>Afganistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="59"/>
        <source>Angola</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="60"/>
        <source>Anguilla</source>
        <translation>Anguilla</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="61"/>
        <source>Albania</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="62"/>
        <source>Andorra</source>
        <translation>Andora</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="63"/>
        <source>Netherlands Antilles</source>
        <translation>Antyle Holenderskie</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="64"/>
        <source>United Arab Emirates</source>
        <translation>Zjednoczone Emiraty Arabskie</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="65"/>
        <source>Argentina</source>
        <translation>Argentyna</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="66"/>
        <source>Armenia</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="67"/>
        <source>American Samoa</source>
        <translation>Samoa Amerykańskie</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="68"/>
        <source>Antarctica</source>
        <translation>Antarktyka</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="69"/>
        <source>French Southern Territories</source>
        <translation>Francuskie Terytoria Południowe</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="70"/>
        <source>Antigua and Barbuda</source>
        <translation>Antigua i Barbuda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="71"/>
        <source>Australia</source>
        <translation>Australia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="72"/>
        <source>Austria</source>
        <translation>Austria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="73"/>
        <source>Azerbaijan</source>
        <translation>Azerbejdżan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="74"/>
        <source>Burundi</source>
        <translation>Burundi</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="75"/>
        <source>Belgium</source>
        <translation>Belgia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="76"/>
        <source>Benin</source>
        <translation>Benin</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="77"/>
        <source>Burkina Faso</source>
        <translation>Burkina Faso</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="78"/>
        <source>Bangladesh</source>
        <translation>Bangladesz</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="79"/>
        <source>Bulgaria</source>
        <translation>Bułgaria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="80"/>
        <source>Bahrain</source>
        <translation>Bahrajn</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="81"/>
        <source>Bahamas</source>
        <translation>Bahamy</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="82"/>
        <source>Bosnia and Herzegovina</source>
        <translation>Bośnia i Hercegowina</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="83"/>
        <source>Belarus</source>
        <translation>Białoruś</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="84"/>
        <source>Belize</source>
        <translation>Belize</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="85"/>
        <source>Bermuda</source>
        <translation>Bermudy</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="86"/>
        <source>Bolivia</source>
        <translation>Boliwia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="87"/>
        <source>Brazil</source>
        <translation>Brazylia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="88"/>
        <source>Barbados</source>
        <translation>Barbados</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="89"/>
        <source>Brunei</source>
        <translation>Brunei</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="90"/>
        <source>Bhutan</source>
        <translation>Bhutan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="91"/>
        <source>Bouvet Island</source>
        <translation>Wyspa Bouveta</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="92"/>
        <source>Botswana</source>
        <translation>Botswana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="93"/>
        <source>Central African Republic</source>
        <translation>Republika Środkowoafrykańska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="94"/>
        <source>Canada</source>
        <translation>Kanada</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="95"/>
        <source>Cocos (Keeling) Islands</source>
        <translation>Wyspy Kokosowe</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="96"/>
        <source>Switzerland</source>
        <translation>Szwajcaria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="97"/>
        <source>Chile</source>
        <translation>Chile</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="98"/>
        <source>China</source>
        <translation>Chiny</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="99"/>
        <source>Ivory Coast</source>
        <translation>Wybrzeże Kości Słoniowej</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="100"/>
        <source>Cameroon</source>
        <translation>Kamerun</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="101"/>
        <source>Congo, the Democratic Republic of the</source>
        <translation>Demokratyczna Republika Konga</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="102"/>
        <source>Congo</source>
        <translation>Kongo</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="103"/>
        <source>Cook Islands</source>
        <translation>Wyspy Cooka</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="104"/>
        <source>Colombia</source>
        <translation>Kolumbia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="105"/>
        <source>Comoros</source>
        <translation>Komory</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="106"/>
        <source>Cape Verde</source>
        <translation>Republika Zielonego Przylądka</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="107"/>
        <source>Costa Rica</source>
        <translation>Kostaryka</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="108"/>
        <source>Cuba</source>
        <translation>Kuba</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="109"/>
        <source>Christmas Island</source>
        <translation>Wyspa Bożego Narodzenia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="110"/>
        <source>Cayman Islands</source>
        <translation>Kajmany</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="111"/>
        <source>Cyprus</source>
        <translation>Cypr</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="112"/>
        <source>Czech Republic</source>
        <translation>Republika Czeska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="113"/>
        <source>Germany</source>
        <translation>Niemcy</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="114"/>
        <source>Djibouti</source>
        <translation>Dżibuti</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="115"/>
        <source>Dominica</source>
        <translation>Dominika</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="116"/>
        <source>Denmark</source>
        <translation>Dania</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="117"/>
        <source>Dominican Republic</source>
        <translation>Republika Dominikańska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="118"/>
        <source>Algeria</source>
        <translation>Algieria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="119"/>
        <source>Ecuador</source>
        <translation>Ekwador</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="120"/>
        <source>Egypt</source>
        <translation>Egipt</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="121"/>
        <source>Eritrea</source>
        <translation>Erytrea</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="122"/>
        <source>Western Sahara</source>
        <translation>Sahara Zachodnia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="123"/>
        <source>Spain</source>
        <translation>Hiszpania</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="124"/>
        <source>Estonia</source>
        <translation>Estonia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="125"/>
        <source>Ethiopia</source>
        <translation>Etiopia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="126"/>
        <source>European Union</source>
        <translation>Unia Europejska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="127"/>
        <source>Finland</source>
        <translation>Finlandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="128"/>
        <source>Fiji</source>
        <translation>Fidżi</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="129"/>
        <source>Falkland Islands (Malvinas)</source>
        <translation>Falklandy (Malwiny)</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="130"/>
        <source>France</source>
        <translation>Francja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="131"/>
        <source>Faroe Islands</source>
        <translation>Wyspy Owcze</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="132"/>
        <source>Micronesia, Federated States of</source>
        <translation>Sfederowane Stany Mikronezji</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="133"/>
        <source>Gabon</source>
        <translation>Gabon</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="134"/>
        <source>United Kingdom</source>
        <translation>Zjednoczone Królestwo</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="135"/>
        <source>Georgia</source>
        <translation>Gruzja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="136"/>
        <source>Guernsey</source>
        <translation>Guernsey</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="137"/>
        <source>Ghana</source>
        <translation>Ghana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="138"/>
        <source>Gibraltar</source>
        <translation>Gibraltar</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="139"/>
        <source>Guinea</source>
        <translation>Gwinea</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="140"/>
        <source>Guadeloupe</source>
        <translation>Gwadelupa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="141"/>
        <source>Gambia</source>
        <translation>Gambia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="142"/>
        <source>Guinea-Bissau</source>
        <translation>Gwinea Bissau</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="143"/>
        <source>Equatorial Guinea</source>
        <translation>Gwinea Równikowa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="144"/>
        <source>Greece</source>
        <translation>Grecja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="145"/>
        <source>Grenada</source>
        <translation>Grenada</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="146"/>
        <source>Greenland</source>
        <translation>Grenlandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="147"/>
        <source>Guatemala</source>
        <translation>Gwatemala</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="148"/>
        <source>French Guiana</source>
        <translation>Gujana Francuska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="149"/>
        <source>Guam</source>
        <translation>Guam</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="150"/>
        <source>Guyana</source>
        <translation>Gujana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="151"/>
        <source>Hong Kong</source>
        <translation>Hong Kong</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="152"/>
        <source>Heard Island and McDonald Islands</source>
        <translation>Wyspy Heard i McDonalda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="153"/>
        <source>Honduras</source>
        <translation>Honduras</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="154"/>
        <source>Croatia</source>
        <translation>Chorwacja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="155"/>
        <source>Haiti</source>
        <translation>Haiti</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="156"/>
        <source>Hungary</source>
        <translation>Węgry</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="157"/>
        <source>Indonesia</source>
        <translation>Indonezja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="158"/>
        <source>Isle of Man</source>
        <translation>Wyspa Man</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="159"/>
        <source>India</source>
        <translation>Indie</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="160"/>
        <source>British Indian Ocean Territory</source>
        <translation>Brytyjskie Terytorium Oceanu Indyjskiego</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="161"/>
        <source>Ireland</source>
        <translation>Irlandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="162"/>
        <source>Iran, Islamic Republic of</source>
        <translation>Iran</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="163"/>
        <source>Iraq</source>
        <translation>Irak</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="164"/>
        <source>Iceland</source>
        <translation>Islandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="165"/>
        <source>Israel</source>
        <translation>Izrael</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="166"/>
        <source>Italy</source>
        <translation>Włochy</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="167"/>
        <source>Jamaica</source>
        <translation>Jamajka</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="168"/>
        <source>Jersey</source>
        <translation>Jersey</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="169"/>
        <source>Jordan</source>
        <translation>Jordan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="170"/>
        <source>Japan</source>
        <translation>Japonia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="171"/>
        <source>Kazakhstan</source>
        <translation>Kazachstan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="172"/>
        <source>Kenya</source>
        <translation>Kenia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="173"/>
        <source>Kyrgyzstan</source>
        <translation>Kirgistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="174"/>
        <source>Cambodia</source>
        <translation>Kambodża</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="175"/>
        <source>Kiribati</source>
        <translation>Kiribati</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="176"/>
        <source>Saint Kitts and Nevis</source>
        <translation>Saint Kitts i Nevis</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="177"/>
        <source>South Korea</source>
        <translation>Korea Południowa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="178"/>
        <source>Kuwait</source>
        <translation>Kuwejt</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="179"/>
        <source>Lao People&apos;s Democratic Republic</source>
        <translation>Laotańska Republika Ludowo-Demokratyczna</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="180"/>
        <source>Lebanon</source>
        <translation>Liban</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="181"/>
        <source>Liberia</source>
        <translation>Liberia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="182"/>
        <source>Libya</source>
        <translation>Libia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="183"/>
        <source>Saint Lucia</source>
        <translation>Saint Lucia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="184"/>
        <source>Liechtenstein</source>
        <translation>Liechtenstein</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="185"/>
        <source>Sri Lanka</source>
        <translation>Sri Lanka</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="186"/>
        <source>Lesotho</source>
        <translation>Lesotho</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="187"/>
        <source>Lithuania</source>
        <translation>Litwa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="188"/>
        <source>Luxembourg</source>
        <translation>Luksemburg</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="189"/>
        <source>Latvia</source>
        <translation>Łotwa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="190"/>
        <source>Macao</source>
        <translation>Makau</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="191"/>
        <source>Morocco</source>
        <translation>Maroko</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="192"/>
        <source>Monaco</source>
        <translation>Monako</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="193"/>
        <source>Moldova, Republic of</source>
        <translation>Mołdawia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="194"/>
        <source>Madagascar</source>
        <translation>Madagaskar</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="195"/>
        <source>Maldives</source>
        <translation>Malediwy</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="196"/>
        <source>Mexico</source>
        <translation>Meksyk</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="197"/>
        <source>Marshall Islands</source>
        <translation>Wyspy Marshalla</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="198"/>
        <source>Macedonia, the former Yugoslav Republic of</source>
        <translation>Macedonia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="199"/>
        <source>Mali</source>
        <translation>Mali</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="200"/>
        <source>Malta</source>
        <translation>Malta</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="201"/>
        <source>Myanmar</source>
        <translation>Mjanma</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="202"/>
        <source>Montenegro</source>
        <translation>Czarnogóra</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="203"/>
        <source>Mongolia</source>
        <translation>Mongolia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="204"/>
        <source>Northern Mariana Islands</source>
        <translation>Mariany Północne</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="205"/>
        <source>Mozambique</source>
        <translation>Mozambik</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="206"/>
        <source>Mauritania</source>
        <translation>Mauretania</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="207"/>
        <source>Montserrat</source>
        <translation>Montserrat</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="208"/>
        <source>Martinique</source>
        <translation>Martynika</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="209"/>
        <source>Mauritius</source>
        <translation>Mauritius</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="210"/>
        <source>Malawi</source>
        <translation>Malawi</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="211"/>
        <source>Malaysia</source>
        <translation>Malezja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="212"/>
        <source>Mayotte</source>
        <translation>Majotta</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="213"/>
        <source>Namibia</source>
        <translation>Namibia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="214"/>
        <source>New Caledonia</source>
        <translation>Nowa Kalidonia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="215"/>
        <source>Niger</source>
        <translation>Niger</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="216"/>
        <source>Norfolk Island</source>
        <translation>Wyspa Norfolk</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="217"/>
        <source>Nigeria</source>
        <translation>Nigeria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="218"/>
        <source>Nicaragua</source>
        <translation>Nikaragua</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="219"/>
        <source>Niue</source>
        <translation>Niue</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="220"/>
        <source>Netherlands</source>
        <translation>Holandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="221"/>
        <source>Norway</source>
        <translation>Norwegia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="222"/>
        <source>Nepal</source>
        <translation>Nepal</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="223"/>
        <source>Nauru</source>
        <translation>Nauru</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="224"/>
        <source>New Zealand</source>
        <translation>Nowa Zelandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="225"/>
        <source>Oman</source>
        <translation>Oman</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="226"/>
        <source>Pakistan</source>
        <translation>Pakistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="227"/>
        <source>Panama</source>
        <translation>Panama</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="228"/>
        <source>Pitcairn</source>
        <translation>Pitcairn</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="229"/>
        <source>Peru</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="230"/>
        <source>Philippines</source>
        <translation>Filipiny</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="231"/>
        <source>Palau</source>
        <translation>Palau</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="232"/>
        <source>Papua New Guinea</source>
        <translation>Papua Nowa Gwinea</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="233"/>
        <source>Poland</source>
        <translation>Polska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="234"/>
        <source>Puerto Rico</source>
        <translation>Portoryko</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="235"/>
        <source>Korea, Democratic People&apos;s Republic of</source>
        <translation>Korea Północna</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="236"/>
        <source>Portugal</source>
        <translation>Portugalia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="237"/>
        <source>Paraguay</source>
        <translation>Paragwaj</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="238"/>
        <source>Palestinian Territory, Occupied</source>
        <translation>Palestyna</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="239"/>
        <source>French Polynesia</source>
        <translation>Polinezja Francuska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="240"/>
        <source>Qatar</source>
        <translation>Katar</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="241"/>
        <source>Réunion</source>
        <translation>Reunion</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="242"/>
        <source>Romania</source>
        <translation>Rumunia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="243"/>
        <source>Russian Federation</source>
        <translation>Rosja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="244"/>
        <source>Rwanda</source>
        <translation>Rwanda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="245"/>
        <source>Saudi Arabia</source>
        <translation>Arabia Saudyjska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="246"/>
        <source>Sudan</source>
        <translation>Sudan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="247"/>
        <source>Senegal</source>
        <translation>Senegal</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="248"/>
        <source>Singapore</source>
        <translation>Singapur</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="249"/>
        <source>South Georgia and the South Sandwich Islands</source>
        <translation>Georgia Południowa i Sandwich Południowy</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="250"/>
        <source>Saint Helena, Ascension and Tristan da Cunha</source>
        <translation>Wyspa Świętej Heleny, Wyspa Wniebowstąpienia i Tristan da Cunha</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="251"/>
        <source>Svalbard and Jan Mayen</source>
        <translation>Svalbard i Jan Mayen</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="252"/>
        <source>Solomon Islands</source>
        <translation>Wyspy Salomona</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="253"/>
        <source>Sierra Leone</source>
        <translation>Sierra Leone</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="254"/>
        <source>El Salvador</source>
        <translation>Salwador</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="255"/>
        <source>San Marino</source>
        <translation>San Marino</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="256"/>
        <source>Somalia</source>
        <translation>Somalia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="257"/>
        <source>Saint Pierre and Miquelon</source>
        <translation>Saint-Pierre i Miquelon</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="258"/>
        <source>Serbia</source>
        <translation>Serbia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="259"/>
        <source>South Sudan</source>
        <translation>Południowy Sudan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="260"/>
        <source>Sao Tome and Principe</source>
        <translation>Wyspy Świętego Tomasza i Książęca</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="261"/>
        <source>Suriname</source>
        <translation>Surinam</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="262"/>
        <source>Slovakia</source>
        <translation>Słowacja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="263"/>
        <source>Slovenia</source>
        <translation>Słowenia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="264"/>
        <source>Sweden</source>
        <translation>Szwecja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="265"/>
        <source>Swaziland</source>
        <translation>Eswatini</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="266"/>
        <source>Seychelles</source>
        <translation>Seszele</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="267"/>
        <source>Syrian Arab Republic</source>
        <translation>Syryjska Republika Arabska</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="268"/>
        <source>Turks and Caicos Islands</source>
        <translation>Turks i Caicos</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="269"/>
        <source>Chad</source>
        <translation>Czad</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="270"/>
        <source>Togo</source>
        <translation>Togo</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="271"/>
        <source>Thailand</source>
        <translation>Tajlandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="272"/>
        <source>Tajikistan</source>
        <translation>Tadżykistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="273"/>
        <source>Tokelau</source>
        <translation>Tokelau</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="274"/>
        <source>Turkmenistan</source>
        <translation>Turkmenistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="275"/>
        <source>Timor-Leste</source>
        <translation>Timor Wschodni</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="276"/>
        <source>Tonga</source>
        <translation>Tonga</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="277"/>
        <source>Trinidad and Tobago</source>
        <translation>Trynidad i Tobago</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="278"/>
        <source>Tunisia</source>
        <translation>Tunezja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="279"/>
        <source>Turkey</source>
        <translation>Turcja</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="280"/>
        <source>Tuvalu</source>
        <translation>Tuwalu</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="281"/>
        <source>Taiwan</source>
        <translation>Tajwan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="282"/>
        <source>Tanzania, United Republic of</source>
        <translation>Zjednoczona Republika Tanzanii</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="283"/>
        <source>Uganda</source>
        <translation>Uganda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="284"/>
        <source>Ukraine</source>
        <translation>Ukraina</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="285"/>
        <source>United States Minor Outlying Islands</source>
        <translation>Dalekie Wyspy Mniejsze Stanów Zjednoczonych</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="286"/>
        <source>Uruguay</source>
        <translation>Urugwaj</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="287"/>
        <source>United States</source>
        <translation>Stany Zjednoczone</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="288"/>
        <source>Uzbekistan</source>
        <translation>Uzbekistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="289"/>
        <source>Holy See (Vatican City State)</source>
        <translation>Stolica Apostolska (Watykan)</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="290"/>
        <source>Saint Vincent and the Grenadines</source>
        <translation>Saint Vincent i Grenadyny</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="291"/>
        <source>Venezuela</source>
        <translation>Wenezuela</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="292"/>
        <source>Virgin Islands, British</source>
        <translation>Brytyjskie Wyspy Dziewicze</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="293"/>
        <source>Virgin Islands, U.S.</source>
        <translation>Wyspy Dziewicze Stanów Zjednoczonych</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="294"/>
        <source>Vietnam</source>
        <translation>Wietnam</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="295"/>
        <source>Vanuatu</source>
        <translation>Vanuatu</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="296"/>
        <source>Wallis and Futuna</source>
        <translation>Wallis i Futuna</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="297"/>
        <source>Samoa</source>
        <translation>Samoa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="298"/>
        <source>Yemen</source>
        <translation>Jemen</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="299"/>
        <source>South Africa</source>
        <translation>Południowa Afryka</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="300"/>
        <source>Zambia</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="301"/>
        <source>Zimbabwe</source>
        <translation>Zimbabwe</translation>
    </message>
</context>
<context>
    <name>IP2CLoader</name>
    <message>
        <source>Failed to read IP2C fallback. Stopping.</source>
        <translation type="vanished">Nie można odczytać zapasowej bazy IP2C. Zakańczam.</translation>
    </message>
    <message>
        <source>IP2C parser is still working, awaiting stop...</source>
        <translation type="vanished">Parser IP2C wciąż pracuje, czekam na zakończenie...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="144"/>
        <source>IP2C update not needed.</source>
        <translation>Aktualizacja IP2C niepotrzebna.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="147"/>
        <source>IP2C update errored. See log for details.</source>
        <translation>Aktualizacja IP2C nieudana. Szczegóły w logu.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="150"/>
        <source>IP2C update bugged out.</source>
        <translation>Aktualizacja IP2C zbugowała się.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="200"/>
        <source>IP2C unable to retrieve the parsing result.</source>
        <translation>IP2C nie powiodło się przechwycenie rezultatów z parsowania.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="210"/>
        <source>Failed to read the IP2C fallback. Stopping.</source>
        <translation>Błąd odczytywania bazy zapasowej IP2C. Zakańczam.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="214"/>
        <source>Failed to read the IP2C database. Reverting...</source>
        <translation>Błąd odczytywania bazy IP2C. Przywracam poprzednią kopię...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="228"/>
        <source>Trying to use the preinstalled IP2C database.</source>
        <translation>Próbuję użyć preinstalowanej bazy IP2C.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="267"/>
        <source>IP2C database is being read. This may take some time.</source>
        <translation>Baza IP2C jest odczytywana. Może to zająć nieco czasu.</translation>
    </message>
    <message>
        <source>IP2C update must wait until parsing of current database finishes. Waiting 1 second.</source>
        <translation type="vanished">Aktualizacja IP2C musi poczekać, aż skończy się parsowanie obecnej bazy danych. Czekam 1 sekundę.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="173"/>
        <source>IP2C database finished downloading.</source>
        <translation>Pobrano bazę IP2C.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="177"/>
        <source>Unable to save IP2C database at path: %1</source>
        <translation>Nie można zapisać bazy IP2C pod ścieżką: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="182"/>
        <source>IP2C download has failed.</source>
        <translation>Pobieranie IP2C zakończone porażką.</translation>
    </message>
    <message>
        <source>Failed to read IP2C database. Reverting...</source>
        <translation type="vanished">Błąd odczytywania bazy IP2C. Przywracam poprzednią kopię...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="219"/>
        <source>IP2C revert attempt failed. Nothing to go back to.</source>
        <translation>Nie udało się przywrócić poprzedniej bazy IP2C. Nie ma do czego wracać.</translation>
    </message>
    <message>
        <source>Trying to use preinstalled IP2C database.</source>
        <translation type="vanished">Próbuję użyć preinstalowanej bazy IP2C.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="126"/>
        <source>Did not find any IP2C database. IP2C functionality will be disabled.</source>
        <translation>Nie znaleziono bazy IP2C. Funkcje IP2C zostaną wyłączone.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="127"/>
        <source>You may install an IP2C database from the &quot;File&quot; menu.</source>
        <translation>Możesz zainstalować bazę danych IP2C z menu &quot;Plik&quot;.</translation>
    </message>
    <message>
        <source>Please wait. IP2C database is being read. This may take some time.</source>
        <translation type="vanished">Proszę czekać. Baza IP2C jest odczytywana. Może to trochę potrwać.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="248"/>
        <source>IP2C parsing finished.</source>
        <translation>Parsowanie IP2C ukończone.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="162"/>
        <source>Starting IP2C update.</source>
        <translation>Rozpoczynanie aktualizacji IP2C.</translation>
    </message>
</context>
<context>
    <name>IP2CParser</name>
    <message>
        <source>Parsing IP2C database: %1</source>
        <translation type="vanished">Parsuję bazę IP2C: %1</translation>
    </message>
    <message>
        <source>Unable to open IP2C file.</source>
        <translation type="vanished">Nie można otworzyć pliku IP2C.</translation>
    </message>
    <message>
        <source>IP2C database read in %1 ms; IP ranges: %2</source>
        <translation type="vanished">Baza IP2C przeczytana w %1 ms. Liczba zakresów IP: %2</translation>
    </message>
    <message>
        <source>IP2C parsing thread has finished.</source>
        <translation type="vanished">Wątek parsujący IP2C zakończył się.</translation>
    </message>
</context>
<context>
    <name>IP2CParserThread</name>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="63"/>
        <source>IP2C database read in %1 ms; IP ranges: %2</source>
        <translation>Baza IP2C przeczytana w %1 ms; liczba zakresów IP: %2</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="68"/>
        <source>Unable to open IP2C file: %1</source>
        <translation>Nie można otworzyć pliku IP2C: %1</translation>
    </message>
</context>
<context>
    <name>IP2CUpdateBox</name>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="20"/>
        <source>Doomseeker - Update IP2C</source>
        <translation>Doomseeker - Aktualizuj IP2C</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="26"/>
        <source>IP2C file location:</source>
        <translation>Położenie pliku IP2C:</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="186"/>
        <source>The downloaded database will be placed in the following location:</source>
        <translation>Pobrana baza zostanie umieszczona pod tą ścieżką:</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="250"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;The IP2C update is performed in the background. Doomseeker will notify you of download progress through a progress bar and log messages.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Aktualizacja bazy IP2C jest przeprowadzana w tle. Doomseeker będzie wyświetlał powiadomienia o postępach poprzez pasek postępu i wiadomości w logu.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="127"/>
        <source>Update</source>
        <translation>Aktualizuj</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="86"/>
        <source>Verifying checksum ...</source>
        <translation>Sprawdzanie sumy kontrolnej ...</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="70"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="79"/>
        <source>File is already downloaded.</source>
        <translation>Plik został już pobrany.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="80"/>
        <source>File doesn&apos;t exist yet or location doesn&apos;t point to a file.</source>
        <translation>Plik jeszcze nie istnieje lub ścieżka nie wskazuje na plik.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="94"/>
        <source>The IP2C database file was not found. Use the &quot;Download&quot; button if you want to download the newest database.</source>
        <translation>Baza IP2C nie została odnaleziona. Użyj przycisku &quot;Pobierz&quot; w celu pobrania najnowszej bazy.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="96"/>
        <source>Download</source>
        <translation>Pobierz</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="109"/>
        <source>Update available.</source>
        <translation>Aktualizacja dostępna.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="118"/>
        <source>Database status check failed. See the log for details.</source>
        <translation>Sprawdzanie statusu bazy nie powiodło się. Szczegóły w logu.</translation>
    </message>
    <message>
        <source>Update required.</source>
        <translation type="vanished">Aktualizacja wymagana.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="113"/>
        <source>Database is up-to-date.</source>
        <translation>Baza jest aktualna.</translation>
    </message>
    <message>
        <source>Database status check failed. See log for details.</source>
        <translation type="vanished">Sprawdzanie statusu bazy nie powiodło się. Szczegóły w logu.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="122"/>
        <source>Unhandled update check status.</source>
        <translation>Nieobsłużony status sprawdzenia aktualizacji.</translation>
    </message>
</context>
<context>
    <name>IP2CUpdater</name>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="67"/>
        <source>IP2C checksum check network error: %1</source>
        <translation>Błąd sieciowy podczas sprawdzania sumy kontrolnej IP2C: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="87"/>
        <source>Comparing IP2C hashes: local = %1, remote = %2</source>
        <translation>Porównywanie haszy IP2C: lokalny = %1, zdalny = %2</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="91"/>
        <source>IP2C update needed.</source>
        <translation>Aktualizacja IP2C potrzebna.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="207"/>
        <source>Checking if IP2C database at &apos;%1&apos; needs updating.</source>
        <translation>Sprawdzam, czy potrzebna jest aktualizacja bazy IP2C w lokacji &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>IRCChannelAdapter</name>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="157"/>
        <source>%1 is now known as %2</source>
        <translation>%1 jest teraz znany jako %2</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="166"/>
        <source>User %1 [%2] has joined the channel.</source>
        <translation>Użytownik %1 [%2] dołączył do kanału.</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="183"/>
        <source>User %1 has left the channel. (PART: %2)</source>
        <translation>Użytownik %1 opuścił kanał. (PART: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="188"/>
        <source>Connection for user %1 has been killed. (KILL: %2)</source>
        <translation>Połączenie dla użytkownika %1 zostało zabite. (KILL: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="193"/>
        <source>User %1 has quit the network. (QUIT: %2)</source>
        <translation>Użytkownik %1 opuścił sieć. (QUIT: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="198"/>
        <source>Unknown quit type from user %1.</source>
        <translation>Użytkownik %1 odszedł, ale nie wiadomo jak.</translation>
    </message>
</context>
<context>
    <name>IRCClient</name>
    <message>
        <location filename="../irc/ircclient.cpp" line="45"/>
        <source>IRC: Connecting: %1:%2</source>
        <translation>IRC: łączenie: %1:%2</translation>
    </message>
</context>
<context>
    <name>IRCConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="35"/>
        <source>Doomseeker - IRC Options</source>
        <translation>Doomseeker - Opcje IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="41"/>
        <source>Settings saved!</source>
        <translation>Zapisano ustawienia!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="43"/>
        <source>Settings save failed!</source>
        <translation>Zapisywanie ustawień nie powiodło się!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="81"/>
        <source>Config validation</source>
        <translation>Weryfikacja konfiguracji</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="81"/>
        <source>You have chosen one or more networks for autojoin startup but you have not defined any nickname. Please define it now.</source>
        <translation>Wybrałeś jedną lub więcej sieci do automatycznego połączenia przy starcie ale nie podałeś swojego nicka. Podaj go teraz.</translation>
    </message>
</context>
<context>
    <name>IRCCtcpParser</name>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="85"/>
        <source>CTCP %1: [%2] %3 %4</source>
        <translation>CTCP %1: [%2] %3 %4</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="89"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="147"/>
        <source>REQUEST</source>
        <translation>ZAPYTANIE</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="149"/>
        <source>REPLY</source>
        <translation>ODPOWIEDŹ</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="151"/>
        <source>????</source>
        <translation>????</translation>
    </message>
</context>
<context>
    <name>IRCDelayedOperationIgnore</name>
    <message>
        <location filename="../irc/ops/ircdelayedoperationignore.cpp" line="85"/>
        <source>Ignore user %1 (username=%2) on network %3:</source>
        <translation>Ignoruj użytkownika %1 (username=%2) w sieci %3:</translation>
    </message>
    <message>
        <location filename="../irc/ops/ircdelayedoperationignore.cpp" line="87"/>
        <source>IRC - Ignore user</source>
        <translation>IRC - Ignoruj użytkownika</translation>
    </message>
</context>
<context>
    <name>IRCDock</name>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="170"/>
        <source>Connect</source>
        <translation>Połącz</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="171"/>
        <location filename="../gui/irc/ircdock.cpp" line="261"/>
        <source>Open chat window</source>
        <translation>Otwórz okno czata</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="256"/>
        <location filename="../gui/irc/ircdock.cpp" line="258"/>
        <source>Doomseeker IRC - Open chat window</source>
        <translation>Doomseeker IRC - Otwórz okno czata</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="256"/>
        <source>Cannot obtain network connection adapter.</source>
        <translation>Nie znaleziono adaptera połączenia z siecią.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="258"/>
        <source>You are not connected to this network.</source>
        <translation>Nie jesteś podłączony do tej sieci.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="261"/>
        <source>Specify a channel or user name:</source>
        <translation>Podaj nazwę kanału bądź użytkownika:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.ui" line="26"/>
        <source>IRC</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IRCDockTabContents</name>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="718"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="933"/>
        <source>CTCP Ping</source>
        <translation>CTCP Ping</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="719"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="932"/>
        <source>CTCP Time</source>
        <translation>CTCP Czas</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="720"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="934"/>
        <source>CTCP Version</source>
        <translation>CTCP Wersja</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="487"/>
        <source>Error: %1</source>
        <translation>Błąd: %1</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="360"/>
        <source>&lt;&lt;&lt;DATE&gt;&gt;&gt; Date on this computer changes to %1</source>
        <translation>&lt;&lt;&lt;DATA&gt;&gt;&gt; Data na tym komputerze zmienia się na %1</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="454"/>
        <source>Failed to create chat log directory:
&apos;%1&apos;</source>
        <translation>Nie udało się stworzyć katalogu dla logów z chata:
&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="460"/>
        <source>&lt;&lt;&lt;DATE&gt;&gt;&gt; Chat log started on %1

</source>
        <translation>&lt;&lt;&lt;DATA&gt;&gt;&gt; Log chata rozpoczęty o dacie %1

</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="559"/>
        <source>---- All lines above were loaded from log ----</source>
        <translation>---- Wszystkie linie powyżej zostały załadowane z loga ----</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="711"/>
        <source>Manage ignores</source>
        <translation>Zarządzaj ignorami</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="717"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="931"/>
        <source>Whois</source>
        <translation>Whois</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="721"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="943"/>
        <source>Ignore</source>
        <translation>Ignoruj</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="842"/>
        <source>Ban user</source>
        <translation>Zbanuj użytkownika</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="842"/>
        <source>Input reason for banning user %1 from channel %2</source>
        <translation>Podaj powód zbanowania użytkownika %1 z kanału %2</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="866"/>
        <source>Kick user</source>
        <translation>Kopnij użytkownika</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="866"/>
        <source>Input reason for kicking user %1 from channel %2</source>
        <translation>Podaj powód kopnięcia użytkownika %1 z kanału %2</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="929"/>
        <source>Open chat window</source>
        <translation>Otwórz okno czata</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="936"/>
        <source>Op</source>
        <translation>Daj opa</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="937"/>
        <source>Deop</source>
        <translation>Zabierz opa</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="938"/>
        <source>Half op</source>
        <translation>Daj pół-opa</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="939"/>
        <source>De half op</source>
        <translation>Zabierz pół-opa</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="940"/>
        <source>Voice</source>
        <translation>Daj voice</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="941"/>
        <source>Devoice</source>
        <translation>Zabierz voice</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="944"/>
        <source>Kick</source>
        <translation>Kopnij</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="945"/>
        <source>Ban</source>
        <translation>Zbanuj</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="69"/>
        <source>Send</source>
        <translation>Wyślij</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="76"/>
        <source>Do not scroll text area</source>
        <translation>Nie przesuwaj ekranu tekstu</translation>
    </message>
</context>
<context>
    <name>IRCIgnoresManager</name>
    <message>
        <location filename="../gui/irc/ircignoresmanager.ui" line="14"/>
        <source>IRC - ignores manager</source>
        <translation>IRC - zarządzanie ignorami</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircignoresmanager.ui" line="30"/>
        <source>Remove selected</source>
        <translation>Usuń zaznaczone</translation>
    </message>
</context>
<context>
    <name>IRCNetworkAdapter</name>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="196"/>
        <source>You are not connected to the network.</source>
        <translation>Nie jesteś podłączony do sieci.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="206"/>
        <source>Insufficient parameters.</source>
        <translation>Zbyt mało parametrów.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="210"/>
        <source>This is a server window. All commands must be prepended with a &apos;/&apos; character.</source>
        <translation>To jest okno serwera. Wszystkie komendy muszą być poprzedzone znakiem &apos;/&apos;.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="214"/>
        <source>Attempted to send empty message.</source>
        <translation>Próba wysłania pustej wiadomości.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="218"/>
        <source>Command is too long.</source>
        <translation>Komenda jest zbyt długa.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="222"/>
        <source>Not a chat window.</source>
        <translation>To nie jest okno czata.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="231"/>
        <source>Quit</source>
        <translation>Wyjście</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="318"/>
        <source>IRC: Successfully registered on network %1 [%2:%3]</source>
        <translation>IRC: Poprawnie zarejestrowano w sieci %1 [%2:%3]</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="358"/>
        <source>Invalid parse result for message: %1</source>
        <translation>Nieprawidłowy rezultat parsowania wiadomości: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="402"/>
        <source>You have been kicked from channel %1 by %2 (Reason: %3)</source>
        <translation>Zostałeś wykopany z kanału %1 przez użytkownika %2 (Powód: %3)</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="407"/>
        <source>%1 was kicked by %2 (%3)</source>
        <translation>%1 zotał wykopany przez %2 (%3)</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="459"/>
        <source>%1 sets mode: %2</source>
        <translation>%1 ustawił tryb: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="482"/>
        <source>Nickname %1 is already taken.</source>
        <translation>Nick %1 jest już zajęty.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="488"/>
        <source>Both nickname and alternate nickname are taken on this network.</source>
        <translation>Zarówno nick jak i alternatywny nick są już zajęte w tej sieci.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="490"/>
        <source>No alternate nickname specified.</source>
        <translation>Nie podano alternatywnego nicka.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="495"/>
        <source>Using alternate nickname %1 to join.</source>
        <translation>Użycie alternatywnego nicka %1 w celu podłączenia.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="504"/>
        <source>User %1 is not logged in.</source>
        <translation>Użytkownik %1 nie jest zalogowany.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="526"/>
        <source>IRC parse error: %1</source>
        <translation>Błąd parsowania: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="631"/>
        <source>Updated own nickname to %1.</source>
        <translation>Zmiana własnego nicka na %1.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="676"/>
        <source>Last activity of user %1 was %2 ago.</source>
        <translation>Ostatnia aktywność użytkownika %1 była %2 temu.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="693"/>
        <source>%1 joined the network on %2</source>
        <translation>%1 dołączył do sieci o dacie %2</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="706"/>
        <source>You left channel %1.</source>
        <translation>Opuściłeś kanał %1.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="736"/>
        <source>Ping to user %1: %2ms</source>
        <translation>Ping do użytkownika %1: %2ms</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="549"/>
        <source>FROM %1: %2</source>
        <translation>OD %1: %2</translation>
    </message>
</context>
<context>
    <name>IRCNetworkSelectionBox</name>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="90"/>
        <source>Doomseeker - edit IRC network</source>
        <translation>Doomseeker - edytuj sieć IRC</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="91"/>
        <source>Cannot edit as no valid network is selected.</source>
        <translation>Nie można edytować, ponieważ nie wybrano żadnej sieci.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="209"/>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="213"/>
        <source>Doomseeker - remove IRC network</source>
        <translation>Doomseeker - usuń sieć IRC</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="210"/>
        <source>Cannot remove as no valid network is selected.</source>
        <translation>Nie można usunąć, ponieważ nie wybrano żadnej prawidłowej sieci.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="214"/>
        <source>Are you sure you wish to remove network &apos;%1&apos;?</source>
        <translation>Czy jesteś pewien, że chcesz usunąć sieć &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="240"/>
        <source>IRC connection error</source>
        <translation>Błąd połączenia z IRCem</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="245"/>
        <source>You must specify a nick.</source>
        <translation>Musisz podać nick.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="251"/>
        <source>You must specify a network address.</source>
        <translation>Musisz podać adres sieci.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="14"/>
        <source>IRC - Connect to Network</source>
        <translation>IRC - Połącz z siecią</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="25"/>
        <source>Nick:</source>
        <translation>Nick:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="32"/>
        <source>DSTest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="39"/>
        <source>Alternate nick:</source>
        <translation>Nick alternatywny:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="46"/>
        <source>DSTestA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="53"/>
        <source>Real name:</source>
        <translation>Prawdziwe imię:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="60"/>
        <source>Doomseeker Tester</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="67"/>
        <source>Username:</source>
        <translation>Nazwa użytkownika:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="74"/>
        <source>Sent to the IRC network during login. If not specified, defaults to nick.</source>
        <translation>Wysyłana do sieci IRC podczas logowania. Jeżeli nie zostanie podana, nick jest używany zamiast niej.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="77"/>
        <source>DSTestUser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="116"/>
        <source>Add network</source>
        <translation>Dodaj sieć</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="133"/>
        <source>Edit network</source>
        <translation>Edytuj sieć</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="150"/>
        <source>Remove network</source>
        <translation>Usuń sieć</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="170"/>
        <source>Server address:</source>
        <translation>Adres serwera:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="177"/>
        <source>74.207.247.18</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="187"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="210"/>
        <source>Password:</source>
        <translation>Hasło:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="226"/>
        <source>Hide</source>
        <translation>Ukryj</translation>
    </message>
</context>
<context>
    <name>IRCPrivAdapter</name>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="37"/>
        <source>This user changed nickname from %1 to %2</source>
        <translation>Ten użytkownik zmienił nicka z %1 na %2</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="61"/>
        <source>This user connection has been killed. (KILL: %1)</source>
        <translation>Ten użytkownik został skillowany. (KILL %1)</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="65"/>
        <source>This user has left the network. (QUIT: %1)</source>
        <translation>Ten użytkownik opuścił sieć. (QUIT: %1)</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="69"/>
        <source>Unhandled IRCQuitType in IRCPrivAdapter::userLeaves()</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IRCResponseParser</name>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="225"/>
        <source>Topic: %1</source>
        <translation>Temat: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="237"/>
        <source>Topic set by %1 on %2.</source>
        <translation>Temat ustawiony przez %1 o dacie %2.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="278"/>
        <source>RPLNamReply: Received names list but no channel name.</source>
        <translation>RPLNamReply: otrzymano listę nazw ale brakuje nazwy kanału.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="591"/>
        <source>Type &apos;%1&apos; was incorrectly parsed in PrivMsg block.</source>
        <translation>Typ &apos;%1&apos; został nieprawidłowo sparsowany w bloku PrivMsg.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="511"/>
        <source>New topic set by user %1:
%2</source>
        <translation>Nowy temat ustawiony przez %1:
%2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="138"/>
        <source>User %1 is away: %2</source>
        <translation>Użytkownik %1 oddalił się: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="197"/>
        <source>%1 is on channels: %2</source>
        <translation>%1 jest na kanałach: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="247"/>
        <source>URL: %1</source>
        <translation>URL: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="256"/>
        <source>Created time: %1</source>
        <translation>Czas utworzenia: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="368"/>
        <source>Erroneous nickname: %1</source>
        <translation>Błędny nick: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="370"/>
        <source> (%1)</source>
        <translation> (%1)</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="404"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="530"/>
        <source>IRCResponseParser: Type &apos;%1&apos; was recognized but there has been no parse code implemented for it.(yep, it&apos;s a bug in the application!)</source>
        <translation>IRCResponseParser: Typ &apos;%1&apos; został rozpoznany, ale nie ma dla niego żadnego kodu parsującego. (To jest błąd w aplikacji!)</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="589"/>
        <source>[%1]: %2</source>
        <translation>[%1]: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="606"/>
        <source>MODE flags string from IRC server are incorrect: &quot;%1&quot;. Information for channel &quot;%2&quot; might not be correct anymore.</source>
        <translation>Flagi MODE z serwera IRCa są nieprawidłowe: &quot;%1&quot;. Informacje o kanale &quot;%2&quot; mogą być teraz nieprawidłowe.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="638"/>
        <source>IRCResponseParser::parseUserModeMessage(): wrong FlagMode. Information for channel &quot;%2&quot; might not be correct anymore.</source>
        <translation>IRCResponseParser::parseUserModeMessage(): zły FlagMode. Informacje o kanale &quot;%2&quot; mogą być teraz nieprawidłowe.</translation>
    </message>
</context>
<context>
    <name>IRCSocketSignalsAdapter</name>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="752"/>
        <source>Connected. Sending registration messages.</source>
        <translation>Połączono. Wysyłam komunikaty rejestrujące.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="772"/>
        <source>IRC: Disconnected from network %1</source>
        <translation>IRC: rozłączono z siecią %1</translation>
    </message>
</context>
<context>
    <name>ImportantMessagesWidget</name>
    <message>
        <location filename="../gui/widgets/importantmessageswidget.ui" line="40"/>
        <source>Clear</source>
        <translation>Wyczyść</translation>
    </message>
</context>
<context>
    <name>IwadAndWadsPickerDialog</name>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="22"/>
        <source>Executable:</source>
        <translation>Plik wykonywalny:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="39"/>
        <source>IWAD:</source>
        <translation>IWAD:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="61"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="72"/>
        <source>Additional WADs and files:</source>
        <translation>Dodatkowe WADy i pliki:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.cpp" line="65"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - przeglądaj plik wykonywalny</translation>
    </message>
</context>
<context>
    <name>IwadPicker</name>
    <message>
        <location filename="../gui/createserver/iwadpicker.cpp" line="74"/>
        <source>Doomseeker - select IWAD</source>
        <translation>Doomseeker - wybierz IWAD</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadpicker.ui" line="42"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
</context>
<context>
    <name>JoinCommandLineBuilder</name>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="116"/>
        <source>Demo set for recording, but couldn&apos;t prepare its save path.

%1</source>
        <translation>Włączono nagrywanie demo, ale nie powiodło się przygotowanie ścieżki pod jego zapis.

%1</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="133"/>
        <source>This server is still refreshing.
Please wait until it is finished.</source>
        <translation>Ten serwer jest nadal odświeżany.
Poczekaj na zakończenie.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="134"/>
        <source>Attempted to obtain a join command line for a &quot;%1&quot; server that is under refresh.</source>
        <translation>Próba stworzenia komendy dołączenia do serwera &quot;%1&quot;, który wciąż jest odświeżany.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="141"/>
        <source>Data for this server is not available.
Operation failed.</source>
        <translation>Dane tego serwera są niedostępne.
Operacja nieudana.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="142"/>
        <source>Attempted to obtain a join command line for an unknown server &quot;%1&quot;</source>
        <translation>Próba stworzenia komendy dołączenia do nieznanego serwera &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="183"/>
        <source>Unknown error.</source>
        <translation>Nieznany błąd.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="187"/>
        <source>Error when obtaining join parameters for server &quot;%1&quot;, game &quot;%2&quot;: %3</source>
        <translation>Błąd otrzymywania parametrów dołączenia do serwera &quot;%1&quot;, gra &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="358"/>
        <source>Game installation failure</source>
        <translation>Błąd instalacji gry</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../fileutils.cpp" line="175"/>
        <source>Failed to remove: %1</source>
        <translation>Nie można usunąć: %1</translation>
    </message>
</context>
<context>
    <name>LogDirectoryPicker</name>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.cpp" line="76"/>
        <source>Doomseeker - select Log path</source>
        <translation>Doomseeker - wybierz ścieżkę dla loga</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="35"/>
        <source>This path could not be found. It will not be used.</source>
        <translation>Ta ścieżka nie mogła zostać odnaleziona. Nie będzie używana.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="51"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="64"/>
        <source>Enabled</source>
        <translation>Włączone</translation>
    </message>
</context>
<context>
    <name>LogDock</name>
    <message>
        <location filename="../gui/logdock.ui" line="20"/>
        <source>Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/logdock.ui" line="67"/>
        <source>Copy all to clipboard</source>
        <translation>Skopiuj wszystko do schowka</translation>
    </message>
    <message>
        <location filename="../gui/logdock.ui" line="74"/>
        <source>Clear</source>
        <translation>Wyczyść</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../main.cpp" line="457"/>
        <source>Doomseeker startup error</source>
        <translation>Doomseeker - błąd uruchomienia</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="240"/>
        <source>Init finished.</source>
        <translation>Inicjalizacja zakończona.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="255"/>
        <source>Doomseeker - Updates Install Failure</source>
        <translation>Doomseeker - Błąd instalacji aktualizacji</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="301"/>
        <source>Failed to open file &apos;%1&apos;.</source>
        <translation>Nie można otworzyć pliku &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="309"/>
        <source>Failed to open stdout.</source>
        <translation>Nie można otworzyć stdout.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="331"/>
        <source>Preparing GUI.</source>
        <translation>Przygotowuję GUI.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="344"/>
        <source>Starting Create Game box.</source>
        <translation>Uruchamiam okno &quot;Utwórz Grę&quot;.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="352"/>
        <source>Starting RCon client.</source>
        <translation>Startuję klienta RCon.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="367"/>
        <source>None of the currently loaded game plugins supports RCon.</source>
        <translation>Żadna z obecnie załadowanych wtyczek do gier nie wspiera konsoli zdalnej.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="369"/>
        <source>Doomseeker RCon</source>
        <translation>Doomseeker konsola zdalna</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="383"/>
        <source>Couldn&apos;t find the specified plugin: </source>
        <translation>Nie znaleziono wtyczki: </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="393"/>
        <source>Plugin does not support RCon.</source>
        <translation>Wtyczka nie wspiera konsoli zdalnej.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="410"/>
        <source>Loading extra CA certificates from &apos;%1&apos;.</source>
        <translation>Ładuję dodatkowe certyfikaty CA z &apos;%1&apos;.</translation>
    </message>
    <message numerus="yes">
        <location filename="../main.cpp" line="415"/>
        <source>Appending %n extra CA certificate(s).</source>
        <translation>
            <numerusform>Dodaję %n certyfikat.</numerusform>
            <numerusform>Dodaję %n certyfikaty.</numerusform>
            <numerusform>Dodaję %n certyfikatów.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="427"/>
        <source>Running in the portable mode. Forcing current directory to: %1</source>
        <translation>Uruchomiono w trybie przenośnym. Wymuszam katalog roboczy: %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="431"/>
        <source>Forcing the current directory failed! Path detection may misbehave.</source>
        <translation>Błąd wymuszenia katalogu roboczego! Rozwiązywanie ścieżek może zachowywać się niepoprawnie.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="449"/>
        <source>Doomseeker will not run because some directories cannot be used properly.
</source>
        <translation>Doomseeker nie wystartuje ponieważ niektóre z katalogów nie mogą zostać poprawnie użyte.
</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="481"/>
        <source>Initializing IP2C database.</source>
        <translation>Inicjalizuję bazę IP2C.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="489"/>
        <source>Initializing IRC configuration file.</source>
        <translation>Inicjalizuję plik konfiguracyjny IRCa.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="507"/>
        <source>Loading translations definitions</source>
        <translation>Wczytuję definicje tłumaczeń</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="515"/>
        <source>Loading translation &quot;%1&quot;.</source>
        <translation>Wczytuję tłumaczenie: &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="519"/>
        <source>Translation loaded.</source>
        <translation>Wczytano tłumaczenie.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="523"/>
        <source>Failed to load translation.</source>
        <translation>Wczytywanie tłumaczenia zakończone niepowodzeniem.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="530"/>
        <source>Initializing configuration file.</source>
        <translation>Inicjalizuję plik konfiguracyjny.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="539"/>
        <source>Could not get an access to the settings directory. Configuration will not be saved.</source>
        <translation>Brak dostępu do katalogu z ustawieniami. Konfiguracja nie zostanie zapisana.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="558"/>
        <source>Initializing passwords configuration file.</source>
        <translation>Inicjalizuję plik konfiguracyjny dla haseł.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="571"/>
        <source>Initializing configuration for plugins.</source>
        <translation>Inicjalizuję konfigurację wtyczek.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="725"/>
        <source>doomseeker: `--connect`, `--create-game` and `--rcon` are mutually exclusive</source>
        <translation>doomseeker: `--connect`, `--create-game` i `--rcon` nie mogą być używane razem</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="319"/>
        <source>Dumping version info to file in JSON format.</source>
        <translation>Zapisuję informacje o wersji do pliku w formacie JSON.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="738"/>
        <source>Starting refreshing thread.</source>
        <translation>Rozpoczynam wątek odświeżający.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/mainwindow.cpp" line="239"/>
        <source>&amp;Buddies</source>
        <translation>&amp;Znajomi</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="240"/>
        <source>CTRL+B</source>
        <translation>CTRL+B</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="412"/>
        <source>Doomseeker - Auto Update</source>
        <translation>Doomseeker - Automatyczna aktualizacja</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="413"/>
        <source>Update is already in progress.</source>
        <translation>Aktualizacja jest już w toku.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="422"/>
        <source>Removing old update packages from local temporary space.</source>
        <translation>Usuwanie starych pakietów aktualizacyjnych z lokalnej przestrzeni tymczasowej.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="427"/>
        <source>Checking for updates...</source>
        <translation>Szukam aktualizacji...</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="636"/>
        <source>All WADs found</source>
        <translation>Wszystkie WADy odnalezione</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="636"/>
        <source>All of the WADs used by this server are present.</source>
        <translation>Wszystkie WADy używane przez ten serwer znajdują się na dysku.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="717"/>
        <source>Plugins are missing from the &quot;engines/&quot; directory.</source>
        <translation>W katalogu &quot;engines/&quot; nie ma wtyczek.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="719"/>
        <source>No master servers are enabled in the &quot;Query&quot; menu.</source>
        <translation>Żaden master serwer nie został włączony w menu &quot;Odświeżanie&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="724"/>
        <source>Doomseeker - refresh problem</source>
        <translation>Doomseeker - problem z odświeżaniem</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="730"/>
        <source>Total refresh initialized!</source>
        <translation>Rozpoczęto totalne odświeżenie!</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="740"/>
        <source>Warning: No master servers were enabled for this refresh. Check your Query menu or &quot;engines/&quot; directory.</source>
        <translation>Ostrzeżenie: nie włączono żadnego master serwera do odświeżenia. Sprawdź menu &quot;Odświeżanie&quot; lub katalog &quot;engines/&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="803"/>
        <source>Auto Updater:</source>
        <translation>Auto-aktualizacja:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="818"/>
        <source>Abort update.</source>
        <translation>Przerwij aktualizację.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="830"/>
        <source>IP2C Update</source>
        <translation>Aktualizacja IP2C</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="841"/>
        <source>&amp;IRC</source>
        <translation>&amp;IRC</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="842"/>
        <source>CTRL+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="857"/>
        <source>&amp;Log</source>
        <translation>&amp;Log</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="858"/>
        <source>CTRL+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="874"/>
        <source>Servers</source>
        <translation>Serwery</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1475"/>
        <source>Failed to build the command line:
%1</source>
        <translation>Nie udało się stworzyć wiersza poleceń:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1584"/>
        <source>Generic servers: %1
</source>
        <translation>Serwery ogólne: %1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1585"/>
        <source>Custom servers: %1
</source>
        <translation>Serwery własne: %1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1586"/>
        <source>LAN servers: %1
</source>
        <translation>Serwery LAN: %1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1587"/>
        <source>Human players: %1</source>
        <translation>Graczy: %1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1599"/>
        <source>Finished refreshing. Servers on the list: %1 (+%2 custom, +%3 LAN). Players: %4.</source>
        <translation>Ukończono odświeżanie. Serwerów na liście: %1 (+%2 własnych, +%3 LAN). Graczy: %4.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1001"/>
        <source>Master server for %1: %2</source>
        <translation>Master serwer dla %1: %2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1005"/>
        <source>Error: %1</source>
        <translation>Błąd: %1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1014"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1045"/>
        <location filename="../gui/mainwindow.cpp" line="1053"/>
        <source>Help error</source>
        <translation>Błąd pomocy</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1045"/>
        <source>No help found.</source>
        <translation>Nie znaleziono pomocy.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1053"/>
        <source>Failed to open URL:
%1</source>
        <translation>Nie można otworzyć adresu:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1136"/>
        <source>Welcome to Doomseeker</source>
        <translation>Witaj w programie Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1137"/>
        <source>Before you start browsing for servers, please ensure that Doomseeker is properly configured.</source>
        <translation>Zanim zaczniesz przeglądać serwery, proszę poświęć chwilę aby skonfigurować Doomseekera.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1154"/>
        <source>Program update detection &amp; download finished with status: [%1] %2</source>
        <translation>Wykrywanie i pobieranie aktualizacji programu ukończone ze statusem: [%1] %2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1164"/>
        <source>Updates will be installed on next program start.</source>
        <translation>Aktualizacje zostaną zainstalowane przy następnym starcie programu.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1170"/>
        <source>Update channel was changed during update process. Discarding update.</source>
        <translation>Kanał aktualizacji został zmieniony w trakcie jej trwania. Porzucam aktualizację.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1209"/>
        <source>Query on startup warning: No master servers are enabled in the Query menu.</source>
        <translation>Ostrzeżenie odświeżenia na starcie: żaden master serwer nie jest włączony w menu &quot;Odświeżanie&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1233"/>
        <source>Doomseeker critical error</source>
        <translation>Doomseeker - błąd krytyczny</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1226"/>
        <source>Doomseeker was unable to find any plugin libraries.
Although the application will still work it will not be possible to fetch any server info or launch any game.

Please check if there are any files in &quot;engines/&quot; directory.
To fix this problem you may try downloading Doomseeker again from the site specified in the Help|About box and reinstalling Doomseeker.</source>
        <translation>Doomseeker nie mógł odnaleźć żadnych wtyczek.
Program będzie działać, ale nie będzie możliwe przeszukiwanie żadnych serwerów ani wystartowanie gry.

Sprawdź, czy w katalogu &quot;engines/&quot; znajdują się jakiekolwiek pliki.
Aby naprawić ten błąd możesz spróbować pobrać Doomseekera jeszcze raz ze strony podanej w menu Pomoc|O programie i przeinstalować Doomseekera.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="713"/>
        <source>Doomseeker is unable to proceed with the refresh operation because the following problem has occurred:

</source>
        <translation>Doomseeker nie mógł odświeżyć, ponieważ wystąpił problem:

</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="721"/>
        <source>Unknown error occurred.</source>
        <translation>Wystąpił nieznany błąd.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1273"/>
        <source>Querying...</source>
        <translation>Odświeżanie...</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1282"/>
        <source>Done</source>
        <translation>Ukończono</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1364"/>
        <source>Main Toolbar</source>
        <translation>Główny pasek narzędziowy</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1369"/>
        <source>Get Servers</source>
        <translation>Pobierz serwery</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1403"/>
        <source>Search:</source>
        <translation>Szukaj:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1489"/>
        <source>Update installation problem:
%1</source>
        <translation>Problem podczas instalowania aktualizacji:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1492"/>
        <source>Update installation failed.</source>
        <translation>Instalacja aktualizacji zakończona niepowodzeniem.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1494"/>
        <source>%1

Remaining updates have been discarded.</source>
        <translation>%1

Pozostałe aktualizacje zostały porzucone.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1501"/>
        <source>Update install problem:
%1

Remaining updates have been discarded.</source>
        <translation>Problem instalacji aktualizacji:
%1

Pozostałe aktualizacje zostały porzucone.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1493"/>
        <location filename="../gui/mainwindow.cpp" line="1502"/>
        <source>Doomseeker - Auto Update problem</source>
        <translation>Doomseeker - Problem automatycznej aktualizacji</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="670"/>
        <source>Doomseeker needs to be restarted for some changes to be applied.</source>
        <translation>Należy zrestartować Doomseekera aby zastosować niektóre zmiany.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="887"/>
        <source>Server &amp;details</source>
        <translation>Szcze&amp;góły serwera</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="888"/>
        <source>CTRL+D</source>
        <translation>CTRL+D</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1474"/>
        <source>Doomseeker - show join command line</source>
        <translation>Doomseeker - pokaż wiersz poleceń do dołączenia</translation>
    </message>
</context>
<context>
    <name>MainWindowWnd</name>
    <message>
        <location filename="../gui/mainwindow.ui" line="14"/>
        <source>Doomseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="350"/>
        <source>&amp;Configure</source>
        <translation>&amp;Konfiguracja</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="353"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="361"/>
        <source>&amp;About</source>
        <translation>&amp;O programie</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="369"/>
        <source>&amp;Quit</source>
        <translation>&amp;Wyjście</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="372"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="383"/>
        <source>Server Info</source>
        <translation>Info o serwerach</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="386"/>
        <source>I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="395"/>
        <source>&amp;Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="406"/>
        <source>&amp;Buddies</source>
        <translation>&amp;Znajomi</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="409"/>
        <source>Ctrl+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="398"/>
        <source>Ctrl+Alt+W</source>
        <translation>Ctrl+Alt+W</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="418"/>
        <source>&amp;Create game</source>
        <translation>&amp;Stwórz grę</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="421"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="429"/>
        <source>&amp;Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="432"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="437"/>
        <source>&amp;Help (Online)</source>
        <translation>&amp;Pomoc (online)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="440"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="449"/>
        <source>&amp;Update IP2C</source>
        <translation>&amp;Uaktualnij IP2C</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="457"/>
        <source>&amp;IRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="460"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="465"/>
        <source>&amp;IRC options</source>
        <translation>Opcje &amp;IRC</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="468"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="473"/>
        <source>About &amp;Qt</source>
        <translation>O &amp;Qt</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="488"/>
        <source>&amp;Record demo</source>
        <translation>&amp;Nagraj demo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="497"/>
        <source>&amp;Demo manager</source>
        <translation>&amp;Menadżer &amp;dem</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="500"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="509"/>
        <source>&amp;Check for updates</source>
        <translation>&amp;Sprawdź aktualizacje</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="518"/>
        <source>&amp;Program args</source>
        <translation>&amp;Argumenty programu</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="523"/>
        <source>Install &amp;Freedoom</source>
        <translation>Zainstaluj &amp;Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="78"/>
        <source>New updates are available:</source>
        <translation>Jest dostępna aktualizacja:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="97"/>
        <source>Download &amp;&amp; Install</source>
        <translation>Pobierz i zainstaluj</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="120"/>
        <source>Discard</source>
        <translation>Porzuć</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="185"/>
        <source>Updates have been downloaded:</source>
        <translation>Aktualizacja została pobrana:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="204"/>
        <source>Restart &amp;&amp; Install now</source>
        <translation>Zrestartuj i zainstaluj teraz</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="223"/>
        <source>Restart &amp;&amp; Install later</source>
        <translation>Zrestartuj i zainstaluj później</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="252"/>
        <source>Server filter is applied</source>
        <translation>Nałożony filtr serwerów</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="297"/>
        <source>&amp;Options</source>
        <translation>&amp;Opcje</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="304"/>
        <source>&amp;Help</source>
        <translation>Po&amp;moc</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="315"/>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="329"/>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="334"/>
        <source>&amp;Query</source>
        <translation>O&amp;dświeżanie</translation>
    </message>
</context>
<context>
    <name>MapListPanel</name>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="51"/>
        <source>Add from loaded WADs</source>
        <translation>Dodaj z załadowanych WADów</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="64"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="75"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="91"/>
        <source>Random map rotation</source>
        <translation>Losowa rotacja listy</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.cpp" line="57"/>
        <source>The current map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>Obecnej mapy nie ma na liście map. Gra może zachować się dziwnie.</translation>
    </message>
</context>
<context>
    <name>MapListSelector</name>
    <message>
        <location filename="../gui/createserver/maplistselector.cpp" line="188"/>
        <source>Invert selection</source>
        <translation>Odwróć zaznaczenie</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="26"/>
        <source>Doomseeker - Select Maps</source>
        <translation>Doomseeker - Wybierz mapy</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="35"/>
        <source>All selected maps will get added to the map list</source>
        <translation>Wszystkie wybrane mapy zostaną dodane do listy map</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="71"/>
        <source>Reading WADs...</source>
        <translation>Czytam WADy...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="86"/>
        <source>Select All</source>
        <translation>Zaznacz wszystko</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="121"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
</context>
<context>
    <name>MasterClient</name>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="156"/>
        <source>Could not fetch a new server list from the master because not enough time has passed.</source>
        <translation>Nie można pobrać nowej listy serwerów z mastera, ponieważ upłynęło zbyt mało czasu.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="161"/>
        <source>Bad response from master server.</source>
        <translation>Zła odpowiedź z master serwera.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="108"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="166"/>
        <source>Could not fetch a new server list. The protocol you are using is too old. An update may be available.</source>
        <translation>Nie można było pobrać nowej listy serwerów. Wersja protokołu jest zbyt stara. Być może jest dostępna aktualizacja.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="263"/>
        <source>Reloading master server results from cache for %1!</source>
        <translation>Ładuję listę serwerów z pamięci lokalnej dla gry %1!</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="354"/>
        <source>Master server timeout</source>
        <translation>Przekroczono limit czasu dla master serwera</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="354"/>
        <source>Connection timeout (%1:%2).</source>
        <translation>Przekroczono limit czasu połączenia (%1:%2).</translation>
    </message>
</context>
<context>
    <name>MiscServerSetupPanel</name>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="34"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="44"/>
        <source>E-mail:</source>
        <translation>E-mail:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="54"/>
        <source>Connect password:</source>
        <translation>Hasło połączenia:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="68"/>
        <source>Join password:</source>
        <translation>Hasło wejścia do gry:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="82"/>
        <source>RCon password:</source>
        <translation>Hasło RCon:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="99"/>
        <source>Hide passwords</source>
        <translation>Ukryj hasła</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="111"/>
        <source>MOTD:</source>
        <translation>MOTD:</translation>
    </message>
</context>
<context>
    <name>MissingWadsDialog</name>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="14"/>
        <source>Doomseeker - files are missing</source>
        <translation>Doomseeker - brakuje plików</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="41"/>
        <source>You don&apos;t have all the files required by this server and an instance of Wadseeker is already running.</source>
        <translation>Nie masz wszystkich plików, które są potrzebne do gry na tym serwerze, a Wadseeker już jest uruchomiony.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="57"/>
        <source>Press &apos;Ignore&apos; to join anyway.</source>
        <translation>Wciśnij &quot;Ignoruj&quot;, aby dołączyć mimo wszystko.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="103"/>
        <source>These files belong to a commercial game or are otherwise blocked from download:</source>
        <translation>Te pliki należą do gry, która jest w sprzedaży, bądź są po prostu zablokowane przed pobraniem:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="116"/>
        <location filename="../gui/missingwadsdialog.ui" line="195"/>
        <source>&lt;files&gt;</source>
        <translation>&lt;pliki&gt;</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="135"/>
        <source>Make sure that this file is in one of the paths specified in Options -&gt; File paths.

If you don&apos;t have this file, and it belongs to a commercial game, you need to purchase the game associated with this file. Wadseeker will not download commercial IWADs or modifications.</source>
        <translation>Upewnij się, że ten plik znajduje się pod jedną ze ścieżek w Opcje -&gt; Ścieżki plików.

Jeżeli nie masz tego pliku, a należy on do gry w sprzedaży, to musisz kupić grę związaną z tym plikiem. Wadseeker nie będzie pobierał komercyjnych IWADów lub modyfikacji.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="147"/>
        <source>You can also install a free replacement IWAD with &quot;Install Freedoom&quot; button.</source>
        <translation>Możesz także zainstalować darmowy IWAD zastępczy używając przycisku &quot;Zainstaluj Freedoom&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="182"/>
        <source>Following files can be downloaded:</source>
        <translation>Następujące pliki mogą zostać pobrane:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="236"/>
        <source>Incompatible files:</source>
        <translation>Pliki niekompatybilne:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="243"/>
        <source>(Your current files will be overwritten)</source>
        <translation>(Twoje obecne pliki zostaną nadpisane)</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="281"/>
        <source>Optional files:</source>
        <translation>Pliki opcjonalne:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="304"/>
        <source>Do you want Wadseeker to find the missing WADs?</source>
        <translation>Czy chcesz aby Wadseeker odszukał brakujące WADy?</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="311"/>
        <source>Alternatively use ignore to connect anyways.</source>
        <translation>Alternatywnie, wciśnij &quot;ignoruj&quot; aby połączyć się mimo wszystko.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="342"/>
        <source>Install Freedoom</source>
        <translation>Zainstaluj Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.cpp" line="63"/>
        <source>Install</source>
        <translation>Instaluj</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Ignoruj</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Anuluj</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../gui/passworddlg.ui" line="32"/>
        <source>Connection Password</source>
        <translation>Hasło połączenia</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="38"/>
        <source>This server requires a password in order to connect, please enter this password below.</source>
        <translation>Ten serwer wymaga hasła do połączenia, wpisz to hasło poniżej.</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="63"/>
        <source>Connect password:</source>
        <translation>Hasło do podłączenia:</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="112"/>
        <source>Ingame password:</source>
        <translation>Hasło w grze:</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="146"/>
        <source>Hide passwords</source>
        <translation>Ukryj hasła</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="156"/>
        <source>Remember password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
</context>
<context>
    <name>PlayerTable</name>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="210"/>
        <source>BOT</source>
        <translation>BOT</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="214"/>
        <source>SPECTATOR</source>
        <translation>WIDZ</translation>
    </message>
    <message>
        <source>Team</source>
        <translation type="vanished">Drużyna</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="146"/>
        <source>Bots</source>
        <translation>Boty</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="151"/>
        <source>Spectators</source>
        <translation>Widzowie</translation>
    </message>
    <message numerus="yes">
        <location filename="../serverapi/tooltips/playertable.cpp" line="238"/>
        <source>(and %n more ...)</source>
        <translation>
            <numerusform>(i jeszcze %n)</numerusform>
            <numerusform>(i jeszcze %n)</numerusform>
            <numerusform>(i jeszcze %n)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="253"/>
        <source>Player</source>
        <translation>Gracz</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="254"/>
        <source>Score</source>
        <translation>Punkty</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="255"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="256"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="287"/>
        <source>Team %1</source>
        <translation>Drużyna %1</translation>
    </message>
</context>
<context>
    <name>PlayersDiagram</name>
    <message>
        <location filename="../gui/helpers/playersdiagram.cpp" line="64"/>
        <source>Numeric</source>
        <translation>Liczbowy</translation>
    </message>
    <message>
        <location filename="../gui/helpers/playersdiagram.cpp" line="65"/>
        <source>Blocks</source>
        <translation>Bloczki</translation>
    </message>
</context>
<context>
    <name>PluginUrlHandler</name>
    <message>
        <location filename="../connectionhandler.cpp" line="226"/>
        <source>Connect to server</source>
        <translation>Połącz z serwerem</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="227"/>
        <source>Do you want to connect to the server at %1?</source>
        <translation>Czy chcesz podłączyć się do serwera pod adresem %1?</translation>
    </message>
</context>
<context>
    <name>ProgramArgsHelpDialog</name>
    <message>
        <location filename="../gui/programargshelpdialog.ui" line="14"/>
        <source>Doomseeker - Program arguments</source>
        <translation>Doomseeker - Argumenty programu</translation>
    </message>
    <message>
        <location filename="../gui/programargshelpdialog.ui" line="20"/>
        <source>These arguments can be passed to Doomseeker&apos;s executable:</source>
        <translation>Te argumenty można przekazać plikowi wykonywalnemu Doomseeker&apos;a:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="114"/>
        <source>DoomseekerConfig.iniSectionForPlugin(): empty plugin name has been specified, returning dummy IniSection.</source>
        <translation>DoomseekerConfig.iniSectionForPlugin(): podano pustą nazwę wtyczki, zwracam atrapę IniSection.</translation>
    </message>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="120"/>
        <source>DoomseekerConfig.iniSectionForPlugin(): plugin name is invalid: %1</source>
        <translation>DoomseekerConfig.iniSectionForPlugin(): błędna nazwa wtyczki: %1</translation>
    </message>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="213"/>
        <source>Setting INI file: %1</source>
        <translation>Ustawienie pliku INI: %1</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="166"/>
        <source>Setting IRC INI file: %1</source>
        <translation>Ustawienie pliku INI dla IRC: %1</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="93"/>
        <source>Add plugin&apos;s IRC channel?</source>
        <translation>Dodać kanały IRCa z wtyczki?</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="94"/>
        <source>Would you like the %1 plugin to add its channel to %2&apos;s auto join?</source>
        <translation>Czy chcesz, aby wtyczka %1 dodała swoje kanały IRCa do automatycznej listy dołączenia dla %2?</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="224"/>
        <source>Skipping loading of forbidden plugin: %1</source>
        <translation>Pomijam ładowanie zabronionej wtyczki: %1</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="67"/>
        <source>plugin ABI version mismatch; plugin: %1, Doomseeker: %2</source>
        <translation>wersja ABI wtyczki nie pasuje; wtyczka: %1, Doomseeker: %2</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="73"/>
        <source>plugin doesn&apos;t report its ABI version</source>
        <translation>wtyczka nie zgłasza swojej wersji ABI</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="75"/>
        <source>Cannot load plugin %1, reason: %2.</source>
        <translation>Nie można załadować wtyczki %1, powód: %2.</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="94"/>
        <source>Loaded plugin: &quot;%1&quot;!</source>
        <translation>Załadowano wtyczkę: &quot;%1&quot;!</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="99"/>
        <source>Failed to open plugin: %1</source>
        <translation>Otwarcie wtyczki nie udane: %1</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="182"/>
        <source>Failed to locate plugins.</source>
        <translation>Nie udało się zlokalizować wtyczek.</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="404"/>
        <source>Cooperative</source>
        <translation>Kooperacja</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="409"/>
        <source>Deathmatch</source>
        <translation>Deathmatch</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="414"/>
        <source>Team DM</source>
        <translation>Drużynowy DM</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="419"/>
        <source>CTF</source>
        <translation>CTF</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="424"/>
        <source>Unknown</source>
        <translation>Nieznany</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="36"/>
        <source>Unlimited</source>
        <translation>Nieograniczony</translation>
    </message>
    <message>
        <source>parent node is not a directory: %1</source>
        <translation type="vanished">węzeł nadrzędny nie jest katalogiem: %1</translation>
    </message>
    <message>
        <source>lack of necessary permissions to the parent directory: %1</source>
        <translation type="vanished">brak odpowiednich uprawnień do nadrzędnego katalogu: %1</translation>
    </message>
    <message>
        <source>cannot create directory</source>
        <translation type="vanished">nie można utworzyć katalogu</translation>
    </message>
</context>
<context>
    <name>RconPasswordDialog</name>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="20"/>
        <source>Connect to Remote Console</source>
        <translation>Połącz z konsolą zdalną</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="26"/>
        <source>Connection</source>
        <translation>Połączenie</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="32"/>
        <source>Game:</source>
        <translation>Gra:</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="42"/>
        <source>Server Address:</source>
        <translation>Adres serwera:</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="55"/>
        <source>Please enter your remote console password.</source>
        <translation>Podaj hasło do konsoli zdalnej.</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="67"/>
        <source>Hide password</source>
        <translation>Ukryj hasło</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="70"/>
        <source>Hide</source>
        <translation>Ukryj</translation>
    </message>
</context>
<context>
    <name>RemoteConsole</name>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="68"/>
        <source>No RCon support</source>
        <translation>Brak wsparcia konsoli zdalnej</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="68"/>
        <source>The selected game has no RCon support.</source>
        <translation>Wybrana gra nie ma wsparcia dla konsoli zdalnej.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="83"/>
        <source>RCon failure</source>
        <translation>Błąd konsoli zdalnej</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="83"/>
        <source>Failed to create RCon protocol for the server.</source>
        <translation>Nie można utworzyć protokołu konsoli zdalnej dla tego serwera.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="110"/>
        <source>RCon Failure</source>
        <translation>Błąd konsoli zdalnej</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="111"/>
        <source>Failed to connect RCon to server %1:%2</source>
        <translation>Nie można podłączyć konsoli zdalnej do serwera %1:%2</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="134"/>
        <source> - Remote Console</source>
        <translation> - konsola zdalna</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="146"/>
        <source>Invalid Password</source>
        <translation>Nieprawidłowe hasło</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="146"/>
        <source>The password you entered appears to be invalid.</source>
        <translation>Podane hasło zdaje się być nieprawidłowe.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="14"/>
        <source>Remote Console</source>
        <translation>Zdalna konsola</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="90"/>
        <source>Disconnect</source>
        <translation>Rozłącz</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="21"/>
        <source>Scoreboard</source>
        <translation>Lista wyników</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="52"/>
        <source>Player Name</source>
        <translation>Nazwa gracza</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="63"/>
        <source>Console</source>
        <translation>Konsola</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="81"/>
        <source>File</source>
        <translation>Plik</translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <location filename="../serverapi/server.cpp" line="148"/>
        <source>&lt;&lt; ERROR &gt;&gt;</source>
        <translation>&lt;&lt; BŁĄD &gt;&gt;</translation>
    </message>
    <message>
        <location filename="../serverapi/server.cpp" line="263"/>
        <source>client</source>
        <translation>klient</translation>
    </message>
    <message>
        <location filename="../serverapi/server.cpp" line="296"/>
        <source>Undefined</source>
        <translation>Niezdefiniowany</translation>
    </message>
</context>
<context>
    <name>ServerConsole</name>
    <message>
        <location filename="../gui/widgets/serverconsole.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ServerDetailsDock</name>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="14"/>
        <source>Server details</source>
        <translation>Szczegóły serwera</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="47"/>
        <source>Server:</source>
        <translation>Serwer:</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="54"/>
        <source>&lt;server&gt;</source>
        <translation>&lt;serwer&gt;</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="70"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="96"/>
        <source>Players</source>
        <translation>Gracze</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="122"/>
        <source>Flags</source>
        <translation>Flagi</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="72"/>
        <source>No server selected.</source>
        <translation>Nie wybrano serwera.</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="92"/>
        <source>&lt;b&gt;Address:&lt;/b&gt; %1
</source>
        <translation>&lt;b&gt;Adres:&lt;/b&gt; %1
</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="105"/>
        <source>No players on this server.</source>
        <translation>Nie ma graczy na tym serwerze.</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="111"/>
        <source>DMFlags unknown or no DMFlags set.</source>
        <translation>Nieznane DMFlagi, albo żadne nie są ustawione.</translation>
    </message>
</context>
<context>
    <name>ServerFilterBuilderMenu</name>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="48"/>
        <source>Build server filter ...</source>
        <translation>Buduj filtr serwerów ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="55"/>
        <source>Show only servers with ping lower than %1</source>
        <translation>Pokaż tylko serwery z pingiem poniżej %1</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="59"/>
        <source>Filter by game mode &quot;%1&quot;</source>
        <translation>Filtruj po trybie gry &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="64"/>
        <source>Hide game mode &quot;%1&quot;</source>
        <translation>Ukryj tryb gry &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="68"/>
        <source>Include WAD ...</source>
        <translation>Z WADem ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="69"/>
        <source>Exclude WAD ...</source>
        <translation>Bez WADa ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="85"/>
        <source>Filter by address</source>
        <translation>Filtruj po adresie</translation>
    </message>
</context>
<context>
    <name>ServerFilterDock</name>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="151"/>
        <source>Server &amp;filter</source>
        <translation>&amp;Filtr serwerów</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="152"/>
        <source>CTRL+F</source>
        <translation>CTRL+F</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="296"/>
        <source>[no filter]</source>
        <translation>[brak filtra]</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="348"/>
        <source>Doomseeker - Remove filter preset</source>
        <translation>Doomseeker - Usuń zapisany filtr</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="349"/>
        <source>Are you sure you wish to remove the filter preset &quot;%1&quot;?</source>
        <translation>Czy jesteś pewien, że chcesz usunąć zapisany filtr &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="362"/>
        <source>Doomseeker - Save filter preset</source>
        <translation>Doomseeker - Zapisz filtr</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="363"/>
        <source>Enter name for the filter preset (will overwrite if exists):</source>
        <translation>Wprowadź nazwę dla zapisanego filtru (zostanie nadpisany jeśli istnieje):</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="26"/>
        <source>Server filter</source>
        <translation>Filtr serwerów</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="65"/>
        <source>Save the current server filter as a preset.</source>
        <translation>Zapisz obecny filtr serwerów.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="82"/>
        <source>Remove the server filter preset.</source>
        <translation>Usuń zapisany filtr.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="112"/>
        <source>Put populated servers on top</source>
        <translation>Umieść niepuste serwery na górze</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="132"/>
        <source>Filtering enabled</source>
        <translation>Filtrowanie włączone</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="191"/>
        <source>Set &apos;0&apos; to disable.</source>
        <translation>Ustaw &apos;0&apos; aby wyłączyć.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="207"/>
        <source>WADs:</source>
        <translation>WADy:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="95"/>
        <source>Server name:</source>
        <translation>Nazwa serwera:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="51"/>
        <location filename="../gui/serverfilterdock.cpp" line="296"/>
        <source>[custom]</source>
        <translation>[własny]</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="184"/>
        <source>Max. ping:</source>
        <translation>Maks. ping:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="214"/>
        <source>Use &apos;,&apos; (a comma) to separate multiple WADs.</source>
        <translation>Użyj &apos;,&apos; (przecinka) aby oddzielić wiele WADów.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="221"/>
        <source>Exclude WADs:</source>
        <translation>Bez WADów:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="228"/>
        <source>Servers with WADs on this list won&apos;t be displayed. Use &apos;,&apos; (a comma) to separate multiple wads.</source>
        <translation>Serwery z WADami na tej liście nie będą wyświetlane. Użyj &apos;,&apos; (przecinka) aby oddzielić wiele WADów.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="235"/>
        <source>Game modes:</source>
        <translation>Tryby gry:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="245"/>
        <source>Exclude game modes:</source>
        <translation>Bez trybów gry:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="252"/>
        <source>Selected game modes won&apos;t appear on the server list.</source>
        <translation>Wybrane tryby gry nie pojawią się na liście serwerów.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="259"/>
        <source>Addresses:</source>
        <translation>Adresy:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="266"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only servers that match these addresses will be displayed. Multiple addresses can be separated by commas (&lt;span style=&quot; font-weight:600;&quot;&gt;,&lt;/span&gt;). Subnets are supported by using the &lt;span style=&quot; font-weight:600;&quot;&gt;ADDR/N&lt;/span&gt; format where &lt;span style=&quot; font-weight:600;&quot;&gt;N&lt;/span&gt; can be a subnet mask or a numerical value for the subnet mask. For IPv4 the subnet can also be specified by omitting the octets. &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/24&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/255.255.255.0&lt;/span&gt; are all the same.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Będą wyświetlane tylko serwery z tymi adresami. Można podać kilka adresów, rozdzielając je przecinkami (&lt;span style=&quot; font-weight:600;&quot;&gt;,&lt;/span&gt;). Można też podawać podsieci wg formatu &lt;span style=&quot; font-weight:600;&quot;&gt;ADDR/N&lt;/span&gt;, gdzie &lt;span style=&quot; font-weight:600;&quot;&gt;N&lt;/span&gt; to może być albo maska podsieci, albo ilość bitów sieci w danej podsieci. W przypadku adresów IPv4, podsieć można też podać poprzez pominięcie dalszych oktetów. &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/24&lt;/span&gt;, oraz &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/255.255.255.0&lt;/span&gt; oznaczają to samo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="273"/>
        <source>Show full servers</source>
        <translation>Pokaż pełne serwery</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="283"/>
        <source>Show empty servers</source>
        <translation>Pokaż puste serwery</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="293"/>
        <source>Show password protected servers</source>
        <translation>Pokaż serwery chronione hasłami</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="303"/>
        <source>Show only valid servers</source>
        <translation>Pokaż tylko prawidłowe serwery</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="325"/>
        <source>Show the servers you were banned from.</source>
        <translation>Pokaż serwery na których zostałeś zbanowany.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="328"/>
        <source>Show banned servers</source>
        <translation>Pokaż serwery z banem</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="338"/>
        <source>Show the &quot;Refreshed too soon&quot; servers.</source>
        <translation>Pokaż serwery &quot;Odświeżono zbyt szybko&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="341"/>
        <source>Show &quot;too soon&quot; servers</source>
        <translation>Pokaż serwery &quot;zbyt szybko&quot;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="351"/>
        <source>&lt;p&gt;Show the servers that didn&apos;t respond at all or returned a bad response.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Pokaż serwery, które w ogóle nie odpowiedziały, lub odpowiedziały błędnie.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="354"/>
        <source>Show not responding servers</source>
        <translation>Pokaż serwery bez odpowiedzi</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="367"/>
        <source>Show testing servers</source>
        <translation>Pokaż serwery testujące</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="390"/>
        <source>Reset filter</source>
        <translation>Zresetuj filtr</translation>
    </message>
</context>
<context>
    <name>ServerList</name>
    <message>
        <location filename="../gui/serverlist.cpp" line="223"/>
        <source>Unhandled behavior in ServerList::contextMenuTriggered()</source>
        <translation>Nieobsłużone zachowanie w ServerList::contextMenuTriggered()</translation>
    </message>
    <message>
        <location filename="../gui/serverlist.cpp" line="222"/>
        <source>Doomseeker - context menu warning</source>
        <translation>Doomseeker - ostrzeżenie o menu kontekstowym</translation>
    </message>
</context>
<context>
    <name>ServerListColumns</name>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="55"/>
        <source>Players</source>
        <translation>Gracze</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="57"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="59"/>
        <source>Server Name</source>
        <translation>Nazwa serwera</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="67"/>
        <source>WADs</source>
        <translation>WADy</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="69"/>
        <source>Game Type</source>
        <translation>Tryb gry</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="61"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="63"/>
        <source>IWAD</source>
        <translation>IWAD</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="65"/>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
</context>
<context>
    <name>ServerListContextMenu</name>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="106"/>
        <source>Copy</source>
        <translation>Kopiuj</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="107"/>
        <source>Copy Address</source>
        <translation>Kopiuj adres</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="110"/>
        <source>Copy E-Mail</source>
        <translation>Kopiuj e-mail</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="113"/>
        <source>Copy URL</source>
        <translation>Kopiuj link</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="115"/>
        <source>Copy Name</source>
        <translation>Kopiuj nazwę</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="126"/>
        <source>Refresh</source>
        <translation>Odśwież</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="127"/>
        <source>Join</source>
        <translation>Dołącz</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="128"/>
        <source>Show join command line</source>
        <translation>Pokaż wierz poleceń dołączenia</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="129"/>
        <source>Find missing WADs</source>
        <translation>Odszukaj brakujące WADy</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="136"/>
        <source>Open URL in browser</source>
        <translation>Otwórz link w przeglądarce</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="139"/>
        <source>Pin</source>
        <translation>Przypnij</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="139"/>
        <source>Unpin</source>
        <translation>Odepnij</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="155"/>
        <source>Remote console</source>
        <translation>Konsola zdalna</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="162"/>
        <source>Sort additionally ascending</source>
        <translation>Sortuj dodatkowo rosnąco</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="163"/>
        <source>Sort additionally descending</source>
        <translation>Sortuj dodatkowo malejąco</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="168"/>
        <source>Remove additional sorting for column</source>
        <translation>Usuń dodatkowe sortowanie dla tej kolumny</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="171"/>
        <source>Clear additional sorting</source>
        <translation>Wyczyść dodatkowe sortowanie</translation>
    </message>
</context>
<context>
    <name>ServerListRowHandler</name>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="269"/>
        <source>&lt;ERROR&gt;</source>
        <translation>&lt;BŁĄD&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="281"/>
        <source>You are banned from this server!</source>
        <translation>Jesteś zbanowany z tego serwera!</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="340"/>
        <source>&lt;REFRESHING&gt;</source>
        <translation>&lt;ODŚWIEŻANIE&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="350"/>
        <source>&lt;NO RESPONSE&gt;</source>
        <translation>&lt;BRAK ODPOWIEDZI&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="362"/>
        <source>&lt;Refreshed too soon, wait a while and try again&gt;</source>
        <translation>&lt;Odświeżono zbyt szybko, poczekaj chwilę i spróbuj ponownie&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="416"/>
        <source>Unknown server response (%1): %2:%3</source>
        <translation>Nieznany typ odpowiedzi serwera (%1): %2:%3</translation>
    </message>
</context>
<context>
    <name>ServerTooltip::L10n</name>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="58"/>
        <source>(alias of: %1)</source>
        <translation>(zamiennik: %1)</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="64"/>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="175"/>
        <source>MISSING</source>
        <translation>BRAKUJE</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="170"/>
        <source>OPTIONAL</source>
        <translation>OPCJONALNY</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="180"/>
        <source>ALIAS</source>
        <translation>ZAMIENNIK</translation>
    </message>
</context>
<context>
    <name>ServersStatusWidget</name>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="83"/>
        <source>Players (Humans + Bots) / Servers Refreshed%</source>
        <translation>Gracze (Ludzie + Boty) / Serwery %Odświeżenia</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="114"/>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="137"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="116"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="130"/>
        <source>%1 (%2+%3) / %4</source>
        <translation>%1 (%2+%3) / %4</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="133"/>
        <source> %1</source>
        <translation> %1</translation>
    </message>
</context>
<context>
    <name>StaticMessages</name>
    <message>
        <location filename="../serverapi/message.cpp" line="30"/>
        <source>You have been banned from the master server.</source>
        <translation>Zostałeś zbanowany z master serwera.</translation>
    </message>
</context>
<context>
    <name>UpdateChannelTr</name>
    <message>
        <location filename="../updater/updatechannel.cpp" line="110"/>
        <source>Beta versions have newer features but they are untested. Releases on this update channel are more often and are suggested for users who want newest functionalities and minor bug fixes as soon as they become implemented and available.</source>
        <translation>Wersje Beta mają nowsze funkcje, ale nie zostały przetestowane. Nowe wersje na tym kanale pojawiają się częściej i są polecane użytkownikom, którzy chcą nowych funkcji i poprawek, gdy tylko zostaną one zaimplementowane i udostępnione.</translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="120"/>
        <source>Stable versions are released rarely. They cover many changes at once and these changes are more certain to work correctly. Critical bug fixes are also provided through this channel.</source>
        <translation>Wersje stabilne pojawiają się rzadko. Pokrywają one wiele zmian naraz i dają większą pewność, że zmiany te będą funkcjonować prawidłowo. Krytyczne poprawki także są udostępniane na tym kanale.</translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="138"/>
        <source>Beta</source>
        <translation>Beta</translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="142"/>
        <source>Stable</source>
        <translation>Stabilny</translation>
    </message>
</context>
<context>
    <name>UpdateInstaller</name>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="82"/>
        <source>Failed to copy the updater executable to a temporary space: &quot;%1&quot; -&gt; &quot;%2&quot;.</source>
        <translation>Nie można skopiować pliku wykonywalnego aktualizatora do przestrzeni tymczasowej: &quot;%1&quot; -&gt; &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="92"/>
        <location filename="../updater/updateinstaller.cpp" line="127"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="94"/>
        <source>Nothing to update.</source>
        <translation>Brak aktualizacji.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="96"/>
        <source>Update package or script are not found. Check log for details.</source>
        <translation>Nie odnaleziono pakietu lub skryptu aktualizacji. Szczegóły w logu.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="98"/>
        <source>Failed to start updater process.</source>
        <translation>Nie udało się wystartować procesu aktualizatora.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="100"/>
        <source>Unknown error: %1.</source>
        <translation>Nieznany błąd: %1.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="129"/>
        <source>Unable to read the update script.</source>
        <translation>Nie można przeczytać skryptu aktualizacyjnego.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="131"/>
        <source>No installation directory specified.</source>
        <translation>Nie podano katalogu aktualizacji.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="133"/>
        <source>Unable to determine the path of the updater.</source>
        <translation>Nie można wykryć ścieżki do aktualizatora.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="135"/>
        <source>General failure.</source>
        <translation>Ogółny błąd.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="137"/>
        <source>Unknown process error code: %1.</source>
        <translation>Nieznany kod błędu procesu: %1.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="147"/>
        <source>Installing update.</source>
        <translation>Instalacja aktualizacji.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="162"/>
        <source>Package directory &quot;%1&quot; doesn&apos;t exist.</source>
        <translation>Katalog pakietów &quot;%1&quot; nie istnieje.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="168"/>
        <source>Update was about to be installed but update script &quot;%1&quot; is missing.</source>
        <translation>Aktualizacja miała zostać zainstalowana, ale brakuje skryptu aktualizacji dla &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="205"/>
        <source>Failed to start updater process: %1 %2</source>
        <translation>Startowanie procesu aktualizatora nie powiodło się: %1 %2</translation>
    </message>
</context>
<context>
    <name>UpdatePackageFilter</name>
    <message>
        <location filename="../updater/updatepackagefilter.cpp" line="164"/>
        <source>-BROKEN</source>
        <translation>-ZEPSUTE</translation>
    </message>
</context>
<context>
    <name>UpdaterInfoParser</name>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="98"/>
        <source>Missing update revision info for package %1.</source>
        <translation>Brakuje informacji o rewizji dla pakietu %1.</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="126"/>
        <source>Invalid update download URL for package %1: %2</source>
        <translation>Nieprawidłowy URL do pobrania pakietu %1: %2</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="133"/>
        <source>Missing update download URL for package %1.</source>
        <translation>Brakuje URL do pobrania pakietu %1.</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="143"/>
        <source>Invalid update script download URL for package %1, %2</source>
        <translation>Nieprawidłowy URL do pobrania skryptu aktualizacyjnego dla pakietu %1: %2</translation>
    </message>
</context>
<context>
    <name>WadsPicker</name>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="114"/>
        <source>Doomseeker - Add file(s)</source>
        <translation>Doomseeker - Dodaj plik(i)</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="164"/>
        <source>%1 MISSING</source>
        <translation>%1 NIE ISTNIEJE</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="50"/>
        <source>Browse for a file.</source>
        <translation>Przeglądaj plik.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="66"/>
        <source>Add an empty path to the list.</source>
        <translation>Dodaj pustą ścieżkę do listy.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="86"/>
        <source>Remove the selected files from the list.</source>
        <translation>Usuń zaznaczone pliki z listy.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="113"/>
        <source>Remove all files from the list.</source>
        <translation>Usuń wszystkie pliki z listy.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="116"/>
        <source>Clear</source>
        <translation>Wyczyść</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="69"/>
        <source>Add empty</source>
        <translation>Dodaj pustą</translation>
    </message>
    <message>
        <source>Browse for a file</source>
        <translation type="vanished">Przeglądaj plik</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="53"/>
        <source>Browse</source>
        <translation>Przeglądaj</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="89"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="62"/>
        <source>Check paths</source>
        <translation>Sprawdź ścieżki</translation>
    </message>
</context>
<context>
    <name>WadseekerInterface</name>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="123"/>
        <source>Downloading WADs for server &quot;%1&quot;</source>
        <translation>Pobieranie WADów dla serwera &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="134"/>
        <source>Aborting service: %1</source>
        <translation>Przerywam usługę: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="140"/>
        <source>Aborting site: %1</source>
        <translation>Przerywam stronę: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="194"/>
        <source>All done. Success.</source>
        <translation>Wszystkie zakończone. Sukces.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="213"/>
        <source>All done. Fail.</source>
        <translation>Wszystkie zakończone. Porażka.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="349"/>
        <source>CRIT</source>
        <translation>KRYT</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="353"/>
        <source>ERROR</source>
        <translation>BŁĄD</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="357"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="361"/>
        <source>NOTI</source>
        <translation>WAŻN</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="365"/>
        <source>NAVI</source>
        <translation>NAWI</translation>
    </message>
    <message>
        <source>CRITICAL ERROR: %1</source>
        <translation type="vanished">BŁĄD KRYTYCZNY: %1</translation>
    </message>
    <message>
        <source>Error: %1</source>
        <translation type="vanished">Błąd: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="14"/>
        <location filename="../gui/wadseekerinterface.cpp" line="224"/>
        <location filename="../gui/wadseekerinterface.cpp" line="398"/>
        <source>Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="22"/>
        <source>Seek WADs, comma (&apos;,&apos;) separated:</source>
        <translation>Szukaj WADów, oddzielając kolejne przecinkiem:</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="59"/>
        <source>Abort</source>
        <translation>Przerwij</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Zamknij</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="557"/>
        <source>[%1%] Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="605"/>
        <source>Context menu error</source>
        <translation>Błąd menu kontekstowego</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="605"/>
        <source>Unknown action selected.</source>
        <translation>Wybrano nieznaną akcję.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="49"/>
        <location filename="../gui/wadseekerinterface.ui" line="91"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="54"/>
        <location filename="../gui/wadseekerinterface.ui" line="96"/>
        <source>Progress</source>
        <translation>Postęp</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="86"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="101"/>
        <source>Speed</source>
        <translation>Prędkość</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="106"/>
        <source>ETA</source>
        <translation>Czas</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="111"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="159"/>
        <source>Start game</source>
        <translation>Wystartuj grę</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="173"/>
        <source>Download</source>
        <translation>Pobierz</translation>
    </message>
</context>
<context>
    <name>WadseekerShow</name>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="53"/>
        <source>Another instance of Wadseeker is already running.</source>
        <translation>Okno Wadseekera jest już uruchomione.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="63"/>
        <source>Wadseeker will not work correctly:

The target directory is either not configured, is invalid or cannot be written to.

Please review your Configuration and/or refer to the online help available from the Help menu.</source>
        <translation>Wadseeker nie zadziała poprawnie:

Docelowy katalog nie jest skonfigurowany, jest nieprawidłowy, albo nie można w nim zapisywać.

Przejrzyj konfigurację, albo skorzystaj z pomocy online dostępnej z menu Pomoc.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="52"/>
        <location filename="../gui/wadseekershow.cpp" line="67"/>
        <source>Wadseeker cannot be launched</source>
        <translation>Wadseeker nie może zostać wystartowany</translation>
    </message>
</context>
<context>
    <name>WadseekerSitesTable</name>
    <message>
        <location filename="../gui/widgets/wadseekersitestable.cpp" line="93"/>
        <source>Abort</source>
        <translation>Przerwij</translation>
    </message>
</context>
<context>
    <name>WadseekerWadsTable</name>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="68"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="69"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="129"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="130"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="307"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="315"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="132"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="133"/>
        <source>Awaiting URLs</source>
        <translation>Oczekiwanie na linki</translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="192"/>
        <source>Done</source>
        <translation>Ukończono</translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="324"/>
        <source>Skip current URL</source>
        <translation>Pomiń obecny link</translation>
    </message>
</context>
</TS>
