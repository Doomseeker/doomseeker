<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES" sourcelanguage="en_GB">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="124"/>
        <source>This program is distributed under the terms of the LGPL v2.1 or later.</source>
        <translation>Este programa es distribuido bajo los términos de la licencia LGPL v2.1 o posterior.</translation>
    </message>
    <message>
        <source>This program uses GeoLite2 database for IP-to-Country (IP2C) purposes. Database and Contents Copyright (c) 2016 MaxMind, Inc.</source>
        <translation type="vanished">Éste programa usa la base de datos de GeoLite2 para localizar países por IP (IP2C). El contenido y la base de datos bajo Copyright (c) 2016 MaxMind, Inc.</translation>
    </message>
    <message>
        <source>GeoLite2 License:
This work is licensed under the Creative Commons Attribution - ShareAlike 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.</source>
        <translation type="vanished">Licencia de GeoLite2:
Se encuentra bajo la licencia de la Creative Commons Attribution-ShareAlike 3.0 Unported License. Para ver una copia de esta licéncia, visita http://creativecommons.org/licenses/by-sa/3.0/.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="122"/>
        <source>Copyright %1 %2 The Doomseeker Team</source>
        <translation>Copyright %1 %2 El Equipo de Doomseeker (The Doomseeker Team)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="127"/>
        <source>Doomseeker translations contributed by:
</source>
        <translation>Traducciones de Doomseeker contribuidas por:
</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="128"/>
        <source>- Polish: Zalewa</source>
        <translation>- Polaco: Zalewa</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="129"/>
        <source>- Spanish: Pol Marcet Sard%1</source>
        <translation>- Español: Pol Marcet Sard%1</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="130"/>
        <source>- Catalan: Pol Marcet Sard%1</source>
        <translation>- Catalán: Pol Marcet Sard%1</translation>
    </message>
    <message>
        <source>This program uses GeoLite2 data for IP-to-Country (IP2C) purposes, available from https://www.maxmind.com</source>
        <translation type="vanished">Este programa usa la información de GeoLite2 para localizar países por IP (IP2C), y la puede encontrar en https://www.maxmind.com</translation>
    </message>
    <message>
        <source>Database and Contents Copyright (c) 2018 MaxMind, Inc.</source>
        <translation type="vanished">La base de datos y su contenido bajo Copyright (c) 2018 MaxMind, Inc.</translation>
    </message>
    <message>
        <source>GeoLite2 License:
This work is licensed under the Creative Commons Attribution - ShareAlike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.</source>
        <translation type="vanished">Licencia de GeoLite2:
Se encuentra bajo la licencia de la Creative Commons Attribution-ShareAlike 4.0 Unported License. Para ver una copia de esta licencia, visita http://creativecommons.org/licenses/by-sa/4.0/.</translation>
    </message>
    <message>
        <source>GeoLite2 available at:
https://dev.maxmind.com/geoip/geoip2/geolite2/</source>
        <translation type="vanished">GeoLite2 está disponible en:
https://dev.maxmind.com/geoip/geoip2/geolite2/</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="66"/>
        <source>&lt;i&gt;No URL available&lt;/i&gt;</source>
        <translation>&lt;i&gt;No URL disponible&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="142"/>
        <source>- Aha-Soft</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="143"/>
        <source>- Crystal Clear by Everaldo Coelho</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="144"/>
        <source>- Fugue Icons (C) 2013 Yusuke Kamiyamane. All rights reserved.</source>
        <translation>- Fugue Icons (C) 2013 Yusuke Kamiyamane. Todos los derechos reservados.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="145"/>
        <source>- Nuvola 1.0 (KDE 3.x icon set)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="146"/>
        <source>- Oxygen Icons 4.3.1 (KDE)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="147"/>
        <source>- Silk Icon Set (C) Mark James (famfamfam.com)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="148"/>
        <source>- Tango Icon Library / Tango Desktop Project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.cpp" line="149"/>
        <source>This program uses icons (or derivates of) from following sources:
</source>
        <translation>Este programa usa iconos (o derivados) de las siguientes fuentes:
</translation>
    </message>
    <message>
        <source>JSON library license</source>
        <translation type="vanished">Licencia de la biblioteca JSON</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="14"/>
        <source>About Doomseeker</source>
        <translation>Acerca de Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="24"/>
        <location filename="../gui/aboutdialog.ui" line="43"/>
        <source>Doomseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="77"/>
        <location filename="../gui/aboutdialog.ui" line="406"/>
        <source>&lt;Version&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="90"/>
        <source>(ABI: &lt;DOOMSEEKER_ABI_VERSION&gt;)</source>
        <translation>(ABI: &lt;DOOMSEEKER_ABI_VERSION&gt;)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="112"/>
        <source>&lt;Changeset&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="140"/>
        <source>Revision:</source>
        <translation>Revisión:</translation>
    </message>
    <message>
        <source>This value is relevant to auto updater feature.</source>
        <translation type="vanished">Este valor es relevante para la función de actualización automática.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="150"/>
        <source>&lt;Revision&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="175"/>
        <location filename="../gui/aboutdialog.ui" line="419"/>
        <source>&lt;a href=&quot;https://doomseeker.drdteam.org/&quot;&gt;https://doomseeker.drdteam.org/&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Copyright © 2009 - 2021 The Doomseeker Team

This program is distributed under the terms of the LGPL v2.1 or later.

This program uses GeoLite2 database for IP-to-Country (IP2C) purposes. Database and Contents Copyright (c) 2016 MaxMind, Inc.

GeoLite2 License:
This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.

GeoLite2 available at:
https://dev.maxmind.com/geoip/geoip2/geolite2/

This program uses icons (or derivates of) from following sources:
- Aha-Soft
- Crystal Clear by Everaldo Coelho
- Fugue Icons (C) 2013 Yusuke Kamiyamane. All rights reserved.
- Nuvola 1.0 (KDE 3.x icon set)
- Oxygen Icons 4.3.1 (KDE)
- Silk Icon Set (C) Mark James (famfamfam.com)
- Tango Icon Library / Tango Desktop Project
</source>
        <translation type="obsolete">Copyright © 2009 - 2021 The Doomseeker Team

Éste programa és distribuido bajo los términos de la licéncia LGPL v2.1 o posterior

Éste programa usa la base de datos de GeoLite2 para localizar países por IP (IP2C). El contenido y la base de datos bajo Copyright (c) 2016 MaxMind, Inc.

Licencia de GeoLite2:
Se encuentra bajo la licencia de la Creative Commons Attribution-ShareAlike 3.0 Unported License. Para ver una copia de esta licéncia, visita http://creativecommons.org/licenses/by-sa/3.0/.

GeoLite2 disponible en:
https://dev.maxmind.com/geoip/geoip2/geolite2/

- Aha-Soft
- Crystal Clear by Everaldo Coelho
- Fugue Icons (C) 2013 Yusuke Kamiyamane. Todos los derechos reservados.
- Nuvola 1.0 (KDE 3.x icon set)
- Oxygen Icons 4.3.1 (KDE)
- Silk Icon Set (C) Mark James (famfamfam.com)
- Tango Icon Library / Tango Desktop Project
</translation>
    </message>
    <message>
        <source>Contact information:</source>
        <translation type="vanished">Información de contacto:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="232"/>
        <source>Zalewa:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="239"/>
        <source>Blzut3:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="290"/>
        <source>Additional contributions from:</source>
        <translation>Contribuidores adicionales:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="300"/>
        <source>- Hyper_Eye, Nece228, Linda &quot;WubTheCaptain&quot; Lapinlampi, Pol Marcet Sardà</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="310"/>
        <source>- Doomseeker logo and main icon design by Rachael &quot;Eruanna&quot; Alexanderson</source>
        <translation>- El logo de Doomseeker y el icono principal diseñados por Rachael &quot;Eruanna&quot; Alexanderson</translation>
    </message>
    <message>
        <source>- Improved Doomseeker icons by MazterQyoun-ae and CarpeDiem</source>
        <translation type="vanished">- Iconos de Doomseeker mejorados por MazterQyoun-ae y CarpeDiem</translation>
    </message>
    <message>
        <source>Show JSON library license</source>
        <translation type="vanished">Mostrar licencia de la librería JSON</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="147"/>
        <source>The auto updater compares the versions by this revision number.</source>
        <translation>El actualizador automático comparará otras versiones con este número de versión.</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="223"/>
        <source>Doomseeker maintainers:</source>
        <translation>Responsables de Doomseeker:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="246"/>
        <source>&lt;a href=&quot;mailto:admin@maniacsvault.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;admin@maniacsvault.net&lt;/span&gt;&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="262"/>
        <source>&lt;a href=&quot;mailto:zalewapl@gmail.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;zalewapl@gmail.com&lt;/span&gt;&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="320"/>
        <source>- Doomseeker icons by MazterQyoun-ae and CarpeDiem</source>
        <translation>- Iconos de Doomseeker hechos por MazterQyoun-ae y CarpeDiem</translation>
    </message>
    <message>
        <source>GeoLite2 Database:</source>
        <translation type="vanished">Base de datos GeoLite2:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="340"/>
        <source>IP2C Database:</source>
        <translation>Base de datos IP2C:</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="350"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&amp;lt;URL&amp;gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="371"/>
        <location filename="../gui/aboutdialog.ui" line="390"/>
        <source>Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="437"/>
        <source>Copyright ©</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="447"/>
        <source>&lt;YearSpan&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="457"/>
        <source>&lt;Author&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="482"/>
        <source>&lt;Description&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="538"/>
        <source>Version: 0.0.0.0</source>
        <translation>Versión: 0.0.0.0</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="551"/>
        <source>(ABI: DOOMSEEKER_ABI_VERSION)</source>
        <translation>(ABI: DOOMSEEKER_ABI_VERSION)</translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="573"/>
        <source>&lt;Plugin Author&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/aboutdialog.ui" line="509"/>
        <source>Plugins</source>
        <translation>Complementos</translation>
    </message>
</context>
<context>
    <name>AddBuddyDlg</name>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="300"/>
        <source>Invalid Pattern</source>
        <translation>Patrón inválido</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="300"/>
        <source>The pattern you have specified is invalid.</source>
        <translation>El patrón que has especificado no es válido.</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="319"/>
        <source>The asterisk (*) can be used as a wild card.</source>
        <translation>El asterisco (*) representa cualquier letra.</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="323"/>
        <source>Use the Regular Expression format.</source>
        <translation>Utiliza el formato de expresiones regulares.</translation>
    </message>
    <message>
        <source>The pattern you have specified is not a valid regular expression.</source>
        <translation type="vanished">El patrón que has especificado no es una expressión regular válida.</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="14"/>
        <source>Add Buddy</source>
        <translation>Añadir Compañero</translation>
    </message>
    <message>
        <source>Type the name of your buddy here.  If the pattern type is set to basic you may use an asterisk (*) as a wild card.</source>
        <translation type="vanished">Escriba aquí el nombre de su compañero. Si el tipo de patrón es básico, puede usar el asterisco (*) como carácter comodín.</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="26"/>
        <source>Buddy name:</source>
        <translation>Nombre de compañero:</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="36"/>
        <source>Pattern Type</source>
        <translation>Tipo de patrón</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="42"/>
        <source>Basic</source>
        <translation>Básico</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="52"/>
        <source>Advanced</source>
        <translation>Avanzado</translation>
    </message>
    <message>
        <location filename="../gui/addBuddyDlg.ui" line="62"/>
        <source>&lt;tip&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppRunner</name>
    <message>
        <location filename="../apprunner.cpp" line="55"/>
        <source>Could not read bundle plist. (%1)</source>
        <translation>No se ha podido leer el paquete plist. (%1)</translation>
    </message>
    <message>
        <location filename="../apprunner.cpp" line="83"/>
        <location filename="../apprunner.cpp" line="114"/>
        <source>Starting (working dir %1): %2</source>
        <translation>Comenzando (directorio de trabajo %1): %2</translation>
    </message>
    <message>
        <location filename="../apprunner.cpp" line="102"/>
        <source>Cannot run file: %1</source>
        <translation>No se puede ejecutar el archivo: %1</translation>
    </message>
    <message>
        <source>File: %1
cannot be run</source>
        <translation type="vanished">El archivo %1
no puede ser ejecutado</translation>
    </message>
</context>
<context>
    <name>AutoUpdater</name>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="91"/>
        <source>Missing main &quot;update&quot; node.</source>
        <translation>Falta nodo principal &quot;update&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="126"/>
        <source>Missing &quot;install&quot; element.</source>
        <translation>Falta elemento &quot;install&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="136"/>
        <source>Missing text node for &quot;package&quot; element for &quot;file&quot; element %1</source>
        <translation>Falta nodo de texto para el elemento &quot;package&quot; para el elemento &quot;file&quot; %1</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="158"/>
        <source>Missing &quot;packages&quot; element.</source>
        <translation>Falta elemento &quot;packages&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="172"/>
        <source>More than one &quot;package&quot; element found.</source>
        <translation>Más de un elemento &quot;package&quot; encontrado.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="174"/>
        <source>Missing &quot;package&quot; element.</source>
        <translation>Falta elemento &quot;package&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updaterscriptparser.cpp" line="183"/>
        <source>Failed to find &quot;name&quot; text node.</source>
        <translation>No se ha encontrado el nodo de texto &quot;name&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="122"/>
        <source>Failed to parse updater XML script: %1, l: %2, c: %3</source>
        <translation>No se ha podido analizar el script actualizador XML: %1, l: %2, c: %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="132"/>
        <source>Failed to modify package name in updater script: %1</source>
        <translation>No se ha podido modificar el nombre del paquete en el script actualizador: %1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="160"/>
        <source>Detected update for package &quot;%1&quot; from version &quot;%2&quot; to version &quot;%3&quot;.</source>
        <translation>Se ha detectado una actualización para el paquete &quot;%1&quot; de la versión &quot;%2&quot; a &quot;%3&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="195"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="197"/>
        <source>Update was aborted.</source>
        <translation>Actualización abortada.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="199"/>
        <source>Update channel is not configured. Please check your configuration.</source>
        <translation>Canal de actualización no configurado. Por favor compruebe la configuración.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="201"/>
        <source>Failed to download updater info file.</source>
        <translation>No se ha podido descargar la información del actualizador.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="203"/>
        <source>Cannot parse updater info file.</source>
        <translation>No se puede procesar el archivo con los datos de actualización.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="205"/>
        <source>Main program node is missing from updater info file.</source>
        <translation>Falta el nodo principal del programa en el archivo de datos de actualización.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="207"/>
        <source>Revision info on one of the packages is missing from the updater info file. Check the log for details.</source>
        <translation>Los datos sobre la revisión de uno de los paquetes no existen en el archivo de datos de actualización. Compruebe el registro para más detalles.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="210"/>
        <source>Download URL for one of the packages is missing from the updater info file. Check the log for details.</source>
        <translation>La URL de descarga de uno de los paquetes no existe en el archivo de datos de actualización. Compruebe el registro para más detalles.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="213"/>
        <source>Download URL for one of the packages is invalid. Check the log for details.</source>
        <translation>La URL de descarga de uno de los paquetes es inválida. Compruebe el registro para más detalles.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="216"/>
        <source>Update package download failed. Check the log for details.</source>
        <translation>Error al descargar el paquete de actualización. Compruebe el registro para más detalles.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="218"/>
        <source>Failed to create directory for updates packages storage.</source>
        <translation>No se ha podido crear el directorio para guardar paquetes de actualización.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="220"/>
        <source>Failed to save update package.</source>
        <translation>No se ha podido guardar el paquete de actualización.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="222"/>
        <source>Failed to save update script.</source>
        <translation>No se ha podido guardar el script de actualización.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="224"/>
        <source>Unknown error.</source>
        <translation>Error desconocido.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="268"/>
        <source>Finished downloading package &quot;%1&quot;.</source>
        <translation>Descarga completa del paquete &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="274"/>
        <source>Network error when downloading package &quot;%1&quot;: [%2] %3</source>
        <translation>Se produjo un error de red mientras se descargaba el paquete &quot;%1&quot;:  [%2] %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="297"/>
        <source>Finished downloading package script &quot;%1&quot;.</source>
        <translation>Se terminó de descargar el script del paquete &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="314"/>
        <source>All packages downloaded. Building updater script.</source>
        <translation>Todos los paquetes se han descargado. Creando el script de actualización.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="321"/>
        <source>Network error when downloading package script &quot;%1&quot;: [%2] %3</source>
        <translation>Se produjo un error de red mientras se descargaba el script del paquete &quot;%1&quot;:  [%2] %3</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="350"/>
        <source>Requesting update confirmation.</source>
        <translation>Solicitando confirmación de actualización.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="351"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="362"/>
        <source>No new program updates detected.</source>
        <translation>No se encontraron actualizaciones.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="365"/>
        <source>Some update packages were ignored. To install them select &quot;Check for updates&quot; option from &quot;Help&quot; menu.</source>
        <translation>Algunos paquetes de actualización han sido ignorados. Para instalarlos, seleccione &quot;Buscar actualizaciones&quot; en el menú &quot;Ayuda&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="430"/>
        <source>Failed to create directory for updates storage: %1</source>
        <translation>No se ha podido crear el directorio para guardar la actualización:%1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="448"/>
        <source>Update info</source>
        <translation>Información de la actualización</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="465"/>
        <source>Invalid download URL for package &quot;%1&quot;: %2</source>
        <translation>La URL de descarga no es válida para el paquete &quot;%1&quot;:%2</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="470"/>
        <source>Package: %1</source>
        <translation>Paquete: %1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="471"/>
        <source>Downloading package &quot;%1&quot; from URL: %2.</source>
        <translation>Descargando paquete &quot;%1&quot; de la URL: %2.</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="486"/>
        <source>Couldn&apos;t save file in path: %1</source>
        <translation>No se pudo guardar el archivo en la ruta:%1</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="516"/>
        <source>Invalid download URL for package script &quot;%1&quot;: %2</source>
        <translation>La URL de descarga no es válida para el script del paquete &quot;%1&quot;:%2</translation>
    </message>
    <message>
        <location filename="../updater/autoupdater.cpp" line="521"/>
        <source>Downloading package script &quot;%1&quot; from URL: %2.</source>
        <translation>Descargando script del paquete &quot;%1&quot; de la URL: %2.</translation>
    </message>
</context>
<context>
    <name>BroadcastManager</name>
    <message>
        <location filename="../serverapi/broadcastmanager.cpp" line="50"/>
        <source>%1 LAN server gone: %2, %3:%4</source>
        <translation>Servidor LAN %1 desaparecido: %2, %3:%4</translation>
    </message>
    <message>
        <location filename="../serverapi/broadcastmanager.cpp" line="63"/>
        <source>New %1 LAN server detected: %2:%3</source>
        <translation>Nuevo servidor LAN %1 detectado: %2:%3</translation>
    </message>
</context>
<context>
    <name>CFGAppearance</name>
    <message>
        <location filename="../gui/configuration/cfgappearance.h" line="45"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="95"/>
        <source>Use system language</source>
        <translation>Usar lengua del sistema</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="187"/>
        <source>Unknown language definition &quot;%1&quot;</source>
        <translation>Definición de lengua desconocida &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="213"/>
        <source>Loading translation &quot;%1&quot;</source>
        <translation>Cargando traducción &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.cpp" line="217"/>
        <source>Program needs to be restarted to fully apply the translation</source>
        <translation>El programa necesita reiniciarse para aplicar completamente la traducción</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="99"/>
        <source>Player slots style:</source>
        <translation>Estilo del número de jugadores:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="113"/>
        <source>Marines</source>
        <translation>Marines</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="118"/>
        <source>Blocks</source>
        <translation>Bloques</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="123"/>
        <source>Numeric</source>
        <translation>Numérico</translation>
    </message>
    <message>
        <source>Custom servers color:</source>
        <translation type="vanished">Color servidores customizados:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="229"/>
        <source>Bots are not players</source>
        <translation>Bots no son jugadores</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="208"/>
        <source>Hide passwords</source>
        <translation>Ocultar contraseñas</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="215"/>
        <source>Lookup server hosts</source>
        <translation>Buscar servidores host</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="34"/>
        <source>Language:</source>
        <translation>Lengua:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="87"/>
        <source>Full retranslation requires a restart.</source>
        <translation>La traducción completa requiere de reiniciar el programa.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="131"/>
        <source>Pinned servers color:</source>
        <translation>Color de los servidores fijados:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="222"/>
        <source>Colorize server console</source>
        <translation>Colorear consola del servidor</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="236"/>
        <source>Draw grid in server table</source>
        <translation>Dibujar cuadrícula en la tabla de servidores</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="165"/>
        <source>Servers with buddies color:</source>
        <translation>Color servidores con compañeros:</translation>
    </message>
    <message>
        <source>Restart will be required to apply the translation fully.</source>
        <translation type="vanished">Es necesario reiniciar para aplicar la traducción completamente.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="148"/>
        <source>LAN servers color:</source>
        <translation>Color servidores LAN:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="243"/>
        <source>Use tray icon</source>
        <translation>Usar icono de la bandeja del sistema</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgappearance.ui" line="252"/>
        <source>When close button is pressed, minimize to tray icon.</source>
        <translation>Cuando se presione el botón &quot;cerrar&quot;, minimizar a la bandeja del sistema.</translation>
    </message>
</context>
<context>
    <name>CFGAutoUpdates</name>
    <message>
        <source>Auto Updates</source>
        <translation type="vanished">Actualizaciones automáticas</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="44"/>
        <source>Disabled</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="51"/>
        <source>Notify me but don&apos;t install</source>
        <translation>Notifícame pero no instales las actualizaciones</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="61"/>
        <source>Install automatically</source>
        <translation>Instala automáticamente</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.ui" line="76"/>
        <source>Update channel:</source>
        <translation>Canal de actualización:</translation>
    </message>
    <message>
        <source>&lt;p&gt;New IP2C database will be downloaded if locally stored one is missing or has a different checksum than the one stored on Doomseeker&apos;s web page.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Se descargará una nueva base de datos IP2C si falta una almacenada localmente o tiene una checksum diferente a la almacenada en la página web de Doomseeker.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>IP2C auto update</source>
        <translation type="vanished">Actualizar automáticamente IP2C</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgautoupdates.h" line="44"/>
        <source>Auto updates</source>
        <translation>Actualizaciones automáticas</translation>
    </message>
</context>
<context>
    <name>CFGCustomServers</name>
    <message>
        <source>Pinned Servers</source>
        <translation type="vanished">Servidores Fijados</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="87"/>
        <source>Toggle enabled state</source>
        <translation>Conmutar el estado</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="112"/>
        <source>Doomseeker - pinned servers</source>
        <translation>Doomseeker - servidores fijados</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="126"/>
        <source>Port must be within range 1 - 65535</source>
        <translation>El puerto debe estar en el rango 1 - 65535</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="130"/>
        <source>Unimplemented behavior!</source>
        <translation>Comportamiento no implementado!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="205"/>
        <source>Host</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.cpp" line="205"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Adds a new entry to the list. To specify the address and port of the custom server double-click on the cells in the table. Domain names are supported.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Agrega una nueva entrada a la lista. Para especificar la dirección y el puerto del servidor fijado, haga doble clic en las celdas de la tabla. Los nombres de dominio están soportados.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="46"/>
        <source>&lt;p&gt;Add a new entry to the list. To specify the address and port of the custom server double-click on the cells in the table. Domain names are supported.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Añadir una entrada en la lista. Para especificar la dirección y el puerto del servidor, haga doble clic en las celdas de la tabla. También puede usar nombres de dominio (DNS).&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="49"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="60"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="78"/>
        <source>Enable selected</source>
        <translation>Activar seleccionados</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="81"/>
        <source>Enable</source>
        <translation>Activar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="92"/>
        <source>Disable selected</source>
        <translation>Desactivar seleccionados</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="95"/>
        <source>Disable</source>
        <translation>Desactivar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="113"/>
        <source>Change the game on the selected servers.</source>
        <translation>Cambia el juego en los servidores seleccionados.</translation>
    </message>
    <message>
        <source>Changes engine on selected servers.</source>
        <translation type="vanished">Cambia el motor en los servidores seleccionados.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.ui" line="116"/>
        <source>Set game</source>
        <translation>Establecer juego</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgcustomservers.h" line="50"/>
        <source>Pinned servers</source>
        <translation>Servidores fijados</translation>
    </message>
</context>
<context>
    <name>CFGFilePaths</name>
    <message>
        <source>File Paths</source>
        <translation type="vanished">Rutas de archivo</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="61"/>
        <source>Path</source>
        <translation>Ruta</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="61"/>
        <source>Recurse</source>
        <translation>Recursivo</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="69"/>
        <source>Doomseeker supports special characters in user-configurable paths. This affects all paths everywhere, not only the file paths configured on this page. The placeholders are:</source>
        <translation>Doomseeker soporta caracteres especiales en rutas configuradas por el usuario. Esto afecta *todas* las rutas, no solo las rutas configuradas en esta página. Los marcadores son:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="75"/>
        <source>&lt;b&gt;~&lt;/b&gt; - if at the beginning of the path, resolved to the home directory of the current user</source>
        <translation>&lt;b&gt;~&lt;/b&gt; - Si está al principio de la ruta, se traduce al directorio principal del usuario actual</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="79"/>
        <source>&lt;b&gt;~&lt;i&gt;user&lt;/i&gt;&lt;/b&gt; - if at the beginning of the path, resolved to the home directory of the specified &lt;i&gt;user&lt;/i&gt;</source>
        <translation>&lt;b&gt;~&lt;i&gt;usuario&lt;/i&gt;&lt;/b&gt; - Si está al principio de la ruta, se traduce al directorio principal del &lt;i&gt;usuario&lt;/i&gt; especificado</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="83"/>
        <source>&lt;b&gt;$PROGDIR&lt;/b&gt; - resolved to the directory where Doomseeker&apos;s executable is located; this may create invalid paths if used in the middle of the path</source>
        <translation>&lt;b&gt;$PROGDIR&lt;/b&gt; - se traduce al directorio donde el ejecutable de Doomseeker se encuentra; si se usa en medio de una ruta, quizá cree rutas inválidas</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="87"/>
        <source>&lt;b&gt;$&lt;i&gt;NAME&lt;/i&gt;&lt;/b&gt; - any &lt;i&gt;NAME&lt;/i&gt; is resolved to the environment variable of the same &lt;i&gt;NAME&lt;/i&gt;, or to empty if the environment variable is absent</source>
        <translation>&lt;b&gt;$&lt;i&gt;NOMBRE&lt;/i&gt;&lt;/b&gt; - cualquier otro &lt;i&gt;NOMBRE&lt;/i&gt; se traduce a la variable de entorno con el mismo &lt;i&gt;NOMBRE&lt;/i&gt;, o se deja vacía si no existe</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="93"/>
        <source>This option controls whether the &lt;b&gt;$PROGDIR&lt;/b&gt; and &lt;b&gt;$&lt;i&gt;NAME&lt;/i&gt;&lt;/b&gt; will be resolved or ignored. If this option is &lt;b&gt;checked&lt;/b&gt;, both are resolved as explained. If this option is &lt;b&gt;unchecked&lt;/b&gt;, both will be taken literally. The &lt;b&gt;~&lt;/b&gt; placeholders are always resolved regardless.</source>
        <translation>Esta opción controla si se resuelven los marcadores &lt;b&gt;$PROGDIR&lt;/b&gt; y &lt;b&gt;$&lt;i&gt;NOMBRE&lt;/i&gt;&lt;/b&gt;. Si está &lt;b&gt;marcado&lt;/b&gt;, se resoverán como se ha explicado. Si está &lt;b&gt;desmarcado&lt;/b&gt;, se tomarán literalmente. El marcador &lt;b&gt;~&lt;/b&gt; se resuelve siempre.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="261"/>
        <source>Path empty.</source>
        <translation>Ruta vacía.</translation>
    </message>
    <message>
        <source>Doomseeker - Add wad path</source>
        <translation type="vanished">Doomseeker - Agregar ruta a WAD</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="151"/>
        <source>Doomseeker - Add game mod path</source>
        <translation>Doomseeker - Añadir ruta de modificaciones del juego</translation>
    </message>
    <message>
        <source>No path specified.</source>
        <translation type="vanished">Ruta no especificada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="265"/>
        <source>Path doesn&apos;t exist.</source>
        <translation>La ruta no existe.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.cpp" line="268"/>
        <source>Path is not a directory.</source>
        <translation>La ruta no es un directorio.</translation>
    </message>
    <message>
        <source>IWAD and PWAD paths:</source>
        <translation type="vanished">Rutas hacia IWADs y PWADs:</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;WARNING: &lt;/span&gt;It&apos;s highly discouraged to enable recursion for directories with lots of subdirectories. You may experience heavy performance loss and high hard drive usage if recursion is used recklessly. When recursion is enabled, file search operations will go the entire way down to the bottom of the directory tree if necessary.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot;font-weight: 600;&quot;&gt;NOTA: &lt;/span&gt;No se recomienda habilitar la recursividad para directorios con muchos subdirectorios. Puede experimentar una pérdida severa de rendimiento y un desgaste excesivo del disco duro si la recursión se usa imprudentemente. Cuando la recursividad está habilitada, la operación de búsqueda de archivos irá hasta el fondo del árbol de directorios, si es necesario.&lt;/P&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;WARNING: &lt;/span&gt;It&apos;s not recommended to enable recursion for directories with lots of subdirectories. With such directories you may experience heavy performance loss and high hard drive usage. When recursion is enabled, file search operations will go the entire way down to the bottom of the directory tree if necessary.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot;font-weight: 600;&quot;&gt;NOTA: &lt;/span&gt;No se recomienda habilitar la recursividad para directorios con muchos subdirectorios. Puede experimentar una pérdida severa de rendimiento y un uso excesivo del disco duro. Cuando la recursividad está habilitada, la operación de búsqueda de archivos irá hasta el fondo del árbol de directorios, si es necesario.&lt;/P&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="29"/>
        <source>Where are the game mods, IWADs, PWADs:</source>
        <translation>Dónde se encuentran las modificaciones, IWADs y PWADs:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="64"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;WARNING: &lt;/span&gt;It&apos;s not recommended to enable recursion for directories with lots of subdirectories. With such directories you may experience heavy performance loss and high hard drive usage. When recursion is enabled, the file search will go the entire way down to the bottom of the directory tree if necessary.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot;font-weight: 600;&quot;&gt;NOTA: &lt;/span&gt;No se recomienda habilitar la recursividad para directorios con muchos subdirectorios. Puede experimentar una pérdida severa de rendimiento y un uso excesivo del disco duro. Cuando la recursividad está habilitada, la operación de búsqueda de archivos irá hasta el fondo del árbol de directorios, si es necesario.&lt;/P&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="92"/>
        <source>Browse for a file path.</source>
        <translation>Buscar una ruta de archivo.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="95"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="102"/>
        <source>Add an empty path to the list.</source>
        <translation>Añadir una ruta vacía a la lista.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="105"/>
        <source>Add empty</source>
        <translation>Añadir vacío</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="116"/>
        <source>Remove the selected paths from the list.</source>
        <translation>Eliminar las rutas seleccionadas de la lista.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="119"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="132"/>
        <source>&lt;p&gt;If selected Doomseeker will attempt to find the files used on a particular server when the mouse cursor is hovered over the WADs and IWAD columns. This requires a hard drive access to search in all the directories specified above each time a new tooltip is generated. If it takes too long to create such tooltip, you may try disabling this option.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si se selecciona, Doomseeker intentará buscar los archivos que usa este servidor cuando tu cursor esté encima de la columna de WADs y IWAD. Esto requiere de buscar en los directorios que has especificado cada vez que se genera una ventana. Si esto toma mucho tiempo, prueba a desactivar esta opción.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="142"/>
        <source>&lt;p&gt;If selected Doomseeker will attempt to check the integrity of the local WADs, compared to the ones used on a particular server. This requires a hard drive access to read the entirety of the WADs. It also requires the checksums to be downloaded from the servers, causing more network traffic. If it takes too long to join servers, or there are problems with the server refresh, you may want to disable this feature.&lt;/p&gt;
&lt;p&gt;This setting is used when joining a server and when clicking the &amp;quot;Find missing WADs&amp;quot; button in the context menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si se selecciona Doomseeker intentará comprobar la integridad de los WADs locales, en comparación con los utilizados en un servidor concreto. Esto requiere un acceso al disco duro para leer la totalidad de los WADs. También requiere que las sumas de control se descarguen de los servidores, provocando mayor tráfico de red. Si se tarda demasiado en unirse a los servidores o hay problemas con la actualización del servidor, es posible que desee desactivar esta característica.&lt;/p&gt;
&lt;p&gt;Esta configuración se utiliza cuando se une a un servidor y cuando se hace clic en el botón &amp;quot;Encuentra los WADs que faltan&amp;quot; en el menú contextual.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;If selected Doomseeker will attempt to check the integrity of the local WADs, compared to the ones used on a particular server. This requires a hard drive access to read the entirety of the WADs. If it takes too long to join servers, you may want to disable this feature.&lt;/p&gt;
&lt;p&gt;This setting is used when joining a server and when clicking the &amp;quot;Find missing WADs&amp;quot; button in the context menu.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Si se selecciona, Doomseeker intentará comprobar la integridad de los WADs locales, en comparación con las utilizadas en un servidor concreto. Esto requiere acceso al disco duro para leer la totalidad de los WADs. Si esto requiere demasiado tiempo para conectarse a los servidores, es posible que desee desactivar esta función.&lt;/p&gt;
&lt;p&gt;Este parámetro se utiliza cuando se une a un servidor y al hacer clic en el botón &amp;quot;Encuentra los WADs faltantes&amp;quot; en el menú contextual del ratón.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="153"/>
        <source>Resolve $PLACEHOLDERS in paths</source>
        <translation>Evaluar $MARCADORES en las rutas</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;If selected Doomseeker will attempt to find files used on a particular server if mouse cursor is hovered over the WADS column. This requires hard drive access and search in all directories specified above each time a new tooltip is generated. If it takes too long to create such tooltip you may try disabling this option.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;This also applies to IWAD column.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Si se selecciona Doomseeker intentará encontrar los archivos utilizados en un servidor particular si el cursor está en la columna WADs de este. Esto requiere de acceso al disco duro y buscar en todos los directorios especificados cada vez que se genera la lista. Si lleva demasiado tiempo crear dicha lista, puede intentar deshabilitar esta opción.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Esto también se aplica a la columna IWAD.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="135"/>
        <source>Tell me where are my WADs located</source>
        <translation>Dime dónde se encuentran mis WADs</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Regular&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;If selected Doomseeker will attempt to check the integrity of the local WADs, compared to the ones used on a particular server. This requires hard drive access to read the entirety of the WADs. If it takes too long to join servers you may want to disable this feature.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;This setting is used when joining a server and when clicking the &amp;quot;Find missing WADs&amp;quot; button on the right-click menu.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Regular&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;Si se selecciona, Doomseeker intentará comprobar la integridad de los WADs locales, en comparación con las utilizadas en un servidor concreto. Esto requiere acceso al disco duro para leer la totalidad de los WADs. Si esto requiere demasiado tiempo para conectarse a los servidores, es posible que desee desactivar esta función.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;Este parámetro se utiliza cuando se une a un servidor y al hacer clic en el botón &amp;quot;Encuentra los WADs faltantes&amp;quot; en el menú contextual del ratón.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.ui" line="146"/>
        <source>Check the integrity of local WADs</source>
        <translation>Comprobar la integridad de los WADs locales</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgfilepaths.h" line="49"/>
        <source>File paths</source>
        <translation>Rutas de archivo</translation>
    </message>
</context>
<context>
    <name>CFGGames</name>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="29"/>
        <source>Player name:</source>
        <translation>Nombre del jugador:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="36"/>
        <source>Use Operating System&apos;s username</source>
        <translation>Utilizar el nombre de usuario del sistema operativo</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="46"/>
        <source>Custom:</source>
        <translation>Customizado:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="56"/>
        <source>Player</source>
        <translation>Jugador</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.ui" line="63"/>
        <source>This is currently used only for recording demos. It&apos;s attached to each demo you record.</source>
        <translation>Actualmente sólo se utiliza para grabar demos. Se adjunta a cada demo que graba.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfggames.h" line="43"/>
        <source>Games</source>
        <translation>Juegos</translation>
    </message>
</context>
<context>
    <name>CFGIP2C</name>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="29"/>
        <source>&lt;p&gt;A new IP2C database will be downloaded if the local one is missing or has a different checksum than the one stored on Doomseeker&apos;s web page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Se descargará una nueva base de datos IP2C si falta una almacenada localmente o tiene una checksum diferente a la almacenada en la página web de Doomseeker.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="32"/>
        <source>IP2C auto update</source>
        <translation>Actualizar automáticamente IP2C</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Servers of some games are able to directly tell Doomseeker in which country they are located in. If this is checked, Doomseeker will honor this information. If unchecked, Doomseeker will always try to detect the country by server&apos;s IP. &lt;/p&gt;&lt;p&gt;Neither of these methods may be accurate - the server can tell false info and the IP2C database may contain inaccuracies. Either method can lead to the wrong country being displayed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Algunos servidores pueden decir a Doomseeker en qué país se encuentran directamente. Si esta opción está activada, Doomseeker usará la información que este le proporciona. Si no, Doomseeker intentará detectar el país según la IP del servidor.&lt;/p&gt;&lt;p&gt;Ambas opciones pueden tener errores: el servidor puede dar información falsa, y la base de datos IP2C puede tener entradas sin actualizar. Ambas opciones pueden acabar con un país erróneo&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.ui" line="42"/>
        <source>Honor countries set by servers</source>
        <translation>Usar el país definido por el servidor</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgip2c.h" line="44"/>
        <source>IP2C</source>
        <translation>IP2C</translation>
    </message>
</context>
<context>
    <name>CFGIRCAppearance</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.h" line="44"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.h" line="49"/>
        <source>IRC - Appearance</source>
        <translation>IRC - Apariencia</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="37"/>
        <source>Default text color:</source>
        <translation>Color de texto predefinido:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="54"/>
        <source>Channel action color:</source>
        <translation>Color de acción en el canal:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="71"/>
        <source>Network action color:</source>
        <translation>Color de acción en la red:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="88"/>
        <source>CTCP color:</source>
        <translation>Color CTCP:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="105"/>
        <source>Error color:</source>
        <translation>Color errores:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="125"/>
        <source>Background color:</source>
        <translation>Color de fondo:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="142"/>
        <source>URL color:</source>
        <translation>Color de links:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="159"/>
        <source>Main font:</source>
        <translation>Fuente principal:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="185"/>
        <source>User list</source>
        <translation>Lista de usuarios</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="194"/>
        <source>Font:</source>
        <translation>Fuente:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="211"/>
        <source>Selected text color:</source>
        <translation>Color de texto seleccionado:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="228"/>
        <source>Selected background:</source>
        <translation>Fondo seleccionado:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="248"/>
        <source>Prepends all entries with [hh:mm:ss] timestamps.</source>
        <translation>Precede todas las entradas con marcas de tiempo [hh:mm:ss].</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="251"/>
        <source>Enable timestamps</source>
        <translation>Activar marcas de tiempo</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircappearance.ui" line="258"/>
        <source>Window/task bar alert on important event (private msg, callsign)</source>
        <translation>Alertar en la ventana/barra de tareas cuando sucedan eventos importantes (mensaje privado, llamada)</translation>
    </message>
</context>
<context>
    <name>CFGIRCDefineNetworkDialog</name>
    <message>
        <source>Following commands have violated the IRC maximum byte number limit (%1):

</source>
        <translation type="vanished">Los siguientes comandos han sobrepasado el límite IRC de máximo bytes (%1):

</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="82"/>
        <source>These commands have violated the IRC maximum byte number limit (%1):

</source>
        <translation>Estos comandos han sobrepasado el límite IRC de máximo bytes (%1):

</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="84"/>
        <source>

If saved, the script may not run properly.

Do you wish to save the script anyway?</source>
        <translation>

Si se guarda, el script puede no ejecutarse correctamente.

¿Desea guardar el script de todos modos?</translation>
    </message>
    <message numerus="yes">
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="89"/>
        <source>

... and %n more ...</source>
        <translation>
            <numerusform>

... y %n más ...</numerusform>
            <numerusform>

... y %n más ...</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="92"/>
        <source>Doomseeker - IRC Commands Problem</source>
        <translation>Doomseeker - Problema Comandos IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="117"/>
        <source>	%1 (...)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="219"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="225"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="234"/>
        <source>Invalid IRC network description</source>
        <translation>Descripción de la red IRC no válida</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="220"/>
        <source>Network description cannot be empty.</source>
        <translation>La descripción de red no puede estar vacía.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="226"/>
        <source>There already is a network with such description.</source>
        <translation>Ya hay una red con tal descripción.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.cpp" line="231"/>
        <source>Network description is invalid.

Only letters, digits, spaces and &quot;%1&quot; are allowed.</source>
        <translation>La descripción de la red no es válida.

Sólo se admiten letras, dígitos, espacios y &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="14"/>
        <source>Define IRC Network</source>
        <translation>Definir la red IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="30"/>
        <source>Description:</source>
        <translation>Descripción:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="40"/>
        <source>Address:</source>
        <translation>Dirección:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="50"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="93"/>
        <source>Server password (usually this should remain empty):</source>
        <translation>Contraseña del servidor (generalmente esto debería permanecer vacío):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="24"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="109"/>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="157"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="141"/>
        <source>Nickserv password:</source>
        <translation>Contraseña Nickserv:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="169"/>
        <source>Nickserv command:</source>
        <translation>Comando Nickserv:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="179"/>
        <source>/privmsg nickserv identify %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="199"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;%1 is substituted with nickserv password.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Usually nickserv command shouldn&apos;t be changed.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;%1 se sustituye por la contraseña Nickserv.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Por lo general, el comando Nickserv no debe modificarse.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="135"/>
        <source>Nickserv</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="221"/>
        <source>Specify channels that you wish to join by default when connecting to this network:</source>
        <translation>Especifique los canales a los que desea unirse de manera predeterminada cuando se conecte a esta red:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="234"/>
        <source>SPACEBAR or ENTER separated</source>
        <translation>BARRA ESPACIADORA o ENTER separados</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="215"/>
        <source>Channels</source>
        <translation>Canales</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="251"/>
        <source>Execute following commands on network join:</source>
        <translation>Ejecute los siguientes comandos al unirse a una red:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="274"/>
        <source>Put each command on a separate line.</source>
        <translation>Pon cada comando en una línea separada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="281"/>
        <source>Max. 512 characters per command.</source>
        <translation>Max. 512 caracteres por comando.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="245"/>
        <source>Join Script</source>
        <translation>Script al unirse</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircdefinenetworkdialog.ui" line="292"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Warning:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; passwords are stored as plain text in configuration file.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Alerta:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; las contraseñas se almacenan en el archivo de configuración como texto plano.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CFGIRCNetworks</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.h" line="49"/>
        <source>Networks</source>
        <translation>Redes</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.h" line="55"/>
        <source>IRC - Networks</source>
        <translation>IRC - Redes</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.cpp" line="189"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.cpp" line="189"/>
        <source>Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="64"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="75"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="86"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircnetworks.ui" line="99"/>
        <source>Channel/network quit message:</source>
        <translation>Mensaje al salir de canal/red:</translation>
    </message>
</context>
<context>
    <name>CFGIRCSounds</name>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.h" line="46"/>
        <source>Sounds</source>
        <translation>Sonidos</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.h" line="51"/>
        <source>IRC - Sounds</source>
        <translation>IRC - Sonidos</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="62"/>
        <source>Pick Sound File</source>
        <translation>Seleccionar archivo de sonido</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="64"/>
        <source>WAVE (*.wav)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="137"/>
        <source>No path specified.</source>
        <translation>Ninguna ruta especificada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="141"/>
        <source>File doesn&apos;t exist.</source>
        <translation>El archivo no existe.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.cpp" line="144"/>
        <source>This is not a file.</source>
        <translation>Esto no es un archivo.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="31"/>
        <source>Nickname used:</source>
        <translation>Alias usado:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="38"/>
        <source>Private message:</source>
        <translation>Mensaje privado:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="57"/>
        <source>Sound played when your nickname is used in a channel.</source>
        <translation>Sonido reproducido cuando su alias se usa en un canal.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="71"/>
        <source>Browse the sound played when your nickname is used in a channel.</source>
        <translation>Buscar el sonido a reproducir cuando seáis mencionado por tu alias en un canal.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="100"/>
        <source>Sound played when a private message is received.</source>
        <translation>Sonido reproducido cuando se recibe un mensaje privado.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="114"/>
        <source>Browse the sound played when a private message is received.</source>
        <translation>Buscar el sonido a reproducir cuando se recibe un mensaje privado.</translation>
    </message>
    <message>
        <source>Sound played when private message is received.</source>
        <translation type="vanished">Sonido reproducido cuando se recibe un mensaje privado.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgircsounds.ui" line="139"/>
        <source>&lt;b&gt;Note:&lt;/b&gt; Use sounds in .WAV format.</source>
        <translation>&lt;b&gt;Nota:&lt;/b&gt; usa sonidos en formato .WAV.</translation>
    </message>
</context>
<context>
    <name>CFGQuery</name>
    <message>
        <location filename="../gui/configuration/cfgquery.h" line="45"/>
        <source>Query</source>
        <translation>Consulta</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="20"/>
        <source>Refresh servers on startup</source>
        <translation>Actualizar servidores al arrancar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="27"/>
        <source>Refresh before launch</source>
        <translation>Actualizar antes de lanzar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="37"/>
        <source>Servers autorefresh</source>
        <translation>Autorefresco de servidores</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="48"/>
        <location filename="../gui/configuration/cfgquery.ui" line="58"/>
        <location filename="../gui/configuration/cfgquery.ui" line="71"/>
        <source>Minimum value: 30 seconds.</source>
        <translation>Valor mínimo: 30 segundos.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="51"/>
        <source>Obtain new server list every:</source>
        <translation>Obtener una nueva lista de servidores cada:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="74"/>
        <source>seconds</source>
        <translation>segundos</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="96"/>
        <source>Enabling this will prevent server list from disappearing when you are browsing through it.</source>
        <translation>Habilitar esto evitará que la lista de servidores desaparezca mientras esté navegando a través de ella.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="99"/>
        <source>Don&apos;t refresh if Doomseeker window is active.</source>
        <translation>No actualice si la ventana de Doomseeker está activa.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="109"/>
        <source>Refresh speed</source>
        <translation>Velocidad de refresco</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="117"/>
        <location filename="../gui/configuration/cfgquery.ui" line="127"/>
        <source>How many times Doomseeker will attempt to query each server before deeming it to be not responding.</source>
        <translation>Cuántas veces Doomseeker intentará consultar cada servidor antes de determinar que no responde.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="120"/>
        <source>Number of attempts per server:</source>
        <translation>Número de intentos por servidor:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="149"/>
        <location filename="../gui/configuration/cfgquery.ui" line="159"/>
        <source>Delay in miliseconds between each query attempt.</source>
        <translation>Retardo en milisegundos entre cada intento de consulta.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="152"/>
        <source>Delay between attemps (ms):</source>
        <translation>Retraso entre intentos (ms):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="181"/>
        <source>Interval between different servers (ms):</source>
        <translation>Intervalo entre diferentes servidores (ms):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="188"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sleep delay that occurs after querying a server and before querying a next one. Increasing this value may negatively affect the speed of server list refresh, but will free up CPU and decrease bandwidth usage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Retraso que sucede después de consultar un servidor y antes de consultar el siguiente. Aumentar este valor puede afectar negativamente la velocidad de actualización de la lista de servidores, pero consumirá menos CPU y reducirá el uso de ancho de banda.&lt;/P&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="211"/>
        <source>Cautious</source>
        <translation>Precavido</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="218"/>
        <source>Moderate</source>
        <translation>Moderado</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="225"/>
        <source>Aggressive</source>
        <translation>Agresivo</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="232"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Be warned: in this mode correctly working servers may appear as &amp;quot;not responding&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tenga cuidado: en este modo, los servidores que funcionan correctamente pueden mostrarse como &amp;quot;no responde&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="235"/>
        <source>Very aggressive</source>
        <translation>Muy agresivo</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgquery.ui" line="244"/>
        <source>These buttons change refresh speed settings to different values. Speed settings determine how long will it take to populate the server list and how accurate the refresh will be. Higher speed can mean lesser accuracy.

If you&apos;re experiencing problems with refreshing servers, you might prefer &quot;Cautious&quot; preset, while &quot;Aggressive&quot; presets should be preferred with good connection quality.

These values can be also modified by hand.</source>
        <translation>Estos botones cambian la configuración de velocidad de refresco a diferentes valores. La configuración de velocidad determina cuánto tiempo llevará rellenar la lista de servidores y qué tan precisa será la actualización. Una velocidad más alta puede significar una menor precisión.

Si tiene problemas con el refresco de servidores, es posible que prefiera el preajuste &quot;Cauteloso&quot;, mientras que los preajustes &quot;agresivos&quot; se recomiendan para conexiones de buena calidad.

Estos valores también se pueden modificar a mano.</translation>
    </message>
</context>
<context>
    <name>CFGServerPasswords</name>
    <message>
        <source>Server Passwords</source>
        <translation type="vanished">Contraseñas de servidores</translation>
    </message>
    <message>
        <source>Reveal</source>
        <translation type="vanished">Mostrar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="31"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="48"/>
        <source>Add password</source>
        <translation>Añadir contraseña</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="92"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="97"/>
        <source>Last Game</source>
        <translation>Último juego</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="102"/>
        <source>Last Server</source>
        <translation>Último servidor</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="107"/>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="194"/>
        <source>Last Time</source>
        <translation>Última vez</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="115"/>
        <source>Remove selected passwords</source>
        <translation>Eliminar contraseñas seleccionadas</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="141"/>
        <source>Servers the selected password was used on:</source>
        <translation>Servidores donde la contraseña seleccionada se usó:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="179"/>
        <source>Game</source>
        <translation>Juego</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="184"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="189"/>
        <source>Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="202"/>
        <source>Remove selected servers</source>
        <translation>Eliminar servidores seleccionados</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Añadir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="228"/>
        <source>Max number of servers to remember per password:</source>
        <translation>Número máximo de servidores a recordar por contraseña:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.ui" line="247"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Warning:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Some servers may be lost.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Advertencia:&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Algunos servidores pueden perderse.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgserverpasswords.h" line="48"/>
        <source>Server passwords</source>
        <translation>Contraseñas de servidores</translation>
    </message>
</context>
<context>
    <name>CFGWadAlias</name>
    <message>
        <source>WAD Aliases</source>
        <translation type="vanished">Alias de WADs</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="66"/>
        <source>Left-to-Right will use the alias files instead of the main file but not vice-versa.</source>
        <translation>Izquierda-a-Derecha utilizará los alias de archivos en lugar del archivo principal, pero no al revés.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="69"/>
        <source>All Equal will treat all files as equal and try to match them in any combination.</source>
        <translation>Todo Equivalente tratará a todos los archivos como iguales e intentará combinarlos en cualquier combinación.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="186"/>
        <source>Left-to-Right</source>
        <translation>Izquierda-a-Derecha</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.cpp" line="187"/>
        <source>All Equal</source>
        <translation>Todo Equivalente</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="32"/>
        <source>Aliases listed here will be used if WADs are not found.</source>
        <translation>Los alias mostrados aquí se usarán si no se encuentran los WADs.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="55"/>
        <source>WAD</source>
        <translation>WAD</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="60"/>
        <source>Aliases</source>
        <translation>Alias</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="65"/>
        <source>Match</source>
        <translation>Coincidencia</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="99"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="75"/>
        <source>Add defaults</source>
        <translation>Agregar predefinidos</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="110"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.ui" line="123"/>
        <source>Multiple aliases can be separated with semicolon &apos;;&apos;</source>
        <translation>Se pueden separar múltiples alias con un punto y coma &apos;;&apos;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadalias.h" line="44"/>
        <source>WAD aliases</source>
        <translation>Alias de WADs</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerAppearance</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.h" line="44"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.h" line="49"/>
        <source>Wadseeker - Appearance</source>
        <translation>Wadseeker - Apariencia</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="31"/>
        <source>Message colors:</source>
        <translation>Color mensajes:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="51"/>
        <source>Notice:</source>
        <translation>Notificación:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="61"/>
        <source>Error:</source>
        <translation>Error:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="71"/>
        <source>Critical error:</source>
        <translation>Error crítico:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekerappearance.ui" line="81"/>
        <source>Navigation:</source>
        <translation>Navegación:</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.h" line="44"/>
        <source>Wadseeker</source>
        <translation>Wadseeker</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.h" line="48"/>
        <source>Wadseeker - General</source>
        <translation>Wadseeker - General</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="131"/>
        <source>No path specified.</source>
        <translation>Ninguna ruta especificada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="134"/>
        <source>This path doesn&apos;t exist.</source>
        <translation>Esta ruta no existe.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="137"/>
        <source>This is not a directory.</source>
        <translation>Esto no es un directorio.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.cpp" line="140"/>
        <source>This directory cannot be written to.</source>
        <translation>No se puede escribir en este directorio.</translation>
    </message>
    <message>
        <source>The specified target directory for Wadseeker could not be found on the file (WAD) paths list.

Doomseeker will automatically add this path to the file search paths.</source>
        <translation type="vanished">El directorio de destino especificado para Wadseeker no se pudo encontrar en la lista de rutas de archivos (WAD).

Doomseeker agregará automáticamente esta ruta a las rutas de búsqueda de archivos.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="29"/>
        <source>Directory where Wadseeker will put WADs into:</source>
        <translation>Directorio donde Wadseeker colocará los WADs:</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Time after which Wadseeker stops connecting if no response is received.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(min: 20 seconds, max: 360 seconds)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Tiempo después del cual Wadseeker deja de conectarse si no se recibe respuesta.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(min: 20 segundos, máx: 360 segundos)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Connect timeout (seconds):</source>
        <translation type="vanished">Tiempo de espera de conexión (segundos):</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Time after which Wadseeker stops downloading if data stops coming.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(min: 60 seconds, max: 360 seconds)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Tiempo después del cual Wadseeker deja de descargar si no se recibe información.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;(min: 60 segundos, máx: 360 segundos)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Download timeout (seconds):</source>
        <translation type="vanished">Tiempo de espera de descarga (segundos):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="48"/>
        <source>Max concurrent site seeks:</source>
        <translation>Máximo número de búsquedas simultáneas en páginas:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekergeneral.ui" line="83"/>
        <source>Max concurrent WAD downloads:</source>
        <translation>Máximo número de descargas simultáneas:</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerIdgames</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.h" line="44"/>
        <source>Archives</source>
        <translation>Archivos</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.h" line="49"/>
        <source>Wadseeker - Archives</source>
        <translation>Wadseeker - Archivos</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="29"/>
        <source>Idgames</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="35"/>
        <source>Use the /idgames Archive</source>
        <translation>Usar Archivo /idgames</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="63"/>
        <source>The /idgames Archive URL (changing this is NOT recommended):</source>
        <translation>URL del Archivo /idgames (NO se recomienda cambiar esto):</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekeridgames.ui" line="75"/>
        <source>Default</source>
        <translation>Predeterminado</translation>
    </message>
    <message>
        <source>Use Wad Archive</source>
        <translation type="vanished">Utilizar Wad Archive</translation>
    </message>
</context>
<context>
    <name>CFGWadseekerSites</name>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.h" line="46"/>
        <source>Sites</source>
        <translation>Sitios</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.h" line="51"/>
        <source>Wadseeker - Sites</source>
        <translation>Wadseeker - Sitios</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="104"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wadseeker library comes with its own list of file storage sites. As these sites come and go the list is modified with new updates of the library. Leave this option to be always up-to-date with the current list. &lt;/p&gt;&lt;p&gt;When enabled the list of hardcoded default sites will always be used in addition to the list above.&lt;/p&gt;&lt;p&gt;When disabled only the list above is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;La biblioteca Wadseeker viene con su propia lista de sitios de almacenamiento de archivos. A medida que estos sitios van y vienen, la lista se modifica con nuevas actualizaciones de la biblioteca. Deje esta opción activada para estar siempre actualizado con la lista actual. &lt;/p&gt;&lt;p&gt;Cuando se habilite, la lista de sitios predeterminados siempre se usará además de la lista anterior.&lt;/p&gt;&lt;p&gt;Cuando está desactivado, solo se utiliza la lista anterior.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="107"/>
        <source>Always use Wadseeker&apos;s default sites</source>
        <translation>Siempre use los sitios predeterminados de Wadseeker</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="53"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adds sites hardcoded within the Wadseeker library to the list. No URL that is currently on the list will be removed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Agrega sitios predeterminados en la biblioteca de Wadseeker a la lista. No se eliminará ninguna URL que esté actualmente en la lista.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="56"/>
        <source>Add defaults</source>
        <translation>Agregar predefinidos</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="80"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../gui/configuration/cfgwadseekersites.ui" line="91"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
</context>
<context>
    <name>CfgChatLogsPage</name>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.h" line="46"/>
        <source>Logging</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.h" line="51"/>
        <source>IRC - Logging</source>
        <translation>IRC - Registro</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="61"/>
        <source>Browse chat logs storage directory</source>
        <translation>Examinar el directorio de almacenamiento de registros de chat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="74"/>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="81"/>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="87"/>
        <source>Directory error</source>
        <translation>Error de directorio</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="74"/>
        <source>Directory not specified.</source>
        <translation>Directorio no especificado.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="81"/>
        <source>Directory doesn&apos;t exist.</source>
        <translation>El directorio no existe.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.cpp" line="136"/>
        <source>The specified path isn&apos;t a directory.</source>
        <translation>La ruta especificada no es un directorio.</translation>
    </message>
    <message>
        <source>Specified path isn&apos;t a directory.</source>
        <translation type="vanished">La ruta especificada no es un directorio.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="32"/>
        <source>Store chat logs</source>
        <translation>Almacenar registros de chat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="39"/>
        <source>Restore logs when re-entering chat</source>
        <translation>Restaurar registros cuando se vuelve a ingresar al chat</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="59"/>
        <source>Remove old log archives</source>
        <translation>Eliminar archivos de registro antiguos</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="68"/>
        <source>Remove all older than:</source>
        <translation>Eliminar todo anterior a:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="81"/>
        <source> days</source>
        <translation> días</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="126"/>
        <source>Logs storage directory:</source>
        <translation>Directorio de almacenamiento de registros:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="151"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/cfgchatlogspage.ui" line="166"/>
        <source>Explore</source>
        <translation>Explorar</translation>
    </message>
</context>
<context>
    <name>ChatLogs</name>
    <message>
        <source>Won&apos;t transfer chat logs from &quot;%1&quot; to &quot;%2&quot; as directory &quot;%2&quot;already exists.</source>
        <translation type="vanished">No transferirá los registros de chat de &quot;%1&quot; a &quot;%2&quot; ya que el directorio &quot;%2&quot; ya existe.</translation>
    </message>
    <message>
        <location filename="../irc/chatlogs.cpp" line="90"/>
        <source>Won&apos;t transfer chat logs from &quot;%1&quot; to &quot;%2&quot; as directory &quot;%2&quot; already exists.</source>
        <translation>No se transferirá los registros de chat de &quot;%1&quot; a &quot;%2&quot; ya que el directorio &quot;%2&quot; ya existe.</translation>
    </message>
    <message>
        <location filename="../irc/chatlogs.cpp" line="94"/>
        <source>Failed to transfer chat from &quot;%1&quot; to &quot;%2&quot;</source>
        <translation>Error al transferir el chat de &quot;%1&quot; a &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../irc/chatlogs.cpp" line="98"/>
        <source>Chat logs transfer</source>
        <translation>Transferencia de registros de chat</translation>
    </message>
</context>
<context>
    <name>CheckWadsDlg</name>
    <message>
        <source>Doomseeker - Checking wads</source>
        <translation type="vanished">Doomseeker - Comprobando WADs</translation>
    </message>
    <message>
        <source>Checking the location and integrity of your wads</source>
        <translation type="vanished">Comprobando la ubicación y la integridad de los WADs</translation>
    </message>
    <message>
        <location filename="../gui/checkwadsdlg.ui" line="17"/>
        <source>Doomseeker - Checking WADs</source>
        <translation>Doomseeker - Comprobando WADs</translation>
    </message>
    <message>
        <location filename="../gui/checkwadsdlg.ui" line="32"/>
        <source>Checking the location and integrity of your WADs</source>
        <translation>Comprobando la ubicación y la integridad de los WADs</translation>
    </message>
</context>
<context>
    <name>CmdArgsHelp</name>
    <message>
        <source>--connect protocol://ip[:port]
    Attempts to connect to the specified server.
</source>
        <translation type="vanished">--connect protocol://ip[:port]
    Intenta conectarse al servidor especificado.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="43"/>
        <source>--create-game
    Launches Doomseeker in &quot;Create Game&quot; mode.
</source>
        <translation>--create-game
    Arranca Doomseeker en modo &quot;Crear Juego&quot;.
</translation>
    </message>
    <message>
        <source>--datadir [directory]
    Sets an explicit search location for
    IP2C data along with plugins.
    Can be specified multiple times.
</source>
        <translation type="vanished">--datadir [directory]
    Establece una ubicación de búsqueda explícita
    para datos IP2C junto con complementos.
    Se puede especificar varias veces.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="40"/>
        <source>--connect &lt;protocol://ip[:port]&gt;
    Attempts to connect to the specified server.
</source>
        <translation>--connect &lt;protocol://ip[:port]&gt;
    Intenta conectarse al servidor especificado.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="46"/>
        <source>--datadir &lt;directory&gt;
    Sets an explicit search location for
    IP2C data along with plugins.
    Can be specified multiple times.
</source>
        <translation>--datadir &lt;directory&gt;
    Establece una ubicación de búsqueda explícita
    para datos IP2C junto con complementos.
    Se puede especificar varias veces.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="62"/>
        <source>--help
    Prints this list of command line arguments.
</source>
        <translation>--help
    muestra esta lista de argumentos de línea de
    comandos.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="65"/>
        <source>--rcon [plugin] [ip]
    Launch the rcon client for the specified ip.
</source>
        <translation>--rcon [plugin] [ip]
    Inicia el cliente rcon para la ip especificada.
</translation>
    </message>
    <message>
        <source>--portable
    Starts application in portable mode.
    In portable mode Doomseeker saves all configuration files
    to the directory where its executable resides.
    Normally, configuration is saved to user&apos;s home directory.
</source>
        <translation type="vanished">--portable
    Inicia la aplicación en modo portátil.
    En modo portátil, Doomseeker guarda todos los archivos de
    configuración en el directorio donde reside su ejecutable.
    Normalmente, la configuración se guarda en el directorio
    de inicio del usuario.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="35"/>
        <source>--basedir &lt;directory&gt;
    Changes the default storage directory to the specified
    one (also in --portable mode). Doomseeker stores all of its
    settings, cache and managed files in this directory.
</source>
        <translation>--basedir &lt;directory&gt;
    Cambia la ruta por defecto de almazenaje a la especificada
    (incluso en modo --portable). Doomseeker guarda toda la 
    configuración, caché, y archivos gestionados en este directorio.
</translation>
    </message>
    <message>
        <source>--portable
    Starts application in a portable mode.
    In the portable mode Doomseeker saves all the configuration
    files to the directory where its executable resides.
    Normally, configuration is saved to user&apos;s home directory.
    See also: --basedir.
</source>
        <translation type="vanished">--portable
    Inicia la aplicación en modo portátil.
    En modo portátil, Doomseeker guarda todos los archivos de
    configuración en el directorio donde reside su ejecutable.
    Normalmente, la configuración se guarda en el directorio
    de inicio del usuario. Mire también: --basedir.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="28"/>
        <source>being assigned a different letter</source>
        <translation>siendo asignado a una letra diferente</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="31"/>
        <source>being mounted on a different path</source>
        <translation>siendo montado a una ruta diferente</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="51"/>
        <source>--disable-plugin &lt;plugin&gt;
    Ban a &lt;plugin&gt; from loading even if it is normally loaded.
    Specify the plugin name without the &apos;lib&apos; prefix or the file
    extension, for ex. &apos;vavoom&apos;, not &apos;libvavoom%1&apos;.
</source>
        <translation>--disable-plugin &lt;plugin&gt;
    Prohibe que &lt;plugin&gt; sea cargado incluso si normalmente
    se habría cargado. Especifique el nombre del plugin sin el
    prefijo &quot;lib&quot; o su extensión, por ejemploe &apos;vavoom&apos;, no 
    &apos;libvavoom%1&apos;.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="56"/>
        <source>--enable-plugin &lt;plugin&gt;
    Load a &lt;plugin&gt; even if it is normally disabled/banned.
    Specify the plugin name without the &apos;lib&apos; prefix or the file
    extension, for ex. &apos;vavoom&apos;, not &apos;libvavoom%1&apos;.
</source>
        <translation>--enable-plugin &lt;plugin&gt;
    Carga un &lt;plugin&gt; incluso si normalmente está desactivado
    Especifique el nombre del plugin sin el prefijo &quot;lib&quot; o su
    extensión, por ejemploe &apos;vavoom&apos;, no &apos;libvavoom%1&apos;.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="68"/>
        <source>--portable
    Starts the application in the portable mode:
    - Doomseeker saves all the configuration files to the directory
      where its executable resides. Normally, configuration is saved to
      user&apos;s home directory. This directory can be changed with --basedir.
    - The current working directory is forced to the directory where
      Doomseeker&apos;s executable resides.
    - Doomseeker will save in the configuration all paths as relative
      in anticipation that the absolute paths may change between the runs,
      for ex. due to the portable device %1.
</source>
        <translation>--portable
    Inicia la aplicación en modo portátil:
    - Doomseeker guarda toda la configuración en el directorio 
      donde se encuentra el ejecutable. Normalmente, la 
      configuración se guarda en el directorio de inicio del usuario.
      Este directorio se puede cambiar con --basedir.
    - El directorio de trabajo se fuerza al directorio donde se 
      encuentra el ejecutable.
    - Doomseeker guardará todas las rutas de forma relativa,
      en anticipación de que las rutas globales cambien entre 
      ejecuciones, debido a, por ejemplo, un pendrive
      %1.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="79"/>
        <source>--version-json [file|-]
    Prints version info on Doomseeker and all
    plugins in JSON format to specified file,
    then closes the program. If file is not
    specified or specified as &apos;-&apos;, version info
    is printed to stdout.
</source>
        <translation>--version-json [file|-]
    Imprime la información de la versión de
    Doomseeker y todos los complementos en
    formato JSON en el archivo especificado, luego
    cierra el programa. Si el archivo no se especifica
    o se especifica como &apos;-&apos;, la información de la
    versión se imprime en stdout.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="86"/>
        <source>--verbose
    Forces verbose logging to stderr.
    This is the default in most cases.
</source>
        <translation>--verbose
    Fuerza el registro detallado a stderr.
    Este es el predeterminado en la mayoría
    de los casos.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="90"/>
        <source>--quiet
    Forces no logging to stderr.
    This is the default when dumping versions.
</source>
        <translation>--quiet
    Prohíbe el registro a stderr.
    Este es el predeterminado en versiones
    lanzadas.
</translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="99"/>
        <source>Available command line parameters:
</source>
        <translation>Parámetros de la línea de comandos disponibles:
</translation>
    </message>
    <message numerus="yes">
        <location filename="../cmdargshelp.cpp" line="106"/>
        <source>doomseeker: expected %n argument(s) in option %1

</source>
        <translation>
            <numerusform>doomseeker: se esperaba %n argumento en la opción %1

</numerusform>
            <numerusform>doomseeker: se esperaban %n argumentos en la opción %1

</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../cmdargshelp.cpp" line="113"/>
        <source>doomseeker: unrecognized option &apos;%1&apos;

</source>
        <translation>doomseeker: opción no reconocida &apos;%1&apos;

</translation>
    </message>
</context>
<context>
    <name>ConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/configurationdialog.ui" line="14"/>
        <source>Doomseeker - Configuration</source>
        <translation>Doomseeker - Configuración</translation>
    </message>
</context>
<context>
    <name>ConnectionHandler</name>
    <message>
        <location filename="../connectionhandler.cpp" line="83"/>
        <location filename="../connectionhandler.cpp" line="87"/>
        <source>Doomseeker - join server</source>
        <translation>Doomseeker - unirse al servidor</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="84"/>
        <source>Connection to server timed out.</source>
        <translation>Se ha excedido el límite de tiempo de conexión al servidor.</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="88"/>
        <source>An error occured while trying to connect to server.</source>
        <translation>Se produjo un error al conectarse al servidor.</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="165"/>
        <source>Doomseeker - join game</source>
        <translation>Doomseeker - unirse al juego</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="177"/>
        <source>Error while launching game &quot;%2&quot; for server &quot;%1&quot;: %3</source>
        <translation>Error al lanzar el juego &quot;%2&quot; para el servidor &quot;%1&quot;: %3</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="179"/>
        <source>Doomseeker - launch game</source>
        <translation>Doomseeker - ejecutar juego</translation>
    </message>
    <message>
        <source>Error while launching executable for server &quot;%1&quot;, game &quot;%2&quot;: %3</source>
        <translation type="vanished">Error al ejecutar ejecutable para el servidor &quot;%1&quot;, juego &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <source>Doomseeker - launch executable</source>
        <translation type="vanished">Doomseeker - ejecutar ejecutable</translation>
    </message>
</context>
<context>
    <name>CopyTextDlg</name>
    <message>
        <location filename="../gui/copytextdlg.ui" line="13"/>
        <source>Doomseeker - Copy Text</source>
        <translation>Doomseeker - Copiar texto</translation>
    </message>
    <message>
        <location filename="../gui/copytextdlg.ui" line="19"/>
        <source>Text to copy:</source>
        <translation>Texto a copiar:</translation>
    </message>
    <message>
        <location filename="../gui/copytextdlg.ui" line="31"/>
        <source>Copy to clipboard</source>
        <translation>Copiar al portapapeles</translation>
    </message>
</context>
<context>
    <name>CreateServerDialog</name>
    <message>
        <source>Doomseeker - load server config</source>
        <translation type="vanished">Doomseeker - cargar la configuración del servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="504"/>
        <location filename="../gui/createserverdialog.cpp" line="518"/>
        <source>Config files (*.ini)</source>
        <translation>Archivos de configuración (*.ini)</translation>
    </message>
    <message>
        <source>Doomseeker - save server config</source>
        <translation type="vanished">Doomseeker - guardar la configuración del servidor</translation>
    </message>
    <message>
        <source>Unable to save server configuration!</source>
        <translation type="vanished">¡No se puede guardar la configuración del servidor!</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="153"/>
        <location filename="../gui/createserverdialog.cpp" line="389"/>
        <source>Doomseeker - create game</source>
        <translation>Doomseeker - crear juego</translation>
    </message>
    <message>
        <source>No engine selected</source>
        <translation type="vanished">Ningún motor seleccionado</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="504"/>
        <source>Doomseeker - load game setup config</source>
        <translation>Doomseeker - carga la configuración del juego</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="518"/>
        <location filename="../gui/createserverdialog.cpp" line="528"/>
        <source>Doomseeker - save game setup config</source>
        <translation>Doomseeker - guarda la configuración del juego</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="528"/>
        <source>Unable to save game setup configuration!</source>
        <translation>No se puede guardar la configuración del juego!</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="156"/>
        <location filename="../gui/createserverdialog.cpp" line="392"/>
        <source>No game selected</source>
        <translation>Ningún juego seleccionado</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="114"/>
        <source>Doomseeker - Play Demo</source>
        <translation>Doomseeker - Reproducir Demo</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="115"/>
        <source>Play demo</source>
        <translation>Reproducir Demo</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="119"/>
        <source>Host server</source>
        <translation>Hostear servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="123"/>
        <source>Play</source>
        <translation>Jugar</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="126"/>
        <source>Doomseeker - Setup Remote Game</source>
        <translation>Doomseeker - Configurar juego remoto</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="118"/>
        <source>Doomseeker - Host Online Game</source>
        <translation>Doomseeker - Hostear juego en línea</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="122"/>
        <source>Doomseeker - Play Offline Game</source>
        <translation>Doomseeker - Jugar fuera de línea</translation>
    </message>
    <message>
        <source>Doomseeker - [Unhandled Host Mode]</source>
        <translation type="vanished">Doomseeker - [Modo desconocido]</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="173"/>
        <source>Doomseeker - error</source>
        <translation>Doomseeker - error</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="69"/>
        <location filename="../gui/createserverdialog.cpp" line="254"/>
        <source>Flags</source>
        <translatorcomment>There&apos;s no good translation for the meaning of &quot;flag&quot; in this context. &quot;Indicadores&quot; is close enough.</translatorcomment>
        <translation>Indicadores</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="127"/>
        <source>Play online</source>
        <translation>Jugar en línea</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="130"/>
        <source>Doomseeker - Create Game</source>
        <translation>Doomseeker - Crear Juego</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="131"/>
        <source>Create game</source>
        <translation>Crear juego</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="449"/>
        <source>&amp;Mode</source>
        <translation>&amp;Modo</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="450"/>
        <source>&amp;Host server</source>
        <translation>&amp;Hostear servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="453"/>
        <source>&amp;Play offline</source>
        <translation>&amp;Jugar fuera de línia</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="457"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="459"/>
        <source>&amp;Load game configuration</source>
        <translation>&amp;Cargar la configuración del juego</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="464"/>
        <source>&amp;Save game configuration</source>
        <translation>&amp;Guargar la configuración del juego</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="470"/>
        <source>&amp;Program settings</source>
        <translation>Configuración del &amp;Programa</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="494"/>
        <source>Run game command line:</source>
        <translation>Comando para ejecutar juego:</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.cpp" line="495"/>
        <source>Host server command line:</source>
        <translation>Comando para ejecutar el servidor:</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="14"/>
        <source>Create Game</source>
        <translation>Crear juego</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="39"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="49"/>
        <source>Rules</source>
        <translation>Reglas</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="59"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="135"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="161"/>
        <source>Command line</source>
        <translation>Línea de comandos</translation>
    </message>
    <message>
        <source>Misc.</source>
        <translation type="vanished">Diverso.</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="79"/>
        <source>Custom parameters</source>
        <translation>Parámetros personalizados</translation>
    </message>
    <message>
        <source>Configure</source>
        <translation type="vanished">Configurar</translation>
    </message>
    <message>
        <source>Load previously saved server configuration.</source>
        <translation type="vanished">Cargar la configuración del servidor previamente guardada.</translation>
    </message>
    <message>
        <source>Load</source>
        <translation type="vanished">Cargar</translation>
    </message>
    <message>
        <source>Save current server configuration.</source>
        <translation type="vanished">Guardar la configuración actual del servidor.</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Guardar</translation>
    </message>
    <message>
        <location filename="../gui/createserverdialog.ui" line="158"/>
        <source>Obtain command line required to launch this game.</source>
        <translation>Obtener la línea de comandos requerida para iniciar este juego.</translation>
    </message>
    <message>
        <source>Offline command line</source>
        <translation type="vanished">Comando para el modo desconectado</translation>
    </message>
    <message>
        <source>Play offline</source>
        <translation type="vanished">Jugar fuera de línea</translation>
    </message>
    <message>
        <source>Host command line</source>
        <translation type="vanished">Comando para el servidor</translation>
    </message>
    <message>
        <source>Obtain command line required to launch this server.</source>
        <translation type="vanished">Obtener la línea de comandos requerida para iniciar este servidor.</translation>
    </message>
    <message>
        <source>Command Line</source>
        <translation type="vanished">Línea de comandos</translation>
    </message>
    <message>
        <source>Start server</source>
        <translation type="vanished">Iniciar el servidor</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Cerrar</translation>
    </message>
</context>
<context>
    <name>CustomParamsPanel</name>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="23"/>
        <source>Custom parameters (ENTER or SPACEBAR separated):</source>
        <translation>Parámetros personalizados (ENTER o ESPACIO separados):</translation>
    </message>
    <message>
        <location filename="../gui/createserver/customparamspanel.ui" line="33"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Note: these are added to the command line as visible on the list.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Console commands usually begin with &apos;+&apos; character. For example: +sv_cheats 1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Nota: estos parámetros se agregan a la lista exactamente como los puede ver.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Los comandos de la consola generalmente comienzan con el carácter &apos;+&apos;. Por ejemplo: + sv_cheats 1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomServers</name>
    <message>
        <location filename="../customservers.cpp" line="163"/>
        <source>Unknown game for custom server %1:%2</source>
        <translation>Juego desconocido para el servidor personalizado %1:%2</translation>
    </message>
    <message>
        <location filename="../customservers.cpp" line="175"/>
        <source>Failed to resolve address for custom server %1:%2</source>
        <translation>Error al resolver la dirección del servidor personalizado %1:%2</translation>
    </message>
    <message>
        <location filename="../customservers.cpp" line="186"/>
        <source>Plugin returned nullptr &quot;Server*&quot; for custom server %1:%2. This is a problem with the plugin.</source>
        <translation>El complemento devolvió nullptr &quot;Server*&quot; para el servidor personalizado %1:%2. Esto es un problema con el complemento.</translation>
    </message>
    <message>
        <source>Plugin returned NULL &quot;Server*&quot; for custom server %1:%2. This is a problem with the plugin.</source>
        <translation type="vanished">El complemento devolvió NULL &quot;Server*&quot; para el servidor personalizado %1:%2. Esto es un problema con el complemento.</translation>
    </message>
</context>
<context>
    <name>DefaultDifficultyProvider</name>
    <message>
        <location filename="../plugins/enginedefaults.h" line="41"/>
        <source>1 - I&apos;m too young to die</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="42"/>
        <source>2 - Hey, not too rough</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="43"/>
        <source>3 - Hurt me plenty</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="44"/>
        <source>4 - Ultra-violence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/enginedefaults.h" line="45"/>
        <source>5 - NIGHTMARE!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DemoManagerDlg</name>
    <message>
        <source>Unable to delete</source>
        <translation type="vanished">No se puede borrar</translation>
    </message>
    <message>
        <source>Could not delete the selected demo.</source>
        <translation type="vanished">No se puede borrar la demo seleccionada.</translation>
    </message>
    <message>
        <source>Delete demo?</source>
        <translation type="vanished">Borrar demo?</translation>
    </message>
    <message>
        <source>Are you sure you want to delete the selected demo?</source>
        <translation type="vanished">¿Seguro que quieres eliminar la demo seleccionada?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="229"/>
        <source>Export plain demo file</source>
        <translation>Exporta archivo &amp;quot;demo&amp;quot; sencillo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="231"/>
        <source>Export with Doomseeker metadata</source>
        <translation>Exporta con metadatos de Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="273"/>
        <source>Unable to remove</source>
        <translation>Incapaz de eliminar</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="273"/>
        <source>Could not remove the selected demo.</source>
        <translation>No se pudo borrar la demo seleccionada.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="284"/>
        <source>Remove demo?</source>
        <translation>Eliminar demo?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="285"/>
        <source>Are you sure you want to remove the selected demo?</source>
        <translation>¿Seguro que quieres eliminar la demo seleccionada?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="329"/>
        <source>Demo export</source>
        <translation>Exportar Demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="330"/>
        <source>The exported files already exist at the specified directory. Overwrite?

%1</source>
        <translation>Los archivos exportados ya existen en el directorio especificado. ¿Sobreescribir?

%1</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="361"/>
        <location filename="../gui/demomanager.cpp" line="384"/>
        <source>Unable to save</source>
        <translation>Error de escritura</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="361"/>
        <location filename="../gui/demomanager.cpp" line="385"/>
        <source>Could not write to the specified location.</source>
        <translation>No se pudo escribir en la ubicación especificada.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="395"/>
        <source>Import demo</source>
        <translation>Importar Demo</translation>
    </message>
    <message>
        <source>Demos (%1)</source>
        <translation type="vanished">Archivos demo (%1)</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="407"/>
        <source>Import game demo</source>
        <translation>Importar juego demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="426"/>
        <source>It looks like this demo is already imported. Overwrite anyway?</source>
        <translation>Parece que esa demo ya está importada. ¿Sobreescribir?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="430"/>
        <source>Another demo with this metadata already exists. Overwrite?</source>
        <translation>Ya existe otra demo con estos metadatos. ¿Sobreescribir?</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="432"/>
        <source>Existing: %1
New: %2
</source>
        <translation>Existente: %1
Nuevo: %2
</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="438"/>
        <location filename="../gui/demomanager.cpp" line="448"/>
        <source>Demo import</source>
        <translation>Importar Demo</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="439"/>
        <source>%1

%2</source>
        <translation>%1

%2</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="449"/>
        <source>Could not import the selected demo.</source>
        <translation>No se pudo importar la demo seleccionada.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="462"/>
        <source>%1 %L2 B (MD5: %3)</source>
        <translation>%1 %L2 B (MD5: %3)</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="490"/>
        <source>No plugin</source>
        <translation>Sin complementos</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="491"/>
        <source>The &quot;%1&quot; plugin does not appear to be loaded.</source>
        <translation>El complemento &quot;%1&quot; no parece estar cargado.</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="526"/>
        <source>Files not found</source>
        <translation>Archivos no encontrados</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="527"/>
        <source>The following files could not be located: </source>
        <translation>Los siguientes archivos no pudieron ser localizados: </translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="574"/>
        <source>Game</source>
        <translation>Juego</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="580"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="582"/>
        <source>WADs / mods</source>
        <translation>WADs / mods</translation>
    </message>
    <message>
        <source>Doomseeker - error</source>
        <translation type="vanished">Doomseeker - error</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">Puerto</translation>
    </message>
    <message>
        <source>WADs</source>
        <translation type="vanished">WADs</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="17"/>
        <source>Demo Manager</source>
        <translation>Administrador de demos</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="134"/>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="161"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="185"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.ui" line="192"/>
        <source>Export ...</source>
        <translation>Exportar...</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Eliminar</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">Exportar</translation>
    </message>
</context>
<context>
    <name>DemoMetaDataDialog</name>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="14"/>
        <source>Game Demo</source>
        <translation>Demo del juego</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="22"/>
        <source>Game:</source>
        <translation>Juego:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="29"/>
        <source>Created time:</source>
        <translation>Tiempo de creación:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="49"/>
        <source>yyyy-MM-dd HH:mm:ss</source>
        <translation>yyyy-MM-dd HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="59"/>
        <source>IWAD:</source>
        <translation>IWAD:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="69"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="76"/>
        <source>Game version:</source>
        <translation>Versión del juego:</translation>
    </message>
    <message>
        <location filename="../gui/demometadatadialog.ui" line="88"/>
        <source>WADs and other game files:</source>
        <translation>WADs y otros archivos del juego:</translation>
    </message>
</context>
<context>
    <name>DemoModel</name>
    <message>
        <location filename="../gui/demomanager.cpp" line="84"/>
        <source>Created Time</source>
        <translation>Tiempo de creación</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="84"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="84"/>
        <source>IWAD</source>
        <translation>IWAD</translation>
    </message>
    <message>
        <location filename="../gui/demomanager.cpp" line="84"/>
        <source>WADs</source>
        <translation>WADs</translation>
    </message>
</context>
<context>
    <name>DockBuddiesList</name>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="84"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="85"/>
        <source>Buddy</source>
        <translation>Compañero</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="86"/>
        <source>Location</source>
        <translation>Ubicación</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="136"/>
        <source>Remove Buddy</source>
        <translation>Eliminar Compañero</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.cpp" line="136"/>
        <source>Are you sure you want to remove this pattern?</source>
        <translation>¿Seguro que quieres eliminar este patrón?</translation>
    </message>
    <message>
        <source>Delete Buddy</source>
        <translation type="vanished">Eliminar Compañero</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this pattern?</source>
        <translation type="vanished">¿Seguro que quieres eliminar este patrón?</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="149"/>
        <location filename="../gui/dockBuddiesList.cpp" line="198"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="20"/>
        <source>Buddies</source>
        <translation>Compañeros</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="51"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="103"/>
        <source>Manage</source>
        <translation>Gestionar</translation>
    </message>
    <message>
        <location filename="../gui/dockBuddiesList.ui" line="160"/>
        <location filename="../gui/dockBuddiesList.cpp" line="204"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
</context>
<context>
    <name>DoomseekerConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/doomseekerconfigurationdialog.cpp" line="112"/>
        <source>Settings saved!</source>
        <translation>¡Ajustes guardados!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/doomseekerconfigurationdialog.cpp" line="114"/>
        <source>Settings save failed!</source>
        <translation>¡Guardado de ajustes ha fallado!</translation>
    </message>
    <message>
        <source>Games</source>
        <translation type="vanished">Juegos</translation>
    </message>
    <message>
        <source>Engines</source>
        <translation type="vanished">Motores</translation>
    </message>
</context>
<context>
    <name>EngineConfigPage</name>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="165"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - buscar ejecutable</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="267"/>
        <source>Failed to automatically find file.
You may need to use the browse button.</source>
        <translation>Error al buscar el archivo automáticamente
Es posible que necesite usar el botón Buscar.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.cpp" line="272"/>
        <source>Game - %1</source>
        <translation>Juego - %1</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="65"/>
        <source>Custom parameters:</source>
        <translation>Parámetros personalizados:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="90"/>
        <source>Add these parameters to the saved list.</source>
        <translation>Añadir estos parámetros a la lista guardada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="101"/>
        <source>Remove these parameters from the saved list.</source>
        <translation>Eliminar estos parámetros a la lista guardada.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="129"/>
        <source>Masterserver address:</source>
        <translation>Dirección de Masterserver:</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="141"/>
        <source>Restore the master server address to the default.</source>
        <translation>Restablecer la dirección maestra a la por defecto.</translation>
    </message>
    <message>
        <source>Restore the master server address to default.</source>
        <translation type="vanished">Restablecer la dirección maestra a la por defecto.</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="144"/>
        <source>Default</source>
        <translation>Predeterminado</translation>
    </message>
    <message>
        <location filename="../gui/configuration/engineconfigpage.ui" line="160"/>
        <source>[error]</source>
        <translation>[error]</translation>
    </message>
</context>
<context>
    <name>EnginePluginComboBox</name>
    <message>
        <location filename="../gui/widgets/engineplugincombobox.cpp" line="81"/>
        <source>%1 (unknown)</source>
        <translation>%1 (desconocido)</translation>
    </message>
</context>
<context>
    <name>ExeFile</name>
    <message>
        <source>No %1 executable specified for %2</source>
        <translation type="vanished">No se ha especificado %1 ejecutable para %2</translation>
    </message>
    <message>
        <source>Executable for %1 %2:
%3
is a directory or doesn&apos;t exist.</source>
        <translation type="vanished">Ejecutable para %1 %2:
%3
es un directorio o no existe.</translation>
    </message>
    <message>
        <source>No %1 executable configured for %2</source>
        <translation type="vanished">No se ha configurado %1 ejecutable para %2</translation>
    </message>
    <message>
        <source>The executable path for %1 %2:
%3
is a directory or doesn&apos;t exist.</source>
        <translation type="vanished">El ejecutable para %1 %2:
%3
es un directorio o no existe.</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="79"/>
        <source>The %1 executable is not set for %2.</source>
        <translation>El ejecutable %1 para %2 no está especificado.</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="87"/>
        <source>The %1 executable for %2 doesn&apos;t exist:
%3</source>
        <translation>El ejecutable %1 para %2 no existe:
%3</translation>
    </message>
    <message>
        <location filename="../serverapi/exefile.cpp" line="97"/>
        <source>The path to %1 executable for %2 is a directory:
%3</source>
        <translation>La ruta al ejecutable %1 para %2 es un directorio:
%3</translation>
    </message>
</context>
<context>
    <name>FileFilter</name>
    <message>
        <source>All files(*)</source>
        <translation type="vanished">Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../filefilter.cpp" line="29"/>
        <source>All files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../filefilter.cpp" line="35"/>
        <location filename="../filefilter.cpp" line="43"/>
        <source>Demos (%1)</source>
        <translation>Demos (%1)</translation>
    </message>
    <message>
        <location filename="../filefilter.cpp" line="49"/>
        <source>Executables (*.exe *.bat *.com)</source>
        <translation>Archivos ejecutables (*.exe *.bat *.com)</translation>
    </message>
</context>
<context>
    <name>FilePickWidget</name>
    <message>
        <source>Doomseeker - choose executable file</source>
        <translation type="vanished">Doomseeker - elija el archivo ejecutable</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="113"/>
        <source>Path to %1 executable:</source>
        <translation>Ruta al ejecutable %1:</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="174"/>
        <source>File doesn&apos;t exist.</source>
        <translation>El archivo no existe.</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.cpp" line="177"/>
        <source>This is a directory.</source>
        <translation>Esto es un directorio.</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="32"/>
        <source>Path to file:</source>
        <translation>Ruta al archivo:</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="54"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/filepickwidget.ui" line="67"/>
        <source>Find</source>
        <translation>Buscar</translation>
    </message>
</context>
<context>
    <name>FileUtilsTr</name>
    <message>
        <location filename="../fileutils.cpp" line="38"/>
        <source>cannot create directory</source>
        <translation>no se puede crear el directorio</translation>
    </message>
    <message>
        <location filename="../fileutils.cpp" line="43"/>
        <source>the parent path is not a directory: %1</source>
        <translation>la ruta padre no es un directorio: %1</translation>
    </message>
    <message>
        <location filename="../fileutils.cpp" line="48"/>
        <source>lack of necessary permissions to the parent directory: %1</source>
        <translation>falta de permisos necesarios para el directorio padre: %1</translation>
    </message>
</context>
<context>
    <name>FreedoomDialog</name>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="96"/>
        <source>Install Freedoom</source>
        <translation>Instalar Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="97"/>
        <source>Select at least one file.</source>
        <translation>Seleccione al menos un archivo.</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="101"/>
        <source>Downloading &amp; installing ...</source>
        <translation>Descargando e instalando ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="128"/>
        <source>Downloading Freedoom version info ...</source>
        <translation>Descargando la información de la versión de Freedoom ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="134"/>
        <source>Working ...</source>
        <translation>Trabajando...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="145"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="174"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="179"/>
        <source>Missing</source>
        <translation>Faltante</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="188"/>
        <source>Different</source>
        <translation>Diferente</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="206"/>
        <source>&lt;p&gt;File: %1&lt;br&gt;Version: %2&lt;br&gt;Description: %3&lt;br&gt;Location: %4&lt;/p&gt;</source>
        <translation>&lt;p&gt;Archivo: %1&lt;br&gt;Versión: %2&lt;br&gt;Descripción: %3&lt;br&gt;Ubicación: %4&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="283"/>
        <source>%1 %p%</source>
        <translation>%1 %p%</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="14"/>
        <source>Freedoom</source>
        <translation>Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="30"/>
        <source>Freedoom is a free-of-charge data set replacement for commercial Doom games. It will allow you to play on most servers that host modded content without having to purchase Doom games. It won&apos;t allow you to join unmodded servers.</source>
        <translation>Freedoom es un reemplazo de datos gratuito para el Doom comercial. Te permitirá jugar en la mayoría de los servidores que alojan contenido modificado sin la necesidad de comprar juegos de la serie Doom. No le permitirá unirse a servidores no modificados.</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="40"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Webpage: &lt;a href=&quot;https://freedoom.github.io&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://freedoom.github.io&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Página web: &lt;a href=&quot;https://freedoom.github.io&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://freedoom.github.io&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="66"/>
        <source>Install path:</source>
        <translation>Ruta de instalación:</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="108"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="113"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="118"/>
        <source>Install?</source>
        <translation>¿Instalar?</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.ui" line="169"/>
        <source>Work in progress ...</source>
        <translation>Trabajo en progreso ...</translation>
    </message>
    <message>
        <location filename="../gui/freedoomdialog.cpp" line="75"/>
        <source>Install</source>
        <translation>Instalar</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Reintentar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Cerrar</translation>
    </message>
</context>
<context>
    <name>GameClientRunner</name>
    <message>
        <source>Path to working directory for game &quot;%1&quot; is empty.

Make sure the configuration for the client executable is set properly.</source>
        <translation type="vanished">La ruta al directorio de trabajo para el juego &quot;%1&quot; está vacía.

Asegúrese de que la configuración del ejecutable del cliente esté configurada correctamente.</translation>
    </message>
    <message>
        <source>%1

This directory cannot be used as working directory for game: %2

Executable: %3</source>
        <translation type="vanished">%1

Este directorio no se puede usar como directorio de trabajo para el juego: %2

Ejecutable: %3</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="217"/>
        <source>Cannot determine the working directory for &quot;%1&quot;. Check the game configuration.</source>
        <translation>No se pudo determinar el directorio de trabajo para &quot;%1&quot;. Compruebe la configuración del juego.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="225"/>
        <source>This path cannot be used as the working directory for %1:
%2.</source>
        <translation>Esta ruta no se puede usar como directorio de trabajo para %1:

%2.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="242"/>
        <source>BUG: Plugin doesn&apos;t specify argument for in-game password, but the server requires such password.</source>
        <translation>BUG: El complemento no especifica el argumento para la contraseña dentro del juego, pero el servidor requiere dicha contraseña.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="294"/>
        <source>BUG: Plugin doesn&apos;t specify argument for connect password, but the server is passworded.</source>
        <translation>BUG: El complemento no especifica el argumento para la contraseña de conexión, pero el servidor requiere tal contraseña.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="496"/>
        <source>The game can be installed by Doomseeker.</source>
        <translation>El juego puede ser instalado por Doomseeker.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameclientrunner.cpp" line="508"/>
        <source>Couldn&apos;t find game %1.</source>
        <translation>El juego %1 no se puede encontrar.</translation>
    </message>
    <message>
        <source>Game %1 cannot be found.</source>
        <translation type="vanished">El juego %1 no se puede encontrar.</translation>
    </message>
    <message>
        <source>Client binary cannot be obtained for %1, please check the location given in the configuration.</source>
        <translation type="vanished">El ejecutable del cliente no se puede obtener para %1, verifique la ubicación dada en la configuración.</translation>
    </message>
</context>
<context>
    <name>GameConfigErrorBox</name>
    <message>
        <location filename="../gui/configuration/gameconfigerrorbox.cpp" line="41"/>
        <source>Configure &amp;game</source>
        <translation>Configurar &amp;juego</translation>
    </message>
</context>
<context>
    <name>GameDemoTr</name>
    <message>
        <location filename="../gamedemo.cpp" line="53"/>
        <source>Doomseeker - Record Demo</source>
        <translation>Doomseeker - Grabar Demo</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="61"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="63"/>
        <source>The demo storage directory doesn&apos;t exist and cannot be created!%1

%2</source>
        <translation>La ruta donde guardar las demos no existe y no se puede crear! %1

%2</translation>
    </message>
    <message>
        <location filename="../gamedemo.cpp" line="69"/>
        <source>The demo storage directory exists but lacks the necessary permissions!

%1</source>
        <translation>La ruta donde guardar las demos existe, pero no tiene suficientes permisos!

%1</translation>
    </message>
</context>
<context>
    <name>GameExeFactory</name>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="72"/>
        <source>game</source>
        <translation>juego</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="78"/>
        <source>client</source>
        <translation>cliente</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexefactory.cpp" line="81"/>
        <source>server</source>
        <translation>servidor</translation>
    </message>
</context>
<context>
    <name>GameExeRetriever</name>
    <message>
        <location filename="../serverapi/gameexeretriever.cpp" line="41"/>
        <source>Game doesn&apos;t define offline executable.</source>
        <translation>El juego no define ningún ejecutable fuera de línea.</translation>
    </message>
    <message>
        <location filename="../serverapi/gameexeretriever.cpp" line="48"/>
        <source>Game offline executable is not configured.</source>
        <translation>El juego ejecutable fuera de línea no está configurado.</translation>
    </message>
</context>
<context>
    <name>GameExecutablePicker</name>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="73"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - buscar ejecutable</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="115"/>
        <source>Plugin doesn&apos;t support configuration.</source>
        <translation>El complemento no soporta configuraciones.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="122"/>
        <source>Game doesn&apos;t define any executables for this game setup.</source>
        <translation>El juego no define ningún ejecutable para la configuración de este juego.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="135"/>
        <source>Default executable for this game isn&apos;t configured.</source>
        <translation>El ejecutable predeterminado para este juego no está configurado.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.cpp" line="161"/>
        <source>Game plugin not set.</source>
        <translation>El complemento del juego no está configurado.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="45"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="58"/>
        <source>Default</source>
        <translation>Predeterminado</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gameexecutablepicker.ui" line="81"/>
        <source>This map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>Este mapa no está presente en la lista de mapas. El juego puede comportarse de forma extraña.</translation>
    </message>
</context>
<context>
    <name>GameHost</name>
    <message>
        <source>Iwad is not set</source>
        <translation type="vanished">IWAD no está configurado</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="151"/>
        <source>IWAD is not set</source>
        <translation>IWAD no está definido</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="159"/>
        <source>IWAD Path error:
&quot;%1&quot; doesn&apos;t exist or is a directory!</source>
        <translation>Error de ruta IWAD:
&quot;%1&quot; no existe o es un directorio!</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="440"/>
        <source>The game executable is not set.</source>
        <translation>El ejecutable del juego no se ha configurado.</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="447"/>
        <source>The executable
%1
doesn&apos;t exist or is not a file.</source>
        <translation>El ejecutable
%1
no existe o no es un archivo.</translation>
    </message>
    <message>
        <location filename="../serverapi/gamehost.cpp" line="463"/>
        <source>PWAD path error:
&quot;%1&quot; doesn&apos;t exist!</source>
        <translation>Error de ruta PWAD:
&quot;%1&quot; no existe!</translation>
    </message>
    <message>
        <source>%1
doesn&apos;t exist or is not a file.</source>
        <translation type="vanished">%1
no existe o no es un archivo.</translation>
    </message>
    <message>
        <source>PWAD path error:
&quot;%1&quot; doesn&apos;t exist or is a directory!</source>
        <translation type="vanished">Error de ruta PWAD:
&quot;%1&quot; no existe o es un directorio!</translation>
    </message>
    <message>
        <source>%1
 doesn&apos;t exist or is not a file.</source>
        <translation type="vanished">%1
 no existe o es un directorio.</translation>
    </message>
</context>
<context>
    <name>GameInfoTip</name>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="67"/>
        <source>Players</source>
        <translation>Jugadores</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="72"/>
        <source>%1 / %2 (%3 can join)</source>
        <translation>%1 / %2 (%3 se pueden unir)</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="96"/>
        <source>Scorelimit</source>
        <translation>Límite puntuación</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="131"/>
        <source>Timelimit</source>
        <translation>Límite tiempo</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="138"/>
        <source>(%1 left)</source>
        <translation>(restan %1)</translation>
    </message>
</context>
<context>
    <name>GameRulesPanel</name>
    <message>
        <location filename="../gui/createserver/gamerulespanel.cpp" line="249"/>
        <source>&lt; NONE &gt;</source>
        <translation>&lt; NADA &gt;</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.cpp" line="283"/>
        <source>%1:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="41"/>
        <source>Map list</source>
        <translation>Lista de mapas</translation>
    </message>
    <message>
        <source>Difficulty:</source>
        <translation type="vanished">Dificultad:</translation>
    </message>
    <message>
        <source>Modifiers:</source>
        <translation type="vanished">Modificadores:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="84"/>
        <source>Hosting limits</source>
        <translation>Límites de alojamiento</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="90"/>
        <source>Max. clients:</source>
        <translation>Max. clientes:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="107"/>
        <source>Max. players:</source>
        <translation>Max. jugadores:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="127"/>
        <source>Modifier</source>
        <translation>Modificador</translation>
    </message>
    <message>
        <location filename="../gui/createserver/gamerulespanel.ui" line="146"/>
        <source>Extra settings</source>
        <translation>Opciones adicionales</translation>
    </message>
    <message>
        <source>Game limits</source>
        <translation type="vanished">Límites del juego</translation>
    </message>
</context>
<context>
    <name>GeneralGameSetupPanel</name>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="283"/>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="313"/>
        <source>&lt; NONE &gt;</source>
        <translation>&lt; NADA &gt;</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="480"/>
        <source>Doomseeker - load server config</source>
        <translation>Doomseeker - cargar la configuración del servidor</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.cpp" line="481"/>
        <source>Plugin for engine &quot;%1&quot; is not present!</source>
        <translation>¡El complemento para el motor &quot;%1&quot; no está presente!</translation>
    </message>
    <message>
        <source>Engine:</source>
        <translation type="vanished">Motor:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="34"/>
        <source>Game:</source>
        <translation>Juego:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="44"/>
        <source>Executable:</source>
        <translation>Ejecutable:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="105"/>
        <source>If locked, executable won&apos;t change when new config for the same engine is loaded</source>
        <translation>Si está bloqueado, el ejecutable no cambiará cuando se carguen nuevas configuraciones para el mismo motor</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="108"/>
        <source>Lock</source>
        <translation>Bloquear</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="117"/>
        <source>Logging:</source>
        <translation>Registro:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="140"/>
        <source>Server name:</source>
        <translation>Nombre del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="147"/>
        <source>Started from Doomseeker</source>
        <translation>Comenzado desde Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="154"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="195"/>
        <source>Allow the game to choose port</source>
        <translation>Permitir que el juego elegir el puerto</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="205"/>
        <source>Game mode:</source>
        <translation>Modo de juego:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="232"/>
        <source>Map:</source>
        <translation>Mapa:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="271"/>
        <source>Demo:</source>
        <translation>Demo:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="311"/>
        <source>This map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>Este mapa no está presente en la lista de mapas. El juego puede comportarse de forma extraña.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="239"/>
        <source>IWAD:</source>
        <translation>IWAD:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="329"/>
        <source>Additional WADs and files (check the required ones):</source>
        <translation>WADs y archivos adicionales (comprueba los que son requeridos):</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="215"/>
        <source>Difficulty:</source>
        <translation>Dificultad:</translation>
    </message>
    <message>
        <source>Additional WADs and files (check required):</source>
        <translation type="vanished">WADs y archivos adicionales (se requiere verificación):</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="361"/>
        <source>Broadcast to LAN</source>
        <translation>Emitir a la LAN</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="371"/>
        <source>Broadcast to master</source>
        <translation>Emitir a master</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="396"/>
        <source>If checked, the game will try to tell your router to forward necessary ports.</source>
        <translation>Si está marcado, el juego intentará decirle a su router que reenvíe los puertos necesarios.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="399"/>
        <source>UPnP</source>
        <translation>UPnP</translation>
    </message>
    <message>
        <location filename="../gui/createserver/generalgamesetuppanel.ui" line="409"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;UPnP port. Set this to 0 to let Doomseeker or game decide.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;puerto UPnP. Establezca esto en 0 para permitir que Doomseeker o el juego decidan.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>GeneralInfoTip</name>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="51"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="52"/>
        <source>E-mail</source>
        <translation>Correo</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="53"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/generalinfotip.cpp" line="59"/>
        <source>Location</source>
        <translation>Ubicación</translation>
    </message>
    <message>
        <source>Location: %1
</source>
        <translation type="vanished">Ubicación: %1
</translation>
    </message>
</context>
<context>
    <name>IP2C</name>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="228"/>
        <source>No flag for country: %1</source>
        <translation>No hay bandera para el país:%1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="173"/>
        <source>Localhost</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="176"/>
        <source>LAN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="219"/>
        <source>Unknown country: %1</source>
        <translation>País desconocido: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2c.cpp" line="234"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
</context>
<context>
    <name>IP2CCountryTr</name>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="57"/>
        <source>Aruba</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="58"/>
        <source>Afghanistan</source>
        <translation>Afganistan</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="59"/>
        <source>Angola</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="60"/>
        <source>Anguilla</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="61"/>
        <source>Albania</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="62"/>
        <source>Andorra</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="63"/>
        <source>Netherlands Antilles</source>
        <translation>Antillas Holandesas</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="64"/>
        <source>United Arab Emirates</source>
        <translation>Emiratos Árabes Unidos</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="65"/>
        <source>Argentina</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="66"/>
        <source>Armenia</source>
        <translation>Armenia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="67"/>
        <source>American Samoa</source>
        <translation>Samoa Americana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="68"/>
        <source>Antarctica</source>
        <translation>Antártida</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="69"/>
        <source>French Southern Territories</source>
        <translation>Territorios Franceses del Sur</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="70"/>
        <source>Antigua and Barbuda</source>
        <translation>Antigua y Barbuda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="71"/>
        <source>Australia</source>
        <translation>Australia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="72"/>
        <source>Austria</source>
        <translation>Austria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="73"/>
        <source>Azerbaijan</source>
        <translation>Azerbaiyán</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="74"/>
        <source>Burundi</source>
        <translation>Burundi</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="75"/>
        <source>Belgium</source>
        <translation>Bélgica</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="76"/>
        <source>Benin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="77"/>
        <source>Burkina Faso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="78"/>
        <source>Bangladesh</source>
        <translation>Bangladesh</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="79"/>
        <source>Bulgaria</source>
        <translation>Bulgaria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="80"/>
        <source>Bahrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="81"/>
        <source>Bahamas</source>
        <translation>Bahamas</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="82"/>
        <source>Bosnia and Herzegovina</source>
        <translation>Bosnia y Herzegovina</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="83"/>
        <source>Belarus</source>
        <translation>Bielorrusia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="84"/>
        <source>Belize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="85"/>
        <source>Bermuda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="86"/>
        <source>Bolivia</source>
        <translation>Bolivia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="87"/>
        <source>Brazil</source>
        <translation>Brasil</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="88"/>
        <source>Barbados</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="89"/>
        <source>Brunei</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="90"/>
        <source>Bhutan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="91"/>
        <source>Bouvet Island</source>
        <translation>Isla Bouvet</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="92"/>
        <source>Botswana</source>
        <translation>Botsuana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="93"/>
        <source>Central African Republic</source>
        <translation>República Centroafricana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="94"/>
        <source>Canada</source>
        <translation>Canadá</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="95"/>
        <source>Cocos (Keeling) Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="96"/>
        <source>Switzerland</source>
        <translation>Suiza</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="97"/>
        <source>Chile</source>
        <translation>Chile</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="98"/>
        <source>China</source>
        <translation>China</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="99"/>
        <source>Ivory Coast</source>
        <translation>Costa de Marfil</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="100"/>
        <source>Cameroon</source>
        <translation>Camerún</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="101"/>
        <source>Congo, the Democratic Republic of the</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="102"/>
        <source>Congo</source>
        <translation>Congo</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="103"/>
        <source>Cook Islands</source>
        <translation>Islas Cook</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="104"/>
        <source>Colombia</source>
        <translation>Colombia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="105"/>
        <source>Comoros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="106"/>
        <source>Cape Verde</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="107"/>
        <source>Costa Rica</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="108"/>
        <source>Cuba</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="109"/>
        <source>Christmas Island</source>
        <translation>Isla de Navidad</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="110"/>
        <source>Cayman Islands</source>
        <translation>Islas Caimán</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="111"/>
        <source>Cyprus</source>
        <translation>Chipre</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="112"/>
        <source>Czech Republic</source>
        <translation>Republica checa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="113"/>
        <source>Germany</source>
        <translation>Alemania</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="114"/>
        <source>Djibouti</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="115"/>
        <source>Dominica</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="116"/>
        <source>Denmark</source>
        <translation>Dinamarca</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="117"/>
        <source>Dominican Republic</source>
        <translation>República Dominicana</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="118"/>
        <source>Algeria</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="119"/>
        <source>Ecuador</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="120"/>
        <source>Egypt</source>
        <translation>Egipto</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="121"/>
        <source>Eritrea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="122"/>
        <source>Western Sahara</source>
        <translation>Sahara Occidental</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="123"/>
        <source>Spain</source>
        <translation>España</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="124"/>
        <source>Estonia</source>
        <translation>Estonia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="125"/>
        <source>Ethiopia</source>
        <translation>Etiopía</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="126"/>
        <source>European Union</source>
        <translation>Unión Europea</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="127"/>
        <source>Finland</source>
        <translation>Finlandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="128"/>
        <source>Fiji</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="129"/>
        <source>Falkland Islands (Malvinas)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="130"/>
        <source>France</source>
        <translation>Francia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="131"/>
        <source>Faroe Islands</source>
        <translation>Islas Faroe</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="132"/>
        <source>Micronesia, Federated States of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="133"/>
        <source>Gabon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="134"/>
        <source>United Kingdom</source>
        <translation>Reino Unido</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="135"/>
        <source>Georgia</source>
        <translation>Georgia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="136"/>
        <source>Guernsey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="137"/>
        <source>Ghana</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="138"/>
        <source>Gibraltar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="139"/>
        <source>Guinea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="140"/>
        <source>Guadeloupe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="141"/>
        <source>Gambia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="142"/>
        <source>Guinea-Bissau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="143"/>
        <source>Equatorial Guinea</source>
        <translation>Guinea Ecuatorial</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="144"/>
        <source>Greece</source>
        <translation>Grecia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="145"/>
        <source>Grenada</source>
        <translation>Granada</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="146"/>
        <source>Greenland</source>
        <translation>Groenlandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="147"/>
        <source>Guatemala</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="148"/>
        <source>French Guiana</source>
        <translation>Guayana Francesa</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="149"/>
        <source>Guam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="150"/>
        <source>Guyana</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="151"/>
        <source>Hong Kong</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="152"/>
        <source>Heard Island and McDonald Islands</source>
        <translation>Islas Heard y McDonald</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="153"/>
        <source>Honduras</source>
        <translation>Honduras</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="154"/>
        <source>Croatia</source>
        <translation>Croacia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="155"/>
        <source>Haiti</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="156"/>
        <source>Hungary</source>
        <translation>Hungría</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="157"/>
        <source>Indonesia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="158"/>
        <source>Isle of Man</source>
        <translation>Isla del hombre</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="159"/>
        <source>India</source>
        <translation>India</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="160"/>
        <source>British Indian Ocean Territory</source>
        <translation>Territorio Británico del Océano Índico</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="161"/>
        <source>Ireland</source>
        <translation>Irlanda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="162"/>
        <source>Iran, Islamic Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="163"/>
        <source>Iraq</source>
        <translation>Irak</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="164"/>
        <source>Iceland</source>
        <translation>Islandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="165"/>
        <source>Israel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="166"/>
        <source>Italy</source>
        <translation>Italia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="167"/>
        <source>Jamaica</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="168"/>
        <source>Jersey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="169"/>
        <source>Jordan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="170"/>
        <source>Japan</source>
        <translation>Japón</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="171"/>
        <source>Kazakhstan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="172"/>
        <source>Kenya</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="173"/>
        <source>Kyrgyzstan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="174"/>
        <source>Cambodia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="175"/>
        <source>Kiribati</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="176"/>
        <source>Saint Kitts and Nevis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="177"/>
        <source>South Korea</source>
        <translation>Corea del Sur</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="178"/>
        <source>Kuwait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="179"/>
        <source>Lao People&apos;s Democratic Republic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="180"/>
        <source>Lebanon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="181"/>
        <source>Liberia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="182"/>
        <source>Libya</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="183"/>
        <source>Saint Lucia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="184"/>
        <source>Liechtenstein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="185"/>
        <source>Sri Lanka</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="186"/>
        <source>Lesotho</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="187"/>
        <source>Lithuania</source>
        <translation>Lituania</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="188"/>
        <source>Luxembourg</source>
        <translation>luxemburgo</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="189"/>
        <source>Latvia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="190"/>
        <source>Macao</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="191"/>
        <source>Morocco</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="192"/>
        <source>Monaco</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="193"/>
        <source>Moldova, Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="194"/>
        <source>Madagascar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="195"/>
        <source>Maldives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="196"/>
        <source>Mexico</source>
        <translation>México</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="197"/>
        <source>Marshall Islands</source>
        <translation>Islas Marshall</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="198"/>
        <source>Macedonia, the former Yugoslav Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="199"/>
        <source>Mali</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="200"/>
        <source>Malta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="201"/>
        <source>Myanmar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="202"/>
        <source>Montenegro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="203"/>
        <source>Mongolia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="204"/>
        <source>Northern Mariana Islands</source>
        <translation>Islas Marianas del Norte</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="205"/>
        <source>Mozambique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="206"/>
        <source>Mauritania</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="207"/>
        <source>Montserrat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="208"/>
        <source>Martinique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="209"/>
        <source>Mauritius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="210"/>
        <source>Malawi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="211"/>
        <source>Malaysia</source>
        <translation>Malasia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="212"/>
        <source>Mayotte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="213"/>
        <source>Namibia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="214"/>
        <source>New Caledonia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="215"/>
        <source>Niger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="216"/>
        <source>Norfolk Island</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="217"/>
        <source>Nigeria</source>
        <translation>Nigeria</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="218"/>
        <source>Nicaragua</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="219"/>
        <source>Niue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="220"/>
        <source>Netherlands</source>
        <translation>Países Bajos</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="221"/>
        <source>Norway</source>
        <translation>Noruega</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="222"/>
        <source>Nepal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="223"/>
        <source>Nauru</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="224"/>
        <source>New Zealand</source>
        <translation>Nueva Zelanda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="225"/>
        <source>Oman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="226"/>
        <source>Pakistan</source>
        <translation>Pakistán</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="227"/>
        <source>Panama</source>
        <translation>Panamá</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="228"/>
        <source>Pitcairn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="229"/>
        <source>Peru</source>
        <translation>Perú</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="230"/>
        <source>Philippines</source>
        <translation>Filipinas</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="231"/>
        <source>Palau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="232"/>
        <source>Papua New Guinea</source>
        <translation>Papúa Nueva Guinea</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="233"/>
        <source>Poland</source>
        <translation>Polonia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="234"/>
        <source>Puerto Rico</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="235"/>
        <source>Korea, Democratic People&apos;s Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="236"/>
        <source>Portugal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="237"/>
        <source>Paraguay</source>
        <translation>Paraguay</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="238"/>
        <source>Palestinian Territory, Occupied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="239"/>
        <source>French Polynesia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="240"/>
        <source>Qatar</source>
        <translation>Katar</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="241"/>
        <source>Réunion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="242"/>
        <source>Romania</source>
        <translation>Rumania</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="243"/>
        <source>Russian Federation</source>
        <translation>Rússia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="244"/>
        <source>Rwanda</source>
        <translation>Ruanda</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="245"/>
        <source>Saudi Arabia</source>
        <translation>Arabia Saudita</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="246"/>
        <source>Sudan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="247"/>
        <source>Senegal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="248"/>
        <source>Singapore</source>
        <translation>Singapur</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="249"/>
        <source>South Georgia and the South Sandwich Islands</source>
        <translation>Georgia del sur y las islas Sandwich del sur</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="250"/>
        <source>Saint Helena, Ascension and Tristan da Cunha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="251"/>
        <source>Svalbard and Jan Mayen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="252"/>
        <source>Solomon Islands</source>
        <translation>Islas Salomón</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="253"/>
        <source>Sierra Leone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="254"/>
        <source>El Salvador</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="255"/>
        <source>San Marino</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="256"/>
        <source>Somalia</source>
        <translation>Somalia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="257"/>
        <source>Saint Pierre and Miquelon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="258"/>
        <source>Serbia</source>
        <translation>Serbia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="259"/>
        <source>South Sudan</source>
        <translation>Sudán del Sur</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="260"/>
        <source>Sao Tome and Principe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="261"/>
        <source>Suriname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="262"/>
        <source>Slovakia</source>
        <translation>Eslovaquia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="263"/>
        <source>Slovenia</source>
        <translation>Eslovenia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="264"/>
        <source>Sweden</source>
        <translation>Suecia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="265"/>
        <source>Swaziland</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="266"/>
        <source>Seychelles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="267"/>
        <source>Syrian Arab Republic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="268"/>
        <source>Turks and Caicos Islands</source>
        <translation>Islas Turcas y Caicos</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="269"/>
        <source>Chad</source>
        <translation>Chad</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="270"/>
        <source>Togo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="271"/>
        <source>Thailand</source>
        <translation>Tailandia</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="272"/>
        <source>Tajikistan</source>
        <translation>Tayikistán</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="273"/>
        <source>Tokelau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="274"/>
        <source>Turkmenistan</source>
        <translation>turkmenistán</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="275"/>
        <source>Timor-Leste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="276"/>
        <source>Tonga</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="277"/>
        <source>Trinidad and Tobago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="278"/>
        <source>Tunisia</source>
        <translation>Túnez</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="279"/>
        <source>Turkey</source>
        <translation>Turquía</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="280"/>
        <source>Tuvalu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="281"/>
        <source>Taiwan</source>
        <translation>Taiwán</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="282"/>
        <source>Tanzania, United Republic of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="283"/>
        <source>Uganda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="284"/>
        <source>Ukraine</source>
        <translation>Ucrania</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="285"/>
        <source>United States Minor Outlying Islands</source>
        <translation>Islas ultramarinas menores de los Estados Unidos</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="286"/>
        <source>Uruguay</source>
        <translation>Uruguay</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="287"/>
        <source>United States</source>
        <translation>Estados Unidos</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="288"/>
        <source>Uzbekistan</source>
        <translation>Uzbekistán</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="289"/>
        <source>Holy See (Vatican City State)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="290"/>
        <source>Saint Vincent and the Grenadines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="291"/>
        <source>Venezuela</source>
        <translation>Venezuela</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="292"/>
        <source>Virgin Islands, British</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="293"/>
        <source>Virgin Islands, U.S.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="294"/>
        <source>Vietnam</source>
        <translation>Vietnam</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="295"/>
        <source>Vanuatu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="296"/>
        <source>Wallis and Futuna</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="297"/>
        <source>Samoa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="298"/>
        <source>Yemen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="299"/>
        <source>South Africa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="300"/>
        <source>Zambia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ip2c/ip2ccountry.cpp" line="301"/>
        <source>Zimbabwe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IP2CLoader</name>
    <message>
        <source>IP2C parser is still working, awaiting stop...</source>
        <translation type="vanished">El analizador IP2C sigue funcionando, esperando a que se detenga...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="144"/>
        <source>IP2C update not needed.</source>
        <translation>Actualización de IP2C no necesaria.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="147"/>
        <source>IP2C update errored. See log for details.</source>
        <translation>La actualización de IP2C falló. Ver registro para más detalles.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="150"/>
        <source>IP2C update bugged out.</source>
        <translation>La actualización de IP2C muestra errores.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="162"/>
        <source>Starting IP2C update.</source>
        <translation>Iniciando la actualización de IP2C.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="200"/>
        <source>IP2C unable to retrieve the parsing result.</source>
        <translation>IP2C incapaz de recuperar el resultado del análisis.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="210"/>
        <source>Failed to read the IP2C fallback. Stopping.</source>
        <translation>Error al leer la base de datos IP2C de repuesto. Parando.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="214"/>
        <source>Failed to read the IP2C database. Reverting...</source>
        <translation>Error al leer la base de datos IP2C. Revirtiendo...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="228"/>
        <source>Trying to use the preinstalled IP2C database.</source>
        <translation>Intentando usar la base de datos IP2C preinstalada.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="267"/>
        <source>IP2C database is being read. This may take some time.</source>
        <translation>La base de datos IP2C está siendo leída. Esto puede tomar algo de tiempo.</translation>
    </message>
    <message>
        <source>IP2C update must wait until parsing of current database finishes. Waiting 1 second</source>
        <translation type="vanished">La actualización de IP2C debe esperar hasta que finalice el análisis de la base de datos actual. Esperando 1 segundo</translation>
    </message>
    <message>
        <source>IP2C update must wait until parsing of current database finishes. Waiting 1 second.</source>
        <translation type="vanished">La actualización de IP2C debe esperar hasta que finalice el análisis de la base de datos actual. Esperando 1 segundo.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="173"/>
        <source>IP2C database finished downloading.</source>
        <translation>La base de datos IP2C se terminó de descargar.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="177"/>
        <source>Unable to save IP2C database at path: %1</source>
        <translation>No se puede guardar la base de datos IP2C en la ruta: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="182"/>
        <source>IP2C download has failed.</source>
        <translation>La descarga de IP2C ha fallado.</translation>
    </message>
    <message>
        <source>Failed to read IP2C fallback. Stopping.</source>
        <translation type="vanished">Error al leer la base de datos IP2C de repuesto. Parando.</translation>
    </message>
    <message>
        <source>Failed to read IP2C database. Reverting...</source>
        <translation type="vanished">Error al leer la base de datos IP2C. Revirtiendo...</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="219"/>
        <source>IP2C revert attempt failed. Nothing to go back to.</source>
        <translation>Intento de revertir IP2C fallido. Nada a lo que regresar.</translation>
    </message>
    <message>
        <source>Trying to use preinstalled IP2C database.</source>
        <translation type="vanished">Intentando usar la base de datos IP2C preinstalada.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="126"/>
        <source>Did not find any IP2C database. IP2C functionality will be disabled.</source>
        <translation>No se encontró ninguna base de datos IP2C. La funcionalidad de IP2C estará deshabilitada.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="127"/>
        <source>You may install an IP2C database from the &quot;File&quot; menu.</source>
        <translation>Puede instalar una base de datos IP2C desde el menú &quot;Archivo&quot;.</translation>
    </message>
    <message>
        <source>Using precompiled IP2C database.</source>
        <translation type="vanished">Usando la base de datos IP2C precompilada.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="248"/>
        <source>IP2C parsing finished.</source>
        <translation>El análisis de IP2C terminó.</translation>
    </message>
    <message>
        <source>Please wait. IP2C database is being read. This may take some time.</source>
        <translation type="vanished">Por favor espera. La base de datos IP2C está siendo leída. Esto puede tomar algo de tiempo.</translation>
    </message>
</context>
<context>
    <name>IP2CParser</name>
    <message>
        <source>Parsing IP2C database: %1</source>
        <translation type="vanished">Analizando base de datos IP2C:%1</translation>
    </message>
    <message>
        <source>Unable to open IP2C file.</source>
        <translation type="vanished">No se puede abrir el archivo IP2C.</translation>
    </message>
    <message>
        <source>IP2C database read in %1 ms; IP ranges: %2</source>
        <translation type="vanished">Base de datos IP2C leída en %1 ms. Rangos de IP: %2</translation>
    </message>
    <message>
        <source>IP2C database read in %1 ms. Entries read: %2</source>
        <translation type="vanished">Base de datos IP2C leída en %1 ms. Entradas leídas: %2</translation>
    </message>
    <message>
        <source>IP2C parsing thread has finished.</source>
        <translation type="vanished">El hilo del análisis IP2C ha terminado.</translation>
    </message>
</context>
<context>
    <name>IP2CParserThread</name>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="63"/>
        <source>IP2C database read in %1 ms; IP ranges: %2</source>
        <translation>Base de datos IP2C leída en %1 ms. Rangos de IP: %2</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cloader.cpp" line="68"/>
        <source>Unable to open IP2C file: %1</source>
        <translation>No se puede abrir el archivo IP2C: %1</translation>
    </message>
</context>
<context>
    <name>IP2CUpdateBox</name>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="86"/>
        <source>Verifying checksum ...</source>
        <translation>Verificando suma de verificación ...</translation>
    </message>
    <message>
        <source>IP2C database file was not found. Precompiled database will be used. Use update button if you want to download the newest database.</source>
        <translation type="vanished">El archivo de la base de datos IP2C no fue encontrado. Se usará la base de datos precompilada. Use el botón de actualización si desea descargar la última base de datos.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="70"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="79"/>
        <source>File is already downloaded.</source>
        <translation>El archivo ya está descargado.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="80"/>
        <source>File doesn&apos;t exist yet or location doesn&apos;t point to a file.</source>
        <translation>El archivo aún no existe o la ubicación no apunta a un archivo.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="94"/>
        <source>The IP2C database file was not found. Use the &quot;Download&quot; button if you want to download the newest database.</source>
        <translation>No se encontró el archivo de base de datos IP2C. Utilice el botón de actualización si desea descargar la base de datos más reciente.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="96"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="109"/>
        <source>Update available.</source>
        <translation>Actualizaciones disponibles.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="118"/>
        <source>Database status check failed. See the log for details.</source>
        <translation>Fallo en la comprobación del estado de la base de datos. Ver registro para más detalles.</translation>
    </message>
    <message>
        <source>IP2C database file was not found. Use update button if you want to download the newest database.</source>
        <translation type="vanished">No se encontró la base de datos IP2C. Utilice el botón de actualización si desea descargar la base de datos más reciente.</translation>
    </message>
    <message>
        <source>IP2C database file was not found. Use the update button if you want to download the newest database.</source>
        <translation type="vanished">No se encontró el archivo de base de datos IP2C. Utilice el botón de actualización si desea descargar la base de datos más reciente.</translation>
    </message>
    <message>
        <source>Update required.</source>
        <translation type="vanished">Actualización necesaria.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="113"/>
        <source>Database is up-to-date.</source>
        <translation>La base de datos está actualizada.</translation>
    </message>
    <message>
        <source>Database status check failed. See log for details.</source>
        <translation type="vanished">Fallo en la comprobación del estado de la base de datos. Ver registro para más detalles.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="122"/>
        <source>Unhandled update check status.</source>
        <translation>Estado de comprobación de actualización no controlado.</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="20"/>
        <source>Doomseeker - Update IP2C</source>
        <translation>Doomseeker - Actualización de IP2C</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="26"/>
        <source>IP2C file location:</source>
        <translation>Ubicación del archivo IP2C:</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="186"/>
        <source>The downloaded database will be placed in the following location:</source>
        <translation>La base de datos descargada estará ubicada en la siguiente ruta:</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.ui" line="250"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;The IP2C update is performed in the background. Doomseeker will notify you of download progress through a progress bar and log messages.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Tenga en cuenta que la actualización de IP2C se realiza en segundo plano. Doomseeker le notificará del progreso de la descarga a través de una barra de progreso y mensajes en el registro.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ip2cupdatebox.cpp" line="127"/>
        <source>Update</source>
        <translation>Actualización</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>IP2CUpdater</name>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="67"/>
        <source>IP2C checksum check network error: %1</source>
        <translation>Error de red al verificar la suma de verificación de IP2C: %1</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="87"/>
        <source>Comparing IP2C hashes: local = %1, remote = %2</source>
        <translation>Comparación de los valores hash de IP2C: local = %1, remoto = %2</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="91"/>
        <source>IP2C update needed.</source>
        <translation>Actualización de IP2C necesaria.</translation>
    </message>
    <message>
        <location filename="../ip2c/ip2cupdater.cpp" line="207"/>
        <source>Checking if IP2C database at &apos;%1&apos; needs updating.</source>
        <translation>Comprobando si la base de datos IP2C en &apos;%1&apos; necesita una actualización.</translation>
    </message>
</context>
<context>
    <name>IRCChannelAdapter</name>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="157"/>
        <source>%1 is now known as %2</source>
        <translation>%1 es conocido como %2</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="166"/>
        <source>User %1 [%2] has joined the channel.</source>
        <translation>El usuario %1 [%2] se ha unido al canal.</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="183"/>
        <source>User %1 has left the channel. (PART: %2)</source>
        <translation>El usuario %1 ha salido del canal. (PART: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="188"/>
        <source>Connection for user %1 has been killed. (KILL: %2)</source>
        <translation>La conexión para el usuario %1 ha sido eliminada. (KILL: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="193"/>
        <source>User %1 has quit the network. (QUIT: %2)</source>
        <translation>El usuario %1 ha abandonado la red. (QUIT: %2)</translation>
    </message>
    <message>
        <location filename="../irc/ircchanneladapter.cpp" line="198"/>
        <source>Unknown quit type from user %1.</source>
        <translation>Tipo de abandono desconocido del usuario %1.</translation>
    </message>
</context>
<context>
    <name>IRCClient</name>
    <message>
        <location filename="../irc/ircclient.cpp" line="45"/>
        <source>IRC: Connecting: %1:%2</source>
        <translation>IRC: Conectando: %1:%2</translation>
    </message>
</context>
<context>
    <name>IRCConfigurationDialog</name>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="35"/>
        <source>Doomseeker - IRC Options</source>
        <translation>Doomseeker - Opciones IRC</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="41"/>
        <source>Settings saved!</source>
        <translation>¡Ajustes guardados!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="43"/>
        <source>Settings save failed!</source>
        <translation>¡Guardado de ajustes ha fallado!</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="81"/>
        <source>Config validation</source>
        <translation>Validación de configuración</translation>
    </message>
    <message>
        <location filename="../gui/configuration/irc/ircconfigurationdialog.cpp" line="81"/>
        <source>You have chosen one or more networks for autojoin startup but you have not defined any nickname. Please define it now.</source>
        <translation>Ha elegido una o más redes para el inicio automático, pero no ha definido ningun alias. Por favor, definala ahora.</translation>
    </message>
</context>
<context>
    <name>IRCCtcpParser</name>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="85"/>
        <source>CTCP %1: [%2] %3 %4</source>
        <translation>CTCP %1: [%2] %3 %4</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="89"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="147"/>
        <source>REQUEST</source>
        <translation>SOLICITUD</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="149"/>
        <source>REPLY</source>
        <translation>RESPUESTA</translation>
    </message>
    <message>
        <location filename="../irc/ircctcpparser.cpp" line="151"/>
        <source>????</source>
        <translation>????</translation>
    </message>
</context>
<context>
    <name>IRCDelayedOperationIgnore</name>
    <message>
        <location filename="../irc/ops/ircdelayedoperationignore.cpp" line="85"/>
        <source>Ignore user %1 (username=%2) on network %3:</source>
        <translation>Ignorar usuario %1 (username=%2) en red %3:</translation>
    </message>
    <message>
        <location filename="../irc/ops/ircdelayedoperationignore.cpp" line="87"/>
        <source>IRC - Ignore user</source>
        <translation>IRC - Ignorar usuario</translation>
    </message>
</context>
<context>
    <name>IRCDock</name>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="170"/>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="171"/>
        <location filename="../gui/irc/ircdock.cpp" line="261"/>
        <source>Open chat window</source>
        <translation>Abrir la ventana de chat</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="256"/>
        <location filename="../gui/irc/ircdock.cpp" line="258"/>
        <source>Doomseeker IRC - Open chat window</source>
        <translation>Doomseeker IRC - Abrir ventana de chat</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="256"/>
        <source>Cannot obtain network connection adapter.</source>
        <translation>No se puede obtener el adaptador de conexión de red.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="258"/>
        <source>You are not connected to this network.</source>
        <translation>Usted no está conectado a esta red.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.cpp" line="261"/>
        <source>Specify a channel or user name:</source>
        <translation>Especifique un canal o nombre de usuario:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdock.ui" line="26"/>
        <source>IRC</source>
        <translation>IRC</translation>
    </message>
</context>
<context>
    <name>IRCDockTabContents</name>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="360"/>
        <source>&lt;&lt;&lt;DATE&gt;&gt;&gt; Date on this computer changes to %1</source>
        <translation>&lt;&lt;&lt;FECHA&gt;&gt;&gt; La fecha en esta computadora cambia a %1</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="454"/>
        <source>Failed to create chat log directory:
&apos;%1&apos;</source>
        <translation>Error al crear el directorio de registro de chat:
&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="460"/>
        <source>&lt;&lt;&lt;DATE&gt;&gt;&gt; Chat log started on %1

</source>
        <translation>&lt;&lt;&lt;FECHA&gt;&gt;&gt; Registro de chat iniciado en %1

</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="487"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="559"/>
        <source>---- All lines above were loaded from log ----</source>
        <translation>---- Todas las líneas de arriba fueron cargadas del registro ----</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="711"/>
        <source>Manage ignores</source>
        <translation>Administrar ignorados</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="717"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="931"/>
        <source>Whois</source>
        <translation>Whois</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="718"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="933"/>
        <source>CTCP Ping</source>
        <translation>CTCP Ping</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="719"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="932"/>
        <source>CTCP Time</source>
        <translation>CTCP Tiempo</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="720"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="934"/>
        <source>CTCP Version</source>
        <translation>CTCP Versión</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="721"/>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="943"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="842"/>
        <source>Ban user</source>
        <translation>Banear usuario</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="842"/>
        <source>Input reason for banning user %1 from channel %2</source>
        <translation>Introduzca el motivo para banear al usuario %1 del canal %2</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="866"/>
        <source>Kick user</source>
        <translation>Echar usuario</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="866"/>
        <source>Input reason for kicking user %1 from channel %2</source>
        <translation>Introduzca el motivo para echar al usuario %1 del canal %2</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="929"/>
        <source>Open chat window</source>
        <translation>Abrir la ventana de chat</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="936"/>
        <source>Op</source>
        <translation>OP</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="937"/>
        <source>Deop</source>
        <translation>DesOP</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="938"/>
        <source>Half op</source>
        <translation>Casi OP</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="939"/>
        <source>De half op</source>
        <translation>Des Casi OP</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="940"/>
        <source>Voice</source>
        <translation>Desilenciar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="941"/>
        <source>Devoice</source>
        <translation>Silenciar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="944"/>
        <source>Kick</source>
        <translation>Echar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.cpp" line="945"/>
        <source>Ban</source>
        <translation>Banear</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="69"/>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircdocktabcontents.ui" line="76"/>
        <source>Do not scroll text area</source>
        <translation>No mover la área del texto</translation>
    </message>
</context>
<context>
    <name>IRCIgnoresManager</name>
    <message>
        <location filename="../gui/irc/ircignoresmanager.ui" line="14"/>
        <source>IRC - ignores manager</source>
        <translation>IRC - administrador de ignorados</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircignoresmanager.ui" line="30"/>
        <source>Remove selected</source>
        <translation>Eliminar seleccionados</translation>
    </message>
    <message>
        <source>Delete selected</source>
        <translation type="vanished">Eliminar seleccionado</translation>
    </message>
</context>
<context>
    <name>IRCNetworkAdapter</name>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="196"/>
        <source>You are not connected to the network.</source>
        <translation>Usted no está conectado a la red.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="206"/>
        <source>Insufficient parameters.</source>
        <translation>Parámetros insuficientes.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="210"/>
        <source>This is a server window. All commands must be prepended with a &apos;/&apos; character.</source>
        <translation>Esta es una ventana del servidor. Todos los comandos deben ir precedidos de un carácter &apos;/&apos;.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="214"/>
        <source>Attempted to send empty message.</source>
        <translation>Intento de enviar un mensaje en blanco.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="218"/>
        <source>Command is too long.</source>
        <translation>El comando es demasiado largo.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="222"/>
        <source>Not a chat window.</source>
        <translation>No es una ventana de chat.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="231"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="318"/>
        <source>IRC: Successfully registered on network %1 [%2:%3]</source>
        <translation>IRC: Se registró correctamente en la red %1 [%2:%3]</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="358"/>
        <source>Invalid parse result for message: %1</source>
        <translation>Resultado de análisis inválido para mesage: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="402"/>
        <source>You have been kicked from channel %1 by %2 (Reason: %3)</source>
        <translation>Has sido expulsado del canal %1 por %2 (Razón: %3)</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="407"/>
        <source>%1 was kicked by %2 (%3)</source>
        <translation>%1 fue expulsado por %2 (%3)</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="459"/>
        <source>%1 sets mode: %2</source>
        <translation>%1 pone el modo: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="482"/>
        <source>Nickname %1 is already taken.</source>
        <translation>El alias %1 ya está en uso.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="488"/>
        <source>Both nickname and alternate nickname are taken on this network.</source>
        <translation>Tanto el alias como el alias alternativo están en uso en esta red.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="490"/>
        <source>No alternate nickname specified.</source>
        <translation>No se ha especificado un alias alternativo.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="495"/>
        <source>Using alternate nickname %1 to join.</source>
        <translation>Usar el alias alternativo %1 para unirse.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="504"/>
        <source>User %1 is not logged in.</source>
        <translation>El usuario %1 no ha iniciado sesión.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="526"/>
        <source>IRC parse error: %1</source>
        <translation>Error de análisis IRC: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="549"/>
        <source>FROM %1: %2</source>
        <translation>FROM %1: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="631"/>
        <source>Updated own nickname to %1.</source>
        <translation>Actualizado el propio sobrenombre a %1.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="676"/>
        <source>Last activity of user %1 was %2 ago.</source>
        <translation>La última actividad del usuario %1 fue hace %2.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="693"/>
        <source>%1 joined the network on %2</source>
        <translation>%1 se unió a la red en %2</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="706"/>
        <source>You left channel %1.</source>
        <translation>Saliste del canal %1.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="736"/>
        <source>Ping to user %1: %2ms</source>
        <translation>Ping al usuario %1: %2ms</translation>
    </message>
</context>
<context>
    <name>IRCNetworkSelectionBox</name>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="90"/>
        <source>Doomseeker - edit IRC network</source>
        <translation>Doomseeker - edita la red IRC</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="91"/>
        <source>Cannot edit as no valid network is selected.</source>
        <translation>No se puede editar ya que no está seleccionada una red válida.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="209"/>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="213"/>
        <source>Doomseeker - remove IRC network</source>
        <translation>Doomseeker - elimina la red IRC0</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="210"/>
        <source>Cannot remove as no valid network is selected.</source>
        <translation>No se puede eliminar ya que no está seleccionada una red válida.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="214"/>
        <source>Are you sure you wish to remove network &apos;%1&apos;?</source>
        <translation>¿Seguro que quieres eliminar la red &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="240"/>
        <source>IRC connection error</source>
        <translation>Error de conexión IRC</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="245"/>
        <source>You must specify a nick.</source>
        <translation>Debes especificar un alias.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.cpp" line="251"/>
        <source>You must specify a network address.</source>
        <translation>Debe especificar una dirección de red.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="14"/>
        <source>IRC - Connect to Network</source>
        <translation>IRC - Conectar a una red</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="25"/>
        <source>Nick:</source>
        <translation>Alias:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="32"/>
        <source>DSTest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="39"/>
        <source>Alternate nick:</source>
        <translation>Alias alternativo:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="46"/>
        <source>DSTestA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="53"/>
        <source>Real name:</source>
        <translation>Nombre real:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="60"/>
        <source>Doomseeker Tester</source>
        <translation></translation>
    </message>
    <message>
        <source>User name</source>
        <translation type="vanished">Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="67"/>
        <source>Username:</source>
        <translation>Nombre de usuario:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="74"/>
        <source>Sent to the IRC network during login. If not specified, defaults to nick.</source>
        <translation>Enviado a la red IRC durante el inicio de sesión. Si no se especifica, el valor predeterminado es nick.</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="77"/>
        <source>DSTestUser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="116"/>
        <source>Add network</source>
        <translation>Agregar red</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="133"/>
        <source>Edit network</source>
        <translation>Editar red</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="150"/>
        <source>Remove network</source>
        <translation>Eliminar red</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="170"/>
        <source>Server address:</source>
        <translation>Dirección del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="177"/>
        <source>74.207.247.18</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="187"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="210"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <location filename="../gui/irc/ircnetworkselectionbox.ui" line="226"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
</context>
<context>
    <name>IRCPrivAdapter</name>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="37"/>
        <source>This user changed nickname from %1 to %2</source>
        <translation>Este usuario cambió su Alias de %1 a %2</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="61"/>
        <source>This user connection has been killed. (KILL: %1)</source>
        <translation>Esta conexión de usuario ha sido eliminada. (KILL: %1)</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="65"/>
        <source>This user has left the network. (QUIT: %1)</source>
        <translation>Este usuario ha abandonado la red. (QUIT: %1)</translation>
    </message>
    <message>
        <location filename="../irc/ircprivadapter.cpp" line="69"/>
        <source>Unhandled IRCQuitType in IRCPrivAdapter::userLeaves()</source>
        <translation>IRCQuitType no controlado en IRCPrivAdapter::userLeaves()</translation>
    </message>
</context>
<context>
    <name>IRCResponseParser</name>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="138"/>
        <source>User %1 is away: %2</source>
        <translation>El usuario %1 está ausente: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="197"/>
        <source>%1 is on channels: %2</source>
        <translation>%1 está en canales:%2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="225"/>
        <source>Topic: %1</source>
        <translation>Tema: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="237"/>
        <source>Topic set by %1 on %2.</source>
        <translation>Tema establecido por %1 el %2.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="247"/>
        <source>URL: %1</source>
        <translation>URL: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="256"/>
        <source>Created time: %1</source>
        <translation>Tiempo creado: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="278"/>
        <source>RPLNamReply: Received names list but no channel name.</source>
        <translation>RPLNamReply: Lista de nombres recibidos pero no el nombre de canal.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="368"/>
        <source>Erroneous nickname: %1</source>
        <translation>Apodo erróneo: %1</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="370"/>
        <source> (%1)</source>
        <translation> (%1)</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="404"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="511"/>
        <source>New topic set by user %1:
%2</source>
        <translation>Nuevo tema establecido por el usuario %1:
%2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="530"/>
        <source>IRCResponseParser: Type &apos;%1&apos; was recognized but there has been no parse code implemented for it.(yep, it&apos;s a bug in the application!)</source>
        <translation>IRCResponseParser: Se ha reconocido el tipo &apos;%1&apos; pero no se ha implementado ningún código de análisis para este.(sip, ¡esto es un error en la aplicación!)</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="589"/>
        <source>[%1]: %2</source>
        <translation>[%1]: %2</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="591"/>
        <source>Type &apos;%1&apos; was incorrectly parsed in PrivMsg block.</source>
        <translation>El tipo &apos;%1&apos; se analizó incorrectamente en el bloque PrivMsg.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="606"/>
        <source>MODE flags string from IRC server are incorrect: &quot;%1&quot;. Information for channel &quot;%2&quot; might not be correct anymore.</source>
        <translation>Las cadenas de indicadores MODE del servidor IRC son incorrectas: &quot;%1&quot;. Es posible que la información para el canal &quot;%2&quot; ya no sea correcta.</translation>
    </message>
    <message>
        <location filename="../irc/ircresponseparser.cpp" line="638"/>
        <source>IRCResponseParser::parseUserModeMessage(): wrong FlagMode. Information for channel &quot;%2&quot; might not be correct anymore.</source>
        <translation>IRCResponseParser :: parseUserModeMessage (): FlagMode incorrecto. Es posible que la información para el canal &quot;%2&quot; ya no sea correcta.</translation>
    </message>
</context>
<context>
    <name>IRCSocketSignalsAdapter</name>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="752"/>
        <source>Connected. Sending registration messages.</source>
        <translation>Conectado. Enviando mensajes de registro.</translation>
    </message>
    <message>
        <location filename="../irc/ircnetworkadapter.cpp" line="772"/>
        <source>IRC: Disconnected from network %1</source>
        <translation>IRC: Desconectado de la red %1</translation>
    </message>
</context>
<context>
    <name>ImportantMessagesWidget</name>
    <message>
        <location filename="../gui/widgets/importantmessageswidget.ui" line="40"/>
        <source>Clear</source>
        <translation>Despejar</translation>
    </message>
</context>
<context>
    <name>IwadAndWadsPickerDialog</name>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.cpp" line="65"/>
        <source>Doomseeker - browse executable</source>
        <translation>Doomseeker - buscar ejecutable</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="22"/>
        <source>Executable:</source>
        <translation>Ejecutable:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="39"/>
        <source>IWAD:</source>
        <translation>IWAD:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="61"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadandwadspickerdialog.ui" line="72"/>
        <source>Additional WADs and files:</source>
        <translation>WADs y archivos adicionales:</translation>
    </message>
</context>
<context>
    <name>IwadPicker</name>
    <message>
        <location filename="../gui/createserver/iwadpicker.cpp" line="74"/>
        <source>Doomseeker - select IWAD</source>
        <translation>Doomseeker - selecciona IWAD</translation>
    </message>
    <message>
        <location filename="../gui/createserver/iwadpicker.ui" line="42"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
</context>
<context>
    <name>JoinCommandLineBuilder</name>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="116"/>
        <source>Demo set for recording, but couldn&apos;t prepare its save path.

%1</source>
        <translation>Demo lista para grabar, pero no se pudo preparar la ruta de guardado.

%1</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="133"/>
        <source>This server is still refreshing.
Please wait until it is finished.</source>
        <translation>Este servidor aún se está refrescando.
Espere hasta que termine.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="134"/>
        <source>Attempted to obtain a join command line for a &quot;%1&quot; server that is under refresh.</source>
        <translation>Se ha intentado obtener una línea de comando para unirse a un servidor &quot;%1&quot; que está refrescando.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="141"/>
        <source>Data for this server is not available.
Operation failed.</source>
        <translation>La información para este servidor no está disponible.
Operación fallida.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="142"/>
        <source>Attempted to obtain a join command line for an unknown server &quot;%1&quot;</source>
        <translation>Se ha intentado obtener una línea de comando para unirse a un servidor desconocido &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="183"/>
        <source>Unknown error.</source>
        <translation>Error desconocido.</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="187"/>
        <source>Error when obtaining join parameters for server &quot;%1&quot;, game &quot;%2&quot;: %3</source>
        <translation>Error al obtener los parámetros para unirse al servidor &quot;%1&quot;, juego &quot;%2&quot;: %3</translation>
    </message>
    <message>
        <location filename="../joincommandlinebuilder.cpp" line="358"/>
        <source>Game installation failure</source>
        <translation>Error de instalación del juego</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../fileutils.cpp" line="175"/>
        <source>Failed to remove: %1</source>
        <translation>Error al eliminar: %1</translation>
    </message>
</context>
<context>
    <name>LogDirectoryPicker</name>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.cpp" line="76"/>
        <source>Doomseeker - select Log path</source>
        <translation>Doomseeker - seleccione la ruta de registro</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="35"/>
        <source>This path could not be found. It will not be used.</source>
        <translation>No se ha encontrado la ruta. No se usarà.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="51"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/logdirectorypicker.ui" line="64"/>
        <source>Enabled</source>
        <translation>habilitado</translation>
    </message>
</context>
<context>
    <name>LogDock</name>
    <message>
        <location filename="../gui/logdock.ui" line="20"/>
        <source>Log</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location filename="../gui/logdock.ui" line="67"/>
        <source>Copy all to clipboard</source>
        <translation>Copiar todo al portapapeles</translation>
    </message>
    <message>
        <location filename="../gui/logdock.ui" line="74"/>
        <source>Clear</source>
        <translation>Limpiar</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../main.cpp" line="457"/>
        <source>Doomseeker startup error</source>
        <translation>Error de inicio de Doomseeker</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="240"/>
        <source>Init finished.</source>
        <translation>Init finalizado.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="255"/>
        <source>Doomseeker - Updates Install Failure</source>
        <translation>Doomseeker - Instalación de actualizaciones fallas</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="301"/>
        <source>Failed to open file &apos;%1&apos;.</source>
        <translation>Error al abrir el archivo &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="309"/>
        <source>Failed to open stdout.</source>
        <translation>Error al abrir stdout.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="319"/>
        <source>Dumping version info to file in JSON format.</source>
        <translation>Depositando la información de la versión al archivo en formato JSON.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="331"/>
        <source>Preparing GUI.</source>
        <translation>Preparando GUI.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="344"/>
        <source>Starting Create Game box.</source>
        <translation>Iniciando la ventana de Creación del Juego.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="352"/>
        <source>Starting RCon client.</source>
        <translation>Iniciando el cliente RCon.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="367"/>
        <source>None of the currently loaded game plugins supports RCon.</source>
        <translation>Ninguno de los complementos cargados actualmente admite RCon.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="369"/>
        <source>Doomseeker RCon</source>
        <translation>Doomseeker RCon</translation>
    </message>
    <message>
        <source>Couldn&apos;t find specified plugin: </source>
        <translation type="vanished">No se pudo encontrar el complemento especificado: </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="383"/>
        <source>Couldn&apos;t find the specified plugin: </source>
        <translation>No se pudo encontrar el complemento especificado: </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="393"/>
        <source>Plugin does not support RCon.</source>
        <translation>El complemento no es compatible con RCon.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="410"/>
        <source>Loading extra CA certificates from &apos;%1&apos;.</source>
        <translation>Cargando certificados CA adicionales de &apos;%1&apos;.</translation>
    </message>
    <message numerus="yes">
        <location filename="../main.cpp" line="415"/>
        <source>Appending %n extra CA certificate(s).</source>
        <translation>
            <numerusform>Anexando un certificado CA adicional.</numerusform>
            <numerusform>Anexando %n certificados CA adicionales.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="427"/>
        <source>Running in the portable mode. Forcing current directory to: %1</source>
        <translation>Ejecutando en modo portable. Forzando el directorio actual a: %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="431"/>
        <source>Forcing the current directory failed! Path detection may misbehave.</source>
        <translation>¡El cambio forzado de directorio ha fallado! La detección de rutas puede fallar.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="449"/>
        <source>Doomseeker will not run because some directories cannot be used properly.
</source>
        <translation>Doomseeker no se ejecutará porque algunos directorios no se pueden usar correctamente.
</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="481"/>
        <source>Initializing IP2C database.</source>
        <translation>Inicializando la base de datos IP2C.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="489"/>
        <source>Initializing IRC configuration file.</source>
        <translation>Inicializando el archivo de configuración de IRC.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="507"/>
        <source>Loading translations definitions</source>
        <translation>Cargando definiciones de traducción</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="515"/>
        <source>Loading translation &quot;%1&quot;.</source>
        <translation>Cargando la traducción &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="519"/>
        <source>Translation loaded.</source>
        <translation>Traducción cargada.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="523"/>
        <source>Failed to load translation.</source>
        <translation>Error al cargar la traducción.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="530"/>
        <source>Initializing configuration file.</source>
        <translation>Inicializando archivo de configuración.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="539"/>
        <source>Could not get an access to the settings directory. Configuration will not be saved.</source>
        <translation>No se pudo obtener acceso al directorio de configuraciones. La configuración no se guardará.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="558"/>
        <source>Initializing passwords configuration file.</source>
        <translation>Inicializando el archivo de configuración de contraseñas.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="571"/>
        <source>Initializing configuration for plugins.</source>
        <translation>Inicializando la configuración para complementos.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="725"/>
        <source>doomseeker: `--connect`, `--create-game` and `--rcon` are mutually exclusive</source>
        <translation>doomseeker: `--connect`, `--create-game` y `--rcon` se excluyen entre sí</translation>
    </message>
    <message>
        <source>doomseeker: expected one argument in option %1</source>
        <translation type="vanished">doomseeker: se esperaba un argumento en la opción %1</translation>
    </message>
    <message>
        <source>doomseeker: expected two arguments in option %1</source>
        <translation type="vanished">doomseeker: se esperaban dos argumentos en la opción %1</translation>
    </message>
    <message>
        <source>Available command line parameters:
</source>
        <translation type="vanished">Parámetros de línea de comandos disponibles:
</translation>
    </message>
    <message>
        <source>doomseeker: unrecognized option &apos;%1&apos;</source>
        <translation type="vanished">doomseeker: opción no reconocida &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="738"/>
        <source>Starting refreshing thread.</source>
        <translation>Iniciando hilo de refresco.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/mainwindow.cpp" line="412"/>
        <source>Doomseeker - Auto Update</source>
        <translation>Doomseeker - Actualización automática</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="413"/>
        <source>Update is already in progress.</source>
        <translation>La actualización ya está en progreso.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="422"/>
        <source>Removing old update packages from local temporary space.</source>
        <translation>Eliminando paquetes de actualización antiguos del espacio temporal local.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="427"/>
        <source>Checking for updates...</source>
        <translation>Comprobando actualizaciones...</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="636"/>
        <source>All WADs found</source>
        <translation>Todos los WADs encontrados</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="636"/>
        <source>All of the WADs used by this server are present.</source>
        <translation>Todos los WAD utilizados por este servidor están presentes.</translation>
    </message>
    <message>
        <source>Doomseeker needs to restart to be able to apply some changes</source>
        <translation type="vanished">Doomseeker necesita reiniciarse para poder aplicar los cambios</translation>
    </message>
    <message>
        <source>Restart needed</source>
        <translation type="vanished">Reinicio necesario</translation>
    </message>
    <message>
        <source>Doomseeker is unable to proceed with the refresh operation because the following problem has occured:

</source>
        <translation type="vanished">Doomseeker no puede continuar con la operación de actualización porque se ha producido el siguiente problema:

</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="717"/>
        <source>Plugins are missing from the &quot;engines/&quot; directory.</source>
        <translation>Los complementos faltan en el directorio &quot;engines/&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="719"/>
        <source>No master servers are enabled in the &quot;Query&quot; menu.</source>
        <translation>No hay servidores maestros habilitados en el menú &quot;Consulta&quot;.</translation>
    </message>
    <message>
        <source>Unknown error occured.</source>
        <translation type="vanished">Se produjo un error desconocido.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="724"/>
        <source>Doomseeker - refresh problem</source>
        <translation>Doomseeker - problema de actualización</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="730"/>
        <source>Total refresh initialized!</source>
        <translation>Actualización total inicializada!</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="740"/>
        <source>Warning: No master servers were enabled for this refresh. Check your Query menu or &quot;engines/&quot; directory.</source>
        <translation>Advertencia: No se habilitaron servidores maestros para esta actualización. Verifique su menú de consulta o el directorio &quot;engines /&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="803"/>
        <source>Auto Updater:</source>
        <translation>Actualizador automático:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="818"/>
        <source>Abort update.</source>
        <translation>Cancelar actualización.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="830"/>
        <source>IP2C Update</source>
        <translation>Actualización de IP2C</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="841"/>
        <source>&amp;IRC</source>
        <translation>&amp;IRC</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="842"/>
        <source>CTRL+I</source>
        <translation>CTRL+I</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="857"/>
        <source>&amp;Log</source>
        <translation>&amp;Registro</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="858"/>
        <source>CTRL+L</source>
        <translation>CTRL+L</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="874"/>
        <source>Servers</source>
        <translation>Servidores</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="887"/>
        <source>Server &amp;details</source>
        <translation>&amp;Detalles Servidor</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="888"/>
        <source>CTRL+D</source>
        <translation>CTRL+D</translation>
    </message>
    <message>
        <source>Server &amp;filter</source>
        <translation type="vanished">&amp;Filtro de Servidores</translation>
    </message>
    <message>
        <source>CTRL+F</source>
        <translation type="vanished">CTRL+F</translation>
    </message>
    <message>
        <source>Warning: 
Doomseeker failed to detect any plugins.
While the core application will still work its functionality is going to be limited.
One of the proper locations for plugin modules is the &quot;engines/&quot; directory.
</source>
        <translation type="vanished">Advertencia:
Doomseeker no pudo detectar ningún complemento.
Si bien la aplicación seguirá funcionando, su funcionalidad será limitada.
Una de las ubicaciones adecuadas para los complementos es el directorio &quot;engines/&quot;.
</translation>
    </message>
    <message>
        <source>Doomseeker - plugin load failure</source>
        <translation type="vanished">Doomseeker - fallo al cargar un complemento</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="239"/>
        <source>&amp;Buddies</source>
        <translation>&amp;Compañeros</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="240"/>
        <source>CTRL+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="670"/>
        <source>Doomseeker needs to be restarted for some changes to be applied.</source>
        <translation>Doomseeker necesita reiniciarse para que algunos de los cambios se apliquen.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1001"/>
        <source>Master server for %1: %2</source>
        <translation>Servidor maestro para %1: %2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1005"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1014"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1045"/>
        <location filename="../gui/mainwindow.cpp" line="1053"/>
        <source>Help error</source>
        <translation>Error en ayuda</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1045"/>
        <source>No help found.</source>
        <translation>No se encontró ayuda.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1053"/>
        <source>Failed to open URL:
%1</source>
        <translation>Error al abrir la URL:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1136"/>
        <source>Welcome to Doomseeker</source>
        <translation>Bienvenido a Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1137"/>
        <source>Before you start browsing for servers, please ensure that Doomseeker is properly configured.</source>
        <translation>Antes de comenzar a buscar servidores, asegúrese de que Doomseeker esté configurado correctamente.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1154"/>
        <source>Program update detection &amp; download finished with status: [%1] %2</source>
        <translation>La detección y descarga de la actualización del programa terminó con el estado: [%1] %2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1164"/>
        <source>Updates will be installed on next program start.</source>
        <translation>Las actualizaciones se instalarán en el próximo inicio del programa.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1170"/>
        <source>Update channel was changed during update process. Discarding update.</source>
        <translation>El canal de actualización cambió durante el proceso de actualización. Descartando la actualización.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1209"/>
        <source>Query on startup warning: No master servers are enabled in the Query menu.</source>
        <translation>Advertencia de consulta al inicio: No hay servidores maestros habilitados en el menú Consulta.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1233"/>
        <source>Doomseeker critical error</source>
        <translation>Error crítico de Doomseeker</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1475"/>
        <source>Failed to build the command line:
%1</source>
        <translation>La línea de comandos no se puede construir:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1226"/>
        <source>Doomseeker was unable to find any plugin libraries.
Although the application will still work it will not be possible to fetch any server info or launch any game.

Please check if there are any files in &quot;engines/&quot; directory.
To fix this problem you may try downloading Doomseeker again from the site specified in the Help|About box and reinstalling Doomseeker.</source>
        <translation>Doomseeker no pudo encontrar ninguna biblioteca de complementos.
Aunque la aplicación seguirá funcionando, no será posible obtener ninguna información de servidores o iniciar ningún juego.

Compruebe si hay archivos en el directorio &quot;engines/&quot;.
Para solucionar este problema, puede intentar descargar Doomseeker nuevamente desde el sitio especificado en el cuadro Ayuda | Acerca de y volver a instalar Doomseeker.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="713"/>
        <source>Doomseeker is unable to proceed with the refresh operation because the following problem has occurred:

</source>
        <translation>Doomseeker no puede continuar con la operación de actualización porque se ha producido el siguiente problema:

</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="721"/>
        <source>Unknown error occurred.</source>
        <translation>Se produjo un error desconocido.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1273"/>
        <source>Querying...</source>
        <translation>Consultando...</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1282"/>
        <source>Done</source>
        <translation>Hecho</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1364"/>
        <source>Main Toolbar</source>
        <translation>Barra de herramientas principal</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1369"/>
        <source>Get Servers</source>
        <translation>Obtener servidores</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1403"/>
        <source>Search:</source>
        <translation>Buscar:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1474"/>
        <source>Doomseeker - show join command line</source>
        <translation>Doomseeker - mostrar comando para unirse</translation>
    </message>
    <message>
        <source>Command line cannot be built:
%1</source>
        <translation type="vanished">La línea de comandos no se puede construir:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1489"/>
        <source>Update installation problem:
%1</source>
        <translation>Problema de instalación de actualización:
%1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1492"/>
        <source>Update installation failed.</source>
        <translation>Error al instalar la actualización.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1493"/>
        <location filename="../gui/mainwindow.cpp" line="1502"/>
        <source>Doomseeker - Auto Update problem</source>
        <translation>Doomseeker - Problema de actualización automática</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1494"/>
        <source>%1

Remaining updates have been discarded.</source>
        <translation>%1

Las actualizaciones restantes se han descartado.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1501"/>
        <source>Update install problem:
%1

Remaining updates have been discarded.</source>
        <translation>Problema de la actualización:
%1

Las actualizaciones restantes se han descartado.</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1584"/>
        <source>Generic servers: %1
</source>
        <translation>Servidores genéricos: %1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1585"/>
        <source>Custom servers: %1
</source>
        <translation>Servidores personalizados: %1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1586"/>
        <source>LAN servers: %1
</source>
        <translation>Servidores LAN: %1
</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1587"/>
        <source>Human players: %1</source>
        <translation>Jugadores humanos: %1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="1599"/>
        <source>Finished refreshing. Servers on the list: %1 (+%2 custom, +%3 LAN). Players: %4.</source>
        <translation>Actualización finalizada. Servidores en la lista:%1 (+%2 personalizados, +%3 LAN). Jugadores: %4.</translation>
    </message>
</context>
<context>
    <name>MainWindowWnd</name>
    <message>
        <location filename="../gui/mainwindow.ui" line="14"/>
        <source>Doomseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="350"/>
        <source>&amp;Configure</source>
        <translation>&amp;Configurar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="353"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="361"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="369"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="372"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="383"/>
        <source>Server Info</source>
        <translation>Información del servidor</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="386"/>
        <source>I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="395"/>
        <source>&amp;Wadseeker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="398"/>
        <source>Ctrl+Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="406"/>
        <source>&amp;Buddies</source>
        <translation>&amp;Compañeros</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="409"/>
        <source>Ctrl+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="418"/>
        <source>&amp;Create game</source>
        <translation>&amp;Crear juego</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="421"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="429"/>
        <source>&amp;Log</source>
        <translation>&amp;Registro</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="432"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="437"/>
        <source>&amp;Help (Online)</source>
        <translation>&amp;Ayuda (Online)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="440"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="449"/>
        <source>&amp;Update IP2C</source>
        <translation>&amp;Actualizar IP2C</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="457"/>
        <source>&amp;IRC</source>
        <translation>&amp;IRC</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="460"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="465"/>
        <source>&amp;IRC options</source>
        <translation>Opciones &amp;IRC</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="468"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="473"/>
        <source>About &amp;Qt</source>
        <translation>Acerca de &amp;Qt</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="488"/>
        <source>&amp;Record demo</source>
        <translation>&amp;Grabar demo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="497"/>
        <source>&amp;Demo manager</source>
        <translation>&amp;Administrador de &amp;demos</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="500"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="509"/>
        <source>&amp;Check for updates</source>
        <translation>&amp;Buscar actualizaciones</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="518"/>
        <source>&amp;Program args</source>
        <translation>&amp;Argumentos del programa</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="523"/>
        <source>Install &amp;Freedoom</source>
        <translation>Instalar &amp;Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="78"/>
        <source>New updates are available:</source>
        <translation>Nuevas actualizaciones están disponibles:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="97"/>
        <source>Download &amp;&amp; Install</source>
        <translation>Descargar y Instalar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="120"/>
        <source>Discard</source>
        <translation>Descartar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="185"/>
        <source>Updates have been downloaded:</source>
        <translation>Las actualizaciones han sido descargadas:</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="204"/>
        <source>Restart &amp;&amp; Install now</source>
        <translation>Reiniciar y Instalar ahora</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="223"/>
        <source>Restart &amp;&amp; Install later</source>
        <translation>Reiniciar y Instalar más tarde</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="252"/>
        <source>Server filter is applied</source>
        <translation>Los filtros de servidor están aplicados</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="297"/>
        <source>&amp;Options</source>
        <translation>&amp;Opciones</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="304"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="315"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="329"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="334"/>
        <source>&amp;Query</source>
        <translation>&amp;Consulta</translation>
    </message>
</context>
<context>
    <name>MapListPanel</name>
    <message>
        <source>Add from loaded wads</source>
        <translation type="vanished">Añadir desde los WADs cargados</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="51"/>
        <source>Add from loaded WADs</source>
        <translation>Añadir desde los WADs cargados</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="64"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="75"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.ui" line="91"/>
        <source>Random map rotation</source>
        <translation>Rotación aleatoria del mapa</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistpanel.cpp" line="57"/>
        <source>The current map isn&apos;t present on the map list. The game may misbehave.</source>
        <translation>El mapa actual no se encuentra en la lista de mapas. El juego puede comportarse de forma extraña.</translation>
    </message>
</context>
<context>
    <name>MapListSelector</name>
    <message>
        <location filename="../gui/createserver/maplistselector.cpp" line="188"/>
        <source>Invert selection</source>
        <translation>Invertir selección</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="26"/>
        <source>Doomseeker - Select Maps</source>
        <translation>Doomseeker - Seleccione Mapas</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="35"/>
        <source>All selected maps will get added to the map list</source>
        <translation>Todos los mapas seleccionados se agregarán a la lista de mapas</translation>
    </message>
    <message>
        <source>Reading wads...</source>
        <translation type="vanished">Leyendo WADs ...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="71"/>
        <source>Reading WADs...</source>
        <translation>Leyendo WADs ...</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="86"/>
        <source>Select All</source>
        <translation>Seleccionar todo</translation>
    </message>
    <message>
        <location filename="../gui/createserver/maplistselector.ui" line="121"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>MasterClient</name>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="108"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="156"/>
        <source>Could not fetch a new server list from the master because not enough time has passed.</source>
        <translation>No se pudo obtener una nueva lista de servidores de master porque no pasó suficiente tiempo.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="161"/>
        <source>Bad response from master server.</source>
        <translation>Mala respuesta del servidor master.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="166"/>
        <source>Could not fetch a new server list. The protocol you are using is too old. An update may be available.</source>
        <translation>No se pudo obtener una nueva lista de servidores. El protocolo que estás usando es muy viejo. Una actualización puede estar disponible.</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="263"/>
        <source>Reloading master server results from cache for %1!</source>
        <translation>¡Recargando los resultados del servidor master de la caché para %1!</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="354"/>
        <source>Master server timeout</source>
        <translation>Tiempo de espera para servidor master</translation>
    </message>
    <message>
        <location filename="../serverapi/masterclient.cpp" line="354"/>
        <source>Connection timeout (%1:%2).</source>
        <translation>Tiempo de espera de conexión (%1:%2).</translation>
    </message>
</context>
<context>
    <name>MiscServerSetupPanel</name>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="34"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="44"/>
        <source>E-mail:</source>
        <translation>E-mail:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="54"/>
        <source>Connect password:</source>
        <translation>Contraseña de conexión:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="68"/>
        <source>Join password:</source>
        <translation>Contraseña de unión:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="82"/>
        <source>RCon password:</source>
        <translation>Contraseña de RCon:</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="99"/>
        <source>Hide passwords</source>
        <translation>Ocultar contraseñas</translation>
    </message>
    <message>
        <location filename="../gui/createserver/miscserversetuppanel.ui" line="111"/>
        <source>MOTD:</source>
        <translation>MOTD:</translation>
    </message>
</context>
<context>
    <name>MissingWadsDialog</name>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="14"/>
        <source>Doomseeker - files are missing</source>
        <translation>Doomseeker - faltan archivos</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="41"/>
        <source>You don&apos;t have all the files required by this server and an instance of Wadseeker is already running.</source>
        <translation>No tiene todos los archivos requeridos por este servidor y ya se está ejecutando Wadseeker.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="57"/>
        <source>Press &apos;Ignore&apos; to join anyway.</source>
        <translation>Presione &apos;Ignorar&apos; para unirse de todos modos.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="103"/>
        <source>These files belong to a commercial game or are otherwise blocked from download:</source>
        <translation>Estos archivos pertenecen a un juego comercial o están bloqueados para su descarga:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="116"/>
        <location filename="../gui/missingwadsdialog.ui" line="195"/>
        <source>&lt;files&gt;</source>
        <translation>&lt;archivos&gt;</translation>
    </message>
    <message>
        <source>Make sure that this file is in one of the paths specified in Options -&gt; File Paths.

If you don&apos;t have this file, and it belongs to a commercial game, you need to purchase the game associated with this file. Wadseeker will not download commercial IWADs or modifications.</source>
        <translation type="vanished">Asegúrese de que este archivo esté en una de las rutas especificadas en Opciones -&gt; Rutas de archivos.

Si no tiene este archivo, y pertenece a un juego comercial, necesita comprar el juego asociado con este archivo. Wadseeker no descargará IWADs o modificaciones comerciales.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="135"/>
        <source>Make sure that this file is in one of the paths specified in Options -&gt; File paths.

If you don&apos;t have this file, and it belongs to a commercial game, you need to purchase the game associated with this file. Wadseeker will not download commercial IWADs or modifications.</source>
        <translation>Asegúrese de que este archivo esté en una de las rutas especificadas en Opciones -&gt; Rutas de archivos.

Si no tiene este archivo, y pertenece a un juego comercial, necesita comprar el juego asociado con este archivo. Wadseeker no descargará IWADs o modificaciones comerciales.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="147"/>
        <source>You can also install a free replacement IWAD with &quot;Install Freedoom&quot; button.</source>
        <translation>También puede instalar un IWAD de reemplazo gratuito con el botón &quot;Instalar Freedoom&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="182"/>
        <source>Following files can be downloaded:</source>
        <translation>Los siguientes archivos se pueden descargar:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="236"/>
        <source>Incompatible files:</source>
        <translation>Archivos incompatibles:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="243"/>
        <source>(Your current files will be overwritten)</source>
        <translation>(Sus archivos actuales serán sobreescritos)</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="281"/>
        <source>Optional files:</source>
        <translation>Archivos opcionales:</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="304"/>
        <source>Do you want Wadseeker to find the missing WADs?</source>
        <translation>¿Quieres que Wadseeker encuentre los WADs que faltan?</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="311"/>
        <source>Alternatively use ignore to connect anyways.</source>
        <translation>Alternativamente, presione &quot;Ignorar&quot; para conectarse de todos modos.</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.ui" line="342"/>
        <source>Install Freedoom</source>
        <translation>Instalar Freedoom</translation>
    </message>
    <message>
        <location filename="../gui/missingwadsdialog.cpp" line="63"/>
        <source>Install</source>
        <translation>Instalar</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">Ignorar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../gui/passworddlg.ui" line="32"/>
        <source>Connection Password</source>
        <translation>Contraseña de conexión</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="38"/>
        <source>This server requires a password in order to connect, please enter this password below.</source>
        <translation>Este servidor requiere una contraseña para conectarse, por favor ingrese la contraseña a continuación.</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="63"/>
        <source>Connect password:</source>
        <translation>Contraseña de conexión:</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="112"/>
        <source>Ingame password:</source>
        <translation>Contraseña en el juego:</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="146"/>
        <source>Hide passwords</source>
        <translation>Ocultar contraseñas</translation>
    </message>
    <message>
        <location filename="../gui/passworddlg.ui" line="156"/>
        <source>Remember password</source>
        <translation>Recordar contraseña</translation>
    </message>
</context>
<context>
    <name>PlayerTable</name>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="210"/>
        <source>BOT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="214"/>
        <source>SPECTATOR</source>
        <translation>ESPECTADOR</translation>
    </message>
    <message>
        <source>Team</source>
        <translation type="vanished">Equipo</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="146"/>
        <source>Bots</source>
        <translation>Bots</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="151"/>
        <source>Spectators</source>
        <translation>Espectadores</translation>
    </message>
    <message numerus="yes">
        <location filename="../serverapi/tooltips/playertable.cpp" line="238"/>
        <source>(and %n more ...)</source>
        <translation>
            <numerusform>(y %n más...)</numerusform>
            <numerusform>(y %n más...)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="253"/>
        <source>Player</source>
        <translation>Jugador</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="254"/>
        <source>Score</source>
        <translation>Puntuación</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="255"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="256"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/playertable.cpp" line="287"/>
        <source>Team %1</source>
        <translation>Equipo %1</translation>
    </message>
</context>
<context>
    <name>PlayersDiagram</name>
    <message>
        <location filename="../gui/helpers/playersdiagram.cpp" line="64"/>
        <source>Numeric</source>
        <translation>Numérico</translation>
    </message>
    <message>
        <location filename="../gui/helpers/playersdiagram.cpp" line="65"/>
        <source>Blocks</source>
        <translation>Bloques</translation>
    </message>
</context>
<context>
    <name>PluginUrlHandler</name>
    <message>
        <location filename="../connectionhandler.cpp" line="226"/>
        <source>Connect to server</source>
        <translation>Conectar con el servidor</translation>
    </message>
    <message>
        <location filename="../connectionhandler.cpp" line="227"/>
        <source>Do you want to connect to the server at %1?</source>
        <translation>¿Desea conectarse al servidor %1?</translation>
    </message>
</context>
<context>
    <name>ProgramArgsHelpDialog</name>
    <message>
        <location filename="../gui/programargshelpdialog.ui" line="14"/>
        <source>Doomseeker - Program arguments</source>
        <translation>Doomseeker - Argumentos del programa</translation>
    </message>
    <message>
        <location filename="../gui/programargshelpdialog.ui" line="20"/>
        <source>These arguments can be passed to Doomseeker&apos;s executable:</source>
        <translation>Estos argumentos se pueden pasar al ejecutable de Doomseeker:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../serverapi/tooltips/gameinfotip.cpp" line="36"/>
        <source>Unlimited</source>
        <translation>Ilimitado</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="404"/>
        <source>Cooperative</source>
        <translation>Cooperativo</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="409"/>
        <source>Deathmatch</source>
        <translation>Deathmatch</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="414"/>
        <source>Team DM</source>
        <translation>DM por equipos</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="419"/>
        <source>CTF</source>
        <translation>CTF</translation>
    </message>
    <message>
        <location filename="../serverapi/serverstructs.cpp" line="424"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="224"/>
        <source>Skipping loading of forbidden plugin: %1</source>
        <translation>Saltando la carga de un complemento prohibido: %1</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="67"/>
        <source>plugin ABI version mismatch; plugin: %1, Doomseeker: %2</source>
        <translation>falta de coincidencia de la versión ABI del complemento; complemento: %1, Doomseeker: %2</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="73"/>
        <source>plugin doesn&apos;t report its ABI version</source>
        <translation>el complemento no proporciona su versión ABI</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="75"/>
        <source>Cannot load plugin %1, reason: %2.</source>
        <translation>No se puede cargar el complemento %1, Motivo: %2.</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="94"/>
        <source>Loaded plugin: &quot;%1&quot;!</source>
        <translation>Complemento cargado: &quot;%1&quot;!</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="99"/>
        <source>Failed to open plugin: %1</source>
        <translation>Error al abrir el complemento: %1</translation>
    </message>
    <message>
        <location filename="../plugins/pluginloader.cpp" line="182"/>
        <source>Failed to locate plugins.</source>
        <translation>Error al localizar complementos.</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="93"/>
        <source>Add plugin&apos;s IRC channel?</source>
        <translation>¿Agregar el canal IRC del complemento?</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="94"/>
        <source>Would you like the %1 plugin to add its channel to %2&apos;s auto join?</source>
        <translation>¿Desea que el complemento %1 agregue su canal al unirse automáticamente a %2?</translation>
    </message>
    <message>
        <location filename="../irc/configuration/ircconfig.cpp" line="166"/>
        <source>Setting IRC INI file: %1</source>
        <translation>Configurando el archivo IRC INI: %1</translation>
    </message>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="114"/>
        <source>DoomseekerConfig.iniSectionForPlugin(): empty plugin name has been specified, returning dummy IniSection.</source>
        <translation>DoomseekerConfig.iniSectionForPlugin(): el nombre del complemento ha sido especificado vacío, devolviendo el IniSection ficticio.</translation>
    </message>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="120"/>
        <source>DoomseekerConfig.iniSectionForPlugin(): plugin name is invalid: %1</source>
        <translation>DoomseekerConfig.iniSectionForPlugin(): el nombre del complemento no es válido: %1</translation>
    </message>
    <message>
        <location filename="../configuration/doomseekerconfig.cpp" line="213"/>
        <source>Setting INI file: %1</source>
        <translation>Configurando archivo INI: %1</translation>
    </message>
    <message>
        <source>parent node is not a directory: %1</source>
        <translation type="vanished">nodo padre no es un directorio: %1</translation>
    </message>
    <message>
        <source>lack of necessary permissions to the parent directory: %1</source>
        <translation type="vanished">falta de permisos necesarios para el directorio padre: %1</translation>
    </message>
    <message>
        <source>cannot create directory</source>
        <translation type="vanished">no se puede crear el directorio</translation>
    </message>
</context>
<context>
    <name>RconPasswordDialog</name>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="20"/>
        <source>Connect to Remote Console</source>
        <translation>Conectarse a consola remota</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="26"/>
        <source>Connection</source>
        <translation>Conexión</translation>
    </message>
    <message>
        <source>Source Port:</source>
        <translation type="vanished">Juego:</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="32"/>
        <source>Game:</source>
        <translation>Juego:</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="42"/>
        <source>Server Address:</source>
        <translation>Dirección del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="55"/>
        <source>Please enter your remote console password.</source>
        <translation>Por favor ingrese la contraseña de su consola remota.</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="67"/>
        <source>Hide password</source>
        <translation>Ocultar contraseña</translation>
    </message>
    <message>
        <location filename="../gui/rconpassworddialog.ui" line="70"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
</context>
<context>
    <name>RemoteConsole</name>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="68"/>
        <source>No RCon support</source>
        <translation>Sin soporte RCon</translation>
    </message>
    <message>
        <source>The selected source port has no RCon support.</source>
        <translation type="vanished">El source port seleccionado no tiene soporte para RCon.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="68"/>
        <source>The selected game has no RCon support.</source>
        <translation>El juego seleccionado no tiene soporte para RCon.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="83"/>
        <source>RCon failure</source>
        <translation>Fallo de RCon</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="83"/>
        <source>Failed to create RCon protocol for the server.</source>
        <translation>Error al crear el protocolo RCon para el servidor.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="110"/>
        <source>RCon Failure</source>
        <translation>Fallo RCon</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="111"/>
        <source>Failed to connect RCon to server %1:%2</source>
        <translation>Error al conectar RCon al servidor %1:%2</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="134"/>
        <source> - Remote Console</source>
        <translation> - Consola remota</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="146"/>
        <source>Invalid Password</source>
        <translation>Contraseña invalida</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.cpp" line="146"/>
        <source>The password you entered appears to be invalid.</source>
        <translation>La contraseña que ingresaste parece ser inválida.</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="14"/>
        <source>Remote Console</source>
        <translation>Consola remota</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="90"/>
        <source>Disconnect</source>
        <translation>Desconectar</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="21"/>
        <source>Scoreboard</source>
        <translation>Marcador</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="52"/>
        <source>Player Name</source>
        <translation>Nombre del jugador</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="63"/>
        <source>Console</source>
        <translation>Consola</translation>
    </message>
    <message>
        <location filename="../gui/remoteconsole.ui" line="81"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <location filename="../serverapi/server.cpp" line="148"/>
        <source>&lt;&lt; ERROR &gt;&gt;</source>
        <translation>&lt;&lt; ERROR &gt;&gt;</translation>
    </message>
    <message>
        <location filename="../serverapi/server.cpp" line="263"/>
        <source>client</source>
        <translation>cliente</translation>
    </message>
    <message>
        <location filename="../serverapi/server.cpp" line="296"/>
        <source>Undefined</source>
        <translation>Indefinido</translation>
    </message>
</context>
<context>
    <name>ServerConsole</name>
    <message>
        <location filename="../gui/widgets/serverconsole.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
</context>
<context>
    <name>ServerDetailsDock</name>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="14"/>
        <source>Server details</source>
        <translation>Detalles del servidor</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="47"/>
        <source>Server:</source>
        <translation>Servidor:</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="54"/>
        <source>&lt;server&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="70"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="96"/>
        <source>Players</source>
        <translation>Jugadores</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.ui" line="122"/>
        <source>Flags</source>
        <translation>Indicadores</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="72"/>
        <source>No server selected.</source>
        <translation>Ningún servidor seleccionado.</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="92"/>
        <source>&lt;b&gt;Address:&lt;/b&gt; %1
</source>
        <translation>&lt;b&gt;Dirección:&lt;/b&gt; %1
</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="105"/>
        <source>No players on this server.</source>
        <translation>No hay jugadores en ese servidor.</translation>
    </message>
    <message>
        <location filename="../gui/serverdetailsdock.cpp" line="111"/>
        <source>DMFlags unknown or no DMFlags set.</source>
        <translation>DMFlags desconocidos o ningún DMFlags establecido.</translation>
    </message>
</context>
<context>
    <name>ServerFilterBuilderMenu</name>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="48"/>
        <source>Build server filter ...</source>
        <translation>Generar filtro de servidor ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="55"/>
        <source>Show only servers with ping lower than %1</source>
        <translation>Mostrar solo servidores con ping inferior a %1</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="59"/>
        <source>Filter by game mode &quot;%1&quot;</source>
        <translation>Filtrar por modo de juego &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="64"/>
        <source>Hide game mode &quot;%1&quot;</source>
        <translation>Ocultar modo de juego &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="68"/>
        <source>Include WAD ...</source>
        <translation>Incluye WAD ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="69"/>
        <source>Exclude WAD ...</source>
        <translation>Excluye WAD ...</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverfilterbuildermenu.cpp" line="85"/>
        <source>Filter by address</source>
        <translation>Filtra por dirección</translation>
    </message>
</context>
<context>
    <name>ServerFilterDock</name>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="26"/>
        <source>Server filter</source>
        <translation>Filtro de servidores</translation>
    </message>
    <message>
        <source>Server Name:</source>
        <translation type="vanished">Nombre del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="112"/>
        <source>Put populated servers on top</source>
        <translation>Poner servidores llenos en la parte superior</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="132"/>
        <source>Filtering enabled</source>
        <translation>Filtrado habilitado</translation>
    </message>
    <message>
        <source>Max. Ping:</source>
        <translation type="vanished">Max. Ping:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="191"/>
        <source>Set &apos;0&apos; to disable.</source>
        <translation>Establecer a &apos;0&apos; para deshabilitar.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="207"/>
        <source>WADs:</source>
        <translation>WADs:</translation>
    </message>
    <message>
        <source>Use &apos;,&apos; (a comma) to separate multiple wads.</source>
        <translation type="obsolete">Utilize &apos;,&apos; (una coma) para separar múltiples WADs.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="221"/>
        <source>Exclude WADs:</source>
        <translation>Excluir WADs:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="214"/>
        <source>Use &apos;,&apos; (a comma) to separate multiple WADs.</source>
        <translation>Utilize &apos;,&apos; (una coma) para separar múltiples WADs.</translation>
    </message>
    <message>
        <source>Servers with WADs on this list won&apos;t be displayed. Use &apos;,&apos; (a comma) to separate multiple WADs.</source>
        <translation type="vanished">Los servidores con WADs en esta lista no se mostrarán. Utilize &apos;,&apos; (una coma) para separar múltiples WADs.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="51"/>
        <location filename="../gui/serverfilterdock.cpp" line="296"/>
        <source>[custom]</source>
        <translation>[customizado]</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="65"/>
        <source>Save the current server filter as a preset.</source>
        <translation>Guardar el filtro de servidores como una nueva configuración.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="82"/>
        <source>Remove the server filter preset.</source>
        <translation>Eliminar la configuración de filtro de servidores.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="95"/>
        <source>Server name:</source>
        <translation>Nombre del servidor:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="184"/>
        <source>Max. ping:</source>
        <translation>Max. Ping:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="228"/>
        <source>Servers with WADs on this list won&apos;t be displayed. Use &apos;,&apos; (a comma) to separate multiple wads.</source>
        <translation>Los servidores con WADs en esta lista no se mostrarán. Utilice &apos;,&apos; (una coma) para separar múltiples WADs.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="235"/>
        <source>Game modes:</source>
        <translation>Modos de juego:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="245"/>
        <source>Exclude game modes:</source>
        <translation>Excluir modos de juego:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="252"/>
        <source>Selected game modes won&apos;t appear on the server list.</source>
        <translation>Los modos de juego seleccionados no aparecerán en la lista de servidores.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="259"/>
        <source>Addresses:</source>
        <translation>Dirección:</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="266"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only servers that match these addresses will be displayed. Multiple addresses can be separated by commas (&lt;span style=&quot; font-weight:600;&quot;&gt;,&lt;/span&gt;). Subnets are supported by using the &lt;span style=&quot; font-weight:600;&quot;&gt;ADDR/N&lt;/span&gt; format where &lt;span style=&quot; font-weight:600;&quot;&gt;N&lt;/span&gt; can be a subnet mask or a numerical value for the subnet mask. For IPv4 the subnet can also be specified by omitting the octets. &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/24&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/255.255.255.0&lt;/span&gt; are all the same.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sólo se mostrarán los servidores que coincidan con estas direcciones. Se pueden separar varias direcciones por comas (&lt;span style=&quot; font-weight:600;&quot;&gt;,&lt;/span&gt;).Las subredes se pueden definir como &lt;span style=&quot; font-weight:600;&quot;&gt;ADDR/N&lt;/span&gt; donde &lt;span style=&quot; font-weight:600;&quot;&gt;N&lt;/span&gt; puede ser una máscara de subred o un valor numérico para la máscara de la subred. Para IPv4 la subred también se puede definir omitiendo bytes finales. &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/24&lt;/span&gt; y &lt;span style=&quot; font-weight:600;&quot;&gt;192.168.0.0/255.255.255.0&lt;/span&gt; son equivalentes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="273"/>
        <source>Show full servers</source>
        <translation>Mostrar servidores llenos</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="283"/>
        <source>Show empty servers</source>
        <translation>Mostrar servidores vacíos</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="293"/>
        <source>Show password protected servers</source>
        <translation>Mostrar servidores protegidos por contraseña</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="303"/>
        <source>Show only valid servers</source>
        <translation>Mostrar solo servidores válidos</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="325"/>
        <source>Show the servers you were banned from.</source>
        <translation>Mostrar los servidores donde has sido baneado.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="328"/>
        <source>Show banned servers</source>
        <translation>Mostrar servidores baneados</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="338"/>
        <source>Show the &quot;Refreshed too soon&quot; servers.</source>
        <translation>Mostrar los servidores &quot;Refrescado demasiado deprisa&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="341"/>
        <source>Show &quot;too soon&quot; servers</source>
        <translation>Mostrar servidores &quot;demasiado deprisa&quot;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="351"/>
        <source>&lt;p&gt;Show the servers that didn&apos;t respond at all or returned a bad response.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Mostrar los servidores que no responden o responden incorrectamente&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="354"/>
        <source>Show not responding servers</source>
        <translation>Mostrar servidores que no responden</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="367"/>
        <source>Show testing servers</source>
        <translation>Mostrar servidores de prueba</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.ui" line="390"/>
        <source>Reset filter</source>
        <translation>Reiniciar filtro</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Despejar</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="151"/>
        <source>Server &amp;filter</source>
        <translation>&amp;Filtro de Servidores</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="152"/>
        <source>CTRL+F</source>
        <translation>CTRL+F</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="296"/>
        <source>[no filter]</source>
        <translation>[sin filtro]</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="348"/>
        <source>Doomseeker - Remove filter preset</source>
        <translation>Doomseeker - Eliminar filtro</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="349"/>
        <source>Are you sure you wish to remove the filter preset &quot;%1&quot;?</source>
        <translation>¿Seguro que quieres eliminar el filtro &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="362"/>
        <source>Doomseeker - Save filter preset</source>
        <translation>Doomseeker - Guardar filtro</translation>
    </message>
    <message>
        <location filename="../gui/serverfilterdock.cpp" line="363"/>
        <source>Enter name for the filter preset (will overwrite if exists):</source>
        <translation>Introduce el nombre para este filtro (si existe, lo sobrescribirá):</translation>
    </message>
</context>
<context>
    <name>ServerList</name>
    <message>
        <location filename="../gui/serverlist.cpp" line="222"/>
        <source>Doomseeker - context menu warning</source>
        <translation>Doomseeker - menú contextual de advertencia</translation>
    </message>
    <message>
        <location filename="../gui/serverlist.cpp" line="223"/>
        <source>Unhandled behavior in ServerList::contextMenuTriggered()</source>
        <translation>Comportamiento no controlado en ServerList::contextMenuTriggered()</translation>
    </message>
</context>
<context>
    <name>ServerListColumns</name>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="55"/>
        <source>Players</source>
        <translation>Jugadores</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="57"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="59"/>
        <source>Server Name</source>
        <translation>Nombre del servidor</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="61"/>
        <source>Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="63"/>
        <source>IWAD</source>
        <translation>IWAD</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="65"/>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="67"/>
        <source>WADs</source>
        <translation>WADs</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistcolumn.cpp" line="69"/>
        <source>Game Type</source>
        <translation>Tipo de juego</translation>
    </message>
</context>
<context>
    <name>ServerListContextMenu</name>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="106"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="107"/>
        <source>Copy Address</source>
        <translation>Copiar Dirección</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="110"/>
        <source>Copy E-Mail</source>
        <translation>Copiar E-Mail</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="113"/>
        <source>Copy URL</source>
        <translation>Copiar URL</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="115"/>
        <source>Copy Name</source>
        <translation>Copiar Nombre</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="126"/>
        <source>Refresh</source>
        <translation>Refrescar</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="127"/>
        <source>Join</source>
        <translation>Unirse</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="128"/>
        <source>Show join command line</source>
        <translation>Mostrar comandos para unirse</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="129"/>
        <source>Find missing WADs</source>
        <translation>Encuentra los WADs que faltan</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="136"/>
        <source>Open URL in browser</source>
        <translation>Abrir URL en el navegador</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="139"/>
        <source>Pin</source>
        <translation>Marcar</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="139"/>
        <source>Unpin</source>
        <translation>Desmarcar</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="155"/>
        <source>Remote console</source>
        <translation>Consola remota</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="162"/>
        <source>Sort additionally ascending</source>
        <translation>Adicionalmente orden ascendente</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="163"/>
        <source>Sort additionally descending</source>
        <translation>Adicionalmente orden descendente</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="168"/>
        <source>Remove additional sorting for column</source>
        <translation>Quitar orden adicional para la columna</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serverlistcontextmenu.cpp" line="171"/>
        <source>Clear additional sorting</source>
        <translation>Borrar orden adicional</translation>
    </message>
</context>
<context>
    <name>ServerListRowHandler</name>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="269"/>
        <source>&lt;ERROR&gt;</source>
        <translation>&lt;ERROR&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="281"/>
        <source>You are banned from this server!</source>
        <translation>¡Has sido baneado de este servidor!</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="340"/>
        <source>&lt;REFRESHING&gt;</source>
        <translation>&lt;REFRESCANDO&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="350"/>
        <source>&lt;NO RESPONSE&gt;</source>
        <translation>&lt;SIN RESPUESTA&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="362"/>
        <source>&lt;Refreshed too soon, wait a while and try again&gt;</source>
        <translation>&lt;Refrescado demasiado pronto, espere un momento e intente de nuevo&gt;</translation>
    </message>
    <message>
        <location filename="../gui/models/serverlistrowhandler.cpp" line="416"/>
        <source>Unknown server response (%1): %2:%3</source>
        <translation>Respuesta desconocida del servidor (%1): %2:%3</translation>
    </message>
</context>
<context>
    <name>ServerTooltip::L10n</name>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="58"/>
        <source>(alias of: %1)</source>
        <translation>(alias de: %1)</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="64"/>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="175"/>
        <source>MISSING</source>
        <translation>FALTANTE</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="170"/>
        <source>OPTIONAL</source>
        <translation>OPCIONAL</translation>
    </message>
    <message>
        <location filename="../serverapi/tooltips/servertooltip.cpp" line="180"/>
        <source>ALIAS</source>
        <translation>ALIAS</translation>
    </message>
</context>
<context>
    <name>ServersStatusWidget</name>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="83"/>
        <source>Players (Humans + Bots) / Servers Refreshed%</source>
        <translation>Jugadores (Humanos + Bots) / Servidores actualizados%</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="114"/>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="137"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="116"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="130"/>
        <source>%1 (%2+%3) / %4</source>
        <translation>%1 (%2+%3) / %4</translation>
    </message>
    <message>
        <location filename="../gui/widgets/serversstatuswidget.cpp" line="133"/>
        <source> %1</source>
        <translation> %1</translation>
    </message>
</context>
<context>
    <name>StaticMessages</name>
    <message>
        <location filename="../serverapi/message.cpp" line="30"/>
        <source>You have been banned from the master server.</source>
        <translation>Usted ha sido baneado del servidor master.</translation>
    </message>
</context>
<context>
    <name>UpdateChannelTr</name>
    <message>
        <location filename="../updater/updatechannel.cpp" line="110"/>
        <source>Beta versions have newer features but they are untested. Releases on this update channel are more often and are suggested for users who want newest functionalities and minor bug fixes as soon as they become implemented and available.</source>
        <translation>Las versiones Beta tienen características más nuevas pero no han sido probadas. Las versiones de este canal de actualización son más frecuentes y se sugieren para usuarios que desean funcionalidades más recientes y correcciones de errores menores tan pronto como se implementan y están disponibles.</translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="120"/>
        <source>Stable versions are released rarely. They cover many changes at once and these changes are more certain to work correctly. Critical bug fixes are also provided through this channel.</source>
        <translation>Las versiones estables se lanzan raramente. Cubren muchos cambios a la vez y es más probable que estos cambios funcionen correctamente. Las correcciones de errores críticas también se proporcionan a través de este canal.</translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="138"/>
        <source>Beta</source>
        <translation>Beta</translation>
    </message>
    <message>
        <location filename="../updater/updatechannel.cpp" line="142"/>
        <source>Stable</source>
        <translation>Estable</translation>
    </message>
</context>
<context>
    <name>UpdateInstaller</name>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="82"/>
        <source>Failed to copy the updater executable to a temporary space: &quot;%1&quot; -&gt; &quot;%2&quot;.</source>
        <translation>Error al copiar el ejecutable del actualizador en un espacio temporal: &quot;%1&quot; -&gt; &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="92"/>
        <location filename="../updater/updateinstaller.cpp" line="127"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="94"/>
        <source>Nothing to update.</source>
        <translation>Nada para actualizar.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="96"/>
        <source>Update package or script are not found. Check log for details.</source>
        <translation>No se encuentran el paquete de actualización o el script. Revise el registro para más detalles.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="98"/>
        <source>Failed to start updater process.</source>
        <translation>Error al iniciar el proceso de actualización.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="100"/>
        <source>Unknown error: %1.</source>
        <translation>Error desconocido: %1.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="129"/>
        <source>Unable to read the update script.</source>
        <translation>No se puede leer el script de la actualización.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="131"/>
        <source>No installation directory specified.</source>
        <translation>No se ha especificado el directorio de instalación.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="133"/>
        <source>Unable to determine the path of the updater.</source>
        <translation>No se puede determinar la ruta del actualizador.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="135"/>
        <source>General failure.</source>
        <translation>Fallo general.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="137"/>
        <source>Unknown process error code: %1.</source>
        <translation>Código de error desconocido para el proceso: %1.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="147"/>
        <source>Installing update.</source>
        <translation>Instalando actualización.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="162"/>
        <source>Package directory &quot;%1&quot; doesn&apos;t exist.</source>
        <translation>El directorio de paquetes &quot;%1&quot; no existe.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="168"/>
        <source>Update was about to be installed but update script &quot;%1&quot; is missing.</source>
        <translation>La actualización estaba a punto de instalarse pero falta el script de actualización &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../updater/updateinstaller.cpp" line="205"/>
        <source>Failed to start updater process: %1 %2</source>
        <translation>Error al iniciar el proceso de actualización: %1 %2</translation>
    </message>
</context>
<context>
    <name>UpdatePackageFilter</name>
    <message>
        <location filename="../updater/updatepackagefilter.cpp" line="164"/>
        <source>-BROKEN</source>
        <translation>-ROTO</translation>
    </message>
</context>
<context>
    <name>UpdaterInfoParser</name>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="98"/>
        <source>Missing update revision info for package %1.</source>
        <translation>Falta información de la actualización para el paquete %1.</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="126"/>
        <source>Invalid update download URL for package %1: %2</source>
        <translation>La URL de descarga de la actualización no es válida para el paquete %1: %2</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="133"/>
        <source>Missing update download URL for package %1.</source>
        <translation>Falta la URL de descarga de la actualización para el paquete %1.</translation>
    </message>
    <message>
        <location filename="../updater/updaterinfoparser.cpp" line="143"/>
        <source>Invalid update script download URL for package %1, %2</source>
        <translation>La URL de descarga del script de actualización no es válida para el paquete %1, %2</translation>
    </message>
</context>
<context>
    <name>WadsPicker</name>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="62"/>
        <source>Check paths</source>
        <translation>Comprobar rutas</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="114"/>
        <source>Doomseeker - Add file(s)</source>
        <translation>Doomseeker - Añadir archivo(s)</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.cpp" line="164"/>
        <source>%1 MISSING</source>
        <translation>%1 FALTANTE</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="116"/>
        <source>Clear</source>
        <translation>Despejar</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="53"/>
        <source>Browse</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="50"/>
        <source>Browse for a file.</source>
        <translation>Buscar un archivo.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="66"/>
        <source>Add an empty path to the list.</source>
        <translation>Añadir una ruta vacía a la lista.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="69"/>
        <source>Add empty</source>
        <translation>Añadir vacío</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="86"/>
        <source>Remove the selected files from the list.</source>
        <translation>Elimina los archivos seleccionados de la lista.</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="89"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../gui/createserver/wadspicker.ui" line="113"/>
        <source>Remove all files from the list.</source>
        <translation>Elimina todos los archivos de la lista.</translation>
    </message>
</context>
<context>
    <name>WadseekerInterface</name>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="123"/>
        <source>Downloading WADs for server &quot;%1&quot;</source>
        <translation>Descargando WADs para el servidor &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="134"/>
        <source>Aborting service: %1</source>
        <translation>Abortando servicio: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="140"/>
        <source>Aborting site: %1</source>
        <translation>Abortando sitio: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="194"/>
        <source>All done. Success.</source>
        <translation>Todo listo. Éxito.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="213"/>
        <source>All done. Fail.</source>
        <translation>Todo listo. Fracaso.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="349"/>
        <source>CRIT</source>
        <translation>FATAL</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="353"/>
        <source>ERROR</source>
        <translation>ERROR</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="357"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="361"/>
        <source>NOTI</source>
        <translation>NOTIF</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="365"/>
        <source>NAVI</source>
        <translation>NAVI</translation>
    </message>
    <message>
        <source>CRITICAL ERROR: %1</source>
        <translation type="vanished">ERROR CRÍTICO: %1</translation>
    </message>
    <message>
        <source>Error: %1</source>
        <translation type="vanished">Error: %1</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="14"/>
        <location filename="../gui/wadseekerinterface.cpp" line="224"/>
        <location filename="../gui/wadseekerinterface.cpp" line="398"/>
        <source>Wadseeker</source>
        <translation>Wadseeker</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="22"/>
        <source>Seek WADs, comma (&apos;,&apos;) separated:</source>
        <translation>Buscar WADs, separados por coma &apos;,&apos;:</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="59"/>
        <source>Abort</source>
        <translation>Abortar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Cerrar</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="557"/>
        <source>[%1%] Wadseeker</source>
        <translation>[%1%] Wadseeker</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="605"/>
        <source>Context menu error</source>
        <translation>Error del menú contextual</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.cpp" line="605"/>
        <source>Unknown action selected.</source>
        <translation>Acción desconocida seleccionada.</translation>
    </message>
    <message>
        <source>Seek WAD or multiple WADs, comma (&apos;,&apos;) separated:</source>
        <translation type="vanished">Buscar WAD o WADs múltiples, separados por comas (&apos;,&apos;):</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="49"/>
        <location filename="../gui/wadseekerinterface.ui" line="91"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="54"/>
        <location filename="../gui/wadseekerinterface.ui" line="96"/>
        <source>Progress</source>
        <translation>Progreso</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="86"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="101"/>
        <source>Speed</source>
        <translation>Velocidad</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="106"/>
        <source>ETA</source>
        <translation>Tiempo estimado</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="111"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="159"/>
        <source>Start game</source>
        <translation>Empezar juego</translation>
    </message>
    <message>
        <location filename="../gui/wadseekerinterface.ui" line="173"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
</context>
<context>
    <name>WadseekerShow</name>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="52"/>
        <location filename="../gui/wadseekershow.cpp" line="67"/>
        <source>Wadseeker cannot be launched</source>
        <translation>Wadseeker no puede ser lanzado</translation>
    </message>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="53"/>
        <source>Another instance of Wadseeker is already running.</source>
        <translation>Otra instancia de Wadseeker ya se está ejecutando.</translation>
    </message>
    <message>
        <location filename="../gui/wadseekershow.cpp" line="63"/>
        <source>Wadseeker will not work correctly:

The target directory is either not configured, is invalid or cannot be written to.

Please review your Configuration and/or refer to the online help available from the Help menu.</source>
        <translation>Wadseeker no funcionará correctamente:

El directorio de destino no está configurado, no es válido o no se puede escribir.

Revise su configuración y/o consulte la ayuda en línea disponible en el menú Ayuda.</translation>
    </message>
    <message>
        <source>Wadseeker will not work correctly:

Target directory is either not set, is invalid or cannot be written to.

Please review your Configuration and/or refer to online help available from the Help menu.</source>
        <translation type="vanished">Wadseeker no funcionará correctamente:

El directorio de destino no está configurado, no es válido o no se puede escribir.

Revise su Configuración y/o consulte la ayuda en línea disponible en el menú Ayuda.</translation>
    </message>
</context>
<context>
    <name>WadseekerSitesTable</name>
    <message>
        <location filename="../gui/widgets/wadseekersitestable.cpp" line="93"/>
        <source>Abort</source>
        <translation>Abortar</translation>
    </message>
</context>
<context>
    <name>WadseekerWadsTable</name>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="68"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="69"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="129"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="130"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="307"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="315"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="132"/>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="133"/>
        <source>Awaiting URLs</source>
        <translation>En espera de URLs</translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="192"/>
        <source>Done</source>
        <translation>Hecho</translation>
    </message>
    <message>
        <location filename="../gui/widgets/wadseekerwadstable.cpp" line="324"/>
        <source>Skip current URL</source>
        <translation>Omitir la URL actual</translation>
    </message>
</context>
</TS>
