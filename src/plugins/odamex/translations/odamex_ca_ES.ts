<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>OdamexAboutProvider</name>
    <message>
        <location filename="../odamexengineplugin.cpp" line="49"/>
        <source>This plugin is distributed under the terms of the LGPL v2.1 or later.

</source>
        <translation>Aquest complement es distribueix sota els termes de la llicència LGPL v2.1 o posterior.

</translation>
    </message>
</context>
<context>
    <name>OdamexGameInfo</name>
    <message>
        <source>Duel</source>
        <translation type="vanished">Duel</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="28"/>
        <source>Items respawn</source>
        <translation>Objectes reapareixen</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="29"/>
        <source>Weapons stay</source>
        <translatorcomment>There&apos;s a better word that saves an &quot;es&quot; -&quot;romanen&quot;- but it&apos;s so formal that it&apos;s never used in a normal enviorment and it&apos;s simply to odd for a program of this nature.</translatorcomment>
        <translation>Armes es mantenen</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="30"/>
        <source>Friendly fire</source>
        <translation>Foc amic</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="31"/>
        <source>Allow exit</source>
        <translation>Permetre sortir</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="32"/>
        <source>Infinite ammo</source>
        <translation>Munició infinita</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="33"/>
        <source>No monsters</source>
        <translation>Sense monstres</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="34"/>
        <source>Monsters respawn</source>
        <translation>Monstres reapareixen</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="35"/>
        <source>Fast monsters</source>
        <translation>Monstres ràpids</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="36"/>
        <source>Jumping allowed</source>
        <translation>Permetre saltar</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="37"/>
        <source>Freelook allowed</source>
        <translation>Càmera lliure permesa</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="38"/>
        <source>Wad can be downloaded</source>
        <translation>El Wad es pot descarregar</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="39"/>
        <source>Server resets on empty</source>
        <translation>El servidor es restableix quan està buit</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="40"/>
        <source>Clean Maps</source>
        <translation>Mapes nets</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="42"/>
        <source>Kill anyone who tries to leave the level</source>
        <translation>Mata a qui intenti abandonar el nivell</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="52"/>
        <source>Lives</source>
        <translation>Vides</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="53"/>
        <source>Teams</source>
        <translation>Equips</translation>
    </message>
    <message>
        <location filename="../odamexgameinfo.cpp" line="54"/>
        <source>Attack &amp;&amp; Defend</source>
        <translation>Atacar i defensar</translation>
    </message>
</context>
<context>
    <name>OdamexGameMode</name>
    <message>
        <location filename="../odamexgamemode.cpp" line="34"/>
        <location filename="../odamexgamemode.cpp" line="54"/>
        <source>Horde</source>
        <translation>Horda</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="48"/>
        <source>Duel</source>
        <translation>Duel</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="49"/>
        <source>Survival</source>
        <translation>Supervivència</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="50"/>
        <source>Last Marine Standing</source>
        <translation>Últim marine en peu</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="51"/>
        <source>Teams Last Marine Standing</source>
        <translation>Últim marine en peu (Equips)</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="52"/>
        <source>Attack &amp; Defend CTF</source>
        <translation>Atacar i defensar (CTF)</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="53"/>
        <source>LMS Capture The Flag</source>
        <translation>Últim marine en peu (CTF)</translation>
    </message>
    <message>
        <location filename="../odamexgamemode.cpp" line="55"/>
        <source>Survival Horde</source>
        <translation>Supervivència de hordes</translation>
    </message>
</context>
</TS>
